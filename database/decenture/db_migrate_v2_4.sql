ALTER TABLE public.attendance_snapshot DROP completion_date;
ALTER TABLE public.attendance_snapshot DROP course_work;
ALTER TABLE public.attendance_snapshot DROP exam;
ALTER TABLE public.attendance_snapshot DROP status_id;


CREATE TABLE semester(
  id                UUID NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
  university_id     UUID NOT NULL REFERENCES university ON DELETE CASCADE,
  begin_date        DATE NOT NULL,
  end_date          DATE NOT NULL CHECK (end_date > begin_date),
  UNIQUE (university_id, begin_date)
);

CREATE TABLE compliance (
  student_id        UUID      NOT NULL REFERENCES student ON DELETE RESTRICT,
  lecture_course_id UUID      NOT NULL REFERENCES lecture_course ON DELETE RESTRICT,
  completion_date   DATE,
  course_work       SMALLINT  NOT NULL DEFAULT 0,
  exam              SMALLINT  NOT NULL DEFAULT 0,
  status_id         SMALLINT  NOT NULL DEFAULT 0,
  PRIMARY KEY (student_id, lecture_course_id)
);
CREATE INDEX ON compliance (begin_date);

COMMENT ON COLUMN compliance.begin_date
IS 'Course work status for student';
COMMENT ON COLUMN compliance.completion_date
IS 'Course work status for student';
COMMENT ON COLUMN compliance.course_work
IS 'Course work status for student';
COMMENT ON COLUMN compliance.exam
IS 'Exam status for student';
COMMENT ON COLUMN compliance.status_id
IS 'Actual completion status of lecture course fo student';
