DROP TABLE IF EXISTS app_user CASCADE;
DROP TABLE IF EXISTS user_profile CASCADE;
DROP TABLE IF EXISTS user_info CASCADE;
DROP TABLE IF EXISTS user_recv CASCADE;
DROP TABLE IF EXISTS scheduled_lecture CASCADE;
DROP TABLE IF EXISTS lecture CASCADE;
DROP TABLE IF EXISTS lecture_course CASCADE;
DROP TABLE IF EXISTS student CASCADE;
DROP TABLE IF EXISTS user_confirmation CASCADE;
DROP TABLE IF EXISTS university CASCADE;
DROP TABLE IF EXISTS government CASCADE;
DROP TABLE IF EXISTS account CASCADE;
DROP TABLE IF EXISTS account_type CASCADE;
DROP TABLE IF EXISTS attendance_snapshot CASCADE;
DROP TABLE IF EXISTS invoice CASCADE;
DROP TABLE IF EXISTS student_schedule CASCADE;
DROP TABLE IF EXISTS scheduled_attendance CASCADE;
DROP TABLE IF EXISTS payment_system_type CASCADE;
DROP TABLE IF EXISTS user_bank_card CASCADE;
DROP TABLE IF EXISTS educational_event_type CASCADE;
DROP TABLE IF EXISTS semester CASCADE;
DROP TABLE IF EXISTS semester_lecture_course CASCADE;
DROP TABLE IF EXISTS compliance CASCADE;

DROP TABLE IF EXISTS dummy_lecture_theatre CASCADE;
DROP TABLE IF EXISTS dummy_invoice CASCADE;
DROP TABLE IF EXISTS dummy_student_course CASCADE;

DROP TABLE IF EXISTS wallet__blockchain_wallet CASCADE;
DROP TABLE IF EXISTS blockchain_invoice CASCADE;
DROP TABLE IF EXISTS blockchain_transaction CASCADE;
DROP TABLE IF EXISTS blockchain_gl CASCADE;
DROP TABLE IF EXISTS blockchain_tr CASCADE;
DROP TABLE IF EXISTS blockchain_attendance CASCADE;

DROP TABLE IF EXISTS erp_invoices CASCADE;


/*
 Extintion for UUID values GENERATION and ability to use uuid type in database
 http://www.postgresqltutorial.com/postgresql-uuid/

 CREATE TABLE contacts (
   contact_id uuid DEFAULT uuid_generate_v4 (),...

 To install the uuid-ossp module:
*/
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";


/**
 app_user.status:
 0  - 'NOT_ACTIVATED',
 1  - 'ACTIVE',
 -1 - 'CLOSED',
 */

CREATE TABLE app_user (
  id         UUID        NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
  email      VARCHAR(64) NOT NULL UNIQUE,
  password   CHAR(40)    NOT NULL, -- sha1 length = char(40)
  token      UUID        NOT NULL UNIQUE      DEFAULT uuid_generate_v4(),
  status_id  SMALLINT    NOT NULL             DEFAULT 0,
  role_id    SMALLINT    NOT NULL             DEFAULT 1,
  entry_date TIMESTAMP   NOT NULL             DEFAULT now(),
  close_date TIMESTAMP
);
COMMENT ON TABLE app_user
IS 'Accounts of application users';
COMMENT ON COLUMN app_user.status_id
IS 'Enumeration: (0) - not verified, (1) - active, (-1) - closed';

-- password: asd
-- sha1('asd') = 'f10e2821bbbea527ea02200352313bc059445190'
-- sha1('1') = '356a192b7913b04c54574d18c28d46e6395428ab'
INSERT INTO app_user (email, password, status_id)
VALUES ('admin@admin', 'f10e2821bbbea527ea02200352313bc059445190', 1);

CREATE TABLE user_info (
  id            UUID        NOT NULL PRIMARY KEY REFERENCES app_user ON DELETE CASCADE,
  first_name    VARCHAR(64) NOT NULL,
  last_name     VARCHAR(64) NOT NULL,
  phone_number  VARCHAR(30),
  date_of_birth DATE
);
COMMENT ON TABLE user_info
IS 'User account personal information';


CREATE TABLE account_type (
  id   SMALLSERIAL NOT NULL PRIMARY KEY,
  code VARCHAR(30) NOT NULL UNIQUE,
  name VARCHAR(64) NOT NULL
);

INSERT INTO account_type (code, name)
VALUES ('1', 'Personal'),
       ('2', 'Escrow');

CREATE TABLE account (
  id         UUID           NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
  name       VARCHAR(255)   NOT NULL,
  amount     NUMERIC(18, 3) NOT NULL             DEFAULT 0,
  currency   CHAR(3)        NOT NULL             DEFAULT 'GBP',
  type_id    SMALLINT       NOT NULL REFERENCES account_type ON DELETE RESTRICT,
  status_id  SMALLINT       NOT NULL             DEFAULT 0,
  user_id    UUID REFERENCES app_user ON DELETE SET NULL,
  owner_id   UUID REFERENCES app_user ON DELETE SET NULL,
  entry_date TIMESTAMP      NOT NULL             DEFAULT now(),
  close_date TIMESTAMP
);
CREATE INDEX ON account (type_id);
CREATE INDEX ON account (user_id);
CREATE INDEX ON account (owner_id);

COMMENT ON COLUMN account.name
IS 'Account name or description';
COMMENT ON COLUMN account.type_id
IS 'Enumeration: (0) - personal account, (1) - escrow (owned by government)';
COMMENT ON COLUMN account.user_id
IS 'User of account';
COMMENT ON COLUMN account.owner_id
IS 'Account owner, for personal account = user_id, for escrow - government user';

-- TODO: !!!!!!!
CREATE TABLE government (
  id           UUID        NOT NULL PRIMARY KEY REFERENCES account ON DELETE RESTRICT,
  country_code CHAR(2)     NOT NULL UNIQUE,
  name         VARCHAR(64) NOT NULL UNIQUE
  -- TODO: we have owner_id (user) in account table, so root_user_id can be dropped
);


CREATE TABLE university (
  id            UUID         NOT NULL PRIMARY KEY REFERENCES account ON DELETE RESTRICT,
  country_code  CHAR(2)      NOT NULL,
  code          VARCHAR(30)  NOT NULL,
  name          VARCHAR(64)  NOT NULL,
  official_name VARCHAR(255) NOT NULL,
  abbreviation  VARCHAR(30)  NOT NULL,
  address       VARCHAR(64),
  domain_name   VARCHAR(64)  NOT NULL,
  erp_info      VARCHAR(255),
  -- TODO: we have owner_id (user) in account table, so root_user_id can be dropped
  UNIQUE (code, country_code)
);


CREATE TABLE student (
  id               UUID        NOT NULL PRIMARY KEY REFERENCES account ON DELETE CASCADE,
  university_id    UUID        NOT NULL REFERENCES university ON DELETE CASCADE,
  erp_email        VARCHAR(64) NOT NULL,
  erp_student_id   VARCHAR(64) NOT NULL, -- student_number from erp system
  first_name       VARCHAR(64) NOT NULL,
  last_name        VARCHAR(64) NOT NULL,
  is_international BOOLEAN     NOT NULL DEFAULT FALSE,
  is_sponsored     BOOLEAN     NOT NULL DEFAULT FALSE,
  degree_name      VARCHAR(64),
  certification    VARCHAR(64),
  UNIQUE (university_id, erp_student_id),
  UNIQUE (erp_email, erp_student_id)
);
COMMENT ON COLUMN student.id
IS 'This is escrow account of student, not a personal account of application user.';


CREATE TABLE payment_system_type (
  id   SMALLSERIAL NOT NULL PRIMARY KEY,
  key  VARCHAR(30) NOT NULL UNIQUE,
  name VARCHAR(64) NOT NULL UNIQUE
);
INSERT INTO payment_system_type (key, name)
VALUES ('VISA', 'VISA'),
       ('MASTERCARD', 'Mastercard'),
       ('DISCOVER', 'Discover'),
       ('AMERICAN-EXPRESS', 'American Express'),
       ('MAESTRO', 'Maestro'),
       ('DINERS-CLUB', 'Diners Club');

CREATE TABLE user_bank_card (
  id                     UUID     NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
  user_id                UUID     NOT NULL REFERENCES app_user ON DELETE CASCADE,
  payment_system_type_id SMALLINT NOT NULL REFERENCES payment_system_type ON DELETE RESTRICT,
  card_number            CHAR(19) NOT NULL UNIQUE,
  holder                 VARCHAR(64),
  expiry_year            CHAR(2)  NOT NULL,
  expiry_month           CHAR(2)  NOT NULL,
  cvv                    CHAR(4)  NOT NULL
);
CREATE INDEX ON user_bank_card (user_id);
CREATE INDEX ON user_bank_card (payment_system_type_id);


CREATE TABLE lecture_course (
  id            UUID        NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
  university_id UUID        NOT NULL REFERENCES university ON DELETE RESTRICT, -- ???
  code          VARCHAR(30) NOT NULL,
  name          VARCHAR(64) NOT NULL,
  total         SMALLINT    NOT NULL             DEFAULT 0,
  course_work   SMALLINT    NOT NULL             DEFAULT 0,
  exam          SMALLINT    NOT NULL             DEFAULT 0,
  UNIQUE (university_id, code)
);
COMMENT ON COLUMN lecture_course.total
IS 'Total count of lectures planed for course';
COMMENT ON COLUMN lecture_course.course_work
IS 'Flag - is course work required for course completion. 0 - no, 1 - yes.';
COMMENT ON COLUMN lecture_course.exam
IS 'Flag - is exam required for course completion. 0 - no, 1 - yes.';


CREATE TABLE educational_event_type (
  id          CHAR(1)      NOT NULL PRIMARY KEY,
  description VARCHAR(255) NOT NULL
);
INSERT INTO educational_event_type (id, description)
VALUES ('A', 'Lesson, lecture, tutorial or seminar'),
       ('B', 'One to one meeting with a supervisor'),
       ('C', 'Placement'),
       ('D', 'Examination'),
       ('E', 'Work Experience'),
       ('F', 'Other');


CREATE TABLE scheduled_lecture (
  id                   UUID        NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
  nfc_id               VARCHAR(64) NOT NULL,
  lecture_course_id    UUID        NOT NULL REFERENCES lecture_course ON DELETE RESTRICT,
  begin_date           TIMESTAMP   NOT NULL,
  end_date             TIMESTAMP   NOT NULL CHECK (end_date > begin_date),
  status_id            SMALLINT    NOT NULL             DEFAULT 0,
  scan_begin_date      TIMESTAMP   NOT NULL,
  scan_end_date        TIMESTAMP   NOT NULL CHECK (scan_end_date > scan_begin_date),
  lecture_theatre_name VARCHAR(64),
  event_type           CHAR(1)     NOT NULL,
  subject              VARCHAR(64),
  description          VARCHAR(255),
  notes                VARCHAR(255),
  UNIQUE (nfc_id, begin_date)
);
CREATE INDEX ON scheduled_lecture (lecture_course_id);
CREATE INDEX ON scheduled_lecture (begin_date, end_date);
CREATE INDEX ON scheduled_lecture (scan_begin_date, scan_end_date);

COMMENT ON TABLE scheduled_lecture
IS 'Education events schedule (calendar). Based on "Lecture Theatre ID Lookup"';
COMMENT ON COLUMN scheduled_lecture.lecture_theatre_name
IS 'Lecture theatre or room name';
COMMENT ON COLUMN scheduled_lecture.subject
IS 'The name of the Lecture and subject';
COMMENT ON COLUMN scheduled_lecture.description
IS 'Description and any additional information about lecture subject';
COMMENT ON COLUMN scheduled_lecture.status_id
IS '0 - pending, 1 - completed';


CREATE TABLE scheduled_attendance (
  student_id           UUID     NOT NULL REFERENCES student ON DELETE RESTRICT,
  scheduled_lecture_id UUID     NOT NULL REFERENCES scheduled_lecture ON DELETE CASCADE,
  status_id            SMALLINT NOT NULL DEFAULT 0,
  timestamp            TIMESTAMP,
  transaction_id       CHAR(64),
  PRIMARY KEY (student_id, scheduled_lecture_id)
);
CREATE INDEX ON scheduled_attendance (transaction_id);

comment on table scheduled_attendance
IS 'Expected (scheduled) student attendance. From "Students Attendance Schedule"';
COMMENT ON COLUMN scheduled_attendance.status_id
IS '0 - pending, 1 - visited, 2 - excused, 3 - canceled, 9 - absence';
COMMENT ON COLUMN scheduled_attendance.transaction_id
IS 'Blockchain attendance transaction reference';


/* todo:
  ? - total = total scheduled, but not finished events
 */
CREATE TABLE attendance_snapshot (
  student_id  UUID      NOT NULL REFERENCES student ON DELETE RESTRICT,
  begin_date  DATE      NOT NULL,
  end_date    DATE      NOT NULL CHECK (end_date > begin_date),
  visited     SMALLINT  NOT NULL DEFAULT 0,
  absence     SMALLINT  NOT NULL DEFAULT 0,
  excused     SMALLINT  NOT NULL DEFAULT 0,
  completed   SMALLINT  NOT NULL DEFAULT 0,
  scheduled   SMALLINT  NOT NULL DEFAULT 0,
  last_update TIMESTAMP NOT NULL DEFAULT now(),
  PRIMARY KEY (student_id, begin_date)
);
CREATE INDEX ON attendance_snapshot (begin_date, end_date);

COMMENT ON TABLE attendance_snapshot
IS 'Student attendance statistic for month period';
COMMENT ON COLUMN attendance_snapshot.begin_date
IS 'Begin date of snapshot period (first day of month)';
COMMENT ON COLUMN attendance_snapshot.end_date
IS 'Begin date of snapshot period (last day of month)';
COMMENT ON COLUMN attendance_snapshot.visited
IS 'Total count of visited lectures by student';
COMMENT ON COLUMN attendance_snapshot.absence
IS 'Total count of missed lectures by student';
COMMENT ON COLUMN attendance_snapshot.excused
IS 'Total count of excused lectures by student';
COMMENT ON COLUMN attendance_snapshot.completed
IS 'Total count of completed lectures for student';
COMMENT ON COLUMN attendance_snapshot.scheduled
IS 'Total count of lectures scheduled for period (month)';


CREATE TABLE semester (
  id            UUID NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
  university_id UUID NOT NULL REFERENCES university ON DELETE CASCADE,
  begin_date    DATE NOT NULL,
  end_date      DATE NOT NULL CHECK (end_date > begin_date),
  UNIQUE (university_id, begin_date)
);


CREATE TABLE semester_lecture_course (
  semester_id       UUID NOT NULL REFERENCES semester ON DELETE CASCADE,
  lecture_course_id UUID NOT NULL REFERENCES lecture_course ON DELETE RESTRICT,
  PRIMARY KEY (semester_id, lecture_course_id)
);


CREATE TABLE compliance (
  student_id        UUID     NOT NULL REFERENCES student ON DELETE RESTRICT,
  semester_id       UUID     NOT NULL REFERENCES semester ON DELETE CASCADE,
  lecture_course_id UUID     NOT NULL REFERENCES lecture_course ON DELETE RESTRICT,
  completion_date   DATE,
  total_lectures    SMALLINT NOT NULL DEFAULT 0,
  course_work       SMALLINT NOT NULL DEFAULT 0,
  exam              SMALLINT NOT NULL DEFAULT 0,
  status_id         SMALLINT NOT NULL DEFAULT 0,
  PRIMARY KEY (student_id, semester_id, lecture_course_id)
);
CREATE INDEX ON compliance (completion_date);

COMMENT ON COLUMN compliance.total_lectures
IS 'Total count of lectures for semester';
COMMENT ON COLUMN compliance.completion_date
IS 'Real date of course completion';
COMMENT ON COLUMN compliance.course_work
IS 'Course work status for student';
COMMENT ON COLUMN compliance.exam
IS 'Exam status for student';
COMMENT ON COLUMN compliance.status_id
IS 'Actual completion status of lecture course for student';


CREATE TABLE erp_invoices (
    id SERIAL NOT NULL,
    invoice TEXT
);
COMMENT ON TABLE public.erp_invoices IS 'Reconciled Invoices';
