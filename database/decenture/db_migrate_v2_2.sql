ALTER TABLE student
  ADD is_international BOOLEAN DEFAULT false  NOT NULL;


ALTER TABLE lecture_course
  ADD degtree_name VARCHAR(64) NULL;
ALTER TABLE lecture_course
  ADD total SMALLINT NOT NULL DEFAULT 0;
ALTER TABLE lecture_course
  ADD course_work SMALLINT NOT NULL DEFAULT 0;
ALTER TABLE lecture_course
  ADD exam SMALLINT NOT NULL DEFAULT 0;

COMMENT ON COLUMN lecture_course.total
IS 'Total count of lectures planed for course';
COMMENT ON COLUMN lecture_course.course_work
IS 'Flag - is course work required for course completion. 0 - no, 1 - yes.';
COMMENT ON COLUMN lecture_course.exam
IS 'Flag - is exam required for course completion. 0 - no, 1 - yes.';


ALTER TABLE scheduled_lecture
  ADD status_id SMALLINT NOT NULL DEFAULT 0;
ALTER TABLE scheduled_lecture
  ADD scan_begin_date TIMESTAMP NULL;
ALTER TABLE scheduled_lecture
  ADD scan_end_date TIMESTAMP NULL
  CHECK (scan_end_date > scan_begin_date);

COMMENT ON COLUMN scheduled_lecture.status_id
IS '0 - pending, 1 - completed';

UPDATE scheduled_lecture SET
  scan_begin_date = begin_date - make_interval(mins := 10),
  scan_end_date = end_date - make_interval(mins := 10);

ALTER TABLE scheduled_lecture ALTER COLUMN scan_begin_date SET NOT NULL;
ALTER TABLE scheduled_lecture ALTER COLUMN scan_end_date SET NOT NULL;


ALTER TABLE attendance_snapshot
  ADD absence SMALLINT NOT NULL DEFAULT 0;
ALTER TABLE attendance_snapshot
  ADD completed SMALLINT NOT NULL DEFAULT 0;
ALTER TABLE attendance_snapshot
  ADD course_work SMALLINT NOT NULL DEFAULT 0;
ALTER TABLE attendance_snapshot
  ADD exam SMALLINT NOT NULL DEFAULT 0;
ALTER TABLE attendance_snapshot
  ADD status_id SMALLINT NOT NULL DEFAULT 0;


COMMENT ON COLUMN attendance_snapshot.visited
IS 'Total count of visited lectures by student (from determined lecture course)';
COMMENT ON COLUMN attendance_snapshot.absence
IS 'Total count of missed lectures by student (from determined lecture course)';
COMMENT ON COLUMN attendance_snapshot.excused
IS 'Total count of excused lectures by student (from determined lecture course)';
COMMENT ON COLUMN attendance_snapshot.completed
IS 'Total count of completed lectures for student (from determined lecture course). this is denormalized value that can be calculated from or calculated from scheduled_lecture';
COMMENT ON COLUMN attendance_snapshot.course_work
IS 'Course work status for student';
COMMENT ON COLUMN attendance_snapshot.exam
IS 'Exam status for student';
COMMENT ON COLUMN attendance_snapshot.status_id
IS 'Actual completion status of lecture course fo student';
