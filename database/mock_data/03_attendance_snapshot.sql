UPDATE scheduled_attendance
SET status_id = CASE WHEN (random() * 100) > 7 THEN 1
                     WHEN (random() * 100) > 30 THEN 2
                     ELSE 9 END
WHERE scheduled_lecture_id IN (SELECT id FROM scheduled_lecture WHERE begin_date < now());

UPDATE scheduled_attendance
SET timestamp = CASE WHEN status_id = 1 THEN l.begin_date ELSE l.begin_date + interval '10 days' END,
    transaction_id = id
FROM (SELECT id, begin_date FROM scheduled_lecture) l
WHERE status_id in (1, 2)
  AND scheduled_lecture_id = l.id;

UPDATE scheduled_lecture SET status_id = 1 WHERE end_date < date_trunc('day', now());

INSERT INTO attendance_snapshot(student_id, begin_date, end_date, visited, absence, excused, completed, scheduled)
WITH
  raw_data AS (
      SELECT a.student_id,
             date_trunc('month', l.begin_date) :: date begin_date,
             (date_trunc('month', l.begin_date) + interval '1 month -1 day') :: date end_date,
             CASE WHEN a.status_id = 1 THEN 1 ELSE 0 END visited,
             CASE WHEN a.status_id = 9 THEN 1 ELSE 0 END absence,
             CASE WHEN a.status_id = 2 THEN 1 ELSE 0 END excused,
             CASE WHEN l.status_id = 1 THEN 1 ELSE 0 END completed
      FROM scheduled_lecture l
             INNER JOIN scheduled_attendance a on l.id = a.scheduled_lecture_id
      WHERE l.begin_date < date_trunc('month', now())
  )
SELECT student_id,
       begin_date,
       end_date,
       sum(visited) visited,
       sum(absence) absence,
       sum(excused) excused,
       sum(completed) completed,
       count(*) scheduled
FROM raw_data
GROUP BY student_id,
         begin_date,
         end_date;

WITH
  compliance_totals AS (
      SELECT a.student_id,
             l.lecture_course_id,
             count(*)        total_lectures,
             max(l.end_date) max_end_date
        FROM scheduled_lecture l
             INNER JOIN scheduled_attendance a on l.id = a.scheduled_lecture_id
        GROUP BY a.student_id, l.lecture_course_id
  ),
  random_satus AS (
      SELECT compliance_totals.*,
             CASE WHEN max_end_date < now() AND (random() * 100) > 7 THEN 1 ELSE 0 END new_status_id
        FROM compliance_totals
  )
UPDATE compliance AS t
SET total_lectures  = ct.total_lectures,
    status_id       = ct.new_status_id,
    completion_date = CASE
                        WHEN ct.new_status_id = 1 THEN max_end_date + interval '10 days'
                        ELSE NULL END
FROM (select * from random_satus) AS ct
WHERE t.student_id = ct.student_id
  AND t.lecture_course_id = ct.lecture_course_id;
