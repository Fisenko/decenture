/*
 Mock data for semester_lecture_course
 */
with lc_dlt as (
  select lc.id,
         lc.code,
         dlt.lecture_theatre_name,
         dlt.lecture_subject_name,
         dlt.begin_time,
         row_number() over (partition by dlt.begin_time order by lc.code) rn
  from lecture_course lc
         inner join dummy_lecture_theatre dlt on dlt.lecture_course_id = lc.code
  where lc.code not like 'DC9__'
  order by dlt.begin_time, lc.code --; -- dlt.lecture_theatre_name;
),
     sem as (
       select *,
              (
                    row_number() over (partition by s.university_id, s.begin_date order by t.begin_time) +
                    row_number() over (partition by s.university_id, t.begin_time order by s.begin_date) -
                    2
                ) % 8 + 1 as rn
       from semester s
              cross join (select begin_time from generate_series(9, 16, 1) as gs (begin_time)) t
       where s.university_id = '9704c732-972a-4543-a9c2-3d88f06ff2f4'
       order by s.university_id, t.begin_time, s.begin_date
     )
insert
into semester_lecture_course (semester_id, lecture_course_id)
select t2.id, t1.id
from lc_dlt t1
       inner join sem t2 on t1.begin_time = t2.begin_time and t1.rn = t2.rn
;

INSERT INTO compliance(student_id, semester_id, lecture_course_id)
SELECT s.id, slc.semester_id, slc.lecture_course_id
FROM student s
       CROSS JOIN semester_lecture_course slc;

/*
 Mock data for scheduled_lecture
 */
WITH days AS (
  SELECT d
  FROM generate_series(date '2016-01-01', date '2019-12-31', INTERVAL '1 day') AS s (d)
  WHERE to_char(d, 'DY') NOT IN ('SAT', 'SUN')
),
     lc_schedule AS (
       SELECT dlc.nfc_id,
              c.lecture_course_id,
              days.d + make_interval(hours := dlc.begin_time)              begin_date,
              days.d + make_interval(hours := dlc.begin_time + 1)          end_date,
              0                                                            status_id,
              days.d + make_interval(hours := dlc.begin_time, mins := -10) scan_begin_date,
              days.d + make_interval(hours := dlc.begin_time, mins := 50)  scan_end_date,
              dlc.lecture_theatre_name,
              dlc.event_type,
              dlc.lecture_subject_name                                     subject,
              dlc.lecture_subject_description                              description
       FROM semester_lecture_course c
              INNER JOIN semester s ON c.semester_id = s.id
              INNER JOIN days ON d between s.begin_date AND s.end_date
              INNER JOIN lecture_course lc ON c.lecture_course_id = lc.id
              INNER JOIN dummy_lecture_theatre dlc ON dlc.lecture_course_id = lc.code
     )
INSERT
INTO scheduled_lecture (nfc_id,
                        lecture_course_id,
                        begin_date,
                        end_date,
                        status_id,
                        scan_begin_date,
                        scan_end_date,
                        lecture_theatre_name,
                        event_type,
                        subject,
                        description)
SELECT *
FROM lc_schedule;


/*
 Mock data for scheduled_attendance
 */
INSERT INTO scheduled_attendance (student_id, scheduled_lecture_id)
SELECT s.id student_id, l.id scheduled_lecture_id
FROM student s
       CROSS JOIN scheduled_lecture l;
