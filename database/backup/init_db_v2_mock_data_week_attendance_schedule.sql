/*
 Mock data for scheduled_lecture
 */
INSERT INTO scheduled_lecture (nfc_id,
                               lecture_course_id,
                               begin_date,
                               end_date,
                               status_id,
                               scan_begin_date,
                               scan_end_date,
                               lecture_theatre_name,
                               event_type,
                               subject,
                               description,
                               notes)
WITH day_numbers AS (SELECT a.i FROM generate_series(0, 4) a (i)),
     monday AS (SELECT date_trunc('week', now()) start_date),
     week AS (SELECT start_date + make_interval(days := dn.i) week_day FROM day_numbers dn
                                                                              CROSS JOIN monday m),
     hours AS (SELECT DISTINCT lt.begin_time FROM dummy_lecture_theatre lt),
     time_table AS (SELECT h.begin_time,
                           w.week_day + make_interval(hours := h.begin_time)              begin_date,
                           w.week_day + make_interval(hours := h.begin_time + 1)          end_date,
                           w.week_day + make_interval(hours := h.begin_time, mins := -10) scan_begin_date,
                           w.week_day + make_interval(hours := h.begin_time, mins := 50)  scan_end_date
                    FROM week w
                           CROSS JOIN hours h),
     lc_schedule AS (SELECT dlc.nfc_id,
                            lc.id                           lecture_course_id,
                            tt.begin_date,
                            tt.end_date,
                            0                               status_id,
                            tt.scan_begin_date,
                            tt.scan_end_date,
                            dlc.lecture_theatre_name,
                            dlc.event_type,
                            dlc.lecture_subject_name        subject,
                            dlc.lecture_subject_description description,
                            null                            notes
                     FROM time_table tt
                            INNER JOIN dummy_lecture_theatre dlc ON tt.begin_time = dlc.begin_time
                            INNER JOIN lecture_course lc ON dlc.lecture_course_id = lc.code)
SELECT *
FROM lc_schedule;


/*
 Mock data for scheduled_attendance
 */
INSERT INTO scheduled_attendance (student_id, scheduled_lecture_id)
SELECT s.id student_id, sl.id scheduled_lecture_id
FROM student s
  CROSS JOIN dummy_student_course dsc
  INNER JOIN lecture_course c ON dsc.lecture_course_id = c.code
  INNER JOIN scheduled_lecture sl on (
    c.id = sl.lecture_course_id
    AND sl.begin_date > date_trunc('week', now())
    AND sl.end_date < date_trunc('week', now()) + make_interval(days := 5)
  );
