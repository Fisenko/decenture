DROP TABLE IF EXISTS wallet__blockchain_wallet CASCADE;
DROP TABLE IF EXISTS blockchain_invoice CASCADE;
DROP TABLE IF EXISTS blockchain_transaction CASCADE;
DROP TABLE IF EXISTS blockchain_gl CASCADE;
DROP TABLE IF EXISTS blockchain_tr CASCADE;
DROP TABLE IF EXISTS blockchain_attendance CASCADE;


CREATE TABLE blockchain_transaction (
  id          UUID      NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
  post_date   TIMESTAMP NOT NULL,
  sender_id   BIGINT    NOT NULL REFERENCES account ON DELETE RESTRICT,
  receiver_id BIGINT    NOT NULL REFERENCES account ON DELETE RESTRICT,
  amount      NUMERIC   NOT NULL,
  description VARCHAR(255)
);
CREATE INDEX ON blockchain_transaction (sender_id);
CREATE INDEX ON blockchain_transaction (receiver_id);

-- todo: GL alternative - v2
CREATE TABLE blockchain_tr (
  id          UUID      NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
  post_date   TIMESTAMP NOT NULL,
  description VARCHAR(255)
);
CREATE INDEX ON blockchain_tr (post_date);


CREATE TABLE blockchain_gl (
  id         UUID           NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
  tr_id      UUID           NOT NULL REFERENCES blockchain_tr ON DELETE CASCADE,
  account_id BIGINT         NOT NULL REFERENCES account ON DELETE RESTRICT,
  post_date  TIMESTAMP      NOT NULL, -- todo: denormalized = blockchain_tr.post_date
  amount     NUMERIC(18, 3) NOT NULL
);
CREATE INDEX ON blockchain_gl (tr_id);
CREATE INDEX ON blockchain_gl (account_id);
CREATE INDEX ON blockchain_gl (post_date, account_id);


CREATE TABLE blockchain_invoice (
  id              UUID           NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
  university_id   BIGINT         NOT NULL REFERENCES university ON DELETE RESTRICT,
  invoice_id      VARCHAR(64)    NOT NULL,
  invoice_number  VARCHAR(30)    NOT NULL,
  invoice_date    DATE           NOT NULL,
  due_date        DATE,
  billed_from_id  BIGINT         NOT NULL REFERENCES account ON DELETE RESTRICT,
  billed_to_id    BIGINT         NOT NULL REFERENCES account ON DELETE RESTRICT,
  amount          NUMERIC(18, 3) NOT NULL,
  status_id       SMALLINT       NOT NULL             DEFAULT 0,
  first_name      VARCHAR(64),
  second_name     VARCHAR(64),
  email           VARCHAR(64),
  business_unit   VARCHAR(30),
  cur_cd          CHAR(30),
  description     VARCHAR(255),
  message         VARCHAR(255),
  account_type_sf CHAR(3),
  item_type_cd    CHAR(1),
  transaction_id  UUID REFERENCES blockchain_transaction ON DELETE SET NULL,
  entry_date      TIMESTAMP      NOT NULL             DEFAULT now(),
  UNIQUE (university_id, invoice_id)
);
COMMENT ON COLUMN blockchain_invoice.invoice_id
IS 'referenced id from remote ERP';
CREATE INDEX ON blockchain_invoice (billed_from_id);
CREATE INDEX ON blockchain_invoice (billed_to_id);


CREATE TABLE blockchain_attendance (
  id            UUID        NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
  student_id    BIGINT      NOT NULL REFERENCES student ON DELETE RESTRICT,
  nfc_id        VARCHAR(64) NOT NULL,
  type_id       SMALLINT    NOT NULL             DEFAULT 1, -- todo: 1 - visited, 2 - excused
  timestamp     TIMESTAMP   NOT NULL,
  schedule_date INTERVAL    NOT NULL, -- todo: to avoid duplicates
  UNIQUE (student_id, schedule_date)
);
CREATE INDEX ON blockchain_attendance (schedule_date);
