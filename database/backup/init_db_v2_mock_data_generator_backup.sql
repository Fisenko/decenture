-- For all users password set to '1'

-- mock government for testing
with usr as (insert into app_user (email, password, status_id, role_id)
values ('uk-magic@magic.uk', '356a192b7913b04c54574d18c28d46e6395428ab', 1, 4)
returning *),
     acc as (insert into account (name, currency, type_id, status_id, user_id, owner_id)
  select 'British Ministry of Magic', 'GBP', 1, 0, u.id, u.id
  from usr u
returning *)
insert into government (id, country_code, name)
select a.id, 'UK', 'British Ministry of Magic'
from acc a;

-- mock university for testing
with usr as (insert into app_user (email, password, status_id, role_id)
values ('hogwarts@hogwarts.magic.uk', '356a192b7913b04c54574d18c28d46e6395428ab', 1, 2)
returning *),
     acc as (insert into account (name, currency, type_id, status_id, user_id, owner_id)
  select 'Hogwarts account', 'GBP', 1, 1, u.id, u.id
  from usr u
returning *)
insert into university (id, country_code, code, name, official_name, abbreviation, address, domain_name, erp_info)
select a.id,
       'UK',
       'HOG',
       'Hogwarts',
       'Hogwarts School of Witchcraft and Wizardry',
       'HSoW&W',
       'Highlands of Scotland alternative reality',
       'hogwarts.magic.uk',
       'unknown'
from acc a;

with usr as (insert into app_user (email, password, role_id)
values ('ilvermorny@ilvermorny.magic.us', '356a192b7913b04c54574d18c28d46e6395428ab', 2)
returning *),
     acc as (insert into account (name, currency, type_id, status_id, user_id, owner_id)
  select 'Ilvermorny account', 'USD', 1, 1, u.id, u.id
  from usr u
returning *)
insert into university (id, country_code, code, name, official_name, abbreviation, address, domain_name, erp_info)
select a.id,
       'US',
       'ILV',
       'Ilvermorny',
       'Ilvermorny School of Witchcraft and Wizardry',
       'ISoW&W',
       'Mount Greylock alternative reality',
       'ilvermorny.magic.us',
       'unknown'
from acc a;

/*
 mock sudents for testing
 */
with snames as (select 'Harry' first_name, 'Potter' last_name, '1' erp_student_id
    union all select 'Hermione' first_name, 'Granger' last_name, '2' erp_student_id
    union all select 'Ron' first_name, 'Weasley' last_name, '3' erp_student_id),
     sul as (select u.id                                                             university_id,
                    lower(s.first_name || '.' || s.last_name) || '@'|| u.domain_name email,
                    s.first_name || ' ' || s.last_name                               full_name,
                    '356a192b7913b04c54574d18c28d46e6395428ab'                       pass,
                    1                                                                status_id,
                    1                                                                role_id,
                    'GBP'                                                            currency,
                    s.first_name,
                    s.last_name,
                    s.erp_student_id
             from university u,
                  snames s
             where u.code = 'HOG'),
     stuser as (insert into app_user (email, password, status_id, role_id)
  select sul.email, sul.pass, sul.status_id, sul.role_id
  from sul
returning *),
     stuser_info as (insert into user_info (id, first_name, last_name)
  select stuser.id, sul.first_name, sul.last_name
  from sul sul,
       stuser stuser
  where sul.email = stuser.email
returning *),
     personal_acc as (insert into account (name, currency, type_id, status_id, user_id, owner_id)
  select sul.full_name || ' personal account', sul.currency, 1, sul.status_id, stuser.id, stuser.id
  from sul sul,
       stuser stuser
  where sul.email = stuser.email
returning *),
     escrow_acc as (insert into account (name, currency, type_id, status_id, user_id, owner_id)
  select sul.full_name || ' escrow account', sul.currency, 2, sul.status_id, stuser.id, gacc.user_id
  from sul sul,
       stuser stuser,
       (select ga.user_id
        from government g,
             account ga
        where g.id = ga.id
          and g.country_code = 'UK') gacc
  where sul.email = stuser.email
returning *)
insert into student (id, university_id, erp_student_id, first_name, last_name, erp_email)
select eacc.id, sul.university_id, sul.erp_student_id, sul.first_name, sul.last_name, sul.email
from escrow_acc eacc,
     stuser stu,
     sul sul
where eacc.user_id = stu.id
  and stu.email = sul.email;


/*
 Mock data for lecture_course
 */
with classes as (select 'HOM' as code, 'History of Magic' as "name"
    union all select 'DADA' as code, 'Defence Against the Dark Arts' "name"
    union all select 'T' as code, 'Transfiguration' as "name"
    union all select 'C' as code, 'Charms' as "name"
    union all select 'D' as code, 'Divination' as "name")
insert into lecture_course (university_id, code, name)
select u.id, c.code, c.name
from university u,
     classes c
where u.code = 'HOG';

INSERT INTO lecture_course (university_id, code, name, degtree_name, total)
SELECT DISTINCT (SELECT id FROM university WHERE code = 'HOG') university_id,
                lecture_course_id code, lecture_course_id as "name", 'unknown' degtree_name, 10 total
FROM dummy_lecture_theatre;

/*
 Mock data for scheduled_lecture
 */
insert into scheduled_lecture (nfc_id,
                               begin_date,
                               end_date,
                               lecture_course_id,
                               lecture_theatre_name,
                               event_type,
                               subject,
                               description,
                               notes)
with numbers as (select a.i from generate_series(0, 50) a (i)),
     start_date as (select date_trunc('day', now()) start_date),
     day_schedule as (select hh.i                                                         hhi,
                             d.start_date + make_interval(hours := hh.i * 2)              begin_date,
                             d.start_date + make_interval(hours := hh.i * 2, mins := 100) end_date,
                             d.start_date + make_interval(hours := hh.i * 1, mins := 50)  scan_begin_date,
                             d.start_date + make_interval(hours := hh.i * 2, mins := 90)  scan_end_date
                      from start_date d,
                           numbers hh
                      where hh.i between 5 and 10),
     schedule as (select dd.i                                            ddi,
                         d.hhi,
                         1 + d.hhi % 5                                   lecture_id,
                         d.begin_date + make_interval(days := dd.i)      begin_date,
                         d.end_date + make_interval(days := dd.i)        end_date,
                         d.scan_begin_date + make_interval(days := dd.i) scan_begin_date,
                         d.scan_end_date + make_interval(days := dd.i)   scan_end_date
                  from day_schedule d,
                       numbers dd
                  where dd.i between 0 and 50),
     lc as (select row_number() over (order by l.code) i, l.* from lecture_course l)
select 'TEST_NFC_ID_' || to_char(s.lecture_id, 'FM90')      nfc_id,
       s.begin_date,
       s.end_date,
       lc.id                                                lecture_course_id,
       'lecture_theatre_name ' || lc.id                     lecture_theatre_name,
       'A'                                                  event_type,
       lc.name || ' subject ' || to_char(s.ddi, 'FM90')     subject,
       lc.name || ' description ' || to_char(s.ddi, 'FM90') description,
       lc.name || ' notes ' || to_char(s.ddi, 'FM90')       notes
from schedule s,
     lc
where s.lecture_id = lc.i
order by s.ddi, s.hhi;

/*
 Mock data for scheduled_attendance
 */
insert into scheduled_attendance (student_id, scheduled_lecture_id)
select s.id student_id, sl.id scheduled_lecture_id
from scheduled_lecture sl,
     student s;
