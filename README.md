#Decenture 

### Requirements:
- OS: debian-9.6.0-amd64

## Install
1) [install Vagga](http://vagga.readthedocs.io/en/latest/installation.html)
2) [install docker](https://docs.docker.com/install/linux/docker-ce/debian/)  
3) [Install docker-compose](https://www.digitalocean.com/community/tutorials/how-to-install-docker-compose-on-debian-9)
4) Install HyperLedger:
    - `cd hyperledger`
    - `sh install.sh`
    - `sh refresh.sh`
    - `cd tutorial-network && sh build.sh`

## Configure
- Change vagga.yaml file for Decenture backend server:
    -  Section `app_env`:
        - `HYPERLEDGER_HOST`: <IP_ADDRESS_HYPERLEDGER>
        - `PUBLIC_HOST`: <IP_ADDRESS_BACKEND>
        - `HOST`: <IP_ADDRESS_BACKEND>
        - `DNS_NAME`: <IP_ADDRESS_BACKEND OR DNS_NAME>
    - Section `smtp_env`:
        - `SMTP_USER`: <SMTP_USER>
        - `SMTP_PASS`: <SMTP_PASSWORD>
## Run
- Backend & Frontend:  
    - `vagga backend-build`
    - `vagga run-prod`
- Hyperledger:
    - `cd hyperledger/tutorial-network && sh start.sh`
    - `composer-playground`    
## Create data: 
- PostgreSQL database: 
    - `vagga _pg_recreate`
- HyperLedger: 
    - `cd hyperledger/test_scripts && sh init_users.sh`  
