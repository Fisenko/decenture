export default class Invoice {
    id: string;
    university: string;
    invoice_id: string;
    invoice_number: string;
    invoice_date: string;
    due_date: string;
    billed_from: string;
    billed_to: string;
    amount: number;
    status: string;
    status_id?: number;
    first_name: string;
    last_name: string;
    email: string;
    business_unit: string;
    cur_cd: string;
    description: string;
    message: string;
    account_type_sf: string;
    item_type_cd: string;
    entry_date: string;

    constructor(data: any = {}) {
        this.id = data.id;
        this.university = data.university;
        this.invoice_id = data.invoice_id;
        this.invoice_number = data.invoice_number;
        this.invoice_date = data.invoice_date || new Date();
        this.due_date = data.due_date;
        this.billed_from = data.billed_from;
        this.billed_to = data.billed_to;
        this.amount = data.amount;
        this.status = data.status || 'NOT PAID';
        this.first_name = data.first_name;
        this.last_name = data.second_name;
        this.email = data.email;
        this.business_unit = data.business_unit;
        this.cur_cd = data.cur_cd;
        this.description = data.description || '';
        this.message = data.message || '';
        this.account_type_sf = data.account_type_sf;
        this.item_type_cd = data.item_type_cd;
        this.entry_date = data.entry_date;
    }
}