import app from "./app";
const PORT = 3501;

app.listen(PORT, () => {
    console.log('Express server listening on port ' + PORT);
});