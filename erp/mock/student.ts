export default class Student {
    id: string;
    university: string;
    first_name: string;
    second_name: string;
    email: string;

    constructor(data: any = {}) {
        this.id = data.id;
        this.university = data.university;
        this.first_name = data.first_name;
        this.second_name = data.second_name;
        this.email = data.email;
    }
}