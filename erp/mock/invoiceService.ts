import Invoice from './invoice';
import Student from './student';
const uuidv1 = require('uuid/v1');


const invoices: Array<Invoice> = [];
const students: Array<Student> = [
    new Student({
        id: '6525b73e-ddd5-4893-8562-da88ea817dc5',
        university: '9704c732-972a-4543-a9c2-3d88f06ff2f4',
        first_name: 'Harry',
        second_name: 'Potter',
        email: 'harry.potter@hogwarts.magic.uk'
    }),
    new Student({
        id: '51feecbe-79b6-4eea-a651-c00c4610543a',
        university: '9704c732-972a-4543-a9c2-3d88f06ff2f4',
        first_name: 'Hermione',
        second_name: 'Granger',
        email: 'hermione.granger@hogwarts.magic.uk'
    }),
    new Student({
        id: 'b7b92e71-5f0c-4fa2-bcfb-b6d04719a865',
        university: '9704c732-972a-4543-a9c2-3d88f06ff2f4',
        first_name: 'Ron',
        second_name: 'Weasley',
        email: 'ron.weasley@hogwarts.magic.uk'
    })
];

function fillInvoices() {
    let count = 0;
    students.forEach(function (student: Student) {
        ++count;
        invoices.push(
            new Invoice({
                id: uuidv1(),
                university: 'resource:org.acme.mynetwork.University#' + student.university,
                invoice_id: uuidv1(),
                invoice_number: String(count),
                invoice_date: new Date(),
                due_date: new Date(),
                billed_from: 'GOV',
                billed_to: student.id,
                amount: getRandomInt(1000, 5000),
                status: 'NOT PAID',
                first_name: 'resource:org.acme.mynetwork.Account#' + student.first_name,
                second_name: 'resource:org.acme.mynetwork.Account#' + student.second_name,
                email: student.email,
                business_unit: '',
                cur_cd: '',
                description: '',
                message: '',
                account_type_sf: '',
                item_type_cd: '',
                entry_date: new Date(),
            })
        );
    });
}

function getRandomInt (min: number, max: number) {
    return Math.floor(Math.random() * (max - min)) + min;
}

export function getInvoiceByStudentId(studentId?: string) {

    if (invoices.length === 0) {
        fillInvoices();
    }

    return invoices

}