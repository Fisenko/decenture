const uuidv1 = require('uuid/v1');
const http = require('http');

class Student {

    constructor(data) {
        this.id = data.id;
        this.university = data.university;
        this.first_name = data.first_name;
        this.second_name = data.second_name;
        this.email = data.email;
    }
}

class Invoice {

    constructor(data) {
        this.id = data.id;
        this.university = data.university;
        this.invoice_id = data.invoice_id;
        this.invoice_number = data.invoice_number;
        this.invoice_date = data.invoice_date || new Date();
        this.due_date = data.due_date;
        this.billed_from = data.billed_from;
        this.billed_to = data.billed_to;
        this.amount = data.amount;
        this.status = data.status || 'NOT PAID';
        this.first_name = data.first_name;
        this.last_name = data.second_name;
        this.email = data.email;
        this.business_unit = data.business_unit;
        this.cur_cd = data.cur_cd;
        this.description = data.description || '';
        this.message = data.message || '';
        this.account_type_sf = data.account_type_sf;
        this.item_type_cd = data.item_type_cd;
        this.entry_date = data.entry_date;
    }
}


const students = [
    new Student({
        id: '6525b73e-ddd5-4893-8562-da88ea817dc5',
        university: '9704c732-972a-4543-a9c2-3d88f06ff2f4',
        first_name: 'Harry',
        second_name: 'Potter',
        email: 'harry.potter@hogwarts.magic.uk'
    }),
    new Student({
        id: '51feecbe-79b6-4eea-a651-c00c4610543a',
        university: '9704c732-972a-4543-a9c2-3d88f06ff2f4',
        first_name: 'Hermione',
        second_name: 'Granger',
        email: 'hermione.granger@hogwarts.magic.uk'
    }),
    new Student({
        id: 'b7b92e71-5f0c-4fa2-bcfb-b6d04719a865',
        university: '9704c732-972a-4543-a9c2-3d88f06ff2f4',
        first_name: 'Ron',
        second_name: 'Weasley',
        email: 'ron.weasley@hogwarts.magic.uk'
    })
];



const invoices = [];

fillInvoices();

function fillInvoices() {
    students.forEach(function (student) {
        let rand = getRandomInt(0, 10);
        invoices.push(
            new Invoice({
                id: uuidv1(),
                university: 'resource:org.acme.mynetwork.University#' + student.university,
                invoice_id: uuidv1(),
                invoice_number: uuidv1(),
                invoice_date: new Date(),
                due_date: new Date(),
                billed_from: 'resource:org.acme.mynetwork.Account#9704c732-972a-4543-a9c2-3d88f06ff2f4',
                billed_to: 'resource:org.acme.mynetwork.Account#' + student.id,
                amount: getRandomInt(1000, 5000),
                status: 'NOT PAID',
                first_name: student.first_name,
                second_name: student.second_name,
                email: student.email,
                business_unit: 'unit',
                cur_cd: 'cur',
                description: rand > 5 ? 'Tuition Fee 1st' : 'Accommodation 2nd',
                message: rand > 5 ? 'Tuition Fee for Semester 1' : 'Accommodation for Semester 2',
                account_type_sf: 'private',
                item_type_cd: 'item',
                entry_date: new Date(),
            })
        );
    });
}

function getRandomInt (min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}

const options = {
    host: '192.168.50.33',
    port: 4601,
    path: `/invoice`,
    method: 'POST',
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    }
};


function insert(options, user) {
    options = Object.assign({}, options);
    const promise = new Promise((resolve) => {
        const request = http.request(options, function (res) {
            res.on('data', function (data) {
                resolve(data);
            });
        });
        request.write(JSON.stringify(user));
        request.end();
    });
    return promise;
}

console.log('invoices: ', invoices);

invoices.forEach(invoice => {
        insert(options, invoice).then(result => {
            console.log(`Добавлен новый инвойс ${decodeURIComponent(result)}`)
        });
    }
);
