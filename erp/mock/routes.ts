import {Request, Response} from 'express';
import { getInvoiceByStudentId } from './invoiceService';
const http = require('http');
const options = {
    host: 'localhost',
    port: 4601,
    path: `/invoice`,
    method: 'POST',
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    }
};

export class Routes {
    public routes(app): void {
        app.route('/invoice')
        .get((req: Request, res: Response) => {

            console.log('req', req);
            // res.status(200).send(
            //     req.url('http://localhost:3500/invoice?studentId=b7b92e71-5f0c-4fa2-bcfb-b6d04719a865')
            //     .body(getInvoiceByStudentId(req.query.studentId))
            // )
            const invoice = getInvoiceByStudentId('7dbf6297-6674-47df-b9fd-b9a8140d293d');
            console.log('INVOICE', invoice);
            insert(options, invoice)
            if (req.query.studentId){
                res.status(200).send({
                    invoice: getInvoiceByStudentId(req.query.studentId)
                })
            } else {
                res.status(400).send({
                    message: 'Required parameter: \'studentId!\''
                })
            }

        })
    }
}

function insert(options, user) {
    options = Object.assign({}, options);
    const promise = new Promise((resolve) => {
        const request = http.request(options, function (res) {
            res.on('data', function (data) {
                resolve(data);
            });
        });
        request.write(JSON.stringify(user));
        request.end();
    });
    return promise;
}