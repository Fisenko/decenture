npm install -g node-gyp
npm install -g generator-hyperledger-composer
npm install -g composer-cli
npm install -g composer-rest-server
npm install -g yo
npm install -g composer-playground
npm install -g wscat
mkdir -p fabric-dev-servers && cd fabric-dev-servers
curl -O https://raw.githubusercontent.com/hyperledger/composer-tools/master/packages/fabric-dev-servers/fabric-dev-servers.tar.gz
tar -xvf fabric-dev-servers.tar.gz
export FABRIC_VERSION=hlfv12
./downloadFabric.sh
