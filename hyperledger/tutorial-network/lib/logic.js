const PAID = 'PAID';

/**
 * Commodity transaction processor function.
 * @param {org.acme.mynetwork.InvoiceTransaction} tx The commodity transaction instance.
 * @transaction
 */
async function invoiceTransaction(tx) {

    if (tx.sender.amount >= tx.amount) {
        tx.sender.amount -= tx.amount;
        tx.receiver.amount += tx.amount;
        const accountRegistry = await getParticipantRegistry('org.acme.mynetwork.Account');
        await accountRegistry.update(tx.sender);
        await accountRegistry.update(tx.receiver);

        if (tx.invoice) {
            const invoiceRegistry = await getAssetRegistry('org.acme.mynetwork.Invoice');
            tx.invoice.status = PAID;
            tx.invoice.paid_date = tx.timestamp;
            await invoiceRegistry.update(tx.invoice);
        }

        const event = getFactory().newEvent('org.acme.mynetwork', 'TransactionEvent');
        event.sender_account_id = tx.sender.id;
        event.receiver_account_id = tx.receiver.id;
        event.sender_name = tx.sender.name;
        event.receiver_name = tx.receiver.name;
        event.amount = tx.amount;
        emit(event);
    } else {
        throw new Error('ERROR: not enough money');
    }
}

/**
 * Commodity transaction processor function.
 * @param {org.acme.mynetwork.AttendanceTransaction} tx The commodity transaction instance.
 * @transaction
 */
async function attendanceTransaction(tx) {
}

/**
 * Commodity transaction processor function.
 * @param {org.acme.mynetwork.AccountTransaction} tx The commodity transaction instance.
 * @transaction
 */
async function accountTransaction(tx) {
    const accountRegistry = await getParticipantRegistry('org.acme.mynetwork.Account');
    tx.account.amount += tx.amount;
    await accountRegistry.update(tx.account);
}

/**
 * Commodity transaction processor function.
 * @param {org.acme.mynetwork.CertificateTransaction} tx The commodity transaction instance.
 * @transaction
 */
async function certificateTransaction(tx) {

}
