const MOK_INVOICE_TRANSACTION = [
    {
        "post_date": new Date(),
        "sender": "resource:org.acme.mynetwork.Account#6525b73e-ddd5-4893-8562-da88ea817dc5",
        "receiver": "resource:org.acme.mynetwork.Account#9704c732-972a-4543-a9c2-3d88f06ff2f4",
        "amount": 96,
        "description": "for food",
        "invoice": "resource:org.acme.mynetwork.Invoice#c1bb50a2-cd5a-11e8-a8d5-f2801f1b9fd1"
    }
];

const http = require('http');
const options = {
    host: 'localhost',
    port: 3000,
    path: `/api/InvoiceTransaction`,
    method: 'GET',
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    }
};


function checkDataBase(options) {
    options = Object.assign({}, options);
    const promise = new Promise(resolve => {
        const request = http.request(options, function (res) {
            res.on('data', function (data) {
                resolve(data);
            });
        });
        request.write('');
        request.end();
    });
    return promise;
}

function insert(options, user) {
    options = Object.assign({}, options);
    options.method = "POST";
    const promise = new Promise((resolve) => {
        const request = http.request(options, function (res) {
            res.on('data', function (data) {
                resolve(data);
            });
        });
        request.write(JSON.stringify(user));
        request.end();
    });
    return promise;
}

checkDataBase(options).then(
    value => {
        value = JSON.parse(decodeURIComponent(value));
        console.log(value);
        MOK_INVOICE_TRANSACTION.forEach(invoice => {
            insert(options, invoice).then(result => {
                console.log(`Transaction created successfully ${decodeURIComponent(result)}`)
            });
        })
    }
);
