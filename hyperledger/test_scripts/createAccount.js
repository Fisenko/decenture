const MOK_USERS = [
    {
        id: '9276d351-648d-4b5e-a129-94c105e6b8d6',
        name: 'British Ministry of Magic',
        amount: 0,
        currency: 'GBP',
        type_id: 1,
        status_id: 0,
        user_id: '61180ba7-5997-4ec3-9646-5288b0404629',
        owner_id: '61180ba7-5997-4ec3-9646-5288b0404629',
        entry_date: '2018-10-17 09:29:00.246904',
        close_date: null
    },
    {
        id: '9704c732-972a-4543-a9c2-3d88f06ff2f4',
        name: 'Hogwarts account',
        amount: 0,
        currency: 'GBP',
        type_id: 1,
        status_id: 1,
        user_id: '0d48cbbd-970e-406c-a3c8-2ddde4b12130',
        owner_id: '0d48cbbd-970e-406c-a3c8-2ddde4b12130',
        entry_date: '2018-10-17 09:29:00.282826',
        close_date: null
    },
    {
        id: '771a5992-63d6-4b80-a145-ab80ea1bc18e',
        name: 'Ilvermorny account',
        amount: 0,
        currency: 'USD',
        type_id: 1,
        status_id: 1,
        user_id: 'c6c240d0-6270-4809-9868-e259ea03276a',
        owner_id: 'c6c240d0-6270-4809-9868-e259ea03276a',
        entry_date: '2018-10-17 09:29:00.325751',
        close_date: null
    },
    {
        id: '7dbf6297-6674-47df-b9fd-b9a8140d293d',
        name: 'Harry Potter personal account',
        amount: 0,
        currency: 'GBP',
        type_id: 1,
        status_id: 1,
        user_id: 'eadd4922-fc58-4697-9b23-d2160ab505e4',
        owner_id: 'eadd4922-fc58-4697-9b23-d2160ab505e4',
        entry_date: '2018-10-17 09:29:00.416542',
        close_date: null
    },
    {
        id: '6ad6f148-ee1b-4aeb-a39e-a47e9f4f91de',
        name: 'Hermione Granger personal account',
        amount: 0,
        currency: 'GBP',
        type_id: 1,
        status_id: 1,
        user_id: '47988b94-50c7-4b12-b3c4-6e86676e6e1f',
        owner_id: '47988b94-50c7-4b12-b3c4-6e86676e6e1f',
        entry_date: '2018-10-17 09:29:00.416542',
        close_date: null
    },
    {
        id: '12320fb0-deb6-45cb-9f76-95b39f906130',
        name: 'Ron Weasley personal account',
        amount: 0,
        currency: 'GBP',
        type_id: 1,
        status_id: 1,
        user_id: 'df163a56-0c9d-4b64-aa97-81925a7f91ae',
        owner_id: 'df163a56-0c9d-4b64-aa97-81925a7f91ae',
        entry_date: '2018-10-17 09:29:00.416542',
        close_date: null
    },
    {
        id: '5b501e3d-5df3-4040-b670-88ee941de372',
        name: 'Emmanuel Young personal account',
        amount: 0,
        currency: 'GBP',
        type_id: 1,
        status_id: 1,
        user_id: 'c7b0399e-7e3a-45bd-972a-239bc8386ac6',
        owner_id: 'c7b0399e-7e3a-45bd-972a-239bc8386ac6',
        entry_date: '2018-11-08 17:41:21.280060',
        close_date: null
    },
    {
        id: '37d6541d-6171-47ef-b575-e9fe8e7d4683',
        name: 'George Benton personal account',
        amount: 0,
        currency: 'GBP',
        type_id: 1,
        status_id: 1,
        user_id: 'f1d7c74c-95df-4fda-951f-7f76d1aaa4fa',
        owner_id: 'f1d7c74c-95df-4fda-951f-7f76d1aaa4fa',
        entry_date: '2018-11-08 17:41:21.280060',
        close_date: null
    },
    {
        id: 'd953c5f3-5aec-4997-890f-757b9c1d3d9a',
        name: 'Loubna Hadid personal account',
        amount: 0,
        currency: 'GBP',
        type_id: 1,
        status_id: 1,
        user_id: 'd43a29c0-b704-4466-bd38-326771c00549',
        owner_id: 'd43a29c0-b704-4466-bd38-326771c00549',
        entry_date: '2018-11-08 17:41:21.280060',
        close_date: null
    },
    {
        id: '25eba29f-405c-4f24-8705-3b88b2a2adb8',
        name: 'Walid Hadid personal account',
        amount: 0,
        currency: 'GBP',
        type_id: 1,
        status_id: 1,
        user_id: '9a5e10fb-5518-46e9-aeec-117bc7f34933',
        owner_id: '9a5e10fb-5518-46e9-aeec-117bc7f34933',
        entry_date: '2018-11-08 17:41:21.280060',
        close_date: null
    },
    {
        id: '9bc0bba5-7758-4e67-834e-88f1d68a42a7',
        name: 'Rageeb Mahtab personal account',
        amount: 0,
        currency: 'GBP',
        type_id: 1,
        status_id: 1,
        user_id: 'bcf85f27-a76b-4143-9c1a-ce546831326d',
        owner_id: 'bcf85f27-a76b-4143-9c1a-ce546831326d',
        entry_date: '2018-11-08 17:41:21.280060',
        close_date: null
    },
    {
        id: '61e796c1-eed4-4818-a0c5-d8ef2ebd9b65',
        name: 'Euclid D Souza personal account',
        amount: 0,
        currency: 'GBP',
        type_id: 1,
        status_id: 1,
        user_id: 'eac4fe9a-58ec-43af-98ad-03f9a61fd8db',
        owner_id: 'eac4fe9a-58ec-43af-98ad-03f9a61fd8db',
        entry_date: '2018-11-08 17:41:21.280060',
        close_date: null
    },
    {
        id: '6525b73e-ddd5-4893-8562-da88ea817dc5',
        name: 'Harry Potter escrow account',
        amount: 0,
        currency: 'GBP',
        type_id: 2,
        status_id: 1,
        user_id: 'eadd4922-fc58-4697-9b23-d2160ab505e4',
        owner_id: '61180ba7-5997-4ec3-9646-5288b0404629',
        entry_date: '2018-10-17 09:29:00.416542',
        close_date: null
    },
    {
        id: '51feecbe-79b6-4eea-a651-c00c4610543a',
        name: 'Hermione Granger escrow account',
        amount: 0,
        currency: 'GBP',
        type_id: 2,
        status_id: 1,
        user_id: '47988b94-50c7-4b12-b3c4-6e86676e6e1f',
        owner_id: '61180ba7-5997-4ec3-9646-5288b0404629',
        entry_date: '2018-10-17 09:29:00.416542',
        close_date: null
    },
    {
        id: 'b7b92e71-5f0c-4fa2-bcfb-b6d04719a865',
        name: 'Ron Weasley escrow account',
        amount: 0,
        currency: 'GBP',
        type_id: 2,
        status_id: 1,
        user_id: 'df163a56-0c9d-4b64-aa97-81925a7f91ae',
        owner_id: '61180ba7-5997-4ec3-9646-5288b0404629',
        entry_date: '2018-10-17 09:29:00.416542',
        close_date: null
    },
    {
        id: '7251728e-cd03-434f-984a-8f085a41c596',
        name: 'Euclid D Souza escrow account',
        amount: 0,
        currency: 'GBP',
        type_id: 2,
        status_id: 1,
        user_id: 'eac4fe9a-58ec-43af-98ad-03f9a61fd8db',
        owner_id: '61180ba7-5997-4ec3-9646-5288b0404629',
        entry_date: '2018-11-01 18:04:06.608379',
        close_date: null
    },
    {
        id: '8c3def2a-27d8-4afa-9c93-518642bb473a',
        name: 'Rageeb Mahtab escrow account',
        amount: 0,
        currency: 'GBP',
        type_id: 2,
        status_id: 1,
        user_id: 'bcf85f27-a76b-4143-9c1a-ce546831326d',
        owner_id: '61180ba7-5997-4ec3-9646-5288b0404629',
        entry_date: '2018-11-01 18:04:06.608379',
        close_date: null
    },
    {
        id: 'a130a991-7133-4a5d-9cda-208bad11709a',
        name: 'Walid Hadid escrow account',
        amount: 0,
        currency: 'GBP',
        type_id: 2,
        status_id: 1,
        user_id: '9a5e10fb-5518-46e9-aeec-117bc7f34933',
        owner_id: '61180ba7-5997-4ec3-9646-5288b0404629',
        entry_date: '2018-11-01 18:04:06.608379',
        close_date: null
    },
    {
        id: '6ecaa91e-9114-4e25-9307-59e60f7a330e',
        name: 'Loubna Hadid escrow account',
        amount: 0,
        currency: 'GBP',
        type_id: 2,
        status_id: 1,
        user_id: 'd43a29c0-b704-4466-bd38-326771c00549',
        owner_id: '61180ba7-5997-4ec3-9646-5288b0404629',
        entry_date: '2018-11-01 18:04:06.608379',
        close_date: null
    },
    {
        id: '0091f0d9-090d-4498-94ae-9bc4336a5830',
        name: 'George Benton escrow account',
        amount: 0,
        currency: 'GBP',
        type_id: 2,
        status_id: 1,
        user_id: 'f1d7c74c-95df-4fda-951f-7f76d1aaa4fa',
        owner_id: '61180ba7-5997-4ec3-9646-5288b0404629',
        entry_date: '2018-11-01 18:04:06.608379',
        close_date: null
    },
    {
        id: 'eeafd6c7-c47d-44cf-85e1-4705b85883f8',
        name: 'Emmanuel Young escrow account',
        amount: 0,
        currency: 'GBP',
        type_id: 2,
        status_id: 1,
        user_id: 'c7b0399e-7e3a-45bd-972a-239bc8386ac6',
        owner_id: '61180ba7-5997-4ec3-9646-5288b0404629',
        entry_date: '2018-11-01 18:04:06.608379',
        close_date: null
    },
    {
        id: 'a8374714-19a7-408c-bbf6-2aa95f617eb1',
        name: 'The New School personal account',
        amount: 0,
        currency: 'GBP',
        type_id: 1,
        status_id: 1,
        user_id: 'd749eb49-1277-4456-80ac-934bc7e09f5d',
        owner_id: 'd749eb49-1277-4456-80ac-934bc7e09f5d',
        entry_date: '2019-02-06 11:48:00.438473',
        close_date: null
    },
    {
        id: 'ff8f2fa2-7787-4319-9c75-82b5c09f8f41',
        name: 'The New School escrow account',
        amount: 0,
        currency: 'GBP',
        type_id: 2,
        status_id: 1,
        user_id: 'd749eb49-1277-4456-80ac-934bc7e09f5d',
        owner_id: '61180ba7-5997-4ec3-9646-5288b0404629',
        entry_date: '2019-02-06 11:48:00.438473',
        close_date: null
    },
    {
        id: '3926c2e7-ba17-484f-a227-406e158defd2',
        name: 'Test First escrow account',
        amount: 0,
        currency: 'GBP',
        type_id: 2,
        status_id: 1,
        user_id: '19196259-ec6a-4b24-8fe6-01f5aa5ec074',
        owner_id: '61180ba7-5997-4ec3-9646-5288b0404629',
        entry_date: '2019-02-26 13:44:33.139512',
        close_date: null
    },
    {
        id: '825175a1-75f8-4e01-b8d5-6aa3bf284bc5',
        name: 'Test Second escrow account',
        amount: 0,
        currency: 'GBP',
        type_id: 2,
        status_id: 1,
        user_id: '5631e5e7-79ad-4604-8949-8924d8aed08c',
        owner_id: '61180ba7-5997-4ec3-9646-5288b0404629',
        entry_date: '2019-02-26 13:44:33.139512',
        close_date: null
    },
    {
        id: 'd8c87c88-398c-4a2f-8e47-3e880530e2c3',
        name: 'Test Third escrow account',
        amount: 0,
        currency: 'GBP',
        type_id: 2,
        status_id: 1,
        user_id: 'e1e98663-3a56-4323-884b-1da79a43330e',
        owner_id: '61180ba7-5997-4ec3-9646-5288b0404629',
        entry_date: '2019-02-26 13:44:33.139512',
        close_date: null
    },
    {
        id: '2843f382-0fbe-41a9-b586-9b3416069df5',
        name: 'Test Fourth escrow account',
        amount: 0,
        currency: 'GBP',
        type_id: 2,
        status_id: 1,
        user_id: '29d2ed6b-bb8e-4d38-a5b1-a1e0e7f6fb06',
        owner_id: '61180ba7-5997-4ec3-9646-5288b0404629',
        entry_date: '2019-02-26 13:44:33.139512',
        close_date: null
    },
    {
        id: '3853e32c-3706-4c99-a4fc-ab5b40f96243',
        name: 'Test Fifth escrow account',
        amount: 0,
        currency: 'GBP',
        type_id: 2,
        status_id: 1,
        user_id: '372dc9f0-6414-4666-88e6-91c625da54ed',
        owner_id: '61180ba7-5997-4ec3-9646-5288b0404629',
        entry_date: '2019-02-26 13:44:33.139512',
        close_date: null
    },
    {
        id: '3f2a7273-7ad0-46f8-b251-079ea771e9ec',
        name: 'Test Sixth escrow account',
        amount: 0,
        currency: 'GBP',
        type_id: 2,
        status_id: 1,
        user_id: '9978caf0-f0c1-4e18-9072-c502afda3cc7',
        owner_id: '61180ba7-5997-4ec3-9646-5288b0404629',
        entry_date: '2019-02-26 13:44:33.139512',
        close_date: null
    },
    {
        id: '603aa304-2cb4-4ef9-a8bd-276a60f6f4b8',
        name: 'Test Seventh escrow account',
        amount: 0,
        currency: 'GBP',
        type_id: 2,
        status_id: 1,
        user_id: '7e2aab83-5013-402f-b11f-fe0636ead664',
        owner_id: '61180ba7-5997-4ec3-9646-5288b0404629',
        entry_date: '2019-02-26 13:44:33.139512',
        close_date: null
    },
    {
        id: 'e22004ad-24bc-4313-b6f9-4d9da1277b98',
        name: 'Test Eighth escrow account',
        amount: 0,
        currency: 'GBP',
        type_id: 2,
        status_id: 1,
        user_id: '3fe41857-ddd9-42fb-9af3-da6fae416024',
        owner_id: '61180ba7-5997-4ec3-9646-5288b0404629',
        entry_date: '2019-02-26 13:44:33.139512',
        close_date: null
    },
    {
        id: '54b8bbc2-d741-4897-8a46-6688d5cf9410',
        name: 'Test Ninth escrow account',
        amount: 0,
        currency: 'GBP',
        type_id: 2,
        status_id: 1,
        user_id: '85c06967-1369-4876-a7f6-78cb2188b696',
        owner_id: '61180ba7-5997-4ec3-9646-5288b0404629',
        entry_date: '2019-02-26 13:44:33.139512',
        close_date: null
    },
    {
        id: '2157560a-a9d8-4f14-835b-fae66726e7d5',
        name: 'Test Tenth escrow account',
        amount: 0,
        currency: 'GBP',
        type_id: 2,
        status_id: 1,
        user_id: '73604cb8-8615-4e18-925d-acc3f892fe89',
        owner_id: '61180ba7-5997-4ec3-9646-5288b0404629',
        entry_date: '2019-02-26 13:44:33.139512',
        close_date: null
    },
    {
        id: 'c810dffa-dc78-4473-90d0-52b632229b3d',
        name: 'Test First personal account',
        amount: 0,
        currency: 'GBP',
        type_id: 1,
        status_id: 1,
        user_id: '19196259-ec6a-4b24-8fe6-01f5aa5ec074',
        owner_id: '19196259-ec6a-4b24-8fe6-01f5aa5ec074',
        entry_date: '2019-02-26 13:44:33.139512',
        close_date: null
    },
    {
        id: '7831ac69-a1fd-4ade-bc4d-42a7ae7b1e80',
        name: 'Test Second personal account',
        amount: 0,
        currency: 'GBP',
        type_id: 1,
        status_id: 1,
        user_id: '5631e5e7-79ad-4604-8949-8924d8aed08c',
        owner_id: '5631e5e7-79ad-4604-8949-8924d8aed08c',
        entry_date: '2019-02-26 13:44:33.139512',
        close_date: null
    },
    {
        id: '879e8d40-3733-4e31-8b55-8d608e3a305c',
        name: 'Test Third personal account',
        amount: 0,
        currency: 'GBP',
        type_id: 1,
        status_id: 1,
        user_id: 'e1e98663-3a56-4323-884b-1da79a43330e',
        owner_id: 'e1e98663-3a56-4323-884b-1da79a43330e',
        entry_date: '2019-02-26 13:44:33.139512',
        close_date: null
    },
    {
        id: 'e8f7e2b4-955c-4079-b5ae-13e76b527429',
        name: 'Test Fourth personal account',
        amount: 0,
        currency: 'GBP',
        type_id: 1,
        status_id: 1,
        user_id: '29d2ed6b-bb8e-4d38-a5b1-a1e0e7f6fb06',
        owner_id: '29d2ed6b-bb8e-4d38-a5b1-a1e0e7f6fb06',
        entry_date: '2019-02-26 13:44:33.139512',
        close_date: null
    },
    {
        id: '5400ea1e-0371-48f2-85a4-009342d3ff03',
        name: 'Test Fifth personal account',
        amount: 0,
        currency: 'GBP',
        type_id: 1,
        status_id: 1,
        user_id: '372dc9f0-6414-4666-88e6-91c625da54ed',
        owner_id: '372dc9f0-6414-4666-88e6-91c625da54ed',
        entry_date: '2019-02-26 13:44:33.139512',
        close_date: null
    },
    {
        id: '27eb132e-316d-45c3-9102-bcdbe65cfef0',
        name: 'Test Sixth personal account',
        amount: 0,
        currency: 'GBP',
        type_id: 1,
        status_id: 1,
        user_id: '9978caf0-f0c1-4e18-9072-c502afda3cc7',
        owner_id: '9978caf0-f0c1-4e18-9072-c502afda3cc7',
        entry_date: '2019-02-26 13:44:33.139512',
        close_date: null
    },
    {
        id: '5032ce28-1a3a-46e6-bece-451fb5e5ac42',
        name: 'Test Seventh personal account',
        amount: 0,
        currency: 'GBP',
        type_id: 1,
        status_id: 1,
        user_id: '7e2aab83-5013-402f-b11f-fe0636ead664',
        owner_id: '7e2aab83-5013-402f-b11f-fe0636ead664',
        entry_date: '2019-02-26 13:44:33.139512',
        close_date: null
    },
    {
        id: '1884372d-d133-4057-9bdf-67227cbdca85',
        name: 'Test Eighth personal account',
        amount: 0,
        currency: 'GBP',
        type_id: 1,
        status_id: 1,
        user_id: '3fe41857-ddd9-42fb-9af3-da6fae416024',
        owner_id: '3fe41857-ddd9-42fb-9af3-da6fae416024',
        entry_date: '2019-02-26 13:44:33.139512',
        close_date: null
    },
    {
        id: '3ff38563-4b2c-45c3-9c5e-5e6c86abcfb0',
        name: 'Test Ninth personal account',
        amount: 0,
        currency: 'GBP',
        type_id: 1,
        status_id: 1,
        user_id: '85c06967-1369-4876-a7f6-78cb2188b696',
        owner_id: '85c06967-1369-4876-a7f6-78cb2188b696',
        entry_date: '2019-02-26 13:44:33.139512',
        close_date: null
    },
    {
        id: 'bd94c1f6-e1c1-4432-94c4-f2e34226ed1c',
        name: 'Test Tenth personal account',
        amount: 0,
        currency: 'GBP',
        type_id: 1,
        status_id: 1,
        user_id: '73604cb8-8615-4e18-925d-acc3f892fe89',
        owner_id: '73604cb8-8615-4e18-925d-acc3f892fe89',
        entry_date: '2019-02-26 13:44:33.139512',
        close_date: null
    }
];

const http = require('http');


const optionsAccount = {
    host: 'localhost',
    port: 3000,
    path: `/api/Account`,
    method: 'GET',
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    }
};

function checkDataBase(options) {
    options = Object.assign({}, options);
    const promise = new Promise(resolve => {
        const request = http.request(options, function (res) {
            res.on('data', function (data) {
                resolve(data);
            });
        });
        request.write('');
        request.end();
    });
    return promise;
}

function insert(options, user) {
    options = Object.assign({}, options);
    options.method = "POST";
    const promise = new Promise((resolve) => {
        const request = http.request(options, function (res) {
            res.on('data', function (data) {
                resolve(data);
            });
        });
        request.write(JSON.stringify(user));
        request.end();
    });
    return promise;
}

checkDataBase(optionsAccount).then(
    value => {
        value = JSON.parse(decodeURIComponent(value));
        console.log(value);
        MOK_USERS.forEach(user => {
            if (!value.find(item => item.id === user.id)) {
                insert(optionsAccount, user).then(result => {
                    console.log(`Account created successfully ${decodeURIComponent(result)}`)
                });
            }
        })
    }
);
