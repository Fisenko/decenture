const asyncForEach = async (array, callback) => {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array);
    }
};
const TYPE = {
    'ADM': 'Admission Fees',
    'CNV': 'Converson Balance from ISIS',
    'END': 'Enrollment Deposits Paid',
    'ENR': 'Enrollment Fees',
    'MCF': 'Misc Charges and Fines',
    'OTH': 'Other',
    'PP': 'Payment Plans',
    'PPL': 'Payment Plans',
    'PRL': 'Payroll Deduction Payment Plan',
    'RMB': 'Room and Board',
    'SCH': 'Scholarships',
    'SPN': 'Sponsored Charges',
    'TUT': 'Tuition',
    'UND': 'Undefined Postings',
    'XCP': 'Cross Campus Account',
    'XFA': 'Excess Financial Aid',
    'XPY': 'Excess Payments',
    'XWA': 'Excess Waivers',
    'XWV': 'Excess Waiver Account'
};

// name, surname, email, student_id, invoice_id, amount, date, account_type
const STATIC_MOCK = [
    [
        'Rageeb ',
        'Mahtab',
        'rageeb@decenture.com',
        '8c3def2a-27d8-4afa-9c93-518642bb473a',
        'DE0001',
        '12000',
        '10.1.18',
        'ENR'
    ],
    [
        'Rageeb ',
        'Mahtab',
        'rageeb@decenture.com',
        '8c3def2a-27d8-4afa-9c93-518642bb473a',
        'DE0002',
        '7800',
        '10.9.18',
        'RMB'
    ],
    [
        'Rageeb ',
        'Mahtab',
        'rageeb@decenture.com',
        '8c3def2a-27d8-4afa-9c93-518642bb473a',
        'DE0003',
        '7000',
        '10.15.18',
        'TUT'
    ],
    [
        'George ',
        'Benton',
        'gbenton@decenture.com',
        '0091f0d9-090d-4498-94ae-9bc4336a5830',
        'DE0004',
        '9500',
        '10.1.18',
        'ENR'
    ],
    [
        'George ',
        'Benton',
        'gbenton@decenture.com',
        '0091f0d9-090d-4498-94ae-9bc4336a5830',
        'DE0005',
        '10900',
        '10.9.18',
        'RMB'
    ],
    [
        'George ',
        'Benton',
        'gbenton@decenture.com',
        '0091f0d9-090d-4498-94ae-9bc4336a5830',
        'DE0006',
        '7400',
        '10.15.18',
        'TUT'
    ],
    [
        'Emmanuel  ',
        'Young',
        'eyoung@decenture.com',
        'eeafd6c7-c47d-44cf-85e1-4705b85883f8',
        'DE0007',
        '7800',
        '10.1.18',
        'ENR'
    ],
    [
        'Emmanuel  ',
        'Young',
        'eyoung@decenture.com',
        'eeafd6c7-c47d-44cf-85e1-4705b85883f8',
        'DE0008',
        '12900',
        '10.9.18',
        'RMB'
    ],
    [
        'Emmanuel  ',
        'Young',
        'eyoung@decenture.com',
        'eeafd6c7-c47d-44cf-85e1-4705b85883f8',
        'DE0009',
        '3800',
        '10.15.18',
        'TUT'
    ],
    [
        'Euclid ',
        'D Souza',
        'edsouza@decenture.com',
        '7251728e-cd03-434f-984a-8f085a41c596',
        'DE0010',
        '9100',
        '10.1.18',
        'ENR'
    ],
    [
        'Euclid ',
        'D Souza',
        'edsouza@decenture.com',
        '7251728e-cd03-434f-984a-8f085a41c596',
        'DE0011',
        '4500',
        '10.9.18',
        'RMB'
    ],
    [
        'Euclid ',
        'D Souza',
        'edsouza@decenture.com',
        '7251728e-cd03-434f-984a-8f085a41c596',
        'DE0012',
        '9200',
        '10.15.18',
        'TUT'
    ],
    [
        'Loubna ',
        'Hadid',
        'lhadid@decenture.com',
        '6ecaa91e-9114-4e25-9307-59e60f7a330e',
        'DE0013',
        '13200',
        '10.1.18',
        'ENR'
    ],
    [
        'Loubna ',
        'Hadid',
        'lhadid@decenture.com',
        '6ecaa91e-9114-4e25-9307-59e60f7a330e',
        'DE0014',
        '10700',
        '10.9.18',
        'RMB'
    ],
    [
        'Loubna ',
        'Hadid',
        'lhadid@decenture.com',
        '6ecaa91e-9114-4e25-9307-59e60f7a330e',
        'DE0015',
        '6600',
        '10.15.18',
        'TUT'
    ],
    [
        'Walid ',
        'Hadid',
        'walid@decenture.com',
        'a130a991-7133-4a5d-9cda-208bad11709a',
        'DE0016',
        '5100',
        '10.1.18',
        'ENR'
    ],
    [
        'Walid ',
        'Hadid',
        'walid@decenture.com',
        'a130a991-7133-4a5d-9cda-208bad11709a',
        'DE0017',
        '10200',
        '10.9.18',
        'RMB'
    ],
    [
        'Walid ',
        'Hadid',
        'walid@decenture.com',
        'a130a991-7133-4a5d-9cda-208bad11709a',
        'DE0018',
        '9700',
        '10.15.18',
        'TUT'
    ],
    [
        'Harry',
        'Potter',
        'harry.potter@hogwarts.magic.uk',
        '6525b73e-ddd5-4893-8562-da88ea817dc5',
        'DE0019',
        '7600',
        '10.1.18',
        'ENR'
    ],
    [
        'Harry',
        'Potter',
        'harry.potter@hogwarts.magic.uk',
        '6525b73e-ddd5-4893-8562-da88ea817dc5',
        'DE0020',
        '11900',
        '10.9.18',
        'RMB'
    ],
    [
        'Harry',
        'Potter',
        'harry.potter@hogwarts.magic.uk',
        '6525b73e-ddd5-4893-8562-da88ea817dc5',
        'DE0021',
        '6700',
        '10.15.18',
        'TUT'
    ],
    [
        'Hermione',
        'Granger',
        'hermione.granger@hogwarts.magic.uk',
        '51feecbe-79b6-4eea-a651-c00c4610543a',
        'DE0022',
        '9000',
        '10.1.18',
        'ENR'
    ],
    [
        'Hermione',
        'Granger',
        'hermione.granger@hogwarts.magic.uk',
        '51feecbe-79b6-4eea-a651-c00c4610543a',
        'DE0023',
        '12200',
        '10.9.18',
        'RMB'
    ],
    [
        'Hermione',
        'Granger',
        'hermione.granger@hogwarts.magic.uk',
        '51feecbe-79b6-4eea-a651-c00c4610543a',
        'DE0024',
        '8200',
        '10.15.18',
        'TUT'
    ],
    [
        'Ron',
        'Weasley',
        'ron.weasley@hogwarts.magic.uk',
        'b7b92e71-5f0c-4fa2-bcfb-b6d04719a865',
        'DE0025',
        '7000',
        '10.1.18',
        'ENR'
    ],
    [
        'Ron',
        'Weasley',
        'ron.weasley@hogwarts.magic.uk',
        'b7b92e71-5f0c-4fa2-bcfb-b6d04719a865',
        'DE0026',
        '10900',
        '10.9.18',
        'RMB'
    ],
    [
        'Ron',
        'Weasley',
        'ron.weasley@hogwarts.magic.uk',
        'b7b92e71-5f0c-4fa2-bcfb-b6d04719a865',
        'DE0027',
        '10900',
        '10.15.18',
        'TUT'
    ],
    [
        "Rageeb ",
        "Mahtab",
        "rageeb@decenture.com",
        "8c3def2a-27d8-4afa-9c93-518642bb473a",
        "DE0028",
        "8600",
        "10.16.18",
        "ADM"
    ],
    [
        "Rageeb ",
        "Mahtab",
        "rageeb@decenture.com",
        "8c3def2a-27d8-4afa-9c93-518642bb473a",
        "DE0029",
        "6600",
        "10.19.18",
        "SCH"
    ],
    [
        "Rageeb ",
        "Mahtab",
        "rageeb@decenture.com",
        "8c3def2a-27d8-4afa-9c93-518642bb473a",
        "DE0030",
        "7200",
        "11.11.18",
        "XFA"
    ],
    [
        "George ",
        "Benton",
        "gbenton@decenture.com",
        "0091f0d9-090d-4498-94ae-9bc4336a5830",
        "DE0031",
        "4600",
        "10.16.18",
        "ADM"
    ],
    [
        "George ",
        "Benton",
        "gbenton@decenture.com",
        "0091f0d9-090d-4498-94ae-9bc4336a5830",
        "DE0032",
        "14800",
        "10.19.18",
        "SCH"
    ],
    [
        "George ",
        "Benton",
        "gbenton@decenture.com",
        "0091f0d9-090d-4498-94ae-9bc4336a5830",
        "DE0033",
        "11700",
        "11.11.18",
        "XFA"
    ],
    [
        "Emmanuel ",
        "Young",
        "eyoung@decenture.com",
        "eeafd6c7-c47d-44cf-85e1-4705b85883f8",
        "DE0034",
        "6100",
        "10.16.18",
        "ADM"
    ],
    [
        "Emmanuel ",
        "Young",
        "eyoung@decenture.com",
        "eeafd6c7-c47d-44cf-85e1-4705b85883f8",
        "DE0035",
        "7000",
        "10.19.18",
        "SCH"
    ],
    [
        "Emmanuel ",
        "Young",
        "eyoung@decenture.com",
        "eeafd6c7-c47d-44cf-85e1-4705b85883f8",
        "DE0036",
        "12100",
        "11.11.18",
        "XFA"
    ],
    [
        "Euclid ",
        "D Souza",
        "edsouza@decenture.com",
        "7251728e-cd03-434f-984a-8f085a41c596",
        "DE0037",
        "8000",
        "10.16.18",
        "ADM"
    ],
    [
        "Euclid ",
        "D Souza",
        "edsouza@decenture.com",
        "7251728e-cd03-434f-984a-8f085a41c596",
        "DE0038",
        "3700",
        "10.19.18",
        "SCH"
    ],
    [
        "Euclid ",
        "D Souza",
        "edsouza@decenture.com",
        "7251728e-cd03-434f-984a-8f085a41c596",
        "DE0039",
        "5600",
        "11.11.18",
        "XFA"
    ],
    [
        "Loubna ",
        "Hadid",
        "lhadid@decenture.com",
        "6ecaa91e-9114-4e25-9307-59e60f7a330e",
        "DE0040",
        "12000",
        "10.16.18",
        "ADM"
    ],
    [
        "Loubna ",
        "Hadid",
        "lhadid@decenture.com",
        "6ecaa91e-9114-4e25-9307-59e60f7a330e",
        "DE0041",
        "9100",
        "10.19.18",
        "SCH"
    ],
    [
        "Loubna ",
        "Hadid",
        "lhadid@decenture.com",
        "6ecaa91e-9114-4e25-9307-59e60f7a330e",
        "DE0042",
        "11200",
        "11.11.18",
        "XFA"
    ],
    [
        "Walid ",
        "Hadid",
        "walid@decenture.com",
        "a130a991-7133-4a5d-9cda-208bad11709a",
        "DE0043",
        "14100",
        "10.16.18",
        "ADM"
    ],
    [
        "Walid ",
        "Hadid",
        "walid@decenture.com",
        "a130a991-7133-4a5d-9cda-208bad11709a",
        "DE0044",
        "6100",
        "10.19.18",
        "SCH"
    ],
    [
        "Walid ",
        "Hadid",
        "walid@decenture.com",
        "a130a991-7133-4a5d-9cda-208bad11709a",
        "DE0045",
        "7400",
        "11.11.18",
        "XFA"
    ],
    [
        "Harry",
        "Potter",
        "harry.potter@hogwarts.magic.uk",
        "6525b73e-ddd5-4893-8562-da88ea817dc5",
        "DE0046",
        "6000",
        "10.16.18",
        "ADM"
    ],
    [
        "Harry",
        "Potter",
        "harry.potter@hogwarts.magic.uk",
        "6525b73e-ddd5-4893-8562-da88ea817dc5",
        "DE0047",
        "6500",
        "10.19.18",
        "SCH"
    ],
    [
        "Harry",
        "Potter",
        "harry.potter@hogwarts.magic.uk",
        "6525b73e-ddd5-4893-8562-da88ea817dc5",
        "DE0048",
        "7500",
        "11.11.18",
        "XFA"
    ],
    [
        "Hermione",
        "Granger",
        "hermione.granger@hogwarts.magic.uk",
        "51feecbe-79b6-4eea-a651-c00c4610543a",
        "DE0049",
        "10700",
        "10.16.18",
        "ADM"
    ],
    [
        "Hermione",
        "Granger",
        "hermione.granger@hogwarts.magic.uk",
        "51feecbe-79b6-4eea-a651-c00c4610543a",
        "DE0050",
        "7200",
        "10.19.18",
        "SCH"
    ],
    [
        "Hermione",
        "Granger",
        "hermione.granger@hogwarts.magic.uk",
        "51feecbe-79b6-4eea-a651-c00c4610543a",
        "DE0051",
        "10100",
        "11.11.18",
        "XFA"
    ],
    [
        "Ron",
        "Weasley",
        "ron.weasley@hogwarts.magic.uk",
        "b7b92e71-5f0c-4fa2-bcfb-b6d04719a865",
        "DE0052",
        "3800",
        "10.16.18",
        "ADM"
    ],
    [
        "Ron",
        "Weasley",
        "ron.weasley@hogwarts.magic.uk",
        "b7b92e71-5f0c-4fa2-bcfb-b6d04719a865",
        "DE0053",
        "12000",
        "10.19.18",
        "SCH"
    ],
    [
        "Ron",
        "Weasley",
        "ron.weasley@hogwarts.magic.uk",
        "b7b92e71-5f0c-4fa2-bcfb-b6d04719a865",
        "DE0054",
        "6500",
        "11.11.18",
        "XFA"
    ],
    [
        "The New",
        "School",
        "thenewschool@decenture.com",
        "ff8f2fa2-7787-4319-9c75-82b5c09f8f41",
        "DE0055",
        "800",
        "05.01.19",
        "END"
    ],
    [
        "The New",
        "School",
        "thenewschool@decenture.com",
        "ff8f2fa2-7787-4319-9c75-82b5c09f8f41",
        "DE0056",
        "1300",
        "05.01.19",
        "RMB"
    ],
    [
        "The New",
        "School",
        "thenewschool@decenture.com",
        "ff8f2fa2-7787-4319-9c75-82b5c09f8f41",
        "DE0057",
        "2100",
        "05.01.19",
        "TUT"
    ],
    [
        'Test',
        'First',
        'test1@decenture.com',
        '3926c2e7-ba17-484f-a227-406e158defd2',
        'DE0058',
        "1200",
        '05.01.19',
        'XCP'
    ],
    [
        'Test',
        'Second',
        'test2@decenture.com',
        '825175a1-75f8-4e01-b8d5-6aa3bf284bc5',
        'DE0059',
        "1300",
        '05.01.19',
        'XPY'
    ],
    [
        'Test',
        'Third',
        'test3@decenture.com',
        'd8c87c88-398c-4a2f-8e47-3e880530e2c3',
        'DE0060',
        "1300",
        '05.01.19',
        'XWA'
    ],
    [
        'Test',
        'Fourth',
        'test4@decenture.com',
        '2843f382-0fbe-41a9-b586-9b3416069df5',
        'DE0061',
        "900",
        '05.01.19',
        'RMB'
    ],
    [
        'Test',
        'Fifth',
        'test5@decenture.com',
        '3853e32c-3706-4c99-a4fc-ab5b40f96243',
        'DE0062',
        "1300",
        '05.01.19',
        'SPN'
    ],
    [
        'Test',
        'Sixth',
        'test6@decenture.com',
        '3f2a7273-7ad0-46f8-b251-079ea771e9ec',
        'DE0063',
        "600",
        '05.01.19',
        'UND'
    ],
    [
        'Test',
        'Seventh',
        'test7@decenture.com',
        '603aa304-2cb4-4ef9-a8bd-276a60f6f4b8',
        'DE0064',
        "600",
        '05.01.19',
        'TUT'
    ],
    [
        'Test',
        'Eighth',
        'test8@decenture.com',
        'e22004ad-24bc-4313-b6f9-4d9da1277b98',
        'DE0065',
        "1200",
        '05.01.19',
        'END'
    ],
    [
        'Test',
        'Ninth',
        'test9@decenture.com',
        '54b8bbc2-d741-4897-8a46-6688d5cf9410',
        'DE0066',
        "2100",
        '05.01.19',
        'XWV'
    ],
    [
        'Test',
        'Tenth',
        'test10@decenture.com',
        '2157560a-a9d8-4f14-835b-fae66726e7d5',
        'DE0067',
        "800",
        '05.01.19',
        'XFA'
    ],
    [
        'Test',
        'First',
        'test1@decenture.com',
        '3926c2e7-ba17-484f-a227-406e158defd2',
        'DE0068',
        "1200",
        '05.01.19',
        'XWV'
    ],
    [
        'Test',
        'Second',
        'test2@decenture.com',
        '825175a1-75f8-4e01-b8d5-6aa3bf284bc5',
        'DE0069',
        "2100",
        '05.01.19',
        'RMB'
    ],
    [
        'Test',
        'Third',
        'test3@decenture.com',
        'd8c87c88-398c-4a2f-8e47-3e880530e2c3',
        'DE0070',
        "1300",
        '05.01.19',
        'MCF'
    ],
    [
        'Test',
        'Fourth',
        'test4@decenture.com',
        '2843f382-0fbe-41a9-b586-9b3416069df5',
        'DE0071',
        "600",
        '05.01.19',
        'PRL'
    ],
    [
        'Test',
        'Fifth',
        'test5@decenture.com',
        '3853e32c-3706-4c99-a4fc-ab5b40f96243',
        'DE0072',
        "800",
        '05.01.19',
        'OTH'
    ],
    [
        'Test',
        'Sixth',
        'test6@decenture.com',
        '3f2a7273-7ad0-46f8-b251-079ea771e9ec',
        'DE0073',
        "900",
        '05.01.19',
        'RMB'
    ],
    [
        'Test',
        'Seventh',
        'test7@decenture.com',
        '603aa304-2cb4-4ef9-a8bd-276a60f6f4b8',
        'DE0074',
        "1300",
        '05.01.19',
        'XCP'
    ],
    [
        'Test',
        'Eighth',
        'test8@decenture.com',
        'e22004ad-24bc-4313-b6f9-4d9da1277b98',
        'DE0075',
        "900",
        '05.01.19',
        'OTH'
    ],
    [
        'Test',
        'Ninth',
        'test9@decenture.com',
        '54b8bbc2-d741-4897-8a46-6688d5cf9410',
        'DE0076',
        "900",
        '05.01.19',
        'XWA'
    ],
    [
        'Test',
        'Tenth',
        'test10@decenture.com',
        '2157560a-a9d8-4f14-835b-fae66726e7d5',
        'DE0077',
        "1200",
        '05.01.19',
        'END'
    ],
    [
        'Test',
        'First',
        'test1@decenture.com',
        '3926c2e7-ba17-484f-a227-406e158defd2',
        'DE0078',
        "900",
        '05.01.19',
        'XWA'
    ],
    [
        'Test',
        'Second',
        'test2@decenture.com',
        '825175a1-75f8-4e01-b8d5-6aa3bf284bc5',
        'DE0079',
        "1300",
        '05.01.19',
        'SPN'
    ],
    [
        'Test',
        'Third',
        'test3@decenture.com',
        'd8c87c88-398c-4a2f-8e47-3e880530e2c3',
        'DE0080',
        "2100",
        '05.01.19',
        'UND'
    ],
    [
        'Test',
        'Fourth',
        'test4@decenture.com',
        '2843f382-0fbe-41a9-b586-9b3416069df5',
        'DE0081',
        "600",
        '05.01.19',
        'END'
    ],
    [
        'Test',
        'Fifth',
        'test5@decenture.com',
        '3853e32c-3706-4c99-a4fc-ab5b40f96243',
        'DE0082',
        "800",
        '05.01.19',
        'MCF'
    ],
    [
        'Test',
        'Sixth',
        'test6@decenture.com',
        '3f2a7273-7ad0-46f8-b251-079ea771e9ec',
        'DE0083',
        "2100",
        '05.01.19',
        'CNV'
    ],
    [
        'Test',
        'Seventh',
        'test7@decenture.com',
        '603aa304-2cb4-4ef9-a8bd-276a60f6f4b8',
        'DE0084',
        "800",
        '05.01.19',
        'OTH'
    ],
    [
        'Test',
        'Eighth',
        'test8@decenture.com',
        'e22004ad-24bc-4313-b6f9-4d9da1277b98',
        'DE0085',
        "2100",
        '05.01.19',
        'XWV'
    ],
    [
        'Test',
        'Ninth',
        'test9@decenture.com',
        '54b8bbc2-d741-4897-8a46-6688d5cf9410',
        'DE0086',
        "2100",
        '05.01.19',
        'PRL'
    ],
    [
        'Test',
        'Tenth',
        'test10@decenture.com',
        '2157560a-a9d8-4f14-835b-fae66726e7d5',
        'DE0087',
        "900",
        '05.01.19',
        'PRL'
    ],
    [
        'Test',
        'First',
        'test1@decenture.com',
        '3926c2e7-ba17-484f-a227-406e158defd2',
        'DE0088',
        900,
        '05.01.19',
        'XCP'
    ],
    [
        'Test',
        'Second',
        'test2@decenture.com',
        '825175a1-75f8-4e01-b8d5-6aa3bf284bc5',
        'DE0089',
        1200,
        '05.01.19',
        'XWV'],
    [
        'Test',
        'Third',
        'test3@decenture.com',
        'd8c87c88-398c-4a2f-8e47-3e880530e2c3',
        'DE0090',
        900,
        '05.01.19',
        'SCH'
    ],
    [
        'Test',
        'Fourth',
        'test4@decenture.com',
        '2843f382-0fbe-41a9-b586-9b3416069df5',
        'DE0091',
        900,
        '05.01.19',
        'TUT'
    ],
    [
        'Test',
        'Fifth',
        'test5@decenture.com',
        '3853e32c-3706-4c99-a4fc-ab5b40f96243',
        'DE0092',
        900,
        '05.01.19',
        'PRL'
    ],
    [
        'Test',
        'Sixth',
        'test6@decenture.com',
        '3f2a7273-7ad0-46f8-b251-079ea771e9ec',
        'DE0093',
        800,
        '05.01.19',
        'XWV'
    ],
    [
        'Test',
        'Seventh',
        'test7@decenture.com',
        '603aa304-2cb4-4ef9-a8bd-276a60f6f4b8',
        'DE0094',
        800,
        '05.01.19',
        'PRL'
    ],
    [
        'Test',
        'Eighth',
        'test8@decenture.com',
        'e22004ad-24bc-4313-b6f9-4d9da1277b98',
        'DE0095',
        2100,
        '05.01.19',
        'UND'
    ],
    [
        'Test',
        'Ninth',
        'test9@decenture.com',
        '54b8bbc2-d741-4897-8a46-6688d5cf9410',
        'DE0096',
        900,
        '05.01.19',
        'OTH'],
    [
        'Test',
        'Tenth',
        'test10@decenture.com',
        '2157560a-a9d8-4f14-835b-fae66726e7d5',
        'DE0097',
        800,
        '05.01.19',
        'XPY'
    ],
    [
        'Test',
        'First',
        'test1@decenture.com',
        '3926c2e7-ba17-484f-a227-406e158defd2',
        'DE0098',
        800,
        '05.01.19',
        'END'
    ],
    [
        'Test',
        'Second',
        'test2@decenture.com',
        '825175a1-75f8-4e01-b8d5-6aa3bf284bc5',
        'DE0099',
        1200,
        '05.01.19',
        'XWV'
    ],
    [
        'Test',
        'Third',
        'test3@decenture.com',
        'd8c87c88-398c-4a2f-8e47-3e880530e2c3',
        'DE00100',
        1300,
        '05.01.19',
        'PP'
    ],
    [
        'Test',
        'Fourth',
        'test4@decenture.com',
        '2843f382-0fbe-41a9-b586-9b3416069df5',
        'DE00101',
        1300,
        '05.01.19',
        'RMB'
    ],
    [
        'Test',
        'Fifth',
        'test5@decenture.com',
        '3853e32c-3706-4c99-a4fc-ab5b40f96243',
        'DE00102',
        2100,
        '05.01.19',
        'CNV'
    ],
    [
        'Test',
        'Sixth',
        'test6@decenture.com',
        '3f2a7273-7ad0-46f8-b251-079ea771e9ec',
        'DE00103',
        600,
        '05.01.19',
        'END'
    ],
    [
        'Test',
        'Seventh',
        'test7@decenture.com',
        '603aa304-2cb4-4ef9-a8bd-276a60f6f4b8',
        'DE00104',
        1200,
        '05.01.19',
        'UND'
    ],
    [
        'Test',
        'Eighth',
        'test8@decenture.com',
        'e22004ad-24bc-4313-b6f9-4d9da1277b98',
        'DE00105',
        600,
        '05.01.19',
        'PRL'
    ],
    [
        'Test',
        'Ninth',
        'test9@decenture.com',
        '54b8bbc2-d741-4897-8a46-6688d5cf9410',
        'DE00106',
        900,
        '05.01.19',
        'XWV'
    ],
    [
        'Test',
        'Tenth',
        'test10@decenture.com',
        '2157560a-a9d8-4f14-835b-fae66726e7d5',
        'DE00107',
        2100,
        '05.01.19',
        'XCP'
    ]
];

class Invoice {

    constructor(name, surname, email, student_id, invoice_id, amount, date, account_type) {
        this.id = invoice_id;
        this.university = `resource:org.acme.mynetwork.University#9704c732-972a-4543-a9c2-3d88f06ff2f4`;
        this.invoice_id = invoice_id;
        this.invoice_number = Invoice.count++;
        this.invoice_date = new Date();
        this.due_date = new Date(date);
        this.billed_from = `resource:org.acme.mynetwork.Account#9704c732-972a-4543-a9c2-3d88f06ff2f4`;
        this.billed_to = `resource:org.acme.mynetwork.Account#${student_id}`;
        this.amount = amount;
        this.status = 'NOT PAID';
        this.first_name = name;
        this.last_name = surname;
        this.email = email;
        this.business_unit = '';
        this.cur_cd = 'GBP';
        this.description = TYPE[account_type];
        this.message = TYPE[account_type];
        this.account_type_sf = account_type;
        this.item_type_cd = '';
        this.entry_date = new Date();
    }
}

Invoice.count = 0;

const http = require('http');
const options = {
    host: 'localhost',
    port: 3000,
    path: `/api/Invoice`,
    method: 'POST',
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    }
};


function insert(invoice) {

    const promise = new Promise((resolve) => {
        const request = http.request(options, function (res) {
            res.on('data', function (data) {
                resolve(data);
            });
        });
        const fullInvoice = new Invoice(...invoice);
        request.write(JSON.stringify(fullInvoice));
        request.end();
        console.log('new invoice ', fullInvoice);
    });
    return promise;
}

asyncForEach(STATIC_MOCK, insert)
    .then(() => {
        console.log('completed')
    });
