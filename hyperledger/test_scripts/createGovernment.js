const MOK_USERS = [
    {
        id: '9276d351-648d-4b5e-a129-94c105e6b8d6',
        country_code: 'UK',
        name: 'British Ministry of Magic'
    }
];

const http = require('http');
const options = {
    host: 'localhost',
    port: 3000,
    path: `/api/Government`,
    method: 'GET',
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    }
};

function checkDataBase(options) {
    options = Object.assign({}, options);
    const promise = new Promise(resolve => {
        const request = http.request(options, function (res) {
            res.on('data', function (data) {
                resolve(data);
            });
        });
        request.write('');
        request.end();
    });
    return promise;
}

function insert(options, user) {
    options = Object.assign({}, options);
    options.method = "POST";
    const promise = new Promise((resolve) => {
        const request = http.request(options, function (res) {
            res.on('data', function (data) {
                resolve(data);
            });
        });
        request.write(JSON.stringify(user));
        request.end();
    });
    return promise;
}

checkDataBase(options).then(
    value => {
        value = JSON.parse(decodeURIComponent(value));
        console.log(value);
        MOK_USERS.forEach(user => {
            if (!value.find(item => item.id === user.id)) {
                insert(options, user).then(result => {
                    console.log(`Government created successfully ${decodeURIComponent(result)}`)
                });
            }
        })
    }
);
