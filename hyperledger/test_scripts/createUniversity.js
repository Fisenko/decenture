const MOK_USERS = [
    {
        id: '9704c732-972a-4543-a9c2-3d88f06ff2f4',
        country_code: 'UK',
        code: 'HOG',
        name: 'Hogwarts',
        official_name: 'Hogwarts School of Witchcraft and Wizardry',
        abbreviation: 'HSoW&W',
        address: 'Highlands of Scotland alternative reality',
        domain_name: 'hogwarts.magic.uk',
        erp_info: 'unknown'
    },
    {
        id: '771a5992-63d6-4b80-a145-ab80ea1bc18e',
        country_code: 'US',
        code: 'ILV',
        name: 'Ilvermorny',
        official_name: 'Ilvermorny School of Witchcraft and Wizardry',
        abbreviation: 'ISoW&W',
        address: 'Mount Greylock alternative reality',
        domain_name: 'ilvermorny.magic.us',
        erp_info: 'unknown'
    }
];

const http = require('http');
const options = {
    host: 'localhost',
    port: 3000,
    path: `/api/University`,
    method: 'GET',
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    }
};

function checkDataBase(options) {
    options = Object.assign({}, options);
    const promise = new Promise(resolve => {
        const request = http.request(options, function (res) {
            res.on('data', function (data) {
                resolve(data);
            });
        });
        request.write('');
        request.end();
    });
    return promise;
}

function insert(options, user) {
    options = Object.assign({}, options);
    options.method = "POST";
    const promise = new Promise((resolve) => {
        const request = http.request(options, function (res) {
            res.on('data', function (data) {
                resolve(data);
            });
        });
        request.write(JSON.stringify(user));
        request.end();
    });
    return promise;
}

checkDataBase(options).then(
    value => {
        value = JSON.parse(decodeURIComponent(value));
        console.log(value);
        MOK_USERS.forEach(university => {
            if (!value.find(item => item.id === university.id)) {

                    insert(options, university).then(result => {
                        console.log(`University created successfully ${decodeURIComponent(result)}`)
                    });

            }
        })
    }
);
