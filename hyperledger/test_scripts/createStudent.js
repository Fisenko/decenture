const MOK_USERS = [
    {
        id: '7251728e-cd03-434f-984a-8f085a41c596',
        university_id: '9704c732-972a-4543-a9c2-3d88f06ff2f4',
        erp_email: 'edsouza@decenture.com',
        erp_student_id: 'STID0004',
        first_name: 'Euclid',
        last_name: 'D Souza',
        degree_name: 'bachelor'
    },
    {
        id: '8c3def2a-27d8-4afa-9c93-518642bb473a',
        university_id: '9704c732-972a-4543-a9c2-3d88f06ff2f4',
        erp_email: 'rageeb@decenture.com',
        erp_student_id: 'STID0001',
        first_name: 'Rageeb',
        last_name: 'Mahtab',
        degree_name: 'master'
    },
    {
        id: 'a130a991-7133-4a5d-9cda-208bad11709a',
        university_id: '9704c732-972a-4543-a9c2-3d88f06ff2f4',
        erp_email: 'walid@decenture.com',
        erp_student_id: 'STID0006',
        first_name: 'Walid',
        last_name: 'Hadid',
        degree_name: 'master'
    },
    {
        id: '6ecaa91e-9114-4e25-9307-59e60f7a330e',
        university_id: '9704c732-972a-4543-a9c2-3d88f06ff2f4',
        erp_email: 'lhadid@decenture.com',
        erp_student_id: 'STID0005',
        first_name: 'Loubna',
        last_name: 'Hadid',
        degree_name: 'bachelor'
    },
    {
        id: '0091f0d9-090d-4498-94ae-9bc4336a5830',
        university_id: '9704c732-972a-4543-a9c2-3d88f06ff2f4',
        erp_email: 'gbenton@decenture.com',
        erp_student_id: 'STID0002',
        first_name: 'George',
        last_name: 'Benton',
        degree_name: 'bachelor'
    },
    {
        id: 'eeafd6c7-c47d-44cf-85e1-4705b85883f8',
        university_id: '9704c732-972a-4543-a9c2-3d88f06ff2f4',
        erp_email: 'eyoung@decenture.com',
        erp_student_id: 'STID0003',
        first_name: 'Emmanuel',
        last_name: 'Young',
        degree_name: 'bachelor'
    },
    {
        id: 'b7b92e71-5f0c-4fa2-bcfb-b6d04719a865',
        university_id: '9704c732-972a-4543-a9c2-3d88f06ff2f4',
        erp_email: 'ron.weasley@hogwarts.magic.uk',
        erp_student_id: 'STID0009',
        first_name: 'Ron',
        last_name: 'Weasley',
        degree_name: 'bachelor'
    },
    {
        id: '6525b73e-ddd5-4893-8562-da88ea817dc5',
        university_id: '9704c732-972a-4543-a9c2-3d88f06ff2f4',
        erp_email: 'harry.potter@hogwarts.magic.uk',
        erp_student_id: 'STID0007',
        first_name: 'Harry',
        last_name: 'Potter',
        degree_name: 'master'
    },
    {
        id: '51feecbe-79b6-4eea-a651-c00c4610543a',
        university_id: '9704c732-972a-4543-a9c2-3d88f06ff2f4',
        erp_email: 'hermione.granger@hogwarts.magic.uk',
        erp_student_id: 'STID0008',
        first_name: 'Hermione',
        last_name: 'Granger',
        degree_name: 'master'
    },
    {
        id: 'ff8f2fa2-7787-4319-9c75-82b5c09f8f41',
        university_id: '9704c732-972a-4543-a9c2-3d88f06ff2f4',
        erp_email: 'thenewschool@decenture.com',
        erp_student_id: 'STID0010',
        first_name: 'The New',
        last_name: 'School',
        degree_name: 'bachelor'
    },
    {
        id: '3926c2e7-ba17-484f-a227-406e158defd2',
        university_id: '9704c732-972a-4543-a9c2-3d88f06ff2f4',
        erp_email: 'test1@decenture.com',
        erp_student_id: 'STID0011',
        first_name: 'Test',
        last_name: 'First',
        degree_name: 'bachelor'
    },
    {
        id: '825175a1-75f8-4e01-b8d5-6aa3bf284bc5',
        university_id: '9704c732-972a-4543-a9c2-3d88f06ff2f4',
        erp_email: 'test2@decenture.com',
        erp_student_id: 'STID0012',
        first_name: 'Test',
        last_name: 'Second',
        degree_name: 'bachelor'
    },
    {
        id: 'd8c87c88-398c-4a2f-8e47-3e880530e2c3',
        university_id: '9704c732-972a-4543-a9c2-3d88f06ff2f4',
        erp_email: 'test3@decenture.com',
        erp_student_id: 'STID0013',
        first_name: 'Test',
        last_name: 'Third',
        degree_name: 'bachelor'
    },
    {
        id: '2843f382-0fbe-41a9-b586-9b3416069df5',
        university_id: '9704c732-972a-4543-a9c2-3d88f06ff2f4',
        erp_email: 'test4@decenture.com',
        erp_student_id: 'STID0014',
        first_name: 'Test',
        last_name: 'Fourth',
        degree_name: 'bachelor'
    },
    {
        id: '3853e32c-3706-4c99-a4fc-ab5b40f96243',
        university_id: '9704c732-972a-4543-a9c2-3d88f06ff2f4',
        erp_email: 'test5@decenture.com',
        erp_student_id: 'STID0015',
        first_name: 'Test',
        last_name: 'Fifth',
        degree_name: 'bachelor'
    },
    {
        id: '3f2a7273-7ad0-46f8-b251-079ea771e9ec',
        university_id: '9704c732-972a-4543-a9c2-3d88f06ff2f4',
        erp_email: 'test6@decenture.com',
        erp_student_id: 'STID0016',
        first_name: 'Test',
        last_name: 'Sixth',
        degree_name: 'bachelor'
    },
    {
        id: '603aa304-2cb4-4ef9-a8bd-276a60f6f4b8',
        university_id: '9704c732-972a-4543-a9c2-3d88f06ff2f4',
        erp_email: 'test7@decenture.com',
        erp_student_id: 'STID0017',
        first_name: 'Test',
        last_name: 'Seventh',
        degree_name: 'master'
    },
    {
        id: 'e22004ad-24bc-4313-b6f9-4d9da1277b98',
        university_id: '9704c732-972a-4543-a9c2-3d88f06ff2f4',
        erp_email: 'test8@decenture.com',
        erp_student_id: 'STID0018',
        first_name: 'Test',
        last_name: 'Eighth',
        degree_name: 'master'
    },
    {
        id: '54b8bbc2-d741-4897-8a46-6688d5cf9410',
        university_id: '9704c732-972a-4543-a9c2-3d88f06ff2f4',
        erp_email: 'test9@decenture.com',
        erp_student_id: 'STID0019',
        first_name: 'Test',
        last_name: 'Ninth',
        degree_name: 'master'
    },
    {
        id: '2157560a-a9d8-4f14-835b-fae66726e7d5',
        university_id: '9704c732-972a-4543-a9c2-3d88f06ff2f4',
        erp_email: 'test10@decenture.com',
        erp_student_id: 'STID0020',
        first_name: 'Test',
        last_name: 'Tenth',
        degree_name: 'master'
    }
];

const http = require('http');
const options = {
    host: 'localhost',
    port: 3000,
    path: `/api/Student`,
    method: 'GET',
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    }
};

function checkDataBase(options) {
    options = Object.assign({}, options);
    const promise = new Promise(resolve => {
        const request = http.request(options, function (res) {
            res.on('data', function (data) {
                resolve(data);
            });
        });
        request.write('');
        request.end();
    });
    return promise;
}

function insert(options, user) {
    options = Object.assign({}, options);
    options.method = "POST";
    const promise = new Promise((resolve) => {
        const request = http.request(options, function (res) {
            res.on('data', function (data) {
                resolve(data);
            });
        });
        request.write(JSON.stringify(user));
        request.end();
    });
    return promise;
}

checkDataBase(options).then(
    value => {
        value = JSON.parse(decodeURIComponent(value));
        console.log(value);
        MOK_USERS.forEach(user => {
            if (!value.find(item => item.id === user.id)) {
                insert(options, user).then(result => {
                    console.log(`Student created successfully ${decodeURIComponent(result)}`)
                });
            }
        })
    }
);
