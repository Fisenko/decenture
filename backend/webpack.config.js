const path = require('path');
const DIR = path.resolve(__dirname);
const fs = require('fs');
const nodeModules = {};

fs.readdirSync('node_modules')
    .filter(function(x) {
        return ['.bin'].indexOf(x) === -1;
    })
    .forEach(function(mod) {
        nodeModules[mod] = 'commonjs ' + mod;
    });

module.exports = {
    mode: 'development',
    entry: path.join(DIR, 'src', 'server.ts'),
    context: path.resolve(DIR, "src"),
    target: "node",
    externals: nodeModules,
    module: {
        rules: [
            {
                test: /\.ts$/,
                loader: 'ts-loader',
                include: [path.resolve(DIR, "src"), path.resolve(DIR, 'server.ts')]
            }
        ]
    },
    plugins: [],
    resolve: {
        extensions: ['.ts', '.js', '.json'],
        alias: {
            src: path.resolve(__dirname, 'src'),
            'pg-native': path.join(__dirname, 'alias/pg-native.js'),
            'pgpass$': path.join(__dirname, 'alias/pgpass.js')
        }
    },
    output: {
        filename: "server.js",
        path: path.join(DIR, 'dist')
    }
};
