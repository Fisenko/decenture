import { selectString } from 'src/db/mapping';

export const propertyNames = ['id', 'email', 'password', 'token', 'status_id', 'role_id', 'entry_date', 'close_date'];

export const tableName = 'app_user';

export const allProps = selectString(propertyNames);

export const CREATE_USER = 'INSERT INTO app_user(email,password,status_id,role_id)' +
    ' VALUES(${email},${password},${statusId},${roleId}) RETURNING id, token';

export const GET_ALL_USERS = 'SELECT ' + allProps + ' FROM app_user';
export const GET_USER_BY_ID = 'SELECT ' + allProps + ' FROM app_user WHERE id=${id}';
export const GET_USER_BY_TOKEN = 'SELECT ' + allProps + ' FROM app_user WHERE token=${token}';
export const GET_USER_BY_TOKEN_AND_STATUS = 'SELECT ' + allProps + ' FROM app_user WHERE token=${token} AND status_id = ${statusId}';
export const GET_USER_BY_EMAIL = 'SELECT ' + allProps + ' FROM app_user WHERE email=${email}';
export const GET_USER_BY_EMAIL_AND_PASSWORD = 'SELECT ' + allProps + ' FROM app_user WHERE email=${email} AND password=${password}';

export const UPDATE_USER_STATUS = 'UPDATE app_user SET status_id = ${statusId} WHERE id=${id}';
