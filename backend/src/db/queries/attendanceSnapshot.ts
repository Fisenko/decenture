import * as utl from 'src/db/mapping';

export const tableName = 'attendance_snapshot';

export const propertyNames = [
    'student_id',
    'lecture_course_id',
    'visited',
    'absence',
    'excused',
    'completed',
    'course_work',
    'exam',
    'status_id',
    'last_update',
];

export const rawProps = utl.rawProps(propertyNames);

export const propsToParams = utl.propsToParams(propertyNames);

export const allProps = utl.selectString(propertyNames);

export const CREATE = 'INSERT INTO ' + tableName + '(' + propertyNames.join(',') + ')' +
    ' VALUES(' + propsToParams + ') RETURNING *';

export const UPDATE =
    'INSERT INTO attendance_snapshot AS t (' + rawProps + ')' + ' VALUES (' + propsToParams + ')' +
    ' ON CONFLICT (student_id, begin_date)' +
    ' DO UPDATE SET' +
    ' visited = t.visited + EXCLUDED.visited,' +
    ' absence = t.absence + EXCLUDED.absence,' +
    ' excused = t.excused + EXCLUDED.excused,' +
    ' completed = t.completed + EXCLUDED.completed,' +
    ' scheduled = t.scheduled + EXCLUDED.scheduled,' +
    ' last_update = EXCLUDED.last_update' +
    ' RETURNING *';

export const UPDATE_ON_NFC_SCAN =
    'INSERT INTO attendance_snapshot AS t (student_id, begin_date, end_date, visited)' +
    ' VALUES (${studentId},' +
    '         date_trunc(\'month\', to_timestamp(${snapshotDate})),' +
    '         date_trunc(\'month\', to_timestamp(${snapshotDate}) + interval \'1 month -1 day\'),' +
    '         ${visited})' +
    ' ON CONFLICT (student_id, begin_date)' +
    ' DO UPDATE SET' +
    ' visited = t.visited + EXCLUDED.visited,' +
    ' last_update = now()' +
    ' RETURNING *';

export const UPDATE_COMPLETED =
    'WITH' +
    '  update_scheduled_lecture AS (' +
    '    UPDATE scheduled_lecture' +
    '    SET status_id = 1' +
    '    WHERE scan_end_date between to_timestamp(${beginDate}) AND to_timestamp(${endDate})' +
    '      AND status_id = 0' +
    '    RETURNING' +
    '      id,' +
    '      nfc_id,' +
    '      lecture_course_id,' +
    '      date_trunc(\'month\', begin_date) :: date begin_date,' +
    '      (date_trunc(\'month\', begin_date) + interval \'1 month - 1 day\') :: date end_date,' +
    '      status_id' +
    '  ),' +
    '  update_scheduled_attendance AS (' +
    '    UPDATE scheduled_attendance AS sa2' +
    '    SET status_id = 9' +
    '    WHERE status_id = 0' +
    '      AND sa2.scheduled_lecture_id IN (SELECT usl2.id from update_scheduled_lecture usl2)' +
    '    RETURNING' +
    '      student_id,' +
    '      scheduled_lecture_id,' +
    '      status_id' +
    '  ),' +
    '  for_snapshot AS (' +
    '    SELECT' +
    '      sa.student_id,' +
    '      usl.begin_date,' +
    '      usl.end_date,' +
    '      sum(CASE WHEN usa.status_id = 9 THEN 1 ELSE 0 END) absence,' +
    '      count(1) completed' +
    '    FROM update_scheduled_lecture usl' +
    '      INNER JOIN scheduled_attendance sa ON sa.scheduled_lecture_id = usl.id' +
    '      INNER JOIN student s ON sa.student_id = s.id' +
    '      LEFT OUTER JOIN update_scheduled_attendance usa' +
    '        ON (usa.scheduled_lecture_id = sa.scheduled_lecture_id AND usa.student_id = sa.student_id)' +
    '      GROUP BY' +
    '        sa.student_id,' +
    '        usl.begin_date,' +
    '        usl.end_date' +
    '  ),' +
    '  update_attendance_snapshot AS (' +
    '    INSERT INTO attendance_snapshot AS t (student_id, begin_date, end_date, absence, completed, last_update)' +
    '    SELECT student_id,' +
    '           begin_date,' +
    '           end_date,' +
    '           absence,' +
    '           completed,' +
    '           now()' +
    '    FROM for_snapshot' +
    '    ON CONFLICT (student_id, begin_date)' +
    '    DO UPDATE SET' +
    '      absence = t.absence + EXCLUDED.absence,' +
    '      completed = t.completed + EXCLUDED.completed,' +
    '      last_update = EXCLUDED.last_update' +
    '    RETURNING *' +
    '  ),' +
    '  update_statistic AS (' +
    '    SELECT \'scheduled_lecture\' table_name, count(1) cnt FROM update_scheduled_lecture' +
    '    UNION ALL' +
    '    SELECT \'scheduled_attendance\', count(1) FROM update_scheduled_attendance' +
    '    UNION ALL' +
    '    SELECT \'attendance_snapshot\', count(1) FROM update_attendance_snapshot' +
    '  )' +
    ' SELECT * FROM update_statistic';


export const UPDATE_MONTH =
    'with' +
    '   attend as (' +
    '     select' +
    '       a.student_id,' +
    '       date_trunc(\'month\', l.begin_date) :: date begin_date,' +
    '       (date_trunc(\'month\', l.begin_date) + interval \'1 month - 1 day\') :: date end_date,' +
    '       case when a.status_id = 1 then 1 else 0 end visited,' +
    '       case when a.status_id = 9 then 1 else 0 end absence,' +
    '       case when a.status_id = 2 then 1 else 0 end excused,' +
    '       case when l.status_id = 1 then 1 else 0 end completed,' +
    '       now()' +
    '     from' +
    '       scheduled_lecture l' +
    '       inner join scheduled_attendance a on l.id = a.scheduled_lecture_id' +
    '     where l.begin_date > date_trunc(\'month\', to_timestamp(${snapshotDate}))' +
    '       and l.begin_date < date_trunc(\'month\', to_timestamp(${snapshotDate}) + interval \'1 month\')' +
    '   ),' +
    '   update_snapshot as (' +
    '     insert into attendance_snapshot (student_id, begin_date, end_date, visited, absence, excused, completed, scheduled)' +
    '     select student_id, begin_date, end_date, sum(visited), sum(absence), sum(excused), sum(completed), count(*)' +
    '     from attend' +
    '     group by student_id, begin_date, end_date' +
    '     on conflict (student_id, begin_date)' +
    '       do update set' +
    '         visited     = excluded.visited,' +
    '         absence     = excluded.absence,' +
    '         excused     = excluded.excused,' +
    '         completed   = excluded.completed,' +
    '         scheduled   = excluded.scheduled,' +
    '         last_update = now()' +
    '     returning *' +
    '   )' +
    ' select count(*) cnt from update_snapshot';
