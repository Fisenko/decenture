export const GET_STUDENTS_ANALYTICS_BY_SEMESTER =
    ' with' +
    '   current_semester as (' +
    '     select st.*,' +
    '            sem.id semester_id,' +
    '            sem.begin_date,' +
    '            sem.end_date' +
    '     from student st' +
    '            inner join semester sem on st.university_id = sem.university_id' +
    '     where st.id = ${studentId}' +
    '       and to_timestamp(${reportDate}) between sem.begin_date and sem.end_date' +
    '     ),' +
    '   current_compliance as (' +
    '     select sum(case when c2.status_id = 1 then 1 else 0 end) completed_courses,' +
    '            count(1) scheduled_courses' +
    '     from current_semester st' +
    '            inner join compliance c2 on st.id = c2.student_id and st.semester_id = c2.semester_id' +
    '     ),' +
    '   current_attendance as (' +
    '     select sum(case when a.status_id = 1 then 1 else 0 end) :: double precision visited,' +
    '            sum(case when a.status_id = 9 then 1 else 0 end) :: double precision absence,' +
    '            sum(case when a.status_id = 2 then 1 else 0 end) :: double precision excused,' +
    '            sum(case when l.status_id = 1 then 1 else 0 end) :: double precision completed,' +
    '            count(*) scheduled' +
    '     from current_semester cs' +
    '            inner join scheduled_lecture l on l.begin_date > cs.begin_date and l.end_date < cs.end_date' +
    '            inner join scheduled_attendance a on' +
    '     )' +
    ' select *,' +
    '        case when ca.scheduled > 0 then 100 *  ca.visited               / ca.scheduled else 0 end as total_progress,' +
    '        case when ca.scheduled > 0 then 100 *  ca.absence               / ca.scheduled else 0 end as total_absence_rate,' +
    '        case when ca.scheduled > 0 then 100 *  ca.excused               / ca.scheduled else 0 end as total_excused_rate,' +
    '        case when ca.scheduled > 0 then 100 * (ca.visited + ca.excused) / ca.scheduled else 0 end as total_progress_with_ex,' +
    '        case when ca.completed > 0 then 100 *  ca.visited               / ca.completed else 0 end as current_progress,' +
    '        case when ca.completed > 0 then 100 * (ca.visited + ca.excused) / ca.completed else 0 end as current_progress_with_ex,' +
    '        case when ca.completed > 0 then 100 *  ca.absence               / ca.completed else 0 end as current_absence_rate,' +
    '        case when ca.completed > 0 then 100 *  ca.excused               / ca.completed else 0 end as current_excused_rate,' +
    '        case when cc.scheduled_courses > 0 then cc.completed_courses / cc.scheduled_courses * 100 else 0 end as completion_rate' +
    ' from current_semester cs' +
    '        cross join current_compliance cc' +
    '        cross join current_attendance ca';


export const GET_STUDENTS_ANALYTICS_BY_SEMESTER_AND_LECTURE_COURSE =
    ' with' +
    '   current_attendance as (' +
    '     select l.lecture_course_id,' +
    '            lc.code lecture_course_code,' +
    '            lc.name lecture_course_name,' +
    '            sum(case when a.status_id = 1 then 1 else 0 end) visited,' +
    '            sum(case when a.status_id = 9 then 1 else 0 end) absence,' +
    '            sum(case when a.status_id = 2 then 1 else 0 end) excused,' +
    '            sum(case when l.status_id = 1 then 1 else 0 end) completed,' +
    '            count(*) scheduled' +
    '     from lecture_course lc' +
    '            inner join scheduled_lecture l ' +
    '              on l.lecture_course_id = lc.id ' +
    '              and l.begin_date > to_timestamp(${beginDate}) ' +
    '              and l.end_date < to_timestamp(${endDate}) + interval \'1 day\'' +
    '            inner join scheduled_attendance a on (a.scheduled_lecture_id = l.id and a.student_id = ${studentId})' +
    '     where lc.university_id = ${universityId}' +
    '     group by l.lecture_course_id,' +
    '              lc.code,' +
    '              lc.name' +
    '   )' +
    ' select ca.*,' +
    '        case when ca.scheduled > 0 then 100 *  ca.visited               / ca.scheduled :: double precision else 0 end as total_progress,' +
    '        case when ca.scheduled > 0 then 100 *  ca.absence               / ca.scheduled :: double precision else 0 end as total_absence_rate,' +
    '        case when ca.scheduled > 0 then 100 *  ca.excused               / ca.scheduled :: double precision else 0 end as total_excused_rate,' +
    '        case when ca.scheduled > 0 then 100 * (ca.visited + ca.excused) / ca.scheduled :: double precision else 0 end as total_progress_with_ex,' +
    '        case when ca.completed > 0 then 100 *  ca.visited               / ca.completed :: double precision else 0 end as current_progress,' +
    '        case when ca.completed > 0 then 100 * (ca.visited + ca.excused) / ca.completed :: double precision else 0 end as current_progress_with_ex,' +
    '        case when ca.completed > 0 then 100 *  ca.absence               / ca.completed :: double precision else 0 end as current_absence_rate,' +
    '        case when ca.completed > 0 then 100 *  ca.excused               / ca.completed :: double precision else 0 end as current_excused_rate' +
    ' from current_attendance ca';


export const GET_UNIVERSITY_ATTENDANCE =
    '  with un as (select *' +
    '              from university' +
    '              where (id = ${universityId} or ${universityId} is null)' +
    '                and (country_code = ${countryCode} or ${countryCode} is null)' +
    '      ),' +
    '       lc as (select l.id,' +
    '                     l.university_id,' +
    '                     case when ${groupByLectureCourseId} then l.id   else null end lecture_course_id,' +
    '                     case when ${groupByLectureCourseId} then l.code else null end lecture_course_code,' +
    '                     case when ${groupByLectureCourseId} then l.name else null end lecture_course_name' +
    '              from lecture_course l' +
    '                     inner join university u on l.university_id = u.id' +
    '              where l.id = ${lectureCourseId} or ${lectureCourseId} is null' +
    '      ),' +
    '       stud as (select s.id,' +
    '                       s.university_id,' +
    '                       case when ${groupByIsInternational} then s.is_international  else null end is_international,' +
    '                       case when ${groupByDegreeName}      then s.degree_name       else null end degree_name' +
    '                from un inner join student s on un.id = s.university_id' +
    '                where (s.is_international = ${isInternational} or ${isInternational} is null)' +
    '                  and (s.degree_name = any(${degreeName}) or ${degreeName} is null)' +
    '      ),' +
    '       attendance as (select lc.university_id,' +
    '                             lc.lecture_course_id,' +
    '                             lc.lecture_course_code,' +
    '                             lc.lecture_course_name,' +
    '                             st.is_international,' +
    '                             st.degree_name,' +
    '                             date_trunc(${dateTrunc}, sl.begin_date)           report_date,' +
    '                             sum(case when sa.status_id = 1 then 1 else 0 end) visited,' +
    '                             sum(case when sa.status_id = 2 then 1 else 0 end) excused,' +
    '                             sum(case when sa.status_id = 9 then 1 else 0 end) absence,' +
    '                             sum(case when sl.status_id = 1 then 1 else 0 end) completed,' +
    '                             count(*)                                          scheduled' +
    '                      from lc' +
    '                             inner join scheduled_lecture sl on (' +
    '                              lc.id = sl.lecture_course_id' +
    '                                and sl.begin_date > to_timestamp(${beginDate})' +
    '                                and sl.end_date < (to_timestamp(${endDate}) + interval \'1 day\')' +
    '                              )' +
    '                             inner join scheduled_attendance sa on sl.id = sa.scheduled_lecture_id' +
    '                             inner join stud st on sa.student_id = st.id' +
    '                      group by lc.university_id,' +
    '                               lc.lecture_course_id,' +
    '                               lc.lecture_course_code,' +
    '                               lc.lecture_course_name,' +
    '                               st.is_international,' +
    '                               st.degree_name,' +
    '                               date_trunc(${dateTrunc}, sl.begin_date)' +
    '      )' +
    '  select a.report_date,' +
    '         a.university_id,' +
    '         un.name,' +
    '         un.official_name,' +
    '         a.lecture_course_id,' +
    '         a.lecture_course_code,' +
    '         a.lecture_course_name,' +
    '         a.is_international,' +
    '         a.degree_name,' +
    '         a.visited,' +
    '         a.excused,' +
    '         a.absence,' +
    '         a.completed,' +
    '         a.scheduled,' +
    '         case when a.scheduled > 0 then 100 *  a.visited              / a.scheduled :: double precision else 0 end as total_progress,' +
    '         case when a.scheduled > 0 then 100 *  a.absence              / a.scheduled :: double precision else 0 end as total_absence_rate,' +
    '         case when a.scheduled > 0 then 100 *  a.excused              / a.scheduled :: double precision else 0 end as total_excused_rate,' +
    '         case when a.scheduled > 0 then 100 * (a.visited + a.excused) / a.scheduled :: double precision else 0 end as total_progress_with_ex,' +
    '         case when a.completed > 0 then 100 *  a.visited              / a.completed :: double precision else 0 end as current_progress,' +
    '         case when a.completed > 0 then 100 * (a.visited + a.excused) / a.completed :: double precision else 0 end as current_progress_with_ex,' +
    '         case when a.completed > 0 then 100 *  a.absence              / a.completed :: double precision else 0 end as current_absence_rate,' +
    '         case when a.completed > 0 then 100 *  a.excused              / a.completed :: double precision else 0 end as current_excused_rate' +
    '  from attendance a' +
    '         inner join un on a.university_id = un.id';


export const GET_UNIVERSITY_COMPLIANCE =
    ' with un as (select stud.university_id,' +
    '                    u.country_code,' +
    '                    u.code university_code,' +
    '                    u.name university_name,' +
    '                    u.official_name,' +
    '                    case when ${groupByLectureCourseId} then l.id   else null end lecture_course_id,' +
    '                    case when ${groupByLectureCourseId} then l.code else null end lecture_course_code,' +
    '                    case when ${groupByLectureCourseId} then l.name else null end lecture_course_name,' +
    '                    case when ${groupByIsInternational} then stud.is_international  else null end is_international,' +
    '                    case when ${groupByDegreeName}      then stud.degree_name       else null end degree_name,' +
    '                    date_trunc(${dateTrunc}, sem.begin_date) report_date,' +
    '                    sum(comp.status_id) completed,' +
    '                    count(*) scheduled' +
    '             from university u' +
    '                    inner join lecture_course l on (u.id = l.university_id' +
    '                                                      and (l.id = ${lectureCourseId} or ${lectureCourseId} is null))' +
    '                    inner join student stud on (u.id = stud.university_id' +
    '                                                and (stud.is_international = ${isInternational} or ${isInternational} is null)' +
    '                                                and (stud.degree_name = any(${degreeName}) or ${degreeName} is null)' +
    '                     )' +
    '                    inner join semester sem on (sem.university_id = u.id' +
    '                                                and sem.begin_date between to_timestamp(${beginDate}) and to_timestamp(${endDate})' +
    '                     )' +
    '                    inner join compliance comp on (l.id = comp.lecture_course_id and sem.id = comp.semester_id and stud.id = comp.student_id)' +
    '             where (u.id = ${universityId} or ${universityId} is null)' +
    '               and (u.country_code = ${countryCode} or ${countryCode} is null)' +
    '             group by stud.university_id,' +
    '                      u.country_code,' +
    '                      u.code,' +
    '                      u.name,' +
    '                      u.official_name,' +
    '                      case when ${groupByLectureCourseId} then l.id   else null end,' +
    '                      case when ${groupByLectureCourseId} then l.code else null end,' +
    '                      case when ${groupByLectureCourseId} then l.name else null end,' +
    '                      case when ${groupByIsInternational} then stud.is_international  else null end,' +
    '                      case when ${groupByDegreeName}      then stud.degree_name       else null end,' +
    '                      date_trunc(${dateTrunc}, sem.begin_date)' +
    '     )' +
    ' select university_id,' +
    '        country_code,' +
    '        university_code,' +
    '        university_name,' +
    '        official_name,' +
    '        lecture_course_id,' +
    '        lecture_course_code,' +
    '        lecture_course_name,' +
    '        is_international,' +
    '        degree_name,' +
    '        report_date,' +
    '        completed,' +
    '        scheduled,' +
    '        case when scheduled > 0 then completed :: double precision / scheduled * 100 else null end as completion_rate' +
    ' from un';


export const GET_UNIVERSITY_ANALYTICS_BY_YEAR =
    ' with' +
    '   semester_compliance as (' +
    '     select sem.university_id,' +
    '            date_trunc(\'year\', sem.begin_date) report_date,' +
    '            sum(cpl.total_lectures) :: double precision total_lectures,' +
    '            sum(cpl.status_id) :: double precision completed_courses,' +
    '            count(*) :: double precision scheduled_courses' +
    '     from semester sem' +
    '          inner join compliance cpl on sem.id = cpl.semester_id' +
    '     where sem.university_id = ${universityId}' +
    '     group by sem.university_id,' +
    '              date_trunc(\'year\', sem.begin_date)' +
    '   ),' +
    '   year_attendance as (' +
    '     select st.university_id,' +
    '            date_trunc(\'year\', att.begin_date) report_date,' +
    '            sum(att.visited) :: double precision visited,' +
    '            sum(att.absence) :: double precision absence,' +
    '            sum(att.excused) :: double precision excused,' +
    '            sum(att.completed) :: double precision completed,' +
    '            sum(att.scheduled) :: double precision scheduled' +
    '     from student st' +
    '          inner join attendance_snapshot att on st.id = att.student_id' +
    '     where st.university_id = ${universityId}' +
    '     group by st.university_id,' +
    '              date_trunc(\'year\', att.begin_date)' +
    '   )' +
    ' select c.university_id,' +
    '        u.code university_code,' +
    '        u.country_code university_country_code,' +
    '        u.name university_name,' +
    '        u.official_name,' +
    '        c.report_date,' +
    '        a.visited,' +
    '        a.absence,' +
    '        a.excused,' +
    '        a.completed,' +
    '        a.scheduled,' +
    '        c.completed_courses,' +
    '        c.scheduled_courses,' +
    '        case when a.scheduled > 0 then 100 *  a.visited              / a.scheduled else 0 end as total_progress,' +
    '        case when a.scheduled > 0 then 100 *  a.absence              / a.scheduled else 0 end as total_absence_rate,' +
    '        case when a.scheduled > 0 then 100 *  a.excused              / a.scheduled else 0 end as total_excused_rate,' +
    '        case when a.scheduled > 0 then 100 * (a.visited + a.excused) / a.scheduled else 0 end as total_progress_with_ex,' +
    '        case when a.completed > 0 then 100 *  a.visited              / a.completed else 0 end as current_progress,' +
    '        case when a.completed > 0 then 100 * (a.visited + a.excused) / a.completed else 0 end as current_progress_with_ex,' +
    '        case when a.completed > 0 then 100 *  a.absence              / a.completed else 0 end as current_absence_rate,' +
    '        case when a.completed > 0 then 100 *  a.excused              / a.completed else 0 end as current_excused_rate,' +
    '        case when c.scheduled_courses > 0 then c.completed_courses   / c.scheduled_courses * 100 else 0 end as completion_rate' +
    ' from semester_compliance c' +
    '      inner join year_attendance a on (c.university_id = a.university_id and c.report_date = a.report_date)' +
    '      inner join university u on (c.university_id = u.id)';


export const ANC_RATE_BY_STUDENT =
    ' with' +
    '   students as (' +
    '     select' +
    '       st.id,' +
    '       st.first_name,' +
    '       st.last_name,' +
    '       st.university_id,' +
    '       st.is_international,' +
    '       st.is_sponsored,' +
    '       sum(case when c2.status_id = 1 and c2.completion_date < ${endDate} then 1 else 0 end) completed_courses,' +
    '       count(1) scheduled_courses' +
    '     from' +
    '       student st' +
    '       inner join compliance c2 on st.id = c2.student_id' +
    '    where st.university_id = ${universityId}' +
    '    group by' +
    '       st.id,' +
    '       st.first_name,' +
    '       st.last_name,' +
    '       st.university_id,' +
    '       st.is_international,' +
    '       st.is_sponsored' +
    '   ),' +
    '   snap as (' +
    '     select s.student_id,' +
    '       st.first_name,' +
    '       st.last_name,' +
    '       st.university_id,' +
    '       st.is_international,' +
    '       st.is_sponsored,' +
    '       st.completed_courses,' +
    '       st.scheduled_courses,' +
    '       sum(s.visited)   visited,' +
    '       sum(s.absence)   absence,' +
    '       sum(s.excused)   excused,' +
    '       sum(s.completed) completed,' +
    '       sum(s.scheduled) scheduled' +
    '     from' +
    '       students st' +
    '       inner join attendance_snapshot s on s.student_id = st.id' +
    '     where s.begin_date < ${endDate}' +
    '       and s.end_date > ${beginDate}' +
    '     group by' +
    '       s.student_id,' +
    '       st.first_name,' +
    '       st.last_name,' +
    '       st.university_id,' +
    '       st.is_international,' +
    '       st.is_sponsored,' +
    '       st.completed_courses,' +
    '       st.scheduled_courses)' +
    ' select' +
    '   u.code university_code,' +
    '   u.country_code university_country_code,' +
    '   u.name university_name,' +
    '   u.official_name,' +
    '   s.*,' +
    '   case when s.scheduled > 0 then s.visited / s.scheduled * 100 else 0 end as total_progress,' +
    '   case when s.scheduled > 0 then (s.visited + s.excused) / s.scheduled * 100 else 0 end as total_progress_with_ex,' +
    '   case when s.scheduled > 0 then s.absence / s.scheduled * 100 else 0 end as total_absence_rate,' +
    '   case when s.completed > 0 then s.visited / s.completed * 100 else 0 end as current_progress,' +
    '   case when s.completed > 0 then (s.visited + s.excused) / s.completed * 100 else 0 end as current_progress_with_ex,' +
    '   case when s.completed > 0 then s.absence / s.completed * 100 else 0 end as current_absence_rate,' +
    '   case when s.scheduled_courses > 0 then s.completed_courses / s.scheduled_courses * 100 else 0 end as completion_rate' +
    ' from' +
    '   snap s' +
    '   inner join university u on u.id = s.university_id';


export const ANC_RATE_BY_UNIVERSITY =
    ' with' +
    '   student_statistic as (' +
    '       select snap.student_id,' +
    '              sum(snap.visited)   visited,' +
    '              sum(snap.excused)   excused,' +
    '              sum(snap.absence)   absence,' +
    '              sum(snap.completed) completed,' +
    '              sum(l.total)        total,' +
    '              sum(case when snap.status_id = 1 then 1 else 0 end) completed_courses,' +
    '              count(1) total_courses' +
    '         from attendance_snapshot snap' +
    '              inner join lecture_course l on snap.lecture_course_id = l.id' +
    '        where l.university_id = ${universityId}' +
    '        group by snap.student_id' +
    '   ),' +
    '   university_statistic as (' +
    '       select s2.university_id,' +
    // todo: removed for later '              s2.is_international,' +
    // todo: removed for later '              s2.is_sponsored,' +
    '              sum(ss2.visited)   visited,' +
    '              sum(ss2.excused)   excused,' +
    '              sum(ss2.absence)   absence,' +
    '              sum(ss2.completed) completed,' +
    '              sum(ss2.total)     total,' +
    '              sum(ss2.completed_courses) completed_courses,' +
    '              sum(case when ss2.completed_courses >= ss2.total then 1 else 0 end) total_completion,' +
    '              sum(ss2.total_courses) total_courses' +
    '         from student_statistic ss2' +
    '              inner join student s2 on ss2.student_id = s2.id' +
    '       group by s2.university_id' +
    // todo: removed for later '                s2.is_international' +
    // todo: removed for later '                s2.is_sponsored' +
    '   )' +
    ' select u.id university_id,' +
    '        u.code university_code,' +
    '        u.country_code university_country_code,' +
    '        u.name university_name,' +
    '        u.official_name,' +
    // todo: removed for later '        ss.is_international,' +
    // todo: removed for later '        ss.is_sponsored,' +
    '        case when ss.total > 0 then ss.visited / ss.total * 100 else 0 end as total_progress,' +
    '        case when ss.total > 0 then (ss.visited + ss.excused) / ss.total * 100 else 0 end as total_progress_with_ex,' +
    '        case when ss.total > 0 then ss.absence / ss.total * 100 else 0 end as total_absence_rate,' +
    '        case when ss.completed > 0 then ss.visited / ss.completed * 100 else 0 end as current_progress,' +
    '        case when ss.completed > 0 then (ss.visited + ss.excused) / ss.completed * 100 else 0 end as current_progress_with_ex,' +
    '        case when ss.completed > 0 then ss.absence / ss.completed * 100 else 0 end as current_absence_rate,' +
    '        case when ss.total_courses > 0 then ss.completed_courses / ss.total_courses * 100 else 0 end as course_completion_rate,' +
    '        case when ss.total_courses > 0 then ss.total_completion / ss.total_courses * 100 else 0 end as total_completion_rate' +
    '   from university_statistic ss' +
    '        inner join university u on ss.university_id = u.id';
