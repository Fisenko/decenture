export const GET_SEMESTER_BY_TIMESTAMP =
    'SELECT * FROM semester s ' +
    ' WHERE s.university_id = ${universityId} ' +
    ' AND s.begin_date < to_timestamp(${timestamp})' +
    ' AND s.end_date > to_timestamp(${timestamp})';
