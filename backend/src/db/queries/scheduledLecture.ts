import * as utl from 'src/db/mapping';
import { ID } from 'src/db/mapping';

export const tableName = 'scheduled_lecture';

export const propertyNames = [
    ID,
    'nfc_id',
    'lecture_course_id',
    'begin_date',
    'end_date',
    'status_id',
    'scan_begin_date',
    'scan_end_date',
    'lecture_theatre_name',
    'event_type',
    'subject',
    'description',
    'notes',
];

export const insertProps = utl.composeInsertProps(propertyNames);

export const insertParams = utl.composeInsertParams(propertyNames);

export const allProps = utl.selectString(propertyNames);

export const CREATE = 'INSERT INTO ' + tableName + '(' + insertProps + ')' + ' VALUES(' + insertParams + ') RETURNING *';

export const FIND_ONE = 'SELECT ' + allProps + ' FROM ' + tableName + ' WHERE id = ${id}';

export const FIND_BY_TRANSACTION_ID = 'SELECT ' + utl.selectString(propertyNames, 'sa') + 'FROM ' + tableName + ' WHERE transaction_id = ${transactionId}';

export const FIND_BY_STUDENT_ID =
    'SELECT *' +
    ' FROM scheduled_attendance a' +
    ' INNER JOIN scheduled_lecture l ON a.scheduled_lecture_id = l.id' +
    ' INNER JOIN lecture_course course ON l.lecture_course_id = course.id' +
    ' WHERE a.student_id = ${studentId}' +
    ' AND a.transaction_id is not null';
