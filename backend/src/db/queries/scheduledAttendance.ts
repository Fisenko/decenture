import { ATTENDANCE_STATUS, selectString } from 'src/db/mapping';

export const tableName = 'scheduled_attendance';

export const propertyNames = ['student_id', 'scheduled_lecture_id', 'status_id', 'timestamp', 'transaction_id'];

export const allProps = selectString(propertyNames);

export const CREATE = 'INSERT INTO ' + tableName + '(' + propertyNames.join(',') + ')' +
    ' VALUES(${studentId},${scheduledLectureId},${statusId},${timestamp},${transactionId}) RETURNING *';

export const UPDATE = 'UPDATE scheduled_attendance' +
    ' SET status_id = ${statusId}, timestamp = to_timestamp(${timestamp}), transaction_id = ${transactionId}' +
    ' WHERE student_id = ${studentId} AND scheduled_lecture_id = ${scheduledLectureId}' +
    ' RETURNING ' + selectString(propertyNames);

export const FIND_ONE = 'SELECT ' + allProps + ' FROM ' + tableName + ' WHERE student_id = ${studentId} AND scheduled_lecture_id = ${scheduledLectureId}';

export const FIND_BY_NFC_ID_TIMESTAMP_STUDENT_ID =
    'SELECT ' + selectString(propertyNames, 'sa') + ', sl.lecture_course_id as "lectureCourseId"' +
    ' FROM scheduled_lecture sl' +
    ' INNER JOIN scheduled_attendance sa ON sl.id = sa.scheduled_lecture_id' +
    ' WHERE sl.nfc_id = ${nfcId}' +
    ' AND sl.begin_date < to_timestamp(${timestamp})' +
    ' AND sl.end_date > to_timestamp(${timestamp})' +
    ' AND sa.student_id = ${studentId}';

export const FIND_BY_TRANSACTION_ID = 'SELECT ' + selectString(propertyNames, 'sa') + 'FROM ' + tableName + ' WHERE transaction_id = ${transactionId}';

export const FIND_BY_STUDENT_ID =
    'SELECT *' +
    ' FROM scheduled_attendance a' +
    ' INNER JOIN scheduled_lecture l ON a.scheduled_lecture_id = l.id' +
    ' INNER JOIN lecture_course course ON l.lecture_course_id = course.id' +
    ' WHERE a.student_id = ${studentId}' +
    ' AND a.transaction_id is not null';

export const FIND_BY_STUDENT_ID_TIMESTAMP =
    'SELECT *' +
    ' FROM scheduled_lecture sl' +
    ' INNER JOIN scheduled_attendance sa ON sa.scheduled_lecture_id = sl.id' +
    ' INNER JOIN lecture_course course ON sl.lecture_course_id = course.id' +
    ' WHERE sa.student_id = ${studentId}' +
    // ' AND sa.transaction_id is not null' +
    ' AND sl.begin_date > to_timestamp(${beginDate})' +
    ' AND sl.end_date < to_timestamp(${endDate})' +
    ' AND sa.status_id =' + ATTENDANCE_STATUS.VISITED;

export const FIND_ABSENCE_BY_STUDENT_ID =
    'SELECT *' +
    ' FROM scheduled_lecture sl' +
    ' INNER JOIN scheduled_attendance sa ON sa.scheduled_lecture_id = sl.id' +
    ' WHERE sa.student_id = ${studentId}' +
    ' AND sa.status_id =' + ATTENDANCE_STATUS.ABSENCE;

export const FIND_ABSENCE_BY_STUDENT_ID_TIMESTAMP =
    'SELECT *' +
    ' FROM scheduled_lecture sl' +
    ' INNER JOIN scheduled_attendance sa ON sa.scheduled_lecture_id = sl.id' +
    ' WHERE sa.student_id = ${studentId}' +
    ' AND sl.begin_date > to_timestamp(${beginDate})' +
    ' AND sl.end_date < to_timestamp(${endDate})' +
    ' AND sa.status_id =' + ATTENDANCE_STATUS.ABSENCE;

export const FIND_ATTENDANCE_PERCENTAGE =
    'SELECT *' +
    ' FROM attendance_snapshot ats' +
    ' INNER JOIN lecture_course lc ON ats.lecture_course_id = lc.id' +
    ' WHERE ats.student_id = ${studentId}';

