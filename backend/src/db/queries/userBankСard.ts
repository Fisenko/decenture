export const GET_TYPE_ID_BY_KEY = 'SELECT id FROM payment_system_type WHERE key=$1';

export const CREATE_CARD = 'INSERT INTO user_bank_card(user_id,payment_system_type_id,card_number,expiry_year,' +
    'expiry_month,cvv,holder)' +
    ' VALUES(${userId},${paymentSystemTypeId},${number},${expiryYear},${expiryMonth},${cvc},${name})' +
    ' RETURNING id';

export const GET_CARD_NUMBER_BY_USER_ID = 'SELECT id, card_number, holder FROM user_bank_card where user_id=${userId}';
