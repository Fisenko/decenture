import { selectString } from 'src/db/mapping';

export const allPropertyNames = [
    'abbreviation',
    'address',
    'code',
    'country_code',
    'domain_name',
    'erp_info',
    'id',
    'name',
    'official_name',
];

export const allProps = selectString(allPropertyNames);

export const GET_UNIVERSITY_BY_ID = 'SELECT ' + allProps + ' FROM university WHERE id=${id}';

export const GET_UNIVERSITY_BY_USER_ID =
    'SELECT ' + selectString(allPropertyNames, 'u') +
    'FROM account a INNER JOIN university u ON a.id = u.id ' +
    'WHERE a.user_id = ${userId} and a.owner_id = ${userId}';
