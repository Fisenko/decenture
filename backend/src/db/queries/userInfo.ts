import { selectString } from 'src/db/mapping';

export const propertyNames = ['id', 'first_name', 'last_name', 'phone_number', 'date_of_birth'];

export const tableName = 'user_info';

export const allProps = selectString(propertyNames);

export const CREATE_USER_INFO = 'INSERT INTO user_info(id,first_name,last_name,phone_number,date_of_birth)' +
    ' VALUES(${id},${firstName},${lastName},${phoneNumber},${dateOfBirth})';

export const GET_USER_INFO_BY_ID = 'SELECT ' + allProps + ' FROM user_info WHERE id=${id}';
