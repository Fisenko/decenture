import { selectString } from 'src/db/mapping';

export const propertyNames = [
    'id',
    'university_id',
    'erp_email',
    'erp_student_id',
    'first_name',
    'last_name',
    'is_international',
    'is_sponsored',
    'degree_name',
    'certification',
];

export const tableName = 'student';

export const allProps = selectString(propertyNames);

export const CREATE_STUDENT = 'INSERT INTO student(id,university_id,erp_student_id,first_name,last_name,' +
    'erp_email,is_international) ' +
    ' VALUES(${id},${universityId},${studentId},${firstMame},${lastName},${email},${isInternational}) RETURNING id';

export const GET_ALL_STUDENTS = 'SELECT ' + allProps + ' FROM student';
export const GET_STUDENTS_BY_EMAIL = 'SELECT ' + allProps + ' FROM student WHERE email=${email}';

export const GET_STUDENT_BY_ID = 'SELECT ' + allProps + ' FROM student WHERE id=${id}';
export const GET_STUDENT_BY_EMAIL_AND_STUDENT_ID = 'SELECT ' + allProps + ' FROM student WHERE id=${id} AND ' +
    'student_id = ${studentId}';

export const GET_STUDENT_BY_USER_ID = 'SELECT ' + selectString(propertyNames, 's') +
    ' FROM student s JOIN account a ON s.id = a.id WHERE a.user_id=${userId}';

export const GET_ALL_STUDENTS_BY_UNIVERSITY_ID = 'SELECT ' + allProps + ' FROM student WHERE ' +
    'university_id=${universityId}';
