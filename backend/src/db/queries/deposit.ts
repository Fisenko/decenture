export const ADD_DEPOSIT_AMOUNT_TO_PERSONAL_ACCOUNT_BY_APP_USER_ID =
    //TODO don't like magic number type_id=1, change it later
    'UPDATE account SET amount = amount + ${amount} WHERE user_id=${userId} AND type_id=1 RETURNING amount';

export const CHECK_CORRECT_CARD_OWNERSHIP_BY_APP_USER_ID_AND_CARD_ID =
    'SELECT * FROM user_bank_card WHERE user_id=${userId} AND id=${cardId}';

