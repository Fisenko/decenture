import { ACCOUNT_TYPE, selectString } from 'src/db/mapping';

export const propertyNames = [
    'id',
    'name',
    'amount',
    'currency',
    'type_id',
    'status_id',
    'user_id',
    'owner_id',
    'entry_date',
    'close_date',
];

export const tableName = 'account';

export const allProps = selectString(propertyNames);

export const CREATE_ACCOUNT =
    'INSERT INTO account(name,currency,type_id,status_id,user_id,owner_id)' +
    ' VALUES(${name},${currency},${typeId},${statusId},${userId},${ownerId}) RETURNING *';

export const GET_ACCOUNT_BY_ID = 'SELECT ' + allProps + ' FROM account WHERE id=${id}';

export const GET_ACCOUNTS_BY_IDS = 'SELECT ' + allProps + ' FROM account WHERE id IN (${ids:list})';

export const GET_ACCOUNT_BY_USER_ID = 'SELECT ' + allProps + ' FROM account WHERE user_id=${userId}';

export const GET_ACCOUNT_BY_USER_ID_AND_TYPE_ID =
    'SELECT ' + allProps + ' FROM account WHERE user_id=${userId}  ' + 'AND type_id=${typeId}';

export const GET_USER_ACCOUNTS_BY_EMAIL_AND_TYPE_ID =
    'SELECT ' +
    selectString(propertyNames, 'a') +
    ' FROM account a JOIN app_user au on a.user_id = au.id' +
    ' WHERE au.email=${email}' +
    ' AND type_id=${typeId}';

export const GET_STUDENT_ACCOUNTS_BY_EMAIL =
    'SELECT ' +
    selectString(propertyNames, 'a') +
    ' FROM account a JOIN student s on a.id = s.id WHERE s.erp_email=${email}';

export const GET_USER_ACCOUNTS_BY_EMAIL =
    'SELECT ' +
    selectString(propertyNames, 'a') +
    ' FROM account a JOIN app_user au on a.user_id = au.id WHERE au.email=${email}';

export const GET_USER_ACCOUNTS_BY_ID =
    'SELECT ' +
    selectString(propertyNames, 'a') +
    ' FROM account a JOIN app_user au on a.user_id = au.id WHERE au.id=${id}';

export const UPDATE_ACCOUNT_AMOUNT =
    'UPDATE account SET amount = ${amount} WHERE id=${id} RETURNING amount::double precision';

export const ADD_ACCOUNT_AMOUNT =
    'UPDATE account SET amount = amount + ${amount} WHERE id=${id} RETURNING amount::double precision';

export const UPDATE_DEBIT_CREDIT =
    'UPDATE account' +
    ' SET amount = amount + CASE WHEN id = ${dt} THEN ${amount} ELSE (- ${amount}) END' +
    ' WHERE id in (${dt}, ${ct})' +
    ' RETURNING *';

export const UPDATE_ACCOUNT_USER = 'UPDATE account SET user_id = ${userId} WHERE id=${id}';

export const GET_ACCOUNT_AMOUNT = 'SELECT amount FROM account WHERE user_id=${userId} AND type_id=${typeId} ';

export const GET_SPONSORED_STUDENT_ACCOUNTS =
    'select a.* from student s inner join account a on (s.id = a.id and a.status_id = 1) where s.is_sponsored';
