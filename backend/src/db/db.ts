import pgp from 'pg-promise';
import { IDatabase } from 'pg-promise';

const cn = {
    host: process.env.PG_HOST || 'localhost',
    port: parseInt(process.env.PG_PORT,  10),
    database: process.env.PG_DB,
    user: process.env.PG_USER,
    password: process.env.PG_PASSWORD
};

const pgpE = pgp();

export class DBC {
    static instance: DBC = undefined;
    connector: IDatabase<any>;

    constructor() {
        if (DBC.instance === undefined) {
            this.connector = pgpE(cn);
            DBC.instance = this;
        }
        return DBC.instance;
    }
}

const dbc = new DBC().connector;
export default dbc;
