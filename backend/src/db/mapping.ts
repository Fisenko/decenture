import { underScoreToCamelCase } from 'src/utils/utils';

export const ID = 'id';

export enum USER_STATUS {
    NOT_ACTIVATED = 0,
    ACTIVE = 1,
    CLOSED = -1,
}

export enum EDUCATIONAL_EVENT_TYPE {
    A = 'Lesson, lecture, tutorial or seminar',
    B = 'One to one meeting with a supervisor',
    C = 'Placement',
    D = 'Examination',
    E = 'Work Experience',
    F = 'Other',
}

export enum ATTENDANCE_STATUS {
    PENDING = 0,
    VISITED = 1,
    EXCUSED = 2,
    CANCELED = 3,
    ABSENCE = 9,
}

export const USER_ROLE = {
    ADMIN: 0,
    STUDENT: 1,
    UNIVERSITY: 2,
    GOVERNMENT: 4,
};

export const APP_ROLES_MAPPING = {
    ADMIN: 0,
    STUDENT: 1,
    UNIVERSITY: 2,
    GOVERNMENT: 4,
};

export const ACCOUNT_TYPE = {
    PERSONAL: 1,
    ESCROW: 2,
};

export const getSelectString = (prop: string, alias?: string) => {
    return `${alias ? alias + '.' : ''}${prop} as "${underScoreToCamelCase(prop)}"`;
};

export const getParamString = (prop: string) => {
    return `\$\{${underScoreToCamelCase(prop)}\}`;
};

export const getSetString = (prop: string, alias?: string) => {
    return `${alias ? alias + '.' : ''}${prop} = ${underScoreToCamelCase(prop)}`;
};

export const rawProps = (props: Array<string>, alias?: string) => {
    return props.join(',');
};

export const propsToParams = (props: Array<string>, alias?: string) => {
    return props.map((prop: string) => getParamString(prop)).join(',');
};

export const selectString = (props: Array<string>, alias?: string) => {
    return props.map((prop: string) => getSelectString(prop, alias)).join(', ');
};

export const composeInsertProps = (props: Array<string>, id?: string) => {
    const filter = id || ID;
    return props.filter((e: string) => (e != filter)).join(', ');
};

export const composeInsertParams = (props: Array<string>, id?: string) => {
    const filter = id || ID;
    return props.filter((e: string) => (e != filter)).map((prop: string) => getParamString(prop)).join(',');
};

export const composeUpdateSet = (props: Array<string>, id?: string) => {
    const filter = id || ID;
    return props.filter((e: string) => (e != filter))
    .map((prop: string) => getSetString(prop))
    .join(',');
};

export const INTERNATIONAL = new Map();
INTERNATIONAL.set(true, 'International');
INTERNATIONAL.set(false, 'Local');
