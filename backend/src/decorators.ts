import {PULL_METHODS} from 'src/utils/utils';
export const registerMethod = function () {
    return function (target: Object, propertyKey: string, descriptor: PropertyDescriptor) {
        PULL_METHODS[propertyKey] = descriptor.value;
        return descriptor.value;
    };
};
