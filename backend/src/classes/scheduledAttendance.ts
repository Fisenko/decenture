export default class ScheduledAttendance {

    public studentId: string;
    public scheduledLectureId: string;
    public stratusId: string;
    public timestamp: string;
    public transactionId: string;

    constructor(data: any) {
        this.studentId = data.studentId;
        this.scheduledLectureId = data.scheduledLectureId;
        this.stratusId = data.stratusId;
        this.timestamp = data.timestamp;
        this.transactionId = data.transactionId;
    }
}
