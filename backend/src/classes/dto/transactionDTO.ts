import Transaction from 'src/hyperledger/models/transaction';
import Account from 'src/classes/account';

const codeLength = 6;

const getTransactionType = ($class: string) => {
    switch ($class) {
        case 'org.acme.mynetwork.InvoiceTransaction':
            return 'Invoice';
        default:
            return $class;
    }
};

export default class TransactionDTO {
    id: string;
    name: string;
    sender: string;
    receiver: string;
    description: string;
    amount: number;
    datetime: Date;
    type: string;

    constructor(transaction: Transaction, sender: Account, receiver: Account) {
        this.id = transaction.transactionId || '';
        this.name = transaction.description || '';
        this.description = transaction.description || '';
        this.amount = transaction.amount || 0;
        this.datetime = new Date(transaction.timestamp);
        this.sender = sender.name;
        this.receiver = receiver.name;
        this.type = getTransactionType(transaction.$class);
    }
}
