export default class AttendanceAbsenceDTO {
    title: string;
    dateTime: Date;
    place: string;

    constructor(data: any = {}) {
        this.title = data.subject;
        this.dateTime = data.begin_date;
        this.place = data.lecture_theatre_name;
    }
}
