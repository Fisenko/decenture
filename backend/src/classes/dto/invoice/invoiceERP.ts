export default class InvoiceERP {

    // TODO: create converter camelCase to UPPER_CASE
    BNK_ID_NBR: string;
    BANK_ACCOUNT_NUM: string;
    TRAN_REF_ID: string;
    TRAN_DT: string;
    BUSINESS_UNIT: string;
    TRAN_AMT: string;
    CURRENCY_CD: string;
    TRAN_DESCR: string;
    RECON_TRANS_CODE: string;
    RECON_TYPE: string;
    RECON_STATUS: string;
    RECONCILE_DT: string;
    RECONCILE_OPRID: string;
    RECON_CYCLE_NBR: string;
    STTLMNT_DT_ACTUAL: string;
    ACCTG_TMPL_ID: string;
    BUILD_ACCTG_STATUS: string;
    TRA_PROCESS_STATUS: string;
    PROCESS_INSTANCE: string;
    OPRID: string;
    DTTM_ENTERED: string;
    RECON_RUN_ID: string;
    BANK_ACCT_RVL_AMT: string;


    constructor(invoice: any = {}, student: any = {}) {
        this.BNK_ID_NBR = '123456';
        this.BANK_ACCOUNT_NUM = '0000001';
        this.TRAN_REF_ID = invoice.id || '';
        this.TRAN_DT = new Date().toLocaleString();
        this.BUSINESS_UNIT = '';
        this.TRAN_AMT = invoice.amount ? `$${invoice.amount}` : '';
        this.CURRENCY_CD = 'USD';
        this.TRAN_DESCR = `student:${student.erpStudentId},invoice:${invoice.id},description:${invoice.description}` || '';
        this.RECON_TRANS_CODE = 'P';
        this.RECON_TYPE = 'U';
        this.RECON_STATUS = 'UNR';
        this.RECONCILE_DT = '';
        this.RECONCILE_OPRID = 'DECENTURE';
        this.RECON_CYCLE_NBR = '';
        this.STTLMNT_DT_ACTUAL = '';
        this.ACCTG_TMPL_ID = 'TR_TUITION';
        this.BUILD_ACCTG_STATUS = '';
        this.TRA_PROCESS_STATUS = '';
        this.PROCESS_INSTANCE = '';
        this.OPRID = 'PSJOB';
        this.DTTM_ENTERED = '';
        this.RECON_RUN_ID = '';
        this.BANK_ACCT_RVL_AMT = '';
    }
}
