import { extractIdFromHyperledger } from 'src/utils/utils';

export default class InvoiceDTO {
    id: string;
    title: string;
    from: string;
    to: string;
    ref: string;
    amount: number;
    type: string;
    description: string;
    code: string;

    billedFrom: string;
    billedTo: string;
    invoiceNumber: string;
    email: string;

    entryDate: string;
    status: string;

    paidDate: Date;
    isInternational: string;
    degreeName: string;

    // TODO: remove unused fields
    constructor(data: any = {}) {
        this.id = data.id;
        this.code = data.id.substring(data.id.length - 6) || '' ;
        this.title = data.description || '';
        this.from = data.from || '';
        this.to = data.to || '';
        this.ref = data.ref || '';
        this.amount = data.amount || null;
        this.type = '';
        this.description = data.description;
        this.billedFrom = extractIdFromHyperledger(data.billedFrom) || '';
        this.billedTo = extractIdFromHyperledger(data.billedTo) || '';
        this.invoiceNumber = data.invoiceNumber || '';
        this.email = data.email || '';
        this.entryDate = data.entryDate;
        this.status = data.status || '';
        this.paidDate = data.paidDate || null;
        this.isInternational = data.isInternational || false;
        this.degreeName = data.degreeName || '';
    }
}
