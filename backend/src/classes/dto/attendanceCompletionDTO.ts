export default class AttendanceCompletionDTO {
    value: number;
    code: string;

    constructor(value: number, code: string) {
        this.value = value;
        this.code = code;
    }
}
