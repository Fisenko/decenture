export interface IAttendanceSnapshot {
    studentId: string;
    lectureCourseId: string;
    visited: number;
    absence: number;
    excused: number;
    completed: number;
    courseWork: number;
    exam: number;
    statusId: number;
    lastUpdate: string;
    snapshotDate?: number;
}

export default class AttendanceSnapshot {
    public studentId: string;
    public lectureCourseId: string;
    public visited: number;
    public absence: number;
    public excused: number;
    public completed: number;
    public courseWork: number;
    public exam: number;
    public statusId: number;
    public lastUpdate: string;
    public snapshotDate?: string;

    constructor(data: any) {
        this.studentId = data.studentId;
        this.lectureCourseId = data.lectureCourseId;
        this.visited = data.visited || 0;
        this.absence = data.absence || 0;
        this.excused = data.excused || 0;
        this.completed = data.completed || 0;
        this.courseWork = data.courseWork || 0;
        this.exam = data.exam || 0;
        this.statusId = data.statusId || 0;
        this.lastUpdate = data.lastUpdate;
        this.snapshotDate = data.snapshotDate;
    }
}
