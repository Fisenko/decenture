export enum InvoiceStatus {
    PAID = 'PAID',
    NOT_PAID = 'NOT PAID',
}
