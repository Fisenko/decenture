export default class Lecture {
    id? : string;
    title: string;
    dateTime: Date;
    place: string;

    constructor(title: string, dateTime: Date, place: string) {
        this.title = title;
        this.dateTime = dateTime;
        this.place = place;
    }
}
