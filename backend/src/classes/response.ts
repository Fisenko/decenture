export class Response {
    public status: number;
    public errMsg: string;
    public data: any;

    constructor(data: any = {}) {
        this.status = data.status || 0;
        this.errMsg = data.errMsg || '';
        this.data = data.data || {};
    }
}
