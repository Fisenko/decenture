import Lecture from './lecture';
import StudentAnalytics from 'src/classes/analytics/studentAnalytics';

export interface IAttendance {
    history: Array<any>;
    absence: Array<Lecture>;
    percentage: Array<StudentAnalytics>;
}

export default class UserAttendance {

    history: Array<any>;
    absence: Array<Lecture>;
    percentage: Array<any>;

    constructor(attendance: IAttendance) {
        this.history = attendance.history || [];
        this.absence = attendance.absence || [];
        this.percentage = attendance.percentage || [];
    }
}
