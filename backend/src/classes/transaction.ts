interface ITransaction {
    code: number;
    name: string;
    description: string;
    type: string;
    price?: number;
    sender?: string;
    receiver?: string;
    time?: string;
    date?: string;
}

export class Transaction {
    code: number;
    name: string;
    description: string;
    type: string;
    price?: number;
    sender?: string;
    receiver?: string;
    time?: string;
    date?: string;

    constructor(transaction: ITransaction) {
        this.code = transaction.code;
        this.name = transaction.name;
        this.description = transaction.description;
        this.type = transaction.type;
        this.price = transaction.price;
        this.sender = transaction.sender;
        this.receiver = transaction.receiver;
        this.time = transaction.time;
        this.date = transaction.date;
    }
}

export class Transactions {
    paid: Array<Transaction>;
    unpaid: Array<Transaction>;

    constructor(paid: Array<Transaction>, unpaid: Array<Transaction>) {
        this.paid = paid;
        this.unpaid = unpaid;
    }
}
