export class Deposit {
    public amount: number;
    public recurring: any;
    public cardId: string;
    public userId: string;

    constructor(data: any = {}) {
        this.amount = data.amount || 0;
        this.recurring = data.recurring || '';
        this.cardId = data.cardId || '';
        this.userId = data.userId || '';
    }
}