export default class RedisUser {
    isAuth: boolean;
    isAdmin: boolean;
    id: string;

    constructor(data?: any) {
        this.isAuth = false;
        if (data && data.id) {
            this.isAuth = true;
            this.id = data.id || '';
            this.isAdmin = data.isAdmin || false;
        }
    }
}
