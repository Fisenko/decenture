export default class ScheduledLecture {

    public id: string;
    public nfcId: string;
    public lectureCourseId: string;
    public beginDate: string;
    public endDate: string;
    public statusId: number;
    public scanBeginDate: string;
    public scanBndDate: string;
    public lectureTheatreName: string;
    public eventType: string;
    public subject: string;
    public description: string;
    public notes: string;

    constructor(data: any) {
        this.id = data.id;
        this.nfcId = data.nfcId;
        this.lectureCourseId = data.lectureCourseId;
        this.beginDate = data.beginDate;
        this.endDate = data.endDate;
        this.statusId = data.statusId;
        this.scanBeginDate = data.scanBeginDate;
        this.scanBndDate = data.scanBndDate;
        this.lectureTheatreName = data.lectureTheatreName;
        this.eventType = data.eventType;
        this.subject = data.subject;
        this.description = data.description;
        this.notes = data.notes;
    }
}
