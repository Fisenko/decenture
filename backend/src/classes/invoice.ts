export class Invoice {
    id: string;
    code: number;
    title: string;
    from: string;
    to: string;
    ref: string;
    amount: number;
    type: string;
    description: string;

    constructor(data: any = {}) {
        this.id = data.id || '';
        this.code = data.code || 0;
        this.title = data.title || '';
        this.from = data.from || '';
        this.to = data.to || '';
        this.ref = data.ref || '';
        this.amount = data.amount || 0;
        this.type = data.type || '';
        this.description = data.description || '';
    }
}
