export class Card {

    public id: string;
    public userId: string;
    public paymentSystemTypeId: number;
    public paymentSystemTypeKey: string;
    public number: string;
    public name?: string;
    public expiryYear: string;
    public expiryMonth: string;
    public cvc: string;

    constructor(data: any = {}) {
        this.id = data.id;
        this.userId = data.userId;
        this.paymentSystemTypeId = data.paymentSystemTypeId || null;
        this.paymentSystemTypeKey = data.type || '';
        this.number = data.number ? data.number.replace(/\s/g, '') : data.number;
        this.name = data.name || '';
        this.expiryMonth = data.expiry ? data.expiry.split('/')[0] : data.expiry;
        this.expiryYear = data.expiry ? data.expiry.split('/')[1] : data.expiry;
        this.cvc = data.cvc || '';
    }
}
