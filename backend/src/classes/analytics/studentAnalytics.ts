
export default class StudentAnalytics {

    public absence: string;
    public completed: string;
    public currentAbsenceRate: number;
    public currentExcusedRate: number;
    public currentProgress: number;
    public currentProgressWithEx: number;
    public excused: string;
    public lectureCourseCode: string;
    public lectureCourseId: string;
    public lectureCourseName: string;
    public scheduled: string;
    public totalAbsenceRate: number;
    public totalExcusedRate: number;
    public totalProgress: number;
    public totalProgressWithEx: number;

    public constructor(data: any = {}) {
       this.absence = data.absence || '';
       this.completed = data.completed || '';
       this.currentAbsenceRate = data.currentAbsenceRate || 0;
       this.currentExcusedRate = data.currentExcusedRate || 0;
       this.currentProgress = data.currentProgress || 0;
       this.currentProgressWithEx = data.currentProgressWithEx || 0;
       this.excused = data.excused || '';
       this.lectureCourseCode = data.lectureCourseCode || '';
       this.lectureCourseId = data.lectureCourseId || '';
       this.lectureCourseName = data.lectureCourseName || '';
       this.scheduled = data.scheduled || '';
       this.totalAbsenceRate = data.totalAbsenceRate || 0;
       this.totalExcusedRate = data.totalExcusedRate || 0;
       this.totalProgress = data.totalProgress || 0;
       this.totalProgressWithEx = data.totalProgressWithEx || 0;
    }
}
