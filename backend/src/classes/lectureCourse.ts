export default class LectureCourse {

    public id: string;
    public universityId: string;
    public code: string;
    public name: string;
    public total: number;
    public course_work: number;
    public exam: number;

    constructor(data: any) {
        this.id = data.id;
        this.universityId = data.universityId;
        this.code = data.code;
        this.name = data.name;
        this.total = data.total;
        this.course_work = data.course_work;
        this.exam = data.exam;
    }
}
