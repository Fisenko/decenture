import AppUser from 'src/classes/appUser';

export default class UserInfo extends AppUser {

    public firstName: string;
    public lastName: string;
    public phoneNumber?: string;
    public dateOfBirth: Date;
    public studentNumber: string;
    public isInternational?: boolean;

    constructor(data: any) {
        super(data);
        this.firstName = data.firstName;
        this.lastName = data.lastName;
        this.phoneNumber = data.phoneNumber;
        this.dateOfBirth = data.dateOfBirth;
        this.studentNumber = data.studentNumber;
        this.isInternational = data.isInternational || false;
    }
}
