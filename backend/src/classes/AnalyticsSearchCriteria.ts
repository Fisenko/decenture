const map = new Map();
map.set('all', null);
map.set('international', 'true');
map.set('local', 'false');

export class AnalyticsSearchCriteria {
    public beginDate: number;
    public endDate: number;
    public dateTrunc: string;
    public countryCode?: string;
    public universityId?: string;
    public lectureCourseId?: string;
    public isInternational?: string;
    public degreeName?: Array<string>;
    public groupByLectureCourseId?: boolean;
    public groupByIsInternational?: boolean;
    public groupByDegreeName?: boolean;

    constructor(data: any = {}) {
        this.beginDate = data.beginDate || 0;
        this.endDate = data.endDate / 1000 || new Date().getTime() / 1000;
        this.dateTrunc = data.dateTrunc || 'year';
        this.universityId = data.universityId || null;
        this.countryCode = data.countryCode  || null;
        this.lectureCourseId = data.lectureCourseId  || null;
        this.isInternational = map.get(data.isInternational)  || null;
        this.degreeName = data.degreeName  || null;
        this.groupByLectureCourseId = data.groupByLectureCourseId  || null;
        this.groupByIsInternational = data.groupByIsInternational  || null;
        this.groupByDegreeName = data.groupByDegreeName  || null;
    }
}
