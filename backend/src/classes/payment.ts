import InvoiceDTO from 'src/classes/dto/invoice/invoiceDTO';

export class Payment {

    // TODO remove counts and use array length
    public paidInvoicesCount: number;
    public paidInvoicesSumAmount: number;
    public unpaidInvoicesCount: number;
    public unpaidInvoicesSumAmount: number;
    public pendingInvoicesCount: number;
    public pendingInvoicesSumAmount: number;
    public totalBalance: number;
    public todaysBeginningBalance: number;
    public monthsBeginningBalance: number;
    public paidInvoices: Array<InvoiceDTO>;
    public unpaidInvoices: Array<InvoiceDTO>;

    constructor(data: any = {}) {
        this.paidInvoicesCount = data.paidInvoicesCount || 0;
        this.paidInvoicesSumAmount = data.paidInvoicesSumAmount || 0;
        this.unpaidInvoicesCount = data.unpaidInvoicesCount || 0;
        this.unpaidInvoicesSumAmount = data.unpaidInvoicesSumAmount || 0;
        this.pendingInvoicesCount = data.pendingInvoicesCount || 0;
        this.pendingInvoicesSumAmount = data.pendingInvoicesSumAmount || 0;
        this.totalBalance = data.totalBalance || 0;
        this.todaysBeginningBalance = data.todaysBeginningBalance || 0;
        this.monthsBeginningBalance = data.monthsBeginningBalance || 0;
        this.paidInvoices = data.paidInvoices || [];
        this.unpaidInvoices = data.unpaidInvoices || [];
    }
}
