export class Student {
    public id: string;
    public universityId: string;
    public erpEmail: string;
    public erpStudentId: string;
    public firstName: string;
    public lastName: string;
    public isInternational: boolean;
    public isSponsored: boolean;
    public degreeName: string;
    public certification: string;

    constructor(data: any) {
        this.id = data.id || '';
        this.universityId = data.universityId || '';
        this.erpEmail = data.erpEmail || '';
        this.erpStudentId = data.erpStudentId || '';
        this.firstName = data.firstName || '';
        this.lastName = data.lastName || '';
        this.isInternational = data.isInternational || false;
        this.isSponsored = data.isSponsored || false;
        this.degreeName = data.degreeName || '';
        this.certification = data.certification || '';
    }
}
