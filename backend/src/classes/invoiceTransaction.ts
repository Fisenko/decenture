export default class InvoiceTransaction {
    sender: string;
    receiver: string;
    amount: number;
    invoice: string;

    post_date: string;
    description: string;

    constructor(data: any = {}) {
        this.post_date = data.entryDate || Date.now();
        this.description = data.description || '';
        this.sender = data.billedTo || '';
        this.receiver = data.billedFrom || '';
        this.amount = data.amount;
        this.invoice = data.id || null;
    }
}

