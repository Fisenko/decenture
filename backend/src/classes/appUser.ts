import sha1 from 'sha1';

export default class AppUser {
    public id: string;
    public email: string;
    public password?: string;
    public token?: string;
    public statusId?: number;
    public roleId?: number;
    public entryDate?: Date;
    public closeDate?: Date;

    constructor(data: any = {}) {
        this.id = data.id;
        this.email = String(data.email || '').replace(/\s/g, '').toLowerCase();
        this.password = sha1(data.password).toString();
        this.token = data.token || null;
        this.statusId = data.status || null;
        this.roleId = data.roleId || null;
        this.entryDate = data.entryDate || null;
        this.closeDate = data.closeDate || null;
    }
}
