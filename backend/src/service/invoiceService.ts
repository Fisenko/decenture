import { registerMethod } from 'src/decorators';
import { Response } from 'src/classes/response';
import { generateInvoicesByUserName } from 'src/mock/invoice_creator';

import * as AccountRepository from 'src/repository/account';
import * as UserInfoRepository from 'src/repository/userInfo';
import * as UniversityRepository from 'src/repository/university';
import * as StudentRepository from 'src/repository/student';
import * as ErpRepository from 'src/repository/erp';

import hyperledger from 'src/hyperledger';
import { asyncForEach, extractIdFromHyperledger, keysToCamelCase } from 'src/utils/utils';
import InvoiceTransaction from 'src/classes/invoiceTransaction';
import InvoiceDTO from 'src/classes/dto/invoice/invoiceDTO';
import { redisGetAsync } from 'src/redis';
import { Provider } from 'src/api/erp/provider';
import decentureProvider from 'src/api/erp/decenture/decentureProvider';
import InvoiceERP from 'src/classes/dto/invoice/invoiceERP';

export class InvoiceService {
    static instance: InvoiceService = undefined;
    static erpProvider: Provider = undefined;

    constructor(provider: Provider) {
        if (InvoiceService.instance === undefined) {
            InvoiceService.erpProvider = provider;
            InvoiceService.instance = this;
        }
        return InvoiceService.instance;
    }

    @registerMethod()
    async getUserInvoices(user: any, token: string) {
        try {
            let redisUser = await redisGetAsync(token);
            redisUser = JSON.parse(redisUser);

            if (!redisUser || !redisUser.id) {
                return new Response({ status: 1, errMsg: 'User not found!' });
            }

            const userAccounts = await AccountRepository.getUserAccountsById(redisUser.id);
            const invoicesByAccounts = await InvoiceService.getInvoicesByAccounts(userAccounts);

            return new Response({ data: invoicesByAccounts });
        } catch (e) {
            console.log('ERROR getUserInvoices method', e);
            return new Response({ status: 1, errMsg: 'Application Error' });
        }
    }

    @registerMethod()
    async payInvoice(invoice: any, token: string) {
        try {
            let redisUser = await redisGetAsync(token);
            redisUser = JSON.parse(redisUser);

            if (!redisUser || !redisUser.id) {
                return new Response({ status: 1, errMsg: 'User not found!' });
            }

            const invoiceTransaction = new InvoiceTransaction(invoice);
            const personalAccount = await AccountRepository.getPersonalAccountByUser(redisUser.id);
            personalAccount.amount = parseInt(personalAccount.amount, 10);
            invoice.amount = parseInt(invoice.amount, 10);
            if (personalAccount.amount < invoice.amount) {
                return new Response({ status: 1, errMsg: 'Not enough money on account' });
            }

            invoiceTransaction.sender = personalAccount.id;
            const blockchainReponse = await hyperledger.sendTransaction(invoiceTransaction);

            // TODO: maybe need throw exception on hyperledger level
            if (blockchainReponse.error) {
                throw new Error('Hyperledger Error');
            }

            const student = await StudentRepository.getStudentByUserId(redisUser.id);
            InvoiceService.erpProvider.reconcileInvoicePayment(new InvoiceERP(invoice, student));

            await AccountRepository.updateDebitCredit(
                invoiceTransaction.receiver, personalAccount.id, blockchainReponse.amount
            );
            personalAccount.amount -= blockchainReponse.amount;

            return new Response({ data: { message: blockchainReponse } });
        } catch (e) {
            console.log('ERROR', e);
            return new Response({ status: 1, errMsg: 'Application Error' });
        }
    }

    @registerMethod()
    async getAllInvoices(data: any, token: string) {
        try {
            return new Response({ data: generateInvoicesByUserName('test') });
        } catch (e) {
            console.log('ERROR getAllInvoices method', e);
            return new Response({ status: 1, errMsg: 'Application Error' });
        }
    }

    @registerMethod()
    async getReconciledInvoices(data: any) {
        try {
            const invoices = await ErpRepository.getInvoices();
            const preparedInvoices = invoices
            .map(invoiceRow => JSON.parse(invoiceRow.invoice))
            .sort((a, b) => +new Date(b.TRAN_DT) - +new Date(a.TRAN_DT));

            return new Response({ data: preparedInvoices });
        } catch (e) {
            console.log('ERROR getReconciledInvoices method', e);
            return new Response({ status: 1, errMsg: 'Application Error' });
        }
    }

    static async getInvoicesByAccounts(accounts: any[]): Promise<InvoiceDTO[]> {
        const result: InvoiceDTO[] = [];

        await asyncForEach(accounts, async (account: any) => {
            const unpaidInvoices = await hyperledger.getUnpaidInvoicesByAccount(account.id);
            const resultArray: InvoiceDTO[] = await Promise.all<InvoiceDTO>(
                unpaidInvoices.map(async (invoice: any) => await InvoiceService.convertInvoiceToDTO(invoice, account)),
            );
            result.push(...resultArray);
        });

        return result.sort((a, b) => new Date(b.entryDate).getTime() - new Date(a.entryDate).getTime());
    }

    static async convertInvoiceToDTO(invoice: any, userAccount: any): Promise<InvoiceDTO> {
        const newInvoice = keysToCamelCase(invoice);
        const senderAccountId = extractIdFromHyperledger(newInvoice.billedFrom);

        const userInfo = await UserInfoRepository.getUserInfoById(userAccount.userId);
        const universityAccount = await AccountRepository.getAccount(senderAccountId);
        const university = await UniversityRepository.getUniversityById(universityAccount.id);

        const invoiceDTO = new InvoiceDTO(newInvoice);
        invoiceDTO.to = `${userInfo.firstName} ${userInfo.lastName}`;
        invoiceDTO.from = `${university.officialName} x ${university.address}`;
        return invoiceDTO;
    }
}

const invoiceService: InvoiceService = new InvoiceService(decentureProvider);

export default invoiceService;
