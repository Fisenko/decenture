import { registerMethod } from 'src/decorators';
import { Response } from 'src/classes/response';
import { buildTransactions } from 'src/mock/transactions_creator';
import hyperledger from 'src/hyperledger';
import { getUserAccountsByEmail, getAccountsByIds } from 'src/repository/account';
import AppUser from 'src/classes/appUser';
import Account from 'src/classes/account';
import { asyncForEach, extractIdFromHyperledger } from 'src/utils/utils';
import TransactionDTO from 'src/classes/dto/transactionDTO';
import Transaction from 'src/hyperledger/models/transaction';

const onlyUnique = (value: any, index: number, self: any) => {
    return self.indexOf(value) === index;
};

export class TransactionService {
    @registerMethod()
    async getUserTransactions(user: AppUser, token: string) {
        try {
            const accounts = await getUserAccountsByEmail(user.email);
            const transactions: Array<Transaction> = [];
            await asyncForEach(accounts, async (account: Account) => {
                const res = await hyperledger.getTransactionsByAccount(account.id);
                transactions.push(...res);
            });

            if (!transactions.length) {
                return new Response({ data: transactions });
            }

            const receiversIds = transactions.map(transaction => transaction.receiver.split('#').pop());
            const sendersIds = transactions.map(transaction => transaction.sender.split('#').pop());
            const uniqueAccountsIds = [...sendersIds, ...receiversIds].filter(onlyUnique);
            const uniqueAccounts: Array<Account> = await getAccountsByIds(uniqueAccountsIds);

            const result: Array<TransactionDTO> = transactions
                .map(transaction => {
                    const senderId = extractIdFromHyperledger(transaction.sender);
                    const receiverId = extractIdFromHyperledger(transaction.receiver);
                    const sender = uniqueAccounts.find(r => r.id === senderId);
                    const receiver = uniqueAccounts.find(r => r.id === receiverId);

                    return new TransactionDTO(transaction, sender, receiver);
                })
                .sort((a, b) => b.datetime.getTime() - a.datetime.getTime());

            return new Response({ data: result });
        } catch (e) {
            console.log('ERROR getTransaction method', e);
            return new Response({ status: 1, errMsg: 'Application Error' });
        }
    }

    @registerMethod()
    async getAllTransactions(data: any, token: string) {
        try {
            return new Response({ data: buildTransactions() });
        } catch (e) {
            console.log('ERROR getAllTransaction method', e);
            return new Response({ status: 1, errMsg: 'Application Error' });
        }
    }
}
