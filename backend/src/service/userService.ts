import dbc from 'src/db/db';
import * as userRepo from 'src/repository/user';
import * as userInfoRepo from 'src/repository/userInfo';
import * as accountRepo from 'src/repository/account';
import Account from 'src/classes/account';
import AppUser from 'src/classes/appUser';
import UserInfo from 'src/classes/userInfo';
import connectionPool from 'src/service/connectionPool';
import { ACCOUNT_TYPE, USER_ROLE, USER_STATUS } from 'src/db/mapping';
import { Login } from 'src/service/login';
import { Response } from 'src/classes/response';
import HyperledgerAccount from 'src/hyperledger/models/account';
import hyperledger from 'src/hyperledger';


const composeUserName = (user: any) => {
    return `${user.firstName} ${user.lastName}`;
};

const composeAccountName = (user: any) => {
    return `Personal wallet of ${composeUserName(user)}`;
};

// TODO CRUD Repository with generics fro student, university, government accounts. Extend them with CustomRepository,
export const createUser = async (userInfo: UserInfo) => {
    return dbc.tx(async t => {
        userInfo.roleId = userInfo.roleId || USER_ROLE.STUDENT;
        try {
            const result = await userRepo.createUser(userInfo, t);
            userInfo.id = result.id;
            userInfo.token = result.token;
            await userInfoRepo.createUserInfo(userInfo, t);
        } catch (ex) {
            throw ex;
        }
    });
};

export const verifyUser = async (user: AppUser) => {
    return dbc.tx(async t => {
        user.statusId = 1;
        userRepo.updateUserStatus(user, t);
        const userInfo: UserInfo = await userInfoRepo.getUserInfoById(user.id, t);

        let account = new Account({
            name: composeAccountName(userInfo),
            currency: 'GBP', // todo: !!!
            typeId: ACCOUNT_TYPE.PERSONAL, // todo: !!!
            statusId: user.statusId, // todo: !!!
            userId: user.id,
            ownerId: user.id,
        });

        // todo: optimisation is possible
        const escrowAccounts = await accountRepo.getStudentAccountsByEmail(user.email, t);
        if (escrowAccounts) {
            escrowAccounts.forEach((e: any) => {
                if (e.userId == null) {
                    e.userId = user.id;
                    accountRepo.updateAccountUser(e, t);
                }
            });
        }

        account = await accountRepo.createAccount(account, t);
        const hyperledgerAccount = new HyperledgerAccount(account);
        const hyperledgerResponse = await hyperledger.createAccount(hyperledgerAccount);

        if (hyperledgerResponse.error) {
            throw new Error(hyperledgerResponse.error.message);
        }

        const userSockets = connectionPool.getSocket(user.id);

        if (userSockets.length) {
            userSockets.forEach(item => {
                const loginResult = new Response({ data: Login.processSuccessfulLogin(user, item.token) });
                if (loginResult && item.socket) {
                    item.socket.emit('message',
                        JSON.stringify({
                            code: 'VERIFIED_USER_RESPONSE',
                            ...loginResult
                        }));
                }
            });
        }

        return user;
    });
};

export const verifyUserByToken = async (token: string) => {
    const user: AppUser = await userRepo.getUserByTokenAndStatus(token, USER_STATUS.NOT_ACTIVATED);
    return user ? verifyUser(user) : null;
};
