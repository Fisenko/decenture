import AppUser from 'src/classes/appUser';
import UserInfo from 'src/classes/userInfo';
import connectionPool from 'src/service/connectionPool';
import { sendVerificationEmail } from 'src/mail/userVerificationEmail';
import * as UserRepository from 'src/repository/user';
import * as userService from './userService';
import { registerMethod } from 'src/decorators';
import { Response } from 'src/classes/response';
import { APP_ROLES_MAPPING, USER_STATUS } from 'src/db/mapping';
import { validateEmail } from 'src/utils/utils';

// TODO: Add logging
export class Registration {
    @registerMethod()
    async registration(data: any, token: string, socket?: any) {
        try {
            const userInfo: UserInfo = new UserInfo(data);
            const userByEmail: AppUser = await UserRepository.getUserByEmailAndPassword(userInfo);
            if (userByEmail) {
                return new Response({ status: 1, errMsg: 'Login with current email already exists' });
            } else {
                return Registration.saveUser(userInfo, token, socket)
                    ? new Response({ data: { message: 'Student successfully saved' } })
                    : new Response({ status: 1, errMsg: 'Wrong login or password' });
            }
        } catch (Error) {
            return new Response({ status: 1, errMsg: 'Application Error' });
        }
    }

    static async saveUser(userInfo: UserInfo, token: string, socket?: any) {
        userInfo.roleId = APP_ROLES_MAPPING.STUDENT;
        userInfo.statusId = USER_STATUS.NOT_ACTIVATED;
        await userService.createUser(userInfo);
        sendVerificationEmail(userInfo);
        connectionPool.addSocket(userInfo.id, { token: token, socket: socket });

        return userInfo.id;
    }

    @registerMethod()
    async checkUserExists(user: any, token?: string) {
        try {
            const userInfo: UserInfo = new UserInfo(user);
            if (!validateEmail(userInfo.email)) {
                return new Response({ status: 1, errMsg: 'Email is invalid' });
            }

            const userByEmail: AppUser = await UserRepository.getUserByEmail(userInfo);

            return userByEmail
                ? new Response({ status: 1, errMsg: 'Login with current email already exists' })
                : new Response();
        } catch (Error) {
            return new Response({ status: 1, errMsg: 'Application Error' });
        }
    }
}
