import cron from 'cron';
import attendance from './attendanceService';
import payment from './paymentService';

const CronJob = cron.CronJob;

const cronJobs: Array<any> = [
    new CronJob('0 0 * * * *', attendance.completeScheduledLectures, null, true),
    new CronJob('0 10 0 * * *', attendance.updateMonthSnapshot, null, true),
    new CronJob('0 20 0 * * *', payment.processEscrowAccounts, null, true),
];

export default cronJobs;
