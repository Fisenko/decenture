import dbc from 'src/db/db';
import { registerMethod } from 'src/decorators';
import { Response } from 'src/classes/response';
import { redisGetAsync } from 'src/redis';
import { ATTENDANCE_STATUS } from 'src/db/mapping';
import hyperledger from 'src/hyperledger';

import Attendance from 'src/hyperledger/models/attendance';
import AttendanceSnapshot from 'src/classes/attendanceSnapshot';

import * as studentRepo from 'src/repository/student';
import * as scheduledAttendanceRepo from 'src/repository/scheduledAttendance';
import { AttendanceFilter } from 'src/repository/scheduledAttendance';
import * as attendanceSnapshotRepo from 'src/repository/attendanceSnapshot';
import * as semesterRepository from 'src/repository/semester';
import analyticsRepository from 'src/repository/analyticsRepository';
import AttendanceCompletionDTO from 'src/classes/dto/attendanceCompletionDTO';
import { keysToCamelCase } from 'src/utils/utils';
import UserAttendance from 'src/classes/attendance';
import StudentAnalytics from 'src/classes/analytics/studentAnalytics';
import { compareAttendanceDateTime, convertAbsenceToDTO } from 'src/utils/attendance_utils';
import Lecture from 'src/classes/lecture';
import LectureAttendanceDTO from 'src/classes/dto/lectureAttendanceDTO';

import moment from 'moment';

// todo: In the future extract to config.
const INTERVAL_2H = 17200;

export class AttendanceService {
    static instance: AttendanceService = undefined;

    constructor() {
        if (AttendanceService.instance === undefined) {
            AttendanceService.instance = this;
        }
        return AttendanceService.instance;
    }

    @registerMethod()
    async getUserAttendance(data: any, token: string) {
        try {
            const redisUser = JSON.parse(await redisGetAsync(token));
            const student = await studentRepo.getStudentByUserId(redisUser.id);
            if (!student) {
                return new Response({ status: 1, errMsg: 'Student not found' });
            }

            const timestamp = moment.utc().unix();
            const universityId = student.universityId;

            const semester = await semesterRepository.getSemester({ timestamp, universityId });

            const attendanceFilter = {
                studentId: student.id,
                universityId: student.universityId,
                beginDate: new Date(semester.begin_date).getTime() / 1000,
                endDate: new Date(semester.end_date).getTime() / 1000,
            };

            const scheduledAttendance = await scheduledAttendanceRepo.findManyByFilter(attendanceFilter);
            const history: Array<LectureAttendanceDTO> = scheduledAttendance
            .map((scheduledAttendance: any) => new LectureAttendanceDTO(scheduledAttendance))
            .sort(compareAttendanceDateTime);
            const absence: Array<Lecture> = await AttendanceService.getUserAbsence(attendanceFilter);

            let rowsPercentage = await analyticsRepository.getStudentsAnalyticsBySemesterAndLectureCourse(
                attendanceFilter,
            );
            const percentage: Array<StudentAnalytics> = rowsPercentage.map(keysToCamelCase);

            const attendance = new UserAttendance({ history, absence, percentage });

            return new Response({ data: attendance });
        } catch (err) {
            console.log('getUserAttendance Error:', err);
            return new Response({ status: 1, errMsg: 'Application Error' });
        }
    }

    @registerMethod()
    async recordAttendance(data: any, token: string) {
        try {
            const redisUser = JSON.parse(await redisGetAsync(token));

            if (redisUser && redisUser.id) {
                const student = await studentRepo.getStudentByUserId(redisUser.id);
                const currentTimestamp = moment.utc().unix();
                const scheduledAttendanceFilter = {
                    nfcId: data.nfcId,
                    timestamp: currentTimestamp,
                    studentId: student.id,
                };
                const scheduledAttendance = await scheduledAttendanceRepo.findByNfcIdAndTimestampAndStudentId(
                    scheduledAttendanceFilter,
                );

                // TODO: ЗАЩИТА ОТ СПАМА, РАБОТАЕТ ЕСЛИ ЛЕКЦИЮ НЕ ПЕРЕНЕСЛИ И СТУДЕН ЧЕКАЕТСЯ НА СВОЕЙ ЛЕКЦИИ
                // TODO: ИНАЧЕ НУЖНО ПРИДУМЫВАТЬ ВЕЛОСИПЕД... !!!
                if (!scheduledAttendance || !scheduledAttendance.statusId) {
                    const hyperledgerAttendance = await hyperledger.attendance(
                        new Attendance({
                            student_id: student.id,
                            nfc_id: data.nfcId,
                            location: 'unknown',
                            erp_email: student.erpEmail,
                            erp_student_id: student.erpStudentId,
                            first_name: student.firstName,
                            last_name: student.lastName,
                            notes: '',
                        }),
                    );

                    if (scheduledAttendance) {
                        await dbc.tx(async t => {
                            scheduledAttendance.statusId = ATTENDANCE_STATUS.VISITED;
                            scheduledAttendance.transactionId = hyperledgerAttendance.transactionId;
                            scheduledAttendance.timestamp = currentTimestamp; // TODO: replace with timestamp from hyperledger response which is undefined now (oO)...

                            await scheduledAttendanceRepo.update(scheduledAttendance, t);

                            const attendanceSnapshot = new AttendanceSnapshot({
                                studentId: scheduledAttendance.studentId,
                                lectureCourseId: scheduledAttendance.lectureCourseId,
                                visited: 1,
                                lastUpdate: scheduledAttendance.timestamp,
                                snapshotDate: currentTimestamp,
                            });

                            await attendanceSnapshotRepo.updateOnNfcScan(attendanceSnapshot, t);
                        });
                    }
                } else {
                    return new Response({ status: 1, errMsg: 'You have already checked in this lecture' });
                }

                return new Response({});
            } else {
                return new Response({ status: 2, errMsg: 'Critical error' });
            }
        } catch (error) {
            console.log('recordAttendance Error:', error);
            return new Response({ status: 1, errMsg: 'Application Error' });
        }
    }

    async completeScheduledLectures() {
        const startDate = new Date();
        const endDate = startDate.getTime() / 1000; // for postgresql to_timestamp
        const beginDate = endDate - INTERVAL_2H;

        console.log(
            `${startDate} : Running scheduled job for lectures completion and attendance calculation (absences)...`,
        );
        const result = await attendanceSnapshotRepo.updateCompleted(beginDate, endDate);

        const completionDate = new Date();
        console.log(
            `${completionDate} : Successfully completed scheduled job for lectures completion and attendance calculation (absences).\n   Execution time : ${completionDate.getTime() -
            startDate.getTime()} ms.`,
        );
        result.forEach((i: any) => {
            console.log(`   ${i.cnt} rows affected in ${i.table_name}`);
        });
    }

    async updateMonthSnapshot() {
        const startDate = new Date();
        console.log(`${startDate} : Running scheduled job for month snapshot calculation...`);

        const snapshotDate = startDate.getTime() / 1000;
        const result = await attendanceSnapshotRepo.updateMonth(snapshotDate);
        const completionDate = new Date();

        console.log(
            `${completionDate} : Successfully completed scheduled job for month snapshot calculation...\n   Execution time : ${completionDate.getTime() -
            startDate.getTime()} ms.`,
        );
        result.forEach((i: any) => {
            console.log(`   ${i.cnt} rows affected (attendance_snapshot).`);
        });
    }

    // todo: this is mock for lecture schedule updates
    async confirmScheduledLecture(id: string) {
        return { mock: 'mock' };
    }

    static async getUserAbsence(filter: AttendanceFilter) {
        try {
            return await dbc.tx(async tx => {
                const absenceLectures = await scheduledAttendanceRepo.findAbsenceByFilter(filter, tx);
                return convertAbsenceToDTO(absenceLectures);
            });
        } catch (Error) {
            console.log('ERROR IN getUserAbsence', Error);
            return [];
        }
    }

    static async getUserAttendancePercentage(studentId: string) {
        try {
            return await dbc.tx(async tx => {
                const absenceLectures = await scheduledAttendanceRepo.findAttendanceCompletionByStudentId(
                    studentId,
                    tx,
                );
                return absenceLectures.map((lecture: any) => {
                    const value = (lecture.visited / lecture.total) * 100;
                    return new AttendanceCompletionDTO(value, lecture.code);
                });
            });
        } catch (Error) {
            console.log('ERROR IN getUserAbsence', Error);
            return [];
        }
    }
}

const attendanceService: AttendanceService = new AttendanceService();

export default attendanceService;
