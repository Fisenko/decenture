import { registerMethod } from 'src/decorators';
import { Response } from 'src/classes/response';
import AppUser from 'src/classes/appUser';
import { redisGetAsync } from 'src/redis';
import { getUniversityByUserId } from 'src/repository/university';

export class UniversityService {
    @registerMethod()
    async getUniversity(data: any, token: string) {
        try {
            const redisUser: AppUser = JSON.parse(await redisGetAsync(token));
            const university = await getUniversityByUserId(redisUser.id);

            return new Response({ data: university });
        } catch (e) {
            console.log('ERROR getUniversity method', e);
            return new Response({ status: 1, errMsg: 'Application Error' });
        }
    }
}
