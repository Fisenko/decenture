import hyperledger from 'src/hyperledger';
import { registerMethod } from 'src/decorators';
import { redisGetAsync } from 'src/redis';
import { Response } from 'src/classes/response';
import * as AccountRepository from 'src/repository/account';
import { getAccountByUser, getSponsoredStudentAccounts } from 'src/repository/account';
import * as studentRepository from 'src/repository/student';
import Account from 'src/classes/account';
import InvoiceDTO from 'src/classes/dto/invoice/invoiceDTO';
import { Payment } from 'src/classes/payment';
import { extractIdFromHyperledger, keysToCamelCase } from 'src/utils/utils';
import { InvoiceStatus } from 'src/classes/invoiceStatus';
import InvoiceTransaction from 'src/classes/invoiceTransaction';
import erpProvider from 'src/api/erp/decenture/decentureProvider';
import InvoiceERP from 'src/classes/dto/invoice/invoiceERP';
import { getAllStudentsByUniversityId } from 'src/repository/studentRepository';
import { Student } from 'src/classes/student';
import { INTERNATIONAL } from 'src/db/mapping';
import Transaction from 'src/hyperledger/models/transaction';

export class PaymentService {

    static instance: PaymentService = undefined;

    constructor() {
        if (PaymentService.instance === undefined) {
            PaymentService.instance = this;
        }
        return PaymentService.instance;
    }

    @registerMethod()
    async getPaymentByUniversity(data: any, token: string) {

        // TODO simple mock but need real data
        let todaysBeginningBalanceMock: number = 11000;
        let monthsBeginningBalanceMock: number = 6000;

        try {
            let redisUser = await redisGetAsync(token);
            redisUser = JSON.parse(redisUser);

            if (!redisUser || !redisUser.id) {
                return new Response({ status: 1, errMsg: 'User not found!' });
            }

            const account: Account = await getAccountByUser(redisUser.id);
            const students: Array<Student> = await getAllStudentsByUniversityId(account.id);
            const allUniversitiesInvoices: Array<InvoiceDTO> = await hyperledger.getInvoicesByAccount(account.id);
            const accountHyperledger: Account = await hyperledger.getAccountById(account.id);

            const payment = new Payment();
            payment.totalBalance = accountHyperledger.amount;
            payment.todaysBeginningBalance = todaysBeginningBalanceMock;
            payment.monthsBeginningBalance = monthsBeginningBalanceMock;

            allUniversitiesInvoices.forEach(currInvoice => {
                let invoice = new InvoiceDTO(keysToCamelCase(currInvoice));
                let student = students.find((currStudent) => currStudent.id === invoice.billedTo);

                if (student) {
                    invoice.isInternational = INTERNATIONAL.get(student.isInternational);
                    invoice.degreeName = student.degreeName;
                }

                switch (invoice.status) {
                    case InvoiceStatus.NOT_PAID:
                        payment.unpaidInvoicesSumAmount += invoice.amount;
                        payment.unpaidInvoices.push(invoice);
                        ++payment.unpaidInvoicesCount;
                        break;
                    case InvoiceStatus.PAID:
                        payment.paidInvoicesSumAmount += invoice.amount;
                        payment.paidInvoices.push(invoice);
                        ++payment.paidInvoicesCount;
                        break;
                    default:
                        console.log('INVOICE STATUS UNKNOWN: ', invoice.status);
                }
            });

            if (payment) {
                return new Response({ status: 0, data: payment });
            } else {
                return new Response({ status: 1, errMsg: 'Payment not found!' });
            }
        } catch (e) {
            console.log('ERROR:', e);
            return new Response({ status: 1, errMsg: 'Application Error' });
        }

    }

    static async payInvoiceFromAccount(invoice: any, account: any) {
        account.amount = parseInt(account.amount, 10);
        invoice.amount = parseInt(invoice.amount, 10);
        if (account.amount < invoice.amount) {
            throw new Error('Not enough money on account');
        }

        const invoiceTransaction = new InvoiceTransaction(invoice);
        invoiceTransaction.receiver = extractIdFromHyperledger(invoice.billedFrom);
        invoiceTransaction.sender = account.id;
        invoiceTransaction.invoice = invoice.id;

        console.log('Creating blockchain transaction...');
        const result = await hyperledger.sendTransaction(invoiceTransaction);

        if (result.error) {
            throw new Error(
                `Hyperledger Error:
                   invoiceTransaction = ${JSON.stringify(invoiceTransaction)}
                   invoice = ${JSON.stringify(invoice)}\n`
            );
        }
        console.log('Blockchain transaction created successfully.');

        result.amount = parseInt(result.amount, 10);
        await AccountRepository.updateDebitCredit(
            invoiceTransaction.receiver, invoiceTransaction.sender, result.amount
        );
        account.amount -= result.amount;

        const student = await studentRepository.getStudentById(extractIdFromHyperledger(invoice.billedTo));

        if (student) {
            console.log('Sending invoice data to ERP...');
            erpProvider.reconcileInvoicePayment(new InvoiceERP(invoice, student));
        }

        return { invoice: invoice, account: account };
    }

    @registerMethod()
    async sendFunds(transaction: Transaction, token: string) {
        try {
            let redisUser = await redisGetAsync(token);
            redisUser = JSON.parse(redisUser);

            if (!redisUser || !redisUser.id) {
                return new Response({ status: 1, errMsg: 'User not found!' });
            }

            const senderPersonalAccount = await AccountRepository.getPersonalAccountByUser(redisUser.id);
            const receiverPersonalAccount = await AccountRepository.getPersonalAccountByEmail(transaction.receiver);

            // TODO clean hotfix and fix it
            senderPersonalAccount.amount = parseInt(senderPersonalAccount.amount, 10);
            receiverPersonalAccount.amount = parseInt(receiverPersonalAccount.amount, 10);

            //TODO: extract validation to some place for validator
            if (senderPersonalAccount.amount < transaction.amount) {
                return new Response({ status: 1, errMsg: 'Not enough money on account' });
            }

            if (!receiverPersonalAccount || senderPersonalAccount.id === receiverPersonalAccount.id) {
                return new Response({ status: 1, errMsg: 'Invalid recipient account' });

            }

            transaction.sender = senderPersonalAccount.id;
            transaction.receiver = receiverPersonalAccount.id;
            const blockchainResponse = await hyperledger.sendTransaction(transaction);

            if (blockchainResponse.error) {
                throw new Error('Hyperledger Error');
            }


            await AccountRepository.updateDebitCredit(
                transaction.receiver, senderPersonalAccount.id, blockchainResponse.amount
            );
            senderPersonalAccount.amount -= blockchainResponse.amount;

            return new Response({ data: { message: blockchainResponse } });
        } catch (e) {
            console.log('ERROR', e);
            return new Response({ status: 1, errMsg: 'Application Error' });
        }
    }

    static async processEscrowAccount(account: any) {
        const unpaidInvoices: Array<any> = (await hyperledger.getUnpaidInvoicesByAccount(account.id))
        .filter((invoice: any) => {
            return invoice.account_type_sf === 'TUT' || invoice.account_type_sf === 'ENR';
        })
        .sort((a: any, b: any) => {
            return b.amount - a.amount;
        });

        if (unpaidInvoices && unpaidInvoices.length) {
            console.log('processEscrowAccount.unpaidInvoices.length', unpaidInvoices.length);
            for (let i = 0; i < unpaidInvoices.length; i++) {
                const invoice = keysToCamelCase(unpaidInvoices[i]);
                console.log(`processEscrowAccount: invoice[${i}] ${invoice.id}, ${invoice.amount} ...`);
                try {
                    if (account.amount >= invoice.amount) {
                        await PaymentService.payInvoiceFromAccount(invoice, account);
                        console.log(`processEscrowAccount: invoice[${i}] - OK.`);
                    } else {
                        console.log(`Insufficient funds on "${account.name}", actual: ${account.amount}, required: ${invoice.amount}`);
                    }
                } catch (e) {
                    console.error('ERROR in processEscrowAccount method: ', e);
                }
            }
        }
    }

    async processEscrowAccounts() {
        const startDate = new Date();
        console.log(`${startDate} : Running scheduled job for month payInvoiceEvent...`);

        try {
            const escrowAccounts: Array<any> = await getSponsoredStudentAccounts();
            console.log(`[payInvoiceEvent]: ${escrowAccounts.length} escrow accounts for sponsored students found...`);
            if (escrowAccounts) {
                for (let i = 0; i < escrowAccounts.length; i++) {
                    console.log(`processEscrowAccounts: account[${i}] ${escrowAccounts[i].name}, ${escrowAccounts[i].amount} ...`);
                    if (escrowAccounts[i].amount > 0) {
                        await PaymentService.processEscrowAccount(escrowAccounts[i]);
                        console.log(`processEscrowAccounts: account[${i}] - OK.`);
                    }
                }
            }
        } catch (e) {
            console.log('ERROR in processEscrowAccounts method:', e);
        }

        const completionDate = new Date();
        console.log(
            `${completionDate} : Successfully completed scheduled job for payInvoiceEvent...
            Execution time : ${completionDate.getTime() - startDate.getTime()} ms.`
        );
    }

}

const paymentService: PaymentService = new PaymentService();

export default paymentService;
