import { registerMethod } from 'src/decorators';
import repo from 'src/repository/analyticsRepository';
import { Response } from 'src/classes/response';
import AppUser from 'src/classes/appUser';
import { redisGetAsync } from 'src/redis';
import { getUserAccountsById } from 'src/repository/account';
import { keysToCamelCase } from 'src/utils/utils';
import { INTERNATIONAL } from 'src/db/mapping';
import { AnalyticsSearchCriteria } from 'src/classes/AnalyticsSearchCriteria';

export class AnalyticsService {
    static instance: AnalyticsService = undefined;

    constructor() {
        if (AnalyticsService.instance === undefined) {
            AnalyticsService.instance = this;
        }
        return AnalyticsService.instance;
    }

    @registerMethod()
    async getStudentsAnalyticsBySemester(data: any, token: string) {
        try {
            const redisUser: AppUser = JSON.parse(await redisGetAsync(token));
            const accounts = await getUserAccountsById(redisUser.id);
            if (!accounts.length) {
                return new Response({ status: 1, errMsg: 'Accounts are missing' });
            }
            const account = accounts.find(acc => acc.userId === redisUser.id);
            if (!account) {
                return new Response({ status: 1, errMsg: 'Account is missing' });
            }
            const analytics = await repo.getStudentsAnalyticsBySemester(account.id, new Date().getTime() / 1000);

            const convertedAnalytics = analytics.map(keysToCamelCase);

            return new Response({ data: convertedAnalytics });
        } catch (error) {
            console.log('ERROR getStudentsAnalyticsBySemester method', error);
            return new Response({ status: 1, errMsg: 'Application Error' });
        }
    }

    @registerMethod()
    async getUniversityAnalyticsByYear(data: any, token: string) {
        try {
            const redisUser: AppUser = JSON.parse(await redisGetAsync(token));
            const accounts = await getUserAccountsById(redisUser.id);
            if (!accounts.length) {
                return new Response({ status: 1, errMsg: 'Accounts are missing' });
            }
            const account = accounts.find(acc => acc.userId === redisUser.id);
            if (!account) {
                return new Response({ status: 1, errMsg: 'Account is missing' });
            }
            const analytics = await repo.getUniversityAnalyticsByYear(account.id);

            const convertedAnalytics = analytics.map((item: any) => {
                const date = new Date(item.report_date);
                item.report_date = date.getFullYear() + '';
                return keysToCamelCase(item);
            });

            return new Response({ data: analytics });
        } catch (error) {
            console.log('ERROR getUniversityAnalyticsByYear method', error);
            return new Response({ status: 1, errMsg: 'Application Error' });
        }
    }

    @registerMethod()
    async getStudentsAnalytics(data: any, token: string) {
        try {
            const redisUser: AppUser = JSON.parse(await redisGetAsync(token));
            const accounts = await getUserAccountsById(redisUser.id);
            if (!accounts.length) {
                return new Response({ status: 1, errMsg: 'Accounts are missing' });
            }
            const account = accounts.find(acc => acc.userId === redisUser.id);
            if (!account) {
                return new Response({ status: 1, errMsg: 'Account is missing' });
            }
            const analytics = await repo.getStudentsAnalytics(account.id);

            // TODO: convert in repo
            const convertedAnalytics = analytics.map(keysToCamelCase);

            return new Response({ data: convertedAnalytics });
        } catch (error) {
            console.log('ERROR getStudentsAnalytics method', error);
            return new Response({ status: 1, errMsg: 'Application Error' });
        }
    }

    @registerMethod()
    async getUniversityAnalytics(data: any, token: string) {
        try {
            const redisUser: AppUser = JSON.parse(await redisGetAsync(token));
            const accounts = await getUserAccountsById(redisUser.id);
            if (!accounts.length) {
                return new Response({ status: 1, errMsg: 'Accounts are missing' });
            }
            const account = accounts.find(acc => acc.userId === redisUser.id);
            if (!account) {
                return new Response({ status: 1, errMsg: 'Account is missing' });
            }
            const analytics = await repo.getUniversityAnalytics(account.id);

            // TODO: convert in repo
            const convertedAnalytics = keysToCamelCase(analytics);

            return new Response({ data: convertedAnalytics });
        } catch (error) {
            console.log('ERROR getUniversityAnalytics method', error);
            return new Response({ status: 1, errMsg: 'Application Error' });
        }
    }

    @registerMethod()
    async getUniversityAttendance(data: any, token: string) {
        try {
            const redisUser: AppUser = JSON.parse(await redisGetAsync(token));
            const accounts = await getUserAccountsById(redisUser.id);
            if (!accounts.length) {
                return new Response({ status: 1, errMsg: 'Accounts are missing' });
            }
            const account = accounts.find(acc => acc.userId === redisUser.id);
            if (!account) {
                return new Response({ status: 1, errMsg: 'Account is missing' });
            }
            const result = await repo.getUniversityAttendance(new AnalyticsSearchCriteria(data));

            const res = result.map((item: any) => {
                item.is_international = INTERNATIONAL.get(item.is_international);
                return item;
            });
            return new Response({ data: res });
        } catch (err) {
            console.log('getUniversityAttendance', err);
            return new Response({ status: 1, errMsg: 'Get University Attendance method error' });
        }
    }

    @registerMethod()
    async getCourseCompletion(data: any, token: string) {
        try {
            const redisUser: AppUser = JSON.parse(await redisGetAsync(token));
            const accounts = await getUserAccountsById(redisUser.id);
            if (!accounts.length) {
                return new Response({ status: 1, errMsg: 'Accounts are missing' });
            }
            const account = accounts.find(acc => acc.userId === redisUser.id);
            if (!account) {
                return new Response({ status: 1, errMsg: 'Account is missing' });
            }

            const result = await repo.getUniversityCompliance(new AnalyticsSearchCriteria(data));

            const res = result.map((item: any) => {
                item.is_international = INTERNATIONAL.get(item.is_international);
                return item;
            });
            return new Response({ data: res });
        } catch (err) {
            console.log('getUniversityAttendance', err);
            return new Response({ status: 1, errMsg: 'Get University Attendance method error' });
        }
    }
}

const analyticsService: AnalyticsService = new AnalyticsService();

export default analyticsService;
