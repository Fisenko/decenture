import { registerMethod } from 'src/decorators';
import { Response } from 'src/classes/response';
import { getUserAccountsById } from 'src/repository/account';
import AppUser from 'src/classes/appUser';
import { redisGetAsync } from 'src/redis';

export class AccountService {
    @registerMethod()
    async getUserAccounts(user: AppUser, token: string) {
        try {
            const redisUser: AppUser = JSON.parse(await redisGetAsync(token));
            const accounts = await getUserAccountsById(redisUser.id);

            return new Response({ data: accounts });
        } catch (e) {
            console.log('ERROR getUserAccounts method', e);
            return new Response({ status: 1, errMsg: 'Application Error' });
        }
    }

    @registerMethod()
    async getAccounts(user: AppUser, token: string) {
        try {
            return new Response({ data: {} });
        } catch (e) {
            console.log('ERROR getUserAccounts method', e);
            return new Response({ status: 1, errMsg: 'Application Error' });
        }
    }
}
