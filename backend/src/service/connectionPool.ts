import { redisGetAsync } from 'src/redis';

export interface IConnectionPoolItem {
    token: string,
    socket: any,
}

export class ConnectionPoolItem implements IConnectionPoolItem {
    token: string;
    socket: any;

    constructor(token: string, socket: any) {
        this.token = token;
        this.socket = socket;
    }
}

export class ConnectionPool {

    private static instance: ConnectionPool = undefined;

    private readonly store: { [key: string]: Array<IConnectionPoolItem> };

    constructor() {
        if (ConnectionPool.instance === undefined) {
            ConnectionPool.instance = this;
            this.store = {};
        }
        return ConnectionPool.instance;
    }

    public addSocket(key: string, value: IConnectionPoolItem) {
        if (key in this.store) {
            const item = this.store[key];
            const found = item.find(v => v.socket.id === value.socket.id);
            if (!found) {
                item.push(value);
            }
        } else {
            this.store[key] = [value];
        }
    }

    public removeSocket(key: string, value: IConnectionPoolItem) {
        if (key in this.store) {
            const list = this.store[key];
            for (let i = 0; i < list.length; i++) {
                if (!list[i].socket || list[i].socket === value.socket) {
                    list.splice(i, 1);
                    break;
                }
            }
            if (list.length == 0) {
                delete this.store[key];
            }
        }
    }

    public async removeSocketBySessionToken(token: string, socket: any) {
        if (token) {
            const redisUser = JSON.parse(await redisGetAsync(token));
            if (redisUser.id) {
                const cpi = this.getSocket(redisUser.id);
                cpi.forEach(i => {
                    this.removeSocket(redisUser.id, { token, socket });
                });
            }
        }
    }


    public getSocket(key: string): Array<any> {
        return (key in this.store) ? this.store[key] : [];
    }

    // todo: Dev tool. Remove in production!!!
    public getStore() {
        return { ...this.store };
    }

}

const connectionPool = new ConnectionPool();
export default connectionPool;
