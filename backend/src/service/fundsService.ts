import { registerMethod } from 'src/decorators';
import { Card } from 'src/classes/card';
import * as FundsRepository from 'src/repository/funds';
import { Response } from 'src/classes/response';
import { redisClient, redisGetAsync } from 'src/redis';
import { Deposit } from 'src/classes/deposit';
import { keysToCamelCase } from 'src/utils/utils';
import hyperledger from 'src/hyperledger';
import { addAccountAmount, getPersonalAccountByUser } from 'src/repository/account';

export class FundsService {
    @registerMethod()
    async saveCard(card: any, token: string) {
        try {
            let redisUser = JSON.parse(await redisGetAsync(token));

                if (redisUser && redisUser.id) {
                    const currentCard = new Card(card);

                    const cardTypeId = await FundsRepository.getCardTypeIdByKey(currentCard.paymentSystemTypeKey);

                    if (!cardTypeId) {
                        return new Response({ status: 1, errMsg: 'Card type not found!' });
                    }

                    const row = await FundsRepository.saveCard({
                        ...currentCard,
                        paymentSystemTypeId: cardTypeId.id,
                        userId: redisUser.id,
                    });

                    if (!row) {
                        return new Response({ status: 1, errMsg: 'Incorrect card info!' });
                    }

                    if (row.id) {
                        return new Response({ status: 0, data: { id: row.id } });
                    } else {
                        console.log('Error: ', row);
                        return new Response({ status: 1, errMsg: 'Application error!' });
                    }
                }
        } catch (err) {
            return new Response({ status: 1, errMsg: err.message });
        }
    }

    @registerMethod()
    async getCardsNumberByAppUserToken(data: any, token: string) {
        return new Promise((resolve, reject) => {
            redisClient.get(token, async function (err: any, result: any) {
                const cardsList: Array<Card> = [];
                result = JSON.parse(result);
                if (result && result.id) {
                    const row = await FundsRepository.getCardsNumberByAppUserId(result.id);
                    if (row) {
                        row.forEach(function (dbvalue: any) {
                            let value = keysToCamelCase(dbvalue);
                            let fullCardNumber = value.cardNumber.trim();
                            let lastFourCardNumberDigits = fullCardNumber.slice(
                                fullCardNumber.length - 4, fullCardNumber.length
                            );
                            const card = new Card({
                                id: value.id,
                                number: lastFourCardNumberDigits,
                                name: value.holder
                            });
                            cardsList.push(card);
                        });
                        resolve(new Response({ data: cardsList }));
                    } else {
                        resolve(new Response({ status: 1 }));
                    }
                }
            });
        });
    }

    @registerMethod()
    async addDepositAmountToPersonalAccountByAppUserId(deposit: any, token: string) {
        if (deposit.amount <= 0) {
            return new Response({ status: 1, errMsg: 'Incorrect amount' });
        }
        const depositCurrent: Deposit = new Deposit(deposit);
        try {
            let redisUser = await redisGetAsync(token);
            redisUser = JSON.parse(redisUser);

            if (redisUser && redisUser.id) {
                const card = await FundsRepository.checkCorrectCardOwnershipByAppUserIdAndCardId(
                    redisUser.id,
                    depositCurrent.cardId,
                );

                if (!card) {
                    return new Response({ status: 1, errMsg: 'This card is not owned by the user!' });
                }
                const account = await getPersonalAccountByUser(redisUser.id);

                const transaction = await initiateTransactionFromBankByCardIdToOurBankPartner(
                    account.id,
                    deposit.amount,
                );

                if (!transaction.transactionId) {
                    return new Response({ status: 1, errMsg: 'Process was unsuccessful!' });
                }

                console.log('Process successfully!');

                const newAmount = await addAccountAmount(account.id, depositCurrent.amount);
                console.log('Deposit was added!');

                if (newAmount >= depositCurrent.amount) {
                    return new Response({ status: 0, errMsg: '', data: { amount: newAmount.amount } });
                } else {
                    return new Response({ status: 1, errMsg: 'Incorrect app user or amount' });
                }
            }
        } catch (error) {
            console.log(error);
            return new Response({ status: 1, errMsg: 'Failed to add deposit!' });
        }
    }

    @registerMethod()
    async getAmountByToken(data: any, token: string) {
        try {
            let redisUser = await redisGetAsync(token);
            redisUser = JSON.parse(redisUser);
            if (redisUser && redisUser.id) {
                const amount = await FundsRepository.getAccountAmountByUserId(redisUser.id);
                return new Response({ status: 0, data: { amount } });
            }
        } catch (e) {
            console.log('ERROR:', e);
            return new Response({ status: 1, errMsg: 'Application Error' });
        }
    }
}

async function initiateTransactionFromBankByCardIdToOurBankPartner(accountId: string, amount: number) {
    return await hyperledger.accountTransaction({
        account: accountId,
        amount: amount,
    });
}
