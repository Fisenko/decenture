import redis from 'redis';
import AppUser from 'src/classes/appUser';
import RedisUser from 'src/classes/redisUser';
import * as UserRepository from 'src/repository/user';
import { redisClient, redisGetAsync } from 'src/redis';
import { Response } from 'src/classes/response';
import { registerMethod } from 'src/decorators';
import { keysToCamelCase } from 'src/utils/utils';
import connectionPool from 'src/service/connectionPool';

export class Login {
    public static processSuccessfulLogin(appUser: AppUser, token: string): Response {
        const result = keysToCamelCase(appUser);
        delete result.password;
        const redisUser = new RedisUser(result);
        redisClient.set(token, JSON.stringify(redisUser), redis.print);
        delete result.password;
        result.isAuth = true;
        return result;
    }

    @registerMethod()
    async login(data: any, token: string, socket?: any) {
        try {
            const appUser: AppUser = new AppUser(data);
            const row = await UserRepository.getUserByEmailAndPassword(appUser);
            if (row && row.statusId) {
                const loginResult = Login.processSuccessfulLogin(row, token);
                connectionPool.addSocket(row.id, { token: token, socket: socket });
                return new Response({ data: loginResult });
            } else {
                return new Response({ status: 1, errMsg: 'Wrong login or password' });
            }
        } catch (e) {
            console.log('ERROR login method', e);
        }
    }

    @registerMethod()
    async checkLogin(data: any, token: string, socket?: any) {
        let result = await redisGetAsync(token);
        result = JSON.parse(result);

        if (result && result.id) {
            const row = await UserRepository.getUserById(result.id);
            if (row) {
                const newData = keysToCamelCase(row);
                delete newData.password;
                newData.isAuth = true; // TODO: Remove it
                connectionPool.addSocket(row.id, { token: token, socket: socket });
                return new Response({ data: newData });
            }
        }
        return new Response({ status: 1, errMsg: 'Could not check login' });
    }

    @registerMethod()
    async logout(data: any, token: string, socket?: any) {
        await connectionPool.removeSocketBySessionToken(token, socket);
        const response = redisClient.set(token, JSON.stringify(new RedisUser()));
        return new Response({ data: response });
    }
}
