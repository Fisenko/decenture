import dbc from 'src/db/db';
import * as SEMESTER_QUERIES from 'src/db/queries/semester';

export type semesterFilter = {
  timestamp: number;
  universityId: string;
};

export const getSemester = (filter: semesterFilter, tx?: any) => {
    return (tx || dbc).one(SEMESTER_QUERIES.GET_SEMESTER_BY_TIMESTAMP, filter);
};
