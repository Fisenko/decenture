import dbc from 'src/db/db';
import * as QUERIES from 'src/db/queries/university';

export const getUniversityById = (id: string, tx?: any) => {
    return (tx || dbc).one(QUERIES.GET_UNIVERSITY_BY_ID, { id });
};

export const getUniversityByUserId = (userId: string, tx?: any) => {
    return (tx || dbc).one(QUERIES.GET_UNIVERSITY_BY_USER_ID, { userId });
};
