import dbc from 'src/db/db';
import * as BANK_CARD_QUERIES from 'src/db/queries/userBankСard';
import * as DEPOSIT_QUERIES from 'src/db/queries/deposit';
import * as ACCOUNT_QUERIES from 'src/db/queries/account';
import { Card } from 'src/classes/card';
import { ACCOUNT_TYPE } from 'src/db/mapping';

export const getCardTypeIdByKey = (paymentSystemTypeKey: string) => {
    return dbc.one(BANK_CARD_QUERIES.GET_TYPE_ID_BY_KEY, paymentSystemTypeKey.toUpperCase());
};

export const saveCard = (card: Card) => {
    return dbc.oneOrNone(BANK_CARD_QUERIES.CREATE_CARD, card);
};

export const getCardsNumberByAppUserId = (userId: string) => {
    return dbc.manyOrNone(BANK_CARD_QUERIES.GET_CARD_NUMBER_BY_USER_ID, { userId: userId });
};

export const addDepositAmountToPersonalAccountByAppUserId = (userId: string, amount: number) => {
    return dbc.one(DEPOSIT_QUERIES.ADD_DEPOSIT_AMOUNT_TO_PERSONAL_ACCOUNT_BY_APP_USER_ID,
        { userId: userId, amount: amount });
};

export const checkCorrectCardOwnershipByAppUserIdAndCardId = (userId: string, cardId: string) => {
    return dbc.oneOrNone(DEPOSIT_QUERIES.CHECK_CORRECT_CARD_OWNERSHIP_BY_APP_USER_ID_AND_CARD_ID,
        { userId: userId, cardId: cardId });
};

export const getAccountAmountByUserId = (userId: string) => {
    return dbc.oneOrNone(ACCOUNT_QUERIES.GET_ACCOUNT_AMOUNT,
        { userId: userId , typeId: ACCOUNT_TYPE.PERSONAL}, value => value.amount);
};
