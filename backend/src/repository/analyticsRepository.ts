import dbc from 'src/db/db';
import * as Q from 'src/db/queries/attendanceAnalytics';
import { AttendanceFilter } from 'src/repository/scheduledAttendance';

export interface IAnalyticsSearchCriteria {
    beginDate: number,
    endDate: number,
    dateTrunc: string,
    countryCode? : string,
    universityId? : string,
    lectureCourseId? : string,
    isInternational? : string,
    degreeName? : Array<string>,
    groupByLectureCourseId? : boolean,
    groupByIsInternational? : boolean,
    groupByDegreeName? : boolean,
}

export class AnalyticsRepository {
    static instance: AnalyticsRepository = undefined;

    constructor() {
        if (AnalyticsRepository.instance === undefined) {
            AnalyticsRepository.instance = this;
        }
        return AnalyticsRepository.instance;
    }

    getStudentsAnalyticsBySemester(studentId: string, reportDate: number, tx?: any) {
        return (tx || dbc).query(Q.GET_STUDENTS_ANALYTICS_BY_SEMESTER, { studentId, reportDate });
    }

    getStudentsAnalyticsBySemesterAndLectureCourse(filter: AttendanceFilter, tx?: any) {
        return (tx || dbc).query(Q.GET_STUDENTS_ANALYTICS_BY_SEMESTER_AND_LECTURE_COURSE, filter);
    }

    getUniversityAttendance(searchCriteria: IAnalyticsSearchCriteria, tx?: any) {
        return (tx || dbc).query(Q.GET_UNIVERSITY_ATTENDANCE, searchCriteria);
    }

    getUniversityCompliance(searchCriteria: IAnalyticsSearchCriteria, tx?: any) {
        return (tx || dbc).query(Q.GET_UNIVERSITY_COMPLIANCE, searchCriteria);
    }

    getUniversityAnalyticsByYear(universityId: string, tx?: any) {
        return (tx || dbc).query(Q.GET_UNIVERSITY_ANALYTICS_BY_YEAR, { universityId });
    }

    getStudentsAnalytics(universityId: string, tx?: any) {
        return (tx || dbc).query(Q.ANC_RATE_BY_STUDENT, { universityId });
    }

    getUniversityAnalytics(universityId: string, tx?: any) {
        return (tx || dbc).oneOrNone(Q.ANC_RATE_BY_UNIVERSITY, { universityId });
    }
}

const attendanceAnalyticsRepository: AnalyticsRepository = new AnalyticsRepository();

export default attendanceAnalyticsRepository;
