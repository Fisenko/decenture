import dbc from 'src/db/db';
import Account from 'src/classes/account';
import * as QUERIES from 'src/db/queries/account';
import { ACCOUNT_TYPE } from 'src/db/mapping';

export const createAccount = (account: Account, tx?: any) => {
    return (tx || dbc).one(QUERIES.CREATE_ACCOUNT, account);
};

export const getAccount = (id: string, tx?: any) => {
    return (tx || dbc).oneOrNone(QUERIES.GET_ACCOUNT_BY_ID, { id });
};

export const getAccountsByIds = (ids: Array<string>, tx?: any) => {
    return (tx || dbc).manyOrNone(QUERIES.GET_ACCOUNTS_BY_IDS, { ids });
};

export const getAccountByUser = (userId: number, tx?: any) => {
    return (tx || dbc).oneOrNone(QUERIES.GET_ACCOUNT_BY_USER_ID, { userId });
};

export const getPersonalAccountByUser = (userId: string, tx?: any) => {
    return (tx || dbc).oneOrNone(QUERIES.GET_ACCOUNT_BY_USER_ID_AND_TYPE_ID, { userId, typeId: ACCOUNT_TYPE.PERSONAL });
};

export const getPersonalAccountByEmail = (email: string, tx?: any) => {
    return (tx || dbc).oneOrNone(
        QUERIES.GET_USER_ACCOUNTS_BY_EMAIL_AND_TYPE_ID,
        {
            email,
            typeId: ACCOUNT_TYPE.PERSONAL
        }
    );
};

export const getStudentAccountsByEmail = (email: string, tx?: any) => {
    return (tx || dbc).query(QUERIES.GET_STUDENT_ACCOUNTS_BY_EMAIL, {
        email: email,
    });
};

export const getUserAccountsByEmail = (email: string, tx?: any): Array<Account> => {
    return (tx || dbc).query(QUERIES.GET_USER_ACCOUNTS_BY_EMAIL, {
        email,
    });
};

export const getUserAccountsById = (id: string, tx?: any): Array<Account> => {
    return (tx || dbc).manyOrNone(QUERIES.GET_USER_ACCOUNTS_BY_ID, {
        id,
    });
};

export const addAccountAmount = (id: string, amount: number, tx?: any) => {
    return (tx || dbc).one(QUERIES.ADD_ACCOUNT_AMOUNT, { id, amount });
};

export const updateDebitCredit = (dt: string, ct: string, amount: number, tx?: any) => {
    return (tx || dbc).query(QUERIES.UPDATE_DEBIT_CREDIT, { dt, ct, amount });
};

export const updateAccountUser = (account: Account, tx?: any) => {
    return (tx || dbc).none(QUERIES.UPDATE_ACCOUNT_USER, account);
};

export const getSponsoredStudentAccounts = (tx?: any) => {
    return (tx || dbc).manyOrNone(QUERIES.GET_SPONSORED_STUDENT_ACCOUNTS);
};
