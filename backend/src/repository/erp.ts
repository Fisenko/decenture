import dbc from 'src/db/db';
import * as ERP_QUERIES from 'src/db/queries/erp';

export const saveInvoice = (invoice: any) => {
    return dbc.many(ERP_QUERIES.SAVE_INVOICE, invoice);
};

export const getInvoices = () => {
    return dbc.manyOrNone(ERP_QUERIES.GET_INVOICES);
};
