import dbc from 'src/db/db';
import UserInfo from 'src/classes/userInfo';
import * as QUERIES from 'src/db/queries/userInfo';

export const createUserInfo = (userInfo: UserInfo, tx?: any) => {
    return (tx || dbc).none(QUERIES.CREATE_USER_INFO, userInfo);
};

export const getUserInfoById = (id: string, tx?: any) => {
    return (tx || dbc).oneOrNone(QUERIES.GET_USER_INFO_BY_ID, { id });
};
