import dbc from 'src/db/db';
import Student from 'src/classes/userInfo';
import * as QUERIES from 'src/db/queries/student';


export const getAllStudents = (tx?: any) => {
    return (tx || dbc).query(QUERIES.GET_ALL_STUDENTS);
};

export const createStudent = (student: Student, tx?: any) => {
    return (tx || dbc).one(QUERIES.CREATE_STUDENT, student);
};

export const getStudentById = (id: string, tx?: any) => {
    return (tx || dbc).oneOrNone(QUERIES.GET_STUDENT_BY_ID, { id: id });
};

export const getStudentsByEmail = (email: string, tx?: any) => {
    return (tx || dbc).query(QUERIES.GET_STUDENTS_BY_EMAIL, { email: email });
};

export const getStudentByEmailAndStudentId = (email: string, studentId: string, tx?: any) => {
    return (tx || dbc).oneOrNone(QUERIES.GET_STUDENT_BY_EMAIL_AND_STUDENT_ID, { email: email, studentId: studentId });
};

export const getStudentByUserId = (userId: string, tx?: any) => {
    return (tx || dbc).oneOrNone(QUERIES.GET_STUDENT_BY_USER_ID, { userId: userId });
};
