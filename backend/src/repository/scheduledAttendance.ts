import dbc from 'src/db/db';
import ScheduledAttendance from 'src/classes/scheduledAttendance';
import * as QUERIES from 'src/db/queries/scheduledAttendance';

export interface INfcTimestampStudent {
    nfcId: string;
    timestamp: number;
    studentId: string;
}

export type AttendanceFilter = {
    studentId?: string;
    universityId?: string;
    beginDate: number;
    endDate: number;
}

export const create = (scheduledAttendance: ScheduledAttendance, tx?: any) => {
    return (tx || dbc).one(QUERIES.CREATE, scheduledAttendance);
};

export const update = (scheduledAttendance: ScheduledAttendance, tx?: any) => {
    return (tx || dbc).oneOrNone(QUERIES.UPDATE, scheduledAttendance);
};

export const findOne = (scheduledAttendance: ScheduledAttendance, tx?: any) => {
    return (tx || dbc).oneOrNone(QUERIES.FIND_ONE, scheduledAttendance);
};

export const findByTransactionId = (transactionId: string, tx?: any) => {
    return (tx || dbc).oneOrNone(QUERIES.FIND_BY_TRANSACTION_ID, { transactionId });
};

export const findManyByStudentId = (studentId: string, tx?: any) => {
    return (tx || dbc).manyOrNone(QUERIES.FIND_BY_STUDENT_ID, { studentId });
};

export const findManyByFilter = (filter: AttendanceFilter, tx?: any) => {
    return (tx || dbc).manyOrNone(QUERIES.FIND_BY_STUDENT_ID_TIMESTAMP, filter);
};

export const findByNfcIdAndTimestampAndStudentId = (filter: INfcTimestampStudent, tx?: any) => {
    // todo: or .query ???
    return (tx || dbc).oneOrNone(QUERIES.FIND_BY_NFC_ID_TIMESTAMP_STUDENT_ID, filter);
};

export const findAbsenceByStudentId = (studentId: string, tx?: any) => {
    return (tx || dbc).many(QUERIES.FIND_ABSENCE_BY_STUDENT_ID, { studentId });
};

export const findAbsenceByFilter = (filter: AttendanceFilter, tx?: any) => {
    return (tx || dbc).manyOrNone(QUERIES.FIND_ABSENCE_BY_STUDENT_ID_TIMESTAMP, filter);
};

export const findAttendanceCompletionByStudentId = (studentId: string, tx?: any) => {
    return (tx || dbc).many(QUERIES.FIND_ATTENDANCE_PERCENTAGE, { studentId });
};
