import dbc from 'src/db/db';
import * as QUERIES from 'src/db/queries/student';

export const getAllStudentsByUniversityId = (universityId: string, tx?: any) => {
    return (tx || dbc).many(QUERIES.GET_ALL_STUDENTS_BY_UNIVERSITY_ID, { universityId });
};
