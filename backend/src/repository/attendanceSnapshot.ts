import dbc from 'src/db/db';
import AttendanceSnapshot from 'src/classes/attendanceSnapshot';
import * as QUERIES from 'src/db/queries/attendanceSnapshot';

export const create = (scheduledAttendance: AttendanceSnapshot, tx?: any) => {
    return (tx || dbc).one(QUERIES.CREATE, scheduledAttendance);
};

export const updateOnNfcScan = (scheduledAttendance: AttendanceSnapshot, tx?: any) => {
    return (tx || dbc).oneOrNone(QUERIES.UPDATE_ON_NFC_SCAN, scheduledAttendance);
};

export const updateCompleted = (beginDate: number, endDate: number, tx?: any) => {
    return (tx || dbc).query(QUERIES.UPDATE_COMPLETED, { beginDate, endDate });
};

export const updateMonth = (snapshotDate: number, tx?: any) => {
    return (tx || dbc).query(QUERIES.UPDATE_MONTH, { snapshotDate});
};
