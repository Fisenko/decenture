import dbc from 'src/db/db';
import AppUser from 'src/classes/appUser';
import * as QUERIES from 'src/db/queries/user';


export const createUser = (user: AppUser, tx?: any) => {
    return (tx || dbc).one(QUERIES.CREATE_USER, user);
};

export const getAllUsers = (tx?: any) => {
    return (tx || dbc).query(QUERIES.GET_ALL_USERS);
};

export const getUserById = (id: number, tx?: any) => {
    return (tx || dbc).oneOrNone(QUERIES.GET_USER_BY_ID, { id });
};

export const getUserByToken = (token: string, tx?: any) => {
    return (tx || dbc).oneOrNone(QUERIES.GET_USER_BY_TOKEN, { token: token });
};

export const getUserByTokenAndStatus = (token: string, statusId: number, tx?: any) => {
    return (tx || dbc).oneOrNone(QUERIES.GET_USER_BY_TOKEN_AND_STATUS, { token: token, statusId: statusId });
};

export const getUserByEmailAndPassword = (user: AppUser, tx?: any) => {
    return (tx || dbc).oneOrNone(QUERIES.GET_USER_BY_EMAIL_AND_PASSWORD, user);
};

export const getUserByEmail = (user: AppUser, tx?: any) => {
    return (tx || dbc).oneOrNone(QUERIES.GET_USER_BY_EMAIL, user);
};

export const updateUserStatus = (user: AppUser, tx?: any) => {
    return (tx || dbc).none(QUERIES.UPDATE_USER_STATUS, user);
};
