export const upperCaseToCamelCase = (string: string): string => {
    return string
    .replace(/(.)/g, function ($1) {
        return $1.toLowerCase();
    })
    .replace(/_(.)/g, function ($1) {
        return $1.toUpperCase();
    })
    .replace(/_/g, '');
};

export const underScoreToCamelCase = (string: string): string => {
    return string
    .replace(/_(.)/g, function ($1) {
        return $1.toUpperCase();
    })
    .replace(/_/g, '');
};

export const keysToCamelCase = function (obj: any) {
    const result: any = {};
    for (const i in obj) {
        if (obj.hasOwnProperty(i)) {
            result[underScoreToCamelCase(i)] = obj[i];
        }
    }
    return result;
};

export const PULL_METHODS: { [methodName: string]: any } = {};

export const getRandomInt = (min: number, max: number) => {
    return Math.floor(Math.random() * (max - min)) + min;
};

export const randomDate = (start: Date, end: Date): Date => {
    return new Date(
        start.getTime() + Math.random() * (end.getTime() - start.getTime()),
    );
};

export const getRandomArbitrary = (min: number, max: number) => {
    return Math.random() * (max - min) + min;
};

export const asyncForEach = async (array: any, callback: any) => {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array);
    }
};

export const validateEmail = (email: string) => {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email.toLowerCase());
};

export const logDebugObjectId = (title: string, o?: any) => {
    let ss = (o === undefined)
        ? 'UNDEFINED'
        : (o === null)
            ? 'NULL'
            : o.id;
    console.log('\n------------------------------------------------------------------------------------------');
    console.log(`${title} -->`);
    console.log(ss);
};

export const logDebugObject = (title: string, o?: any) => {
    let ss = (o === undefined)
        ? 'UNDEFINED'
        : (o === null)
            ? 'NULL'
            : o;
    console.log('\n------------------------------------------------------------------------------------------');
    console.log(`${title} -->`);
    console.log(ss);
};

export const extractIdFromHyperledger = (id: string) => {
    return id.split('#').pop();
};
