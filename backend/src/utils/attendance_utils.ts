import AttendanceAbsenceDTO from 'src/classes/dto/attendanceAbsenceDTO';
import LectureAttendanceDTO from 'src/classes/dto/lectureAttendanceDTO';

export const convertAbsenceToDTO = (absence: Array<any>) => {
    return absence
        .map((lecture: any) => new AttendanceAbsenceDTO(lecture))
        .sort((a: AttendanceAbsenceDTO, b: AttendanceAbsenceDTO) =>
            +new Date(b.dateTime) - +new Date(a.dateTime));
};

export const compareAttendanceDateTime =(a: LectureAttendanceDTO, b: LectureAttendanceDTO) => {
    const aTime = new Date(a.datetime).getTime();
    const bTime = new Date(b.datetime).getTime();

    if (aTime > bTime) {
        return -1;
    }
    if (aTime < bTime) {
        return 1;
    }
    return 0;
};
