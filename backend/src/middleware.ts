import { PULL_METHODS, upperCaseToCamelCase } from 'src/utils/utils';
import { Login } from 'src/service/login';
import { Registration } from 'src/service/registration';
import { FundsService } from 'src/service/fundsService';
import { AttendanceService } from 'src/service/attendanceService';
import { TransactionService } from 'src/service/transactionService';
import invoiceService, { InvoiceService } from 'src/service/invoiceService';
import { AccountService } from 'src/service/accountService';
import { AnalyticsService } from 'src/service/analyticsService';
import { UniversityService } from 'src/service/universityService';
import { PaymentService } from 'src/service/paymentService';

export class Ws {
    static instance: Ws = undefined;
    login: any;
    registration: any;
    funds: any;
    attendance: AttendanceService;
    transaction: TransactionService;
    invoice: InvoiceService;
    account: AccountService;
    university: UniversityService;
    attendanceAnalytics: AnalyticsService;
    payment: PaymentService;

    constructor() {
        if (Ws.instance === undefined) {
            Ws.instance = this;
            this.login = new Login();
            this.registration = new Registration();
            this.funds = new FundsService();
            this.attendance = new AttendanceService();
            this.transaction = new TransactionService();
            this.account = new AccountService();
            this.university = new UniversityService();
            this.attendanceAnalytics = new AnalyticsService();
            this.payment = new PaymentService();
            this.invoice = invoiceService;
        }
        return Ws.instance;
    }

    listen(code: string, data: any, token?: string, socket?: any) {
        const methodName = upperCaseToCamelCase(code);
        const method = PULL_METHODS[methodName];
        if (typeof method === 'function') {
            return method(data, token, socket);
        }
    }
}

const ws: Ws = new Ws();
export default ws;
