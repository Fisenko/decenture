import { redisClient, redisGetAsync } from 'src/redis';
import User from 'src/classes/redisUser';
import ws from 'src/middleware';
import Routes from 'src/routes';
import connectionPool from 'src/service/connectionPool';
import cronJobs from 'src/service/cronJobs';
import bodyParser = require('body-parser');
const app = require('express')();
const server = require('http').createServer(app);
const redis = require('redis');
const io = require('socket.io')(server, {
    serveClient: false,
    wsEngine: 'ws',
    pingTimeout: 30000,
    pingInterval: 30000,
});
const cors = require('cors');
const uuidv1 = require('uuid/v1');
const port = process.env.BACKEND_PORT || 4601;

app.use(bodyParser.json());
app.use(cors());
io.on('connect', onConnect);
server.listen(port, () => console.log('server listening on port ' + port));

function onConnect(socket: any) {
    console.log(socket.handshake);
    let token = '';

    socket.emit('OPEN');
    socket.on('CHECK_TOKEN', async function (data: any) {
        console.log('TOKEN: ' + JSON.stringify(data));
        data = JSON.parse(data);
        if (!data) {
            return;
        }
        token = data.token;
        const result = JSON.parse(await redisGetAsync(token));
        if (result === null) {
            token = uuidv1();
            redisClient.set(token, JSON.stringify(new User()), redis.print);
        }
        socket.emit('SET_TOKEN', JSON.stringify({ token }));
        socket.on('message', function (json: string) {
            onMessage(json, socket);
        });
    });
    socket.on('disconnect', async function () {
        await connectionPool.removeSocketBySessionToken(token, socket);
    });
}

async function onMessage(json: string, socket: any) {
    const { token, code, data } = JSON.parse(json);
    // console.log('REQUEST:', code, data);
    const response = await ws.listen(code, data, token, socket);
    // console.log('RESPONSE: ' + JSON.stringify(response));
    socket.emit(
        'message',
        JSON.stringify({
            code: code + '_RESPONSE',
            ...response,
        }),
    );
}

console.log(`${cronJobs.length} scheduled jobs started...`);

Routes.routes(app);
