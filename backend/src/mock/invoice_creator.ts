import { getRandomInt } from 'src/utils/utils';
import { Invoice } from 'src/classes/invoice';

const countInvoices = 2;

const titles = ['Tuition Fee 1st', 'Accommodation 2nd'];

const descriptions = [
    'Tuition Fee for Semester 1',
    'Accommodation for Semester 2',
];

const university = 'Decenture University';
const types = ['money', 'room'];

const generateInvoiceByUserName = (userName: string): Invoice => {
    const index = getRandomInt(0, countInvoices);

    const invoice = {
        id: getRandomInt(10000, 99999).toString(),
        title: titles[index],
        description: descriptions[index],
        code: getRandomInt(10000, 99999).toString(),
        type: types[index],
        from: university,
        to: userName,
        ref: userName,
        amount: getRandomInt(1000, 10000),
    };
    return new Invoice(invoice);
};

export const generateInvoicesByUserName = (userName: string) => {
    console.log(userName);
    const invoices = [];
    for (let i = 0; i < countInvoices; i++) {
        invoices.push(generateInvoiceByUserName(userName));
    }
    return invoices;
};
