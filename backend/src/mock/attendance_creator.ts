import { getRandomInt, randomDate } from 'src/utils/utils';
import UserAttendance from 'src/classes/attendance';
import Lecture from 'src/classes/lecture';

const startDate = new Date(2018, 7, 1);
const endDate = new Date(2018, 7, 10);
const countLectures = 10;

export const lectureTitles = [
    'Language Lang 1020',
    'Calculus(II) MATH2021',
    'Business Communication LA BU2040',
    'Contemporary China HUMA2104',
    'Business Case Study GBUS2020',
];

export const lecturePlaces = [
    'G012 LSK BLsg',
    'Rm 1104 Consource',
    'Rm 4503',
    'Rm 5619',
    'Lecture Threater F',
];

export const percentage = [
    {
        value: 100,
        code: 'LANG'
    },
    {
        value: 85,
        code: 'MATH'
    },
    {
        value: 75,
        code: 'LABU'
    },
    {
        value: 100,
        code: 'HUMA'
    },
    {
        value: 85,
        code: 'MATH'
    },
    {
        value: 75,
        code: 'LABU'
    },
    {
        value: 75,
        code: 'LABU'
    },
    {
        value: 75,
        code: 'LABU'
    },
    {
        value: 75,
        code: 'LABU'
    },

];

export const buildAttendance = (): UserAttendance => {
    let history = [];
    for (let i = 0; i < countLectures; i++) {
        history.push(generateLecture());
    }

    const attendanceHistoryMap: { [key: string]: any } = {};
    history.map(attendance => {
        const attendanceDateTime = attendance.dateTime.toLocaleDateString('en-US');
        attendanceHistoryMap[attendanceDateTime] ?
            attendanceHistoryMap[attendanceDateTime].push(attendance) :
            attendanceHistoryMap[attendanceDateTime] = [attendance];
    });

    history = Object.keys(attendanceHistoryMap)
    .map((key: string) => {
        return {
            date: key,
            lectures: attendanceHistoryMap[key]
        };
    });

    const absence = [generateLecture(), generateLecture()];

    // return new UserAttendance({ history, absence, percentage });
    return;
};

const generateLecture = (): Lecture => {
    const randomLecture = lectureTitles[getRandomInt(0, lectureTitles.length)];
    const randomPlace = lecturePlaces[getRandomInt(0, lecturePlaces.length)];
    const randomDateTime = randomDate(startDate, endDate);

    return new Lecture(randomLecture, randomDateTime, randomPlace);
};
