import { getRandomInt, getRandomArbitrary, randomDate } from '../utils/utils';
import { Transaction, Transactions } from '../classes/transaction';

const startDate = new Date();
const endDate = new Date();
endDate.setDate(startDate.getDate() - 5);

const you = 'You';

const names = [
    'Kwilith',
    'Blognation',
    'Avamm',
    'Dablist',
    'Fivespan',
    'Kwideo',
    'Aimbo',
    'Realbuzz',
    'Vinte',
    'Realblab',
    'Jayo',
];

const types = ['money', 'coffee', 'room'];

const descriptions = [
    '80 Petterle Court',
    '499 Heffernan Point',
    '356 3rd Trail',
    '7694 Marquette Hill',
    '088 Rusk Drive',
    '82913 Rutledge Lane',
    '348 Banding Trail',
    '93 Forest Hill',
    '5 Washington Pass',
    '3 Del Sol Trail',
    '4118 Stoughton Court',
    '95 8th Way',
    '45095 Utah Pass',
];

const receivers = [
    'Victor',
    'Karisa',
    'Rennie',
    'Salomon',
    'Lari',
    'Andree',
    'Mayne',
    'Renado',
    'Pembroke',
    'Mariya',
];

const getRandomReceiver = () => receivers[getRandomInt(0, receivers.length - 1)];

const generateUnpaidTransaction = (): Transaction => {
    const transaction = {
        name: names[getRandomInt(0, names.length - 1)],
        description: descriptions[getRandomInt(0, descriptions.length - 1)],
        code: getRandomInt(10000, 99999),
        type: types[getRandomInt(0, types.length - 1)],
    };

    return new Transaction(transaction);
};

const generateTransaction = (): Transaction => {
    const date = randomDate(startDate, endDate);
    const sender = getRandomInt(0, 2) === 1 ? you : getRandomReceiver();
    const receiver = sender === you ? getRandomReceiver() : you;
    const transaction = {
        ...generateUnpaidTransaction(),
        price: getRandomArbitrary(1, 100),
        sender,
        receiver,
        name: sender === you ? receiver : sender,
        time: date.toLocaleTimeString('en-US', {
            hour: '2-digit',
            minute: '2-digit',
        }),
        date: date.toLocaleString('en-US').split(',')[0],
    };

    return new Transaction(transaction);
};

const generateTransactions = (count: number, type: string) => {
    const transactions = [];
    const f =
        type === 'unpaid' ? generateUnpaidTransaction : generateTransaction;

    for (let i = 0; i < count; i++) {
        transactions.push(f());
    }

    return transactions;
};

export const buildTransactions = (): Transactions => {
    const unpaid = generateTransactions(getRandomInt(3, 10), 'unpaid');
    const paid = generateTransactions(getRandomInt(10, 15), 'paid');

    return new Transactions(paid, unpaid);
};
