export default class AccountTransaction {

    account: string;
    amount: number;

    constructor(data: any = {}) {
        this.account = `resource:org.acme.mynetwork.Account#${data.account}`;
        this.amount = data.amount || 0;
    }
}
