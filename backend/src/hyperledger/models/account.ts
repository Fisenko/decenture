export default class Account {
    id: string;
    name: string;
    amount: number;
    currency: string;
    type_id: number;
    status_id: number;
    user_id: string;
    owner_id: string;
    entry_date: string;
    close_date?: string;

    constructor(data: any = {}) {
        this.id = data.id;
        this.name = data.name;
        this.amount = data.amount || 0;
        this.currency = data.currency;
        this.type_id = data.type_id;
        this.status_id = data.status_id;
        this.user_id = data.user_id;
        this.owner_id = data.owner_id;
        this.entry_date = data.entry_date;
        this.close_date = data.close_date || null;
    }
}
