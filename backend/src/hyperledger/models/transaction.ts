export default class Transaction {
    post_date: string;
    sender: string;
    receiver: string;
    amount: number;
    description: string;
    invoice?: string;
    transactionId?: string;
    timestamp?: string;
    $class?: string;

    constructor(data: any = {}) {
        this.post_date = data.post_date || data.entryDate;
        this.sender = `resource:org.acme.mynetwork.Account#${data.sender}`;
        this.receiver = `resource:org.acme.mynetwork.Account#${data.receiver}`;
        this.amount = data.amount;
        this.description = data.description;
        this.invoice = data.invoice
            ? `resource:org.acme.mynetwork.Invoice#${data.invoice}`
            : null;
        this.timestamp = data.timestamp || new Date();
        this.$class = data.$class || '';
    }
}
