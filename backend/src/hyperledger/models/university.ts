export default class University {
    id: string;
    name: string;
    address: string;
    code: string;
    country_code: string;
    domain_name: string;
    erp_info: string;
    official_name: string;
    abbreviation: string;
    root_user_id: string;

    constructor(data: any = {}) {
        this.id = data.id;
        this.name = data.name;
        this.address = data.address;
        this.code = data.code;
        this.country_code = data.country_code;
        this.domain_name = data.domain_name;
        this.erp_info = data.erp_info;
        this.official_name = data.official_name;
        this.abbreviation = data.abbreviation;
        this.root_user_id = data.root_user_id;
    }
}
