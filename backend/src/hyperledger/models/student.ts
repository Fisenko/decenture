export default class Student {
    id: string;
    university_id: string;
    erp_student_id: string;
    first_name: string;
    last_name: string;
    erp_email: string;

    constructor(data: any = {}) {
        this.id = data.id;
        this.university_id = `resource:org.acme.mynetwork.University#${data.university_id}`;
        this.erp_student_id = data.erp_student_id;
        this.first_name = data.first_name;
        this.last_name = data.last_name;
        this.erp_email = data.erp_email;
    }
}
