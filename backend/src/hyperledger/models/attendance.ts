export default class Attendance {
    student_id: string;
    nfc_id: string;
    location: string;
    erp_email: string;
    erp_student_id: string;
    first_name: string;
    last_name: string;
    notes?: string;

    constructor(data: Attendance) {
        this.student_id = `resource:org.acme.mynetwork.Student#${data.student_id}`;
        this.nfc_id = data.nfc_id;
        this.location = data.location;
        this.erp_email = data.erp_email;
        this.erp_student_id = data.erp_student_id;
        this.first_name = data.first_name;
        this.last_name = data.last_name;
        this.notes = data.notes || '';
    }
}
