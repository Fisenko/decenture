export default class Government {
    id: string;
    country_code: string;
    name: string;
    root_user_id: string;

    constructor(data: any = {}) {
        this.id = data.id;
        this.country_code = data.country_code;
        this.name = data.name;
        this.root_user_id = data.root_user_id;
    }
}
