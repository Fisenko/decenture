import fetch from 'node-fetch';
import Invoice from 'src/hyperledger/models/invoice';
import Attendance from 'src/hyperledger/models/attendance';
import Transaction from 'src/hyperledger/models/transaction';
import Account from 'src/hyperledger/models/account';
import Government from 'src/hyperledger/models/government';
import Student from 'src/hyperledger/models/student';
import University from 'src/hyperledger/models/university';
import AccountTransaction from 'src/hyperledger/models/accountTransaction';

const uuidv4 = require('uuid/v4');

const DEFAULT_HYPERLEDGER_PORT = 3000;
const DEFAULT_HYPERLEDGER_HOST = '0.0.0.0';
export const HL_PORT = process.env.HYPERLEDGER_PORT || DEFAULT_HYPERLEDGER_PORT;
export const HL_HOST = process.env.HYPERLEDGER_HOST || DEFAULT_HYPERLEDGER_HOST;

export const HEADERS = {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
};

const dataToQueryParams = (params: any) => {
    return Object.keys(params)
    .map(k => encodeURIComponent(k) + '=' + encodeURIComponent(params[k]))
    .join('&');
};

export const ACCOUNT_RESOURCE = 'resource:org.acme.mynetwork.Account#';
export const STUDENT_RESOURCE = 'resource:org.acme.mynetwork.Student#';
export const INVOICE_RESOURCE = 'resource:org.acme.mynetwork.Invoice#';

export class Hyperledger {
    static instance: Hyperledger = undefined;

    constructor() {
        if (Hyperledger.instance === undefined) {
            Hyperledger.instance = this;
        }
        return Hyperledger.instance;
    }

    getSmartContract(smartContractName: String, data: any) {
        return fetch(`http://${HL_HOST}:${HL_PORT}/api/${smartContractName}?${dataToQueryParams(data)}`, {
            headers: HEADERS,
            method: 'GET'
        })
        .then((response: any) => response.json());
    }

    executeSmartContract(smartContractName: String, data: any) {
        return fetch(`http://${HL_HOST}:${HL_PORT}/api/${smartContractName}`, {
            headers: HEADERS,
            method: 'POST',
            body: JSON.stringify(data)
        })
        .then((response: any) => response.json());
    }

    createInvoice(data: Invoice) {
        return this.executeSmartContract('Invoice', new Invoice(data));
    }

    accountTransaction(data: AccountTransaction) {
        return this.executeSmartContract('AccountTransaction', new AccountTransaction(data));
    }

    createUniversity(data: University) {
        return this.executeSmartContract('University', new University(data));
    }

    createStudent(data: Student) {
        return this.executeSmartContract('Student', new Student(data));
    }

    createGovernment(data: Government) {
        return this.executeSmartContract('Government', new Government(data));
    }

    createAccount(data: Account) {
        return this.executeSmartContract('Account', new Account(data));
    }

    attendance(data: Attendance) {
        return this.executeSmartContract('AttendanceTransaction', data);
    }

    sendTransaction(data: Transaction) {
        return this.executeSmartContract('InvoiceTransaction', new Transaction(data));
    }

    getAttendanceByStudent(id: string) {
        return this.getSmartContract(
            'queries/selectAttendanceByStudent',
            { student: STUDENT_RESOURCE + id }
        );
    }

    getTransactionsByAccount(id: string) {
        return this.getSmartContract(
            'queries/selectTransactionByAccount',
            { account: ACCOUNT_RESOURCE + id }
        );
    }

    getAttendanceByNfcAndDate(data: any) {
        return this.getSmartContract(
            'queries/selectAttendanceByNFCID',
            { nfc_id: data.nfc_id, begin_date: data.begin_date, end_date: data.end_date }
        );
    }

    getInvoicesByAccount(id: string) {
        return this.getSmartContract(
            'queries/selectInvoiceByAccount',
            { account: ACCOUNT_RESOURCE + id }
        );
    }

    getUnpaidInvoicesByAccount(id: string) {
        return this.getSmartContract(
            'queries/selectUnpaidInvoicesByAccount',
            { account: ACCOUNT_RESOURCE + id }
        );
    }

    getAccountById(id: string) {
        return this.getSmartContract(
            'Account' + '/' + id, {}
        );
    }
}

const hyperledger = new Hyperledger();
export default hyperledger;
