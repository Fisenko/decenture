import { Request, Response } from 'express';
import HttpStatus from 'http-status-codes';

import { verifyUserByToken } from 'src/service/userService';
import connectionPool from 'src/service/connectionPool';

const VERIFICATION_SUCCESS = 'User verification successful.';
const BAD_REQUEST = 'Bad request: user token is undefined!';

export const emailVerification = (req: Request, res: Response) => {
    const token = req.query.token;
    if (token) {
        verifyUserByToken(token)
        .then((user: any) => {
            user
                ? res.status(HttpStatus.OK).json({ msg: VERIFICATION_SUCCESS })
                : res.status(HttpStatus.BAD_REQUEST).json({
                    msg: `Verification error: token ${token} not found or user is already active!`,
                });
        })
        .catch((err: Error) => {
            res.status(HttpStatus.INTERNAL_SERVER_ERROR).send({ msg: err.message });
        });
    } else {
        res.status(HttpStatus.BAD_REQUEST).json({ msg: BAD_REQUEST });
    }
};

// todo: Dev tool. Remove in production!!!
export const testConnectionPool = (req: Request, res: Response) => {
    const store = connectionPool.getStore();

    const result: Array<any> = [];

    Object.keys(store).forEach(key => {
        connectionPool.getSocket(key).forEach(cpi => {
            result.push({
                key: key,
                token: cpi.token,
                socketId: cpi.socket.id,
            });
        });
    });

    return res.status(HttpStatus.OK).json(result);
};
