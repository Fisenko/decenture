import { Request, Response } from 'express';
import attendanceService from 'src/service/attendanceService';
import attendanceAnalyticsService from 'src/service/analyticsService';

const OK = 'OK.';
const BAD_REQUEST = 'Bad request: scheduled lecture not found!';

export const confirmScheduledLecture = (req: Request, res: Response) => {
    const id = req.query.id;

    console.log('----------------------------------------------------------------------------------------------------');
    console.log('export const confirmScheduledLecture = (req: Request, res: Response) => ');
    console.log(id);

    if (id) {
        attendanceService.confirmScheduledLecture(id)
        .then((data: any) => {
            return data
                ? res.status(200).json({ msg: OK })
                : res.status(400).json({ msg: `BADABOOM!` });
        })
        .catch(() => {
            res.status(500).send(null);
        });
    } else {
        res.status(400).json({ msg: BAD_REQUEST });
    }
};

export const ancRateByStudent = (req: Request, res: Response) => {
    const id = req.query.id;


    if (id) {
        attendanceAnalyticsService.getStudentsAnalytics({ universityId: id }, null)
        .then((data: any) => {
            console.log('testAnalytics1 : BEGIN');
            if (data) {
                data.forEach((i: any) => console.log(i));
                return res.status(200).json(data);
            } else {
                console.log('data is null!');
                return res.status(400).json({ msg: 'DATA IS NULL!' });
            }
        })
        .catch(() => {
            res.status(500).send({ msg: 'ERROR!' });
        });
    } else {
        res.status(400).json({ msg: 'BAD REQUEST!' });
    }
};

export const ancRateByUniversity = (req: Request, res: Response) => {
    const id = req.query.id;
    if (id) {
        attendanceAnalyticsService.getUniversityAnalytics({ universityId: id }, null)
        .then((data: any) => {
            console.log('testAnalytics1 : BEGIN');
            if (data) {
                data.forEach((i: any) => console.log(i));
                return res.status(200).json(data);
            } else {
                console.log('data is null!');
                return res.status(400).json({ msg: 'DATA IS NULL!' });
            }
        })
        .catch(() => {
            res.status(500).send({ msg: 'ERROR!' });
        });
    } else {
        res.status(400).json({ msg: 'BAD REQUEST!' });
    }
};
