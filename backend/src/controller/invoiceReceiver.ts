import { Request, Response } from 'express';
import http from 'http';

const SUCCESS_STATUS_CODE = 200;
const DEFAULT_OPTIONS = {
    host: process.env.HYPERLEDGER_HOST || '0.0.0.0',
    port: 3000,
    path: `/api/Invoice`,
    method: 'POST',
    headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
    },
};

// TODO: refactor. Create InvoiceDataProvider Class. This class will be used as Holder for
// TODO: RestInvoiceDataProvider, PostgreSQLInvoiceDataProvider, etc. Controllers used for
// TODO: processing, converting to DTO's.
export const invoiceReceiver = (req: Request, res: Response) => {
    console.log(req.body, JSON.stringify(req.body));

    console.log('Invoice', req.body);
    insert(DEFAULT_OPTIONS, req.body).then(result => {
        console.log(`Added a new invoice: ${decodeURIComponent(JSON.stringify(result))}`);
        res.status(SUCCESS_STATUS_CODE).json({ msg: 'OK' });
    });
};

const insert = (options: any, user: any) => {
    return new Promise(resolve => {
        const request = http.request(options, (res: any) => {
            res.on('data', (data: any) => {
                resolve(data);
            });
        });
        request.write(JSON.stringify(user));
        request.end();
    });
};
