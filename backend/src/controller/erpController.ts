import { Request, Response } from 'express';
import * as ErpRepository from 'src/repository/erp';

export const reconcileInvoice = async (req: Request, res: Response) => {
    console.log('RECEIVED INVOICE: ', req.body);
    const invoice = req.body;
    res.status(200).send({
        msg: 'Invoice accepted'
    });
    await ErpRepository.saveInvoice({ invoice });
};


export const checkStatus = async (req: Request, res: Response) => {
    res.status(200).send({ status: 'UP' });
};
