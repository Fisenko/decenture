import * as nodemailer from 'nodemailer';

export const TEMPLATES_HOME = 'templates';

export const transporterOptions = {
    service: 'gmail',
    auth: {
        user: process.env.SMTP_USER,
        pass: process.env.SMTP_PASS,
    },
};

const transporter = nodemailer.createTransport(transporterOptions);

export default transporter;

export const sendEmail = (email: any) => {
    return transporter.sendMail(email, (err, info) => {
        if (err) {
            console.error(err);
        } else {
            console.log(info);
        }
    });
};

