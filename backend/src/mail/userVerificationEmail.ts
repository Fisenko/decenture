import { sendEmail, TEMPLATES_HOME, transporterOptions } from 'src/mail/index';
import path from 'path';
import AppUser from 'src/classes/appUser';

// @ts-ignore
import { EmailTemplate } from 'email-templates';

const ENDPOINT_HOST = process.env.DNS_NAME || '0.0.0.0';
const ENDPOINT_PORT = process.env.WEB_FRONTEND_PORT || 4600;
const VERIFICATION_ENDPOINT = `http://${ENDPOINT_HOST}:${ENDPOINT_PORT}/email-verification?token=`;

const TEMPLATE_NAME = 'confirmation';

export const getPathToTemplate = (templateName: string) => {
    return path.join(path.resolve(TEMPLATES_HOME), templateName);
};

const PATH_TO_TEMPLATE = getPathToTemplate(TEMPLATE_NAME);
const TEMPLATE = new EmailTemplate(PATH_TO_TEMPLATE);

const LOGO_FILENAME = 'decenture_logo_horizontal_blue.png';
const LOGO_PATH = path.join(getPathToTemplate(TEMPLATE_NAME), LOGO_FILENAME);

const LOGO = {
    filename: LOGO_FILENAME,
    path: LOGO_PATH,
    contentType: 'image/png',
    cid: 'logo',
};

const ATTACHMENTS = [LOGO];

export const renderTemplate = async (template: any, context: any) => {
    try {
        return await template.render(context);
    } catch (e) {
        console.error(e);
    }
};

export const sendVerificationEmail = (user: AppUser) => {
    renderTemplate(
        TEMPLATE,
        {
            ...user,
            link: `${VERIFICATION_ENDPOINT}${user.token}`,
        }
    )
    .then(renderedTemplate => {
        sendEmail(
            {
                from: transporterOptions.auth.user,
                to: user.email,
                subject: renderedTemplate.subject,
                html: renderedTemplate.html,
                text: renderedTemplate.text,
                attachments: ATTACHMENTS,
            }
        );
    })
    .catch(e => {
        console.error(e);
    });
};
