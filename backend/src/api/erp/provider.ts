export interface Provider {
    reconcileInvoicePayment(invoice: any): Promise<void>;
}
