import { Provider } from 'src/api/erp/provider';
import { decentureConnection } from '../config/httpConfig';

export class DecentureProvider implements Provider {

    static instance: DecentureProvider = undefined;

    constructor() {
        if (DecentureProvider.instance === undefined) {
            DecentureProvider.instance = this;
        }
        return DecentureProvider.instance;
    }

    async reconcileInvoicePayment(invoice: any) {
        try {
            const response = await decentureConnection.post('/erp/reconcile-invoice', invoice);
            console.log('API_RESPONSE:', response.data);
        } catch (e) {
            console.log('ERROR:', e);
        }
    }
}

const provider: DecentureProvider = new DecentureProvider();

export default provider;
