import axios from 'axios';

export const decentureConnection = axios.create({
    baseURL: `http://${process.env.ERP_HOST}:${process.env.ERP_PORT}/`,
    timeout: 5000,
    headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
    },
});
