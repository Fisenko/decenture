import { Application } from 'express';
import * as verification from 'src/controller/emailVerification';
import { invoiceReceiver } from 'src/controller/invoiceReceiver';
import * as lectureCourseController from 'src/controller/lectureCourseController';
import { checkStatus, reconcileInvoice } from 'src/controller/erpController';

class Routes {
    public routes(app: Application): void {
        app.get('/email-verification', verification.emailVerification);
        app.post('/invoice', invoiceReceiver);
        app.get('/confirm-scheduled-lecture', lectureCourseController.confirmScheduledLecture);
        app.get('/test-connection-pool', verification.testConnectionPool);
        app.get('/anc-rate-by-student', lectureCourseController.ancRateByStudent);
        app.get('/anc-rate-by-university', lectureCourseController.ancRateByUniversity);
        app.post('/erp/reconcile-invoice', reconcileInvoice);
        app.get('/api/check-status', checkStatus)
    }
}

export default new Routes();
