var path = require('path');
var webpack = require('webpack');
const HTMLWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

module.exports = {
    target: 'web',
    mode: 'development',
    // devtool: 'inline-source-map', // is very slow
    entry: path.join(__dirname, '/src/index.tsx'),
    output: {
        path: path.resolve(__dirname, 'dist'),
        publicPath: '/',
        filename: 'bundle.js', // this is the compiled final javascript file which we will include in the index.html
        pathinfo: false,
    },
    resolve: {
        extensions: ['.ts', '.tsx', '.js'],
        alias: {
            src: path.resolve(__dirname, 'src'),
        },
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                loader: 'ts-loader',
                exclude: /node_modules/,
                options: {
                    transpileOnly: true,
                    experimentalWatchApi: true,
                },
            },
            {
                loader: 'babel-loader',
                test: /\.tsx$/,
                exclude: /node_modules/,
            },
            {
                test: /\.(svg|jpg|png)$/,
                exclude: /node_modules/,
                use: {
                    loader: 'url-loader',
                },
            },
        ],
    },
    devServer: {
        publicPath: '/',
        disableHostCheck: true,
        host: process.env.HOST || '0.0.0.0',
        progress: true,
        port: process.env.WEB_FRONTEND_PORT || 4600,
        contentBase: path.resolve(__dirname, '/public'),
        watchContentBase: true,
        hot: true,
        historyApiFallback: true, // this prevents the default browser full page refresh on form submission and link change
    },
    plugins: [
        new HTMLWebpackPlugin({
            favicon: 'public/favicon.ico',
            template: 'index.html',
        }),
        new CopyWebpackPlugin([
            {
                from: path.resolve(__dirname, 'public'),
                to: path.resolve(__dirname, 'dist/public'),
            },
        ]),
        // new webpack.HotModuleReplacementPlugin(),
        // new BundleAnalyzerPlugin(),
    ],
};
