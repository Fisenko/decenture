const IMAGES: { [key: string]: any } = {
    decentureLogo: require('../images/Decenture_Logo_Vertical_blue.svg'),
    decentureLogoWhite: require('../images/Decenture_Logo_Horizontal_white.svg'),
    decentureLogoBlue: require('../images/Decenture_Logo_Horizontal_blue.svg'),
    avatar: require('../images/remy.jpg'),
};

export const getImage = (name: string) => {
    return IMAGES[name];
};
