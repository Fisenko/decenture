export const prepareData = ({ lines, duration }, time = '') => {
    const result = [];
    if (!duration.length) {
        return [];
    }
    const columns = Object.keys(lines);
    result.push([time, ...columns]);
    duration.forEach((item, i) => {
        const temp = [duration[i]];
        columns.forEach(column => {
            temp.push(lines[column][i]);
        });
        result.push(temp);
    });
    return result;
};

export const preparePieChart = ({ lines, duration }) => {
    const columns = Object.keys(lines);
    const result = columns.map(item => {
        const header = ['', item];
        const swap = lines[item].map((it, i) => [duration[i], it]);
        return [header, ...swap];
    });
    return result;
};
const DATE_TYPES = { YEAR: 'YEAR', MONTH: 'MONTH', DATE: 'DATE' };

const toDateType = (type: string, data) => {
    switch (type) {
        case DATE_TYPES.YEAR:
            return (new Date(data)).getFullYear() + '';
        case DATE_TYPES.MONTH:
            const month = new Date(data);
            return month.getFullYear() + '-' + (month.getMonth() + 1);
        case DATE_TYPES.DATE:
            const date = new Date(data);
            return date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
        default:
            return data;
    }
};

export const sortDateFunction = (a, b) => {
    const dateA = new Date(a).getTime();
    const dateB = new Date(b).getTime();
    return dateA > dateB ? 1 : -1;
};

export const addColumn = (data: Array<any>, obj) => {
    return data.map(item => {
        return { ...item, ...obj };
    });
};

export const groupData = (oX: string, target: string, line: string, data: Array<any>, groupType = null) => {
    const duration = [];
    const lines = {};

    data.forEach(item => {
        if (duration.indexOf(toDateType(groupType, item[oX])) === -1) {
            duration.push(toDateType(groupType, item[oX]));
        }
        if (!(item[line] in lines)) {
            lines[item[line]] = [];
        }
    });
    duration.sort(sortDateFunction);
    duration.forEach(header => {
        data.forEach(item => {
            if (header === toDateType(groupType, item[oX])) {
                lines[item[line]].push(parseFloat(item[target]));
            }
        });
    });
    return { lines, duration };
};

function sumArray(data: Array<any>, target) {
    let sum = 0;
    data.forEach(function (a) {
        sum += a[target] || 0;
    });
    return sum;
}

export const aggregate = function (data: Array<any>, groupField, target, line, groupType = null) {
    const DURATION = [];

    const lines = {};
    data.forEach(item => {
        if (DURATION.indexOf(toDateType(groupType, item[groupField])) === -1) {
            DURATION.push(toDateType(groupType, item[groupField]));
        }
        if (!(item[line] in lines)) {
            lines[item[line]] = [];
        }
    });
    const result = [];

    const TYPES = Object.keys(lines);

    TYPES.forEach(type => {
        DURATION.forEach(time => {
            result.push({
                [groupField]: time,
                [target]: sumArray(data.filter(item => item[line] === type && toDateType(groupType, item[groupField]) === time), target),
                [line]: type
            });
        });

    });
    return result;

};

export const filter = function (data: Array<any>, filters: any) {
    return data;
};
