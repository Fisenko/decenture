import { ThemeOptions } from '@material-ui/core/styles/createMuiTheme';

export const COLORS = {
    white: '#fff',
    blue: '#2979FF',
    blueDark: '#002F6C',
    blueLight: '#F5F9FF',
    gray: '#D4D4D4',
    grayLight: '#E9E9F0',
    grayDark: '#4D565C',
};

export const THEME: ThemeOptions = {
    palette: {
        primary: {
            main: COLORS.blueDark,
            contrastText: '#fff',
        },
        secondary: {
            main: COLORS.blue,
            contrastText: '#fff',
        },
        background: {
            default: COLORS.blueLight,
        },
        text: {
            primary: COLORS.blueDark,
            secondary: COLORS.blueDark,
        },
        action: {
            active: COLORS.blueDark,
        },
    },
    typography: {
        body1: {
            color: 'rgba(0, 47, 108, 0.4)',
        },
        h4: {
            color: COLORS.blueDark,
        },
        fontFamily: 'OpenSans, Roboto-Regular',
        useNextVariants: true,
        subtitle1: {
            color: '#99ACC4',
        },
        subtitle2: {
            fontWeight: 600,
        },
    },
    overrides: {
        MuiInput: {
            root: {
                color: COLORS.blueDark,
            },
            underline: {
                '&:before': {
                    borderBottom: '1px solid #E4E9EF',
                },
            },
        },
        MuiFormLabel: {
            root: {
                color: 'rgba(0, 47, 108, 0.4)',
            },
        },
        MuiButton: {
            root: {
                textTransform: 'capitalize',
                color: COLORS.blueDark,
            },
            outlined: {
                border: '1px solid #E4E9EF',
            },
        },
        MuiTypography: {
            colorTextSecondary: {
                color: COLORS.blueDark,
                opacity: 0.8,
            },
            headline: {
                color: COLORS.blueDark,
            },
        },
        MuiAppBar: {
            colorPrimary: {
                color: COLORS.blueDark,
                backgroundColor: COLORS.white,
            },
        },
        MuiMenuItem: {
            root: {
                color: COLORS.grayDark,
            },
        },
        MuiTabs: {
            root: {
                borderBottom: '1px solid #E4E9EF',
            },
        },
        MuiTab: {
            root: {
                '&:hover': {
                    color: '#2979FF',
                    opacity: 1,
                },
            },
            textColorInherit: {
                color: '#7F96B5',
                opacity: 1,
                textTransform: 'capitalize',
                fontSize: 15,
            },
            selected: {
                color: '#2979FF',
            },
        },
        MuiTableCell: {
            head: {
                textTransform: 'uppercase',
                fontWeight: 'bold',
                color: '#7F96B5',
                backgroundColor: '#F1F3F6',
            },
            body: {
                color: COLORS.blueDark,
            },
        },
        MuiTableRow: {
            root: {
                '&:nth-of-type(even)': {
                    backgroundColor: '#f6faff',
                },
            },
        },
        MuiListItemIcon: {
            root: {
                color: 'inherit',
            },
        },
        MuiFormControl: {
            root: {
                minWidth: 300,
            },
        },
    },
};
