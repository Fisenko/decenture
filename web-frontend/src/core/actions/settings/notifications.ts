import * as actionTypes from './actionTypes';

export const notificationsInputChange = (data: any) => ({
    type: actionTypes.NOTIFICATION_INPUT_CHANGE,
    payload: data,
});

export const emailNotificationsInputChange = (data: any) => ({
    type: actionTypes.EMAIL_NOTIFICATIONS_INPUT_CHANGE,
    payload: data,
});
