import SOCKET, { IResponse } from 'src/shared/SOCKET';
import * as actionTypes from 'src/core/actions/user/actionTypes';
import * as socketChannels from 'src/core/actions/socketChannels';

const ws = new SOCKET();

export const initMain = () => dispatch => {
    dispatch({ type: actionTypes.CHECK_LOGIN_REQUEST });
    ws.emit(socketChannels.CHECK_LOGIN);

    ws.subscribe(socketChannels.CHECK_LOGIN_RESPONSE, (res: IResponse) => {
        if (res.status === 0) {
            dispatch({ type: actionTypes.CHECK_LOGIN_SUCCESS, payload: res.data });
        } else {
            dispatch({ type: actionTypes.CHECK_LOGIN_FAILED });
        }
    });
};
