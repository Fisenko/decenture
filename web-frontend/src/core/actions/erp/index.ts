import User from 'src/core/models/user';
import * as socketChannels from 'src/core/actions/socketChannels';
import SOCKET, { IResponse } from 'src/shared/SOCKET';
import { GET_RECONCILED_INVOICES_SUCCESS } from 'src/core/actions/erp/actionTypes';

const ws = new SOCKET();

export const getReconciledInvoices = () => (dispatch, getState) => {
    const user: User = getState().user.current.info;
    ws.emit(socketChannels.GET_RECONCILED_INVOICES, user);

    ws.subscribe(socketChannels.GET_RECONCILED_INVOICES_RESPONSE, (res: IResponse) => {
        if (res.status === 0) {
            dispatch({
                type: GET_RECONCILED_INVOICES_SUCCESS,
                payload: res.data,
            });
        }
    });
};
