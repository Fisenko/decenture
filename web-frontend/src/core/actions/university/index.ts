import * as socketChannels from 'src/core/actions/socketChannels';
import * as actionTypes from './actionTypes';
import SOCKET, { IResponse } from 'src/shared/SOCKET';

const ws = new SOCKET();

export const getUniversity = () => (dispatch: any) => {
    dispatch({ type: actionTypes.GET_UNIVERSITY_REQUEST });
    ws.emit(socketChannels.GET_UNIVERSITY);

    ws.subscribe(socketChannels.GET_UNIVERSITY_RESPONSE, (res: IResponse) => {
        if (res.status === 0) {
            dispatch({ type: actionTypes.GET_UNIVERSITY_SUCCESS, payload: res.data });
        } else {
            dispatch({ type: actionTypes.GET_UNIVERSITY_FAILED });
        }
    });
};
