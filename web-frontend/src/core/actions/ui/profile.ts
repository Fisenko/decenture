import * as actionTypes from './actionTypes';

export const openMenu = (anchorEl: any) => ({
    type: actionTypes.PROFILE_MENU_OPEN,
    payload: anchorEl,
});

export const closeMenu = () => ({
    type: actionTypes.PROFILE_MENU_CLOSE,
});
