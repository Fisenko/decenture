import * as actionTypes from './actionTypes';

export const setUI = (state: object) => ({
    type: actionTypes.SET_TRANSACTION_UI,
    payload: state,
});
