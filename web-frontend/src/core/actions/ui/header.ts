import * as actionTypes from './actionTypes';

export const toggleIsOpen = () => ({
    type: actionTypes.TOGGLE_HEADER_OPEN,
});
