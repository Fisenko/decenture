import * as actionTypes from './actionTypes';

export const toggleFullReportVisible = () => ({
    type: actionTypes.TOGGLE_FULL_REPORT_VISIBLE,
});
