import * as actionTypes from './actionTypes';
export const chooseChartType = (type: string) => ({
    type: actionTypes.SET_CHART_TYPE,
    payload: {
        type: type
    },
});

export const toggleAdditional = (data: string) => ({
    type: actionTypes.TOGGLE_ADDITIONAL_CHART,
    payload: {
        additional: !data
    },
});

export const setGroupParameters = (data) => {
    return {
        type: actionTypes.SET_CHART_GROUP_TYPE,
        payload: data
    }
};
