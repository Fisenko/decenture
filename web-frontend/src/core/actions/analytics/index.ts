import * as socketChannels from 'src/core/actions/socketChannels';
import * as actionTypes from './actionTypes';
import SOCKET, { IResponse } from 'src/shared/SOCKET';
import State from 'src/core/models/state';
import {setGroupParameters} from "src/core/actions/ui/chart";

const ws = new SOCKET();

export const getUniversityAnalytics = () => (dispatch: any, getState: () => State) => {
    dispatch({ type: actionTypes.GET_UNIVERSITY_ANALITYCS_REQUEST });

    const user = getState().user.current.info;
    ws.emit(socketChannels.GET_UNIVERSITY_ANALYTICS, user);

    ws.subscribe(socketChannels.GET_UNIVERSITY_ANALYTICS_RESPONSE, (res: IResponse) => {
        if (res.status === 0) {
            dispatch({ type: actionTypes.GET_UNIVERSITY_ANALITYCS_SUCCESS, payload: res.data });
        } else {
            dispatch({ type: actionTypes.GET_UNIVERSITY_ANALITYCS_FAILED });
        }
    });
};

export const getStudentsAnalytics = () => (dispatch: any, getState: () => State) => {
    dispatch({ type: actionTypes.GET_STUDENTS_ANALITYCS_REQUEST });

    const user = getState().user.current.info;
    ws.emit(socketChannels.GET_STUDENTS_ANALYTICS, user);

    ws.subscribe(socketChannels.GET_STUDENTS_ANALYTICS_RESPONSE, (res: IResponse) => {
        if (res.status === 0) {
            dispatch({ type: actionTypes.GET_STUDENTS_ANALITYCS_SUCCESS, payload: res.data });
        } else {
            dispatch({ type: actionTypes.GET_STUDENTS_ANALITYCS_FAILED });
        }
    });
};

export const initAttendance = (data = {}) => (dispatch: any, getState: () => State) => {
    ws.emit(socketChannels.GET_UNIVERSITY_ATTENDANCE, data);

    ws.subscribe(socketChannels.GET_UNIVERSITY_ATTENDANCE_RESPONSE, (res: IResponse) => {
        if (res.status === 0) {
            dispatch({ type: actionTypes.GET_ATTENDANCE_ANALITYCS_SUCCESS, payload: res.data });
        } else {
            dispatch({ type: actionTypes.GET_ATTENDANCE_ANALITYCS_FAILED });
        }
    });
};
