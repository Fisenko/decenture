import { GET_TRANSACTIONS_SUCCESS } from './actionTypes';
import SOCKET, { IResponse } from 'src/shared/SOCKET';
import * as socketChannels from 'src/core/actions/socketChannels';

const ws = new SOCKET();

export const getTransactions = () => (dispatch, getState) => {
    const user = getState().user.current.info;
    ws.emit(socketChannels.GET_USER_TRANSACTIONS, user);

    ws.subscribe(socketChannels.GET_USER_TRANSACTIONS_RESPONSE, (res: IResponse) => {
        if (res.status === 0) {
            dispatch({
                type: GET_TRANSACTIONS_SUCCESS,
                payload: res.data,
            });
        } else {
        }
    });
};
