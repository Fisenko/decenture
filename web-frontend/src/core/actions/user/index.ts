import * as socketChannels from 'src/core/actions/socketChannels';
import * as actionTypes from 'src/core/actions/user/actionTypes';
import SOCKET, { IResponse } from 'src/shared/SOCKET';

const ws = new SOCKET();

export const onInputChange = data => ({
    type: actionTypes.USER_FORM_INPUT_CHANGE,
    data,
});

export const login = data => (dispatch, getState) => {
    ws.emit(socketChannels.LOGIN, data);

    ws.subscribe(socketChannels.LOGIN_RESPONSE, (res: IResponse) => {
        if (res.status === 0) {
            dispatch({
                type: actionTypes.LOGIN_SUCCESS,
                payload: res.data,
            });
        } else {
        }
    });
};

export const logout = () => (dispatch: any) => {
    ws.emit(socketChannels.LOGOUT);

    ws.subscribe(socketChannels.LOGOUT_RESPONSE, () => {
        dispatch({
            type: actionTypes.LOGOUT_SUCCESS,
        });
    });
};
