import * as socketChannels from 'src/core/actions/socketChannels';
import * as actionTypes from './actionTypes';
import SOCKET, { IResponse } from 'src/shared/SOCKET';
import State from 'src/core/models/state';

const ws = new SOCKET();

export const initCourseCompletion = (data = {}) => (dispatch: any, getState: () => State) => {

    ws.emit(socketChannels.GET_COURSE_COMPLETION, data);

    ws.subscribe(socketChannels.GET_COURSE_COMPLETION_RESPONSE, (res: IResponse) => {
        if (res.status === 0) {
            dispatch({ type: actionTypes.INIT_COURSE_COMPLETION_SUCCESS, payload: res.data });
        } else {
            dispatch({ type: actionTypes.INIT_COURSE_COMPLETION_FAILED });
        }
    });
};
