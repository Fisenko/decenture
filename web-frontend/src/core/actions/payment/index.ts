import { GET_PAYMENT_BY_UNIVERSITY_SUCCESS } from './actionTypes';
import SOCKET, { IResponse } from 'src/shared/SOCKET';
import * as socketChannels from 'src/core/actions/socketChannels';
import User from 'src/core/models/user';

const ws = new SOCKET();

export const getPaymentByUniversity = () => (dispatch, getState) => {
    const user: User = getState().user.current.info;
    ws.emit(socketChannels.GET_PAYMENT_BY_UNIVERSITY, user);

    ws.subscribe(socketChannels.GET_PAYMENT_BY_UNIVERSITY_RESPONSE, (res: IResponse) => {
        if (res.status === 0) {
            dispatch({
                type: GET_PAYMENT_BY_UNIVERSITY_SUCCESS,
                payload: res.data,
            });
            // TODO Add for unsuccess response
        } else {
        }
    });
};
