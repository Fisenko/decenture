import Compliance from "src/core/models/compliance";
import {INIT_COURSE_COMPLETION_SUCCESS} from "src/core/actions/compliance/actionTypes";

const compliance = (state = new Compliance(), action: any) => {
    switch (action.type) {
        case INIT_COURSE_COMPLETION_SUCCESS:
            return {...state, list: action.payload};
        default:
            return state;
    }
};

export default compliance;
