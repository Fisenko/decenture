import { combineReducers } from 'redux';

import students from './students';
import university from './university';
import attendance from './attendance';

export default combineReducers({
    students,
    university,
    attendance
});
