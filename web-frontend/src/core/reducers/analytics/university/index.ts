import { GET_UNIVERSITY_ANALITYCS_SUCCESS } from 'src/core/actions/analytics/actionTypes';

export default (state = {}, action: any) => {
    switch (action.type) {
        case GET_UNIVERSITY_ANALITYCS_SUCCESS:
            return action.payload;
        default:
            return state;
    }
};
