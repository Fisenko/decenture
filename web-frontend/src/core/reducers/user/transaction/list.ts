import { GET_TRANSACTIONS_SUCCESS } from 'src/core/actions/user/actionTypes';

const list = (state = [], action: any) => {
    switch (action.type) {
        case GET_TRANSACTIONS_SUCCESS:
            return action.payload;
        default:
            return state;
    }
};

export default list;
