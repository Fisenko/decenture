import { combineReducers } from 'redux';

import info from './info';
import settings from './settings';

const current = combineReducers({
    settings,
    info,
});

export default current;
