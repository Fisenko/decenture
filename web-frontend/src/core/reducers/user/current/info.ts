import User from 'src/core/models/user';
import * as actionTypes from 'src/core/actions/user/actionTypes';

export default (state = new User(), action) => {
    switch (action.type) {
        case actionTypes.USER_FORM_INPUT_CHANGE:
            return { ...state, [action.data.name]: action.data.value };
        case actionTypes.LOGIN_SUCCESS:
            return { ...state, ...action.payload };
        case actionTypes.CHECK_LOGIN_SUCCESS:
            return { ...state, ...action.payload };
        default:
            return state;
    }
};
