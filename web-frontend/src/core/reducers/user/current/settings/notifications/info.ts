import * as actionTypes from 'src/core/actions/settings/actionTypes';
import Notifications from 'src/core/models/notifications';

export default (state = new Notifications(), action) => {
    switch (action.type) {
        case actionTypes.NOTIFICATION_INPUT_CHANGE:
            return { ...state, [action.payload.name]: action.payload.value };
        default:
            return state;
    }
};
