import { combineReducers } from 'redux';

import email from './email';
import info from './info';

const current = combineReducers({
    email,
    info,
});

export default current;
