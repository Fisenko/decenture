import { combineReducers } from 'redux';

import notifications from './notifications';

const current = combineReducers({
    notifications,
});

export default current;
