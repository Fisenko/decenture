import { combineReducers } from 'redux';

import current from './current';
import transaction from './transaction';

export default combineReducers({
    current,
    transaction,
});
