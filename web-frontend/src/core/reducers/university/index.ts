import {
    GET_UNIVERSITY_REQUEST,
    GET_UNIVERSITY_SUCCESS,
    GET_UNIVERSITY_FAILED,
} from 'src/core/actions/university/actionTypes';
import Univesity from 'src/core/models/university';

const university = (state = new Univesity(), action: any) => {
    switch (action.type) {
        case GET_UNIVERSITY_SUCCESS:
            return new Univesity(action.payload);
        default:
            return state;
    }
};

export default university;
