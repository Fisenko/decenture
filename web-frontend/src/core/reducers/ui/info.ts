import * as actionTypes from 'src/core/actions/user/actionTypes';
import Info from 'src/core/models/ui/info';

export default (state = new Info(), action) => {
    switch (action.type) {
        case actionTypes.LOGIN_SUCCESS:
            return { ...state, isAuth: true, isPending: false };
        case actionTypes.LOGOUT_SUCCESS:
            return { ...state, isAuth: false, isPending: false };
        case actionTypes.CHECK_LOGIN_REQUEST:
            return { ...state, isAuth: false, isPending: true };
        case actionTypes.CHECK_LOGIN_SUCCESS:
            return { ...state, isAuth: true, isPending: false };
        case actionTypes.CHECK_LOGIN_FAILED:
            return { ...state, isAuth: false, isPending: false };
        default:
            return state;
    }
};
