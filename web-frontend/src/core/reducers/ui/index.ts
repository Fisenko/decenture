import { combineReducers } from 'redux';

import header from './header';
import info from './info';
import transactions from './transactions';
import chart from './chart';
import analytics from './analytics';

export default combineReducers({
    header,
    info,
    transactions,
    chart,
    analytics,
});
