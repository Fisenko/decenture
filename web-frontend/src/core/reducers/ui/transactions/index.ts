import Transactions from 'src/core/models/ui/transactions';
import { SET_TRANSACTION_UI } from 'src/core/actions/ui/actionTypes';
import { GET_TRANSACTIONS_SUCCESS } from 'src/core/actions/user/actionTypes';

export default (state = new Transactions(), action) => {
    switch (action.type) {
        case SET_TRANSACTION_UI:
            return { ...state, ...action.payload };
        case GET_TRANSACTIONS_SUCCESS:
            return { ...state, search: '', filtered: action.payload };
        default:
            return state;
    }
};
