import * as actionTypes from 'src/core/actions/ui/actionTypes';
import Analytics from 'src/core/models/analitycs';

export default (state = new Analytics(), action) => {
    switch (action.type) {
        case actionTypes.TOGGLE_FULL_REPORT_VISIBLE:
            return {...state, isVisibleFullReport: !state.isVisibleFullReport};
        default:
            return state;
    }
};
