import Header from 'src/core/models/ui/header';
import * as actionTypes from 'src/core/actions/ui/actionTypes';

export default (state = new Header(), action) => {
    switch (action.type) {
        case actionTypes.TOGGLE_HEADER_OPEN:
            return { ...state, isOpen: !state.isOpen };
        default:
            return state;
    }
};
