import Profile from 'src/core/models/ui/profile';
import * as actionTypes from 'src/core/actions/ui/actionTypes';

export default (state = new Profile(), action) => {
    switch (action.type) {
        case actionTypes.PROFILE_MENU_OPEN:
            return { ...state, anchorEl: action.payload };
        case actionTypes.PROFILE_MENU_CLOSE:
            return { ...state, anchorEl: null };
        default:
            return state;
    }
};
