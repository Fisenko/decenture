import { combineReducers } from 'redux';

import info from './info';
import profile from './profile';

export default combineReducers({
    info,
    profile,
});
