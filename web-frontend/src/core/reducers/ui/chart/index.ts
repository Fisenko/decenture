import { SET_CHART_TYPE, SET_CHART_GROUP_TYPE, TOGGLE_ADDITIONAL_CHART } from 'src/core/actions/ui/actionTypes';
import Chart from "src/core/models/ui/chart";

export default (state = new Chart(), action) => {
    switch (action.type) {
        case SET_CHART_TYPE:
        case TOGGLE_ADDITIONAL_CHART:
            return {...state, ...action.payload};
        case SET_CHART_GROUP_TYPE:
            return {...state, ...action.payload};
        default:
            return state;
    }
};
