import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import user from 'src/core/reducers/user';
import ui from 'src/core/reducers/ui';
import analytics from 'src/core/reducers/analytics';
import university from 'src/core/reducers/university';
import payment from 'src/core/reducers/payment';
import erp from './erp';
import compliance from 'src/core/reducers/compliance'

export default combineReducers({
    ui,
    user,
    analytics,
    university,
    router: routerReducer,
    payment,
    erp,
    compliance
});
