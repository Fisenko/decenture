import * as actionTypes from 'src/core/actions/erp/actionTypes';

export default (state = [], action) => {
    switch (action.type) {
        case actionTypes.GET_RECONCILED_INVOICES_SUCCESS:
            return action.payload;
        default:
            return state;
    }
};
