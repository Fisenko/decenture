import invoices from './invoices';
import { combineReducers } from 'redux';

export default combineReducers({
    invoices,
});
