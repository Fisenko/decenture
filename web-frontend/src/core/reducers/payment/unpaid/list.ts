import * as actionTypes from 'src/core/actions/payment/actionTypes';

const list = (state = [], action: any) => {
    switch (action.type) {
        case actionTypes.GET_PAYMENT_BY_UNIVERSITY_SUCCESS:
            return action.payload.unpaidInvoices;
        default:
            return state;
    }
};

export default list;
