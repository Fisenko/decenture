import * as actionTypes from 'src/core/actions/payment/actionTypes';
import Payment from 'src/core/models/payment';

export default (state = new Payment(), action) => {
    switch (action.type) {
        case actionTypes.GET_PAYMENT_BY_UNIVERSITY_SUCCESS:
            return new Payment(action.payload);
        default:
            return state;
    }
};
