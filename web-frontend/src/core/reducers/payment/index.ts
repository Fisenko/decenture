import { combineReducers } from 'redux';

import current from './current';
import paid from './paid';
import unpaid from './unpaid';

export default combineReducers({
    current,
    paid,
    unpaid,
});
