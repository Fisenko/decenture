export enum ISortDirection {
    asc = 'asc',
    desc = 'desc',
}
