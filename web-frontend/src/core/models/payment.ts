export default class Payment {
    public paidInvoicesCount: number;
    public paidInvoicesSumAmount: number;
    public unpaidInvoicesCount: number;
    public unpaidInvoicesSumAmount: number;
    public pendingInvoicesCount: number;
    public pendingInvoicesSumAmount: number;
    public totalBalance: number;
    public todaysBeginningBalance: number;
    public monthsBeginningBalance: number;

    constructor(data: any = {}) {
        this.paidInvoicesCount = data.paidInvoicesCount || 0;
        this.paidInvoicesSumAmount = data.paidInvoicesSumAmount || 0;
        this.unpaidInvoicesCount = data.unpaidInvoicesCount || 0;
        this.unpaidInvoicesSumAmount = data.unpaidInvoicesSumAmount || 0;
        this.pendingInvoicesCount = data.pendingInvoicesCount || 0;
        this.pendingInvoicesSumAmount = data.pendingInvoicesSumAmount || 0;
        this.totalBalance = data.totalBalance || 0;
        this.todaysBeginningBalance = data.todaysBeginningBalance || 0;
        this.monthsBeginningBalance = data.monthsBeginningBalance || 0;
    }

    isEmpty = (): boolean => {
        if (!this.paidInvoicesSumAmount) {
            return true;
        } else if (!this.unpaidInvoicesSumAmount) {
            return true;
        }

        return false;
    };
}
