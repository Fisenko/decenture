export default class Notifications {
    public message: boolean;
    public analytics: boolean;
    public invoices: boolean;

    constructor(data: any = {}) {
        this.message = data.message || false;
        this.analytics = data.analytics || false;
        this.invoices = data.invoices || false;
    }
}
