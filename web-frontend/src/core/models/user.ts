export default class User {
    public id: string;
    public email: string;
    public password: string;
    public name: string;
    public notifications: number;

    constructor(data: any = {}) {
        this.id = data.id || '234234';
        this.email = data.email || '';
        this.name = data.name || 'George Benton';
        this.password = data.password || '';
        this.notifications = data.notifications || 0;
    }
}
