export default class Analytics {
    public isVisibleFullReport: boolean;

    constructor(data: any = {}) {
        this.isVisibleFullReport = data.isVisibleFullReport || false;
    }
}
