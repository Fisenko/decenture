import Profile from './profile';
import Header from './header';
import Info from './info';
import Transactions from './transactions';
import Compliance from './compliance';
import Chart from "src/core/models/ui/chart";
import Analytics from 'src/core/models/analitycs';

export default interface UI {
    header: {
        profile: {
            info: Profile;
        };
        info: Header;
    };
    transactions: Transactions;
    info: Info;
    chart: Chart;
    analytics: {
        info: Analytics;
    };
}
