export default class Info {
    public isAuth: boolean;
    public isPending: boolean;

    constructor(data: any = {}) {
        this.isAuth = data.isAuth || false;
        this.isPending = data.isPending || true;
    }
}
