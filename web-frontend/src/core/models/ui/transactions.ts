import { ISortDirection } from '..';
import ITransaction from 'src/core/models/transaction';

export default class Transactions {
    public page: number;
    public rowsPerPage: number;
    public order: string;
    public orderBy: string;
    public search: string;
    public filtered: Array<ITransaction>;

    constructor(data: any = {}) {
        this.page = data.page || 0;
        this.rowsPerPage = data.rowsPerPage || 10;
        this.order = data.order || ISortDirection.desc;
        this.orderBy = data.orderBy || 'datetime';
        this.search = data.search || '';
        this.filtered = data.filtered || [];
    }
}
