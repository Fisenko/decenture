export default class Chart {
    public type: string;
    public line: string;
    public groupField: string;
    public target: string;
    public groupType?: string;
    public serverOption?: any;
    public chartOption?: any;
    public yUnit: string;
    public additional: boolean;

    constructor(data: any = {}) {
        this.type = data.type || 'columnChart';
        this.line = data.line || '';
        this.groupField = data.groupField || '';
        this.target = data.target || '';
        this.groupType = data.groupType || 'YEAR';
        this.serverOption = data.serverOption || {};
        this.chartOption = {};
        this.yUnit = '';
        this.additional = false;
    }
}
