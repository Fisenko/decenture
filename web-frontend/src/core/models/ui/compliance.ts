export default class Compliance {
    public tabNumber: number;

    constructor(data: any = {}) {
        this.tabNumber = data.tabNumber || 0;
    }
}
