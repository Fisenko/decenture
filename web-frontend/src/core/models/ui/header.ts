export default class Header {
    public isOpen: boolean;

    constructor(data: any = {}) {
        this.isOpen = data.isOpen || true;
    }
}
