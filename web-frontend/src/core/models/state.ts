import User from './user';
import UI from './ui';
import Transaction from './transaction';
import Univesity from 'src/core/models/university';
import Payment from 'src/core/models/payment';
import Notifications from 'src/core/models/notifications';

export default interface State {
    ui: UI;
    user: {
        current: {
            info: User;
            settings: {
                notifications: {
                    info: Notifications;
                    email: {
                        info: Notifications;
                    };
                };
            };
        };
        transaction: {
            list: Array<Transaction>,
        },
    };
    university: Univesity;
    analytics: {
        university: any;
        students: Array<any>;
        attendance: Array<any>;
    };
    payment: {
        current: {
            info: Payment;
        };
        paid: {
            list: Array<any>;
        };
        unpaid: {
            list: Array<any>;
        };
    };
    erp: {
        invoices: Array<any>;
    };
    compliance: {
        list: Array<any>
    }

}
