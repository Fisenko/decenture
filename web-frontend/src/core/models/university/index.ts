export default class Univesity {
    public id: string;
    public name: string;
    public officialName: string;

    constructor(data: any = {}) {
        this.id = data.id || '1';
        this.name = data.name || 'University Name';
        this.officialName = data.officialName || 'Official University Name';
    }
}
