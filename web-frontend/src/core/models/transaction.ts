export default class ITransaction {
    public id: string;
    public block: number;
    public datetime: Date;
    public amount: number;
    public type: string;
    public receiver: string;
    public sender: string;

    constructor(data: any = {}) {
        this.id = data.id || 1;
        this.block = data.block || 1;
        this.datetime = data.datetime || new Date();
        this.amount = data.amount || 0;
        this.type = data.type || '';
        this.receiver = data.receiver || '';
        this.sender = data.sender || '';
    }
}
