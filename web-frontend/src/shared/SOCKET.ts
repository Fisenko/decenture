import autobind from 'autobind-decorator';
import * as io from 'socket.io-client';

export interface IResponse {
    data: any;
    status: number;
    errMsg: string;
}

export default class SOCKET {
    private static port = process.env.BACKEND_PORT || 4601;
    private static host = process.env.PUBLIC_HOST || window.location.hostname;
    private static _instance: SOCKET;
    private static _isOpen = false;
    private static _QUEUE = [];

    private static LISTENER_MAP: { [listener: string]: (data: any) => void } = {};
    public ws: SocketIOClient.Socket;

    constructor() {
        if (SOCKET._instance) {
            return SOCKET._instance;
        }
        this.init();
        SOCKET._instance = this;
    }

    init() {
        console.log('============ BACKEND URL:', `${SOCKET.host}:${SOCKET.port}`, '============');
        // console.log('============ ENVIRONMENTS:', JSON.stringify(process.env), '============');

        this.ws = io(`ws://${SOCKET.host}:${SOCKET.port}`);

        this.ws.on('OPEN', () => {
            console.log('WS:OPEN');
            this.ws.emit(
                'CHECK_TOKEN',
                JSON.stringify({
                    token: localStorage.getItem('token'),
                }),
            );

            if (SOCKET._isOpen) {
                // Fix multiple connections
                return;
            }

            this.ws.on('SET_TOKEN', (data: any) => {
                data = JSON.parse(data);
                localStorage.setItem('token', data.token);
                SOCKET._isOpen = true;
                SOCKET._QUEUE.forEach(elem => {
                    this.emit(elem.code, elem.data);
                });
            });
        });

        this.ws.on('message', this.onMessage);
    }

    @autobind
    onMessage(response: any) {
        const { code, data, status, errMsg } = JSON.parse(response);
        console.log(code, data, status, errMsg);
        if (status === 0 || status === 1) {
            try {
                if (code && data) {
                    if (typeof SOCKET.LISTENER_MAP[code] === 'function') {
                        SOCKET.LISTENER_MAP[code]({ status, data, errMsg });
                    } else {
                        console.error('WS:MESSAGE_WITHOUT_HANDLER', code);
                    }
                }
            } catch (e) {
                console.error('WS:MESSAGE_PARSE_ERROR', data);
                console.error(e);
            }
        } else {
            console.error('WS:ERROR', errMsg);
        }
    }

    @autobind
    subscribe(messageCode, func) {
        SOCKET.LISTENER_MAP[messageCode] = func;
    }

    @autobind
    emit(code: string, data: any = '') {
        if (SOCKET._isOpen) {
            this.ws.emit(
                'message',
                JSON.stringify({
                    code: code,
                    token: localStorage.getItem('token'),
                    data: data,
                }),
            );
        } else {
            SOCKET._QUEUE.push({ code, data });
        }
    }
}

const socket = new SOCKET();
socket.subscribe('ANSWER', data => console.log('WS:ANSWER', data));
socket.subscribe('ERROR', data => console.error('WS_BACKEND:ERROR', data.error));
socket.subscribe('OPEN_SUCCESS', data => {});
