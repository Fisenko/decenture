import axios from 'axios';

export const backendConnection = axios.create({
    baseURL: `http://${process.env.PUBLIC_HOST}:${process.env.BACKEND_PORT}/`,
    timeout: 5000,
    headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
    },
});
