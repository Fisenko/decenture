import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { applyMiddleware, createStore, AnyAction } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { Provider } from 'react-redux';
import createHistory from 'history/createBrowserHistory';
import { ConnectedRouter, routerMiddleware } from 'react-router-redux';
import thunk, { ThunkMiddleware } from 'redux-thunk';

import App from 'src/container';
import rootReducer from 'src/core/reducers';

declare module 'redux' {
    export type GenericStoreEnhancer = any;
}

const history = createHistory();
const middleware = routerMiddleware(history);

const store = createStore(
    rootReducer,
    composeWithDevTools(applyMiddleware(middleware, thunk as ThunkMiddleware)),
);

const rootEl = document.getElementById('app');

ReactDOM.render(
    <Provider store={store}>
        <ConnectedRouter history={history}>
            <App />
        </ConnectedRouter>
    </Provider>,
    rootEl,
);
