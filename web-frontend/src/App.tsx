import * as React from 'react';
import { MuiThemeProvider } from '@material-ui/core/styles';
import createMuiTheme from '@material-ui/core/styles/createMuiTheme';
import CssBaseline from '@material-ui/core/CssBaseline';
import { hot } from 'react-hot-loader';

import { THEME } from 'src/theme';
import Routes from './routes';

const theme = createMuiTheme(THEME);

interface AppProps {
    user: any;
    initMain: any;
}

class App extends React.Component<AppProps> {
    componentWillMount() {
        this.props.initMain();
    }

    render() {
        return (
            <MuiThemeProvider theme={theme}>
                <CssBaseline />
                <Routes />
            </MuiThemeProvider>
        );
    }
}

export default hot(module)(App);
