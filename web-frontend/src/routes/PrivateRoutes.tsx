import * as React from 'react';
import { Switch, Route } from 'react-router-dom';

import ContactUs from 'src/components/contact_us';
import Transactions from 'src/components/transactions';
import Wrapper from 'src/components/common/wrapper';
import Home from 'src/components/home';
import Compliance from 'src/components/compliance/container';
import About from 'src/components/about';
import Analytics from 'src/components/analytics/container';
import Settings from 'src/components/settings';

const PrivateRoutes = () => {
    return (
        <Wrapper>
            <Switch>
                <Route exact path="/" component={Home} />
                <Route path="/contact-us" component={ContactUs} />
                <Route path="/transactions" component={Transactions} />
                <Route path="/compliance" component={Compliance} />
                <Route path="/finance" component={Analytics} />
                <Route path="/about" component={About} />
                <Route path="/settings" component={Settings} />
            </Switch>
        </Wrapper>
    );
};

export default PrivateRoutes;
