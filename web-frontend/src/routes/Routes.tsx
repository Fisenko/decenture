import * as React from 'react';
import { Switch, Route } from 'react-router-dom';

import PrivateRoute from 'src/components/common/private_route';
import Login from 'src/components/authentication/login';
import PrivateRoutes from './PrivateRoutes';
import InvoiceViewer from 'src/components/erp';
import EmailVerification from 'src/components/email_verification/EmailVerification.tsx';

const Routes = () => (
    <Switch>
        <Route path="/login" component={Login} />
        <Route path="/erp" component={InvoiceViewer} />
        <Route path="/email-verification" component={EmailVerification} />
        {/* path="/" must be last */}
        <PrivateRoute path="/" component={PrivateRoutes} />
    </Switch>
);

export default Routes;
