import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import Routes from './Routes';
import State from 'src/core/models/state';

const mapStateToProps = (state: State, props) => ({
    ...state.ui.info,
});

export default withRouter(connect(mapStateToProps)(Routes));
