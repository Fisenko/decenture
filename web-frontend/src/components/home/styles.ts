import { StyleRulesCallback } from '@material-ui/core';

const styles: StyleRulesCallback = theme => ({
    page: {
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'flex-start',
    },
});

export default styles;
