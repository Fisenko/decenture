import * as React from 'react';
import AccountBalanceWalletRounded from '@material-ui/icons/AccountBalanceWalletRounded';
import BarChart from '@material-ui/icons/BarChart';
import DonutSmall from '@material-ui/icons/DonutSmall';
import HomeCard from './home_card';
import { withStyles, WithStyles } from '@material-ui/core/styles';

import styles from './styles';
import Page from '../common/page';
import Univesity from 'src/core/models/university';

export interface HomeProps extends WithStyles<any> {
    university: Univesity;
}

class Home extends React.Component<HomeProps> {
    render() {
        const { classes, university } = this.props;

        return (
            <Page title={university.officialName}>
                <div className={classes.page}>
                    <HomeCard title="Compliance" to="/compliance" Icon={DonutSmall} />
                    <HomeCard title="Finance" to="/finance" Icon={BarChart} />
                    <HomeCard title="Transactions" to="/transactions" Icon={AccountBalanceWalletRounded} />
                </div>
            </Page>
        );
    }
}

export default withStyles(styles)(Home);
