import * as React from 'react';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import { Link } from 'react-router-dom';
import { withStyles, WithStyles } from '@material-ui/core/styles';

import styles from './styles';

export interface CardProps extends WithStyles<any> {
    title: string;
    to: string;
    Icon: React.ComponentType<any>;
}

const HomeCard = ({ title, to, Icon, classes }: CardProps) => {
    const link = props => <Link to={to} {...props} />;

    return (
        <Card className={classes.card}>
            <Button component={link}>
                <CardContent className={classes.cardContent}>
                    <div className={classes.cardTitle}>{title}</div>
                    <div>
                        <Icon className={classes.icon} />
                    </div>
                </CardContent>
            </Button>
        </Card>
    );
};

export default withStyles(styles)(HomeCard);
