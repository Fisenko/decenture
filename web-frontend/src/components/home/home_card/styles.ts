import { StyleRulesCallback } from '@material-ui/core';

const styles: StyleRulesCallback = theme => ({
    card: {
        width: 368,
        height: 136,
        marginRight: 24,
        marginBottom: 24,
    },
    cardTitle: {
        flex: '1 1 auto',
        width: 180,
        fontSize: 24,
        color: '#002F6C',
    },
    cardContent: {
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'nowrap',
        alignContent: 'stretch',
        justifyContent: 'space-between',
    },
    icon: {
        fontSize: '180px',
        color: '#B1BFD2',
    },
});

export default styles;
