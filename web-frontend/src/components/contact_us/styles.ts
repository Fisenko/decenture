import { StyleRulesCallback } from '@material-ui/core';
import { COLORS } from 'src/theme';

const styles: StyleRulesCallback = (theme): any => ({
    wrap: {
        backgroundColor: theme.palette.background.default,
        height: '100vh',
        display: 'flex',
        padding: '30px 75px',
    },
    content: { padding: '40px 32px' },
    title: {
        fontWeight: 'bold',
        color: COLORS.blueDark,
    },
    subtitle: {
        fontSize: 13,
    },
});

export default styles;
