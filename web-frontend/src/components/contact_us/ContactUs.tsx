import * as React from 'react';
import { withStyles, WithStyles } from '@material-ui/core/styles';
import { Paper, TextField, Button, Typography, FormControl, InputLabel, Select, MenuItem } from '@material-ui/core';

import styles from './styles';
import Page from '../common/page';

interface ContactUsProps {}

class ContactUs extends React.Component<ContactUsProps & WithStyles<any>> {
    state = {
        topic: '',
    };

    handleChange = event => {
        this.setState({ [event.target.name]: event.target.value });
    };

    handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
    };

    render() {
        const { classes } = this.props;

        return (
            <Page title="Contact Us">
                <Paper className={classes.content}>
                    <Typography variant="subtitle1" className={classes.title}>
                        Get In Touch
                    </Typography>
                    <Typography variant="subtitle1" className={classes.subtitle}>
                        Need help? Use the form below to get in touch.
                    </Typography>
                    <br />
                    <form onSubmit={this.handleSubmit}>
                        <FormControl className={classes.formControl}>
                            <InputLabel htmlFor="contact-us-topic">Topic</InputLabel>
                            <Select
                                value={this.state.topic}
                                onChange={this.handleChange}
                                inputProps={{
                                    name: 'topic',
                                    id: 'contact-us-topic',
                                }}
                            >
                                <MenuItem value="general">General Support / Feedback</MenuItem>
                            </Select>
                        </FormControl>
                        <div>
                            <TextField id="contact-us-full-name" label="Full Name" margin="normal" />
                        </div>
                        <div>
                            <TextField id="contact-us-email" label="Email" margin="normal" />
                        </div>
                        <div>
                            <TextField id="contact-us-message" label="Message" multiline rows="4" margin="normal" />
                        </div>
                        <br />
                        <Button type="submit" color="secondary" variant="contained">
                            Send Message
                        </Button>
                    </form>
                </Paper>
            </Page>
        );
    }
}

export default withStyles(styles)(ContactUs);
