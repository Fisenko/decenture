import * as React from 'react';
import { withStyles, WithStyles } from '@material-ui/core/styles';
import { Paper } from '@material-ui/core';

import styles from './styles';
import { TableHeadRow } from '../common/enhanced_table_head/EnhancedTableHead';
import Page from '../common/page';
import UITransactions from 'src/core/models/ui/transactions';
import ITransaction from 'src/core/models/transaction';
import Table from '../common/table';
import Row from './table/Row';
import { ISortDirection } from 'src/core/models';
import FilterContainer from './FilterContainer';

export interface TransactionsProps extends WithStyles<any>, UITransactions {
    transactions: Array<ITransaction>;
    order: ISortDirection;
    orderBy: string;
    getTransactions: any;

    setUI(state: object): void;
}

const tableHead: Array<TableHeadRow> = [
    { id: 'datetime', numeric: false, disablePadding: false, label: 'Date' },
    { id: 'amount', numeric: false, disablePadding: false, label: 'Amount' },
    { id: 'type', numeric: false, disablePadding: false, label: 'Type' },
    { id: 'sender', numeric: false, disablePadding: false, label: 'Sender' },
    { id: 'receiver', numeric: false, disablePadding: false, label: 'Recipient' },
    { id: 'id', numeric: false, disablePadding: false, label: 'Transaction ID', right: true },
];

class Transactions extends React.Component<TransactionsProps> {
    componentDidMount() {
        if (!this.props.transactions.length) {
            this.props.getTransactions();
        }
    }

    render() {
        const { classes, rowsPerPage, page, order, orderBy, filtered } = this.props;

        return (
            <Page title="Transactions">
                <Paper className={classes.content}>
                    <div className={classes.right}>
                        <FilterContainer />
                    </div>
                    <Table
                        rows={filtered}
                        tableHead={tableHead}
                        rowsPerPage={rowsPerPage}
                        page={page}
                        order={order}
                        orderBy={orderBy}
                        Row={Row}
                        setUI={this.props.setUI}
                    />
                </Paper>
            </Page>
        );
    }
}

export default withStyles(styles)(Transactions);
