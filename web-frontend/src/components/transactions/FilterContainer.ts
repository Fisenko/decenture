import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import { withRouter } from 'react-router-dom';

import State from 'src/core/models/state';
import { setUI } from 'src/core/actions/ui/transaction';
import Filter from 'src/components/common/filter';

const mapStateToProps = (state: State, props) => ({
    ...state.ui.transactions,
    ...state.user.transaction,
});

const mapDispatchToProps = (dispatch: Dispatch) => bindActionCreators({ setUI }, dispatch);

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps,
    )(Filter),
);
