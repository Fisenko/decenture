import { randomDate, getRandomInt } from 'src/utils';
import ITransaction from 'src/core/models/transaction';

const differenceDate = 7;
const endDate = new Date();
const startDate = new Date();
startDate.setDate(endDate.getDate() - differenceDate);

const senders = [
    'Jeannie Bodechon',
    'Clayton Linscott',
    'Darius Creebo',
    'Susy De Ruggiero',
    'Tresa Venn',
    'Bar Daftor',
    'Boote Athy',
    'Salomo Franklyn',
    'Verna Moneti',
    'Ryon Felkin',
    'Rhody Wyrill',
    'Ginny Meharg',
    'Maurie Woodroof',
    'Ferdie Keave',
    'Joellen Folca',
    'Diarmid Blumfield',
    'Rebeka Andreassen',
    'Michaella Pinckstone',
    'Emmaline Noller',
    'Timmy Verlinde',
];

const recipients = [
    'Malayer University',
    'Shihezi University',
    'Helwan University',
    'National University',
    'University of Dammam',
    'Ilia Chavchavadze State University',
    'Long Island University',
    'Yarmouk University',
    'Chowan College',
];

const types = ['Tuition', 'Textbooks'];

const random = (): ITransaction => {
    const datetime = randomDate(startDate, endDate);

    return {
        id: getRandomInt(1, 999999).toString(),
        block: getRandomInt(1, 100),
        amount: getRandomInt(100, 10000),
        type: types[getRandomInt(0, types.length)],
        sender: senders[getRandomInt(0, senders.length)],
        receiver: recipients[getRandomInt(0, recipients.length)],
        datetime,
    };
};

export const generate = (count: number): Array<ITransaction> => {
    const transactions = [];
    for (let i = 0; i < count; i++) {
        transactions.push(random());
    }
    return transactions;
};
