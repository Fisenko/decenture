import * as React from 'react';
import * as moment from 'moment';
import { TableRow, TableCell, withStyles, WithStyles } from '@material-ui/core';

import ITransaction from 'src/core/models/transaction';
import commonStyles from 'src/components/common/styles';

const codeLength = 20;

export interface RowProps extends ITransaction, WithStyles {
    index: number;
}

const Row = (props: RowProps) => {
    const { classes } = props;

    return (
        <TableRow>
            <TableCell>{moment(props.datetime).format('MM/DD/YYYY hh:mm A')}</TableCell>
            <TableCell>${props.amount}</TableCell>
            <TableCell>{props.type}</TableCell>
            <TableCell>{props.sender}</TableCell>
            <TableCell>{props.receiver}</TableCell>
            <TableCell className={classes.alignRight}>{props.id.substring(props.id.length - codeLength)}</TableCell>
        </TableRow>
    );
};

export default withStyles(commonStyles)(Row);
