import { StyleRulesCallback } from '@material-ui/core';

const styles: StyleRulesCallback = theme => ({
    content: {
        padding: 25,
    },
    table: {
        minWidth: 700,
    },
    right: {
        display: 'flex',
        justifyContent: 'flex-end',
        marginBottom: 15,
    },
    tableWrapper: {
        overflowX: 'auto',
    },
});

export default styles;
