import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import { withRouter } from 'react-router-dom';

import Transactions from './Transactions';
import State from 'src/core/models/state';
import { setUI } from 'src/core/actions/ui/transaction';
import { getTransactions } from 'src/core/actions/user/transaction';

const mapStateToProps = (state: State, props) => ({
    ...state.ui.transactions,
    transactions: state.user.transaction.list,
});

const mapDispatchToProps = (dispatch: Dispatch) =>
    bindActionCreators({ getTransactions, setUI }, dispatch);

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps,
    )(Transactions),
);
