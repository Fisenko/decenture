import { WithStyles } from '@material-ui/core';
import Divider from '@material-ui/core/Divider';
import withStyles from '@material-ui/core/es/styles/withStyles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import * as React from 'react';
import Page from 'src/components/common/page/Page';
import styles from 'src/components/email_verification/styles';
import { backendConnection } from 'src/config';
import { getImage } from 'src/utils/image_holder';

export interface EmailVerificationProps extends WithStyles<any> {
    location: any;
}

interface State {
    isResponsed: boolean;
    message: string;
}

const SUCCESS_MESSAGE = 'Your account has been activated, please launch the Decenture App and enter your credentials.';
const ERROR_MESSAGE = 'Verification failed';

class EmailVerification extends React.Component<EmailVerificationProps, State> {

    constructor(props) {
        super(props);
        this.state = {
            isResponsed: false,
            message: '',
        };
    }

    componentDidMount() {
        backendConnection.get(`email-verification${this.props.location.search}`)
            .then(res => {
                this.setState({ message: SUCCESS_MESSAGE , isResponsed: true});
            })
            .catch( (error)  => {
                this.setState({ message: ERROR_MESSAGE , isResponsed: true});
                console.log(error);
            });
    }

    render() {

        const { classes } = this.props;

        return (
            <Page className={classes.layout}>
                {this.state.isResponsed &&
                <Paper className={classes.paper}>
                    <img className={classes.logo} src={getImage('decentureLogoBlue')}/>
                    <Divider variant='middle' className={classes.line}/>
                    <Typography className={classes.text}>
                        {this.state.message}
                    </Typography>
                    <Typography className={classes.footer}>
                        2018 Decenture, Inc. All rights reserved
                    </Typography>
                </Paper>
                }
            </Page>
        );
    }
}

export default withStyles(styles)(EmailVerification);
