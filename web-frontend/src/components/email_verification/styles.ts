import { StyleRulesCallback } from '@material-ui/core';

const styles: StyleRulesCallback = theme => ({

    layout: {
        height: '100vh',
    },
    paper: {
        height: '600px',
        width: '600px',
        display: 'flex',
        justifyContent: 'flex-start',
        flexDirection: 'column',
        alignItems: 'center',
        margin: '120px auto',
        padding: '20px 60px',
        textAlign: 'center',
    },
    logo: {
        width: 200,
        height: 34,
        objectFit: 'contain',
        margin: '40px 20px 20px',
    },
    center: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    line: {
        width: '100%',
        margin: '16px auto 100px',
    },
    text: {
        padding: '0px 50px 0px 50px',
        color: '#002F6C',
        fontSize: '16px',
    },
    footer: {
        margin: 'auto auto 0 auto',
        color: '#002F6C',
        opacity: '0.5',
    },

});

export default styles;
