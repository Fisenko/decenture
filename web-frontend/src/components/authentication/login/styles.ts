import { StyleRulesCallback } from '@material-ui/core';

const styles: StyleRulesCallback = (theme): any => ({
    layout: {
        display: 'flex',
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
        width: 570,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        padding: 80,
        marginBottom: 100,
    },
    form: {
        width: '100%',
        marginTop: theme.spacing.unit,
    },
    submit: {
        marginTop: theme.spacing.unit * 6,
        fontSize: 18,
        padding: '13px 100px',
    },
    logo: {
        width: 240,
        height: 130,
        objectFit: 'contain',
        marginTop: 60,
        marginBottom: 60,
    },
    spaceBetween: {
        display: 'flex',
        justifyContent: 'space-between',
    },
    center: {
        display: 'flex',
        justifyContent: 'center',
    },
});

export default styles;
