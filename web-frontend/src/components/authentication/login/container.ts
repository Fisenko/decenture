import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import { withRouter } from 'react-router-dom';

import Login from './Login';
import { login, onInputChange } from 'src/core/actions/user';
import State from 'src/core/models/state';

const mapStateToProps = (state: State, props) => ({
    user: state.user.current.info,
    isAuth: state.ui.info.isAuth,
});

const mapDispatchToProps = (dispatch: Dispatch) =>
    bindActionCreators(
        {
            onInputChange,
            login,
        },
        dispatch,
    );

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps,
    )(Login),
);
