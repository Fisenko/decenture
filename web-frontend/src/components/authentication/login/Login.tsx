import * as React from 'react';
import { withStyles, WithStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import Paper from '@material-ui/core/Paper';
import { Redirect } from 'react-router';
import { Typography } from '@material-ui/core';

import styles from 'src/components/authentication/login/styles';
import { getImage } from 'src/utils/image_holder';
import EmptyPage from 'src/components/common/empty_page';
import User from 'src/core/models/user';

export interface LoginProps extends WithStyles<any> {
    user: User;
    isAuth: boolean;
    onInputChange: any;
    login: any;
}

class Login extends React.Component<LoginProps> {
    handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const { name, value } = e.target;
        this.props.onInputChange({ name, value });
    };

    handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        this.props.login(this.props.user);
    };

    render() {
        const { classes, isAuth, user } = this.props;

        if (isAuth) {
            return <Redirect to="/" />;
        }

        return (
            <EmptyPage className={classes.layout}>
                <div className={classes.center}>
                    <img className={classes.logo} src={getImage('decentureLogo')} />
                </div>
                <Paper className={classes.paper}>
                    <Typography color="textSecondary">Welcome back! Please login to your account.</Typography>
                    <form className={classes.form} onSubmit={this.handleSubmit}>
                        <FormControl margin="normal" fullWidth>
                            <InputLabel htmlFor="email">University admin ID</InputLabel>
                            <Input
                                id="email"
                                name="email"
                                type="email"
                                autoFocus
                                value={user.email}
                                onChange={this.handleInputChange}
                            />
                        </FormControl>
                        <FormControl margin="normal" fullWidth>
                            <InputLabel htmlFor="password">Password</InputLabel>
                            <Input
                                name="password"
                                type="password"
                                id="password"
                                value={user.password}
                                onChange={this.handleInputChange}
                            />
                        </FormControl>
                        <div className={classes.spaceBetween}>
                            <FormControlLabel
                                control={<Checkbox value="remember" color="primary" />}
                                label="Remember me"
                            />
                            <Button>Forgot Password</Button>
                        </div>
                        <div className={classes.center}>
                            <Button type="submit" variant="contained" color="secondary" className={classes.submit}>
                                Login
                            </Button>
                        </div>
                    </form>
                </Paper>
            </EmptyPage>
        );
    }
}

export default withStyles(styles)(Login);
