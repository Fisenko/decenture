import * as React from 'react';
import { WithStyles } from '@material-ui/core';
import withStyles from '@material-ui/core/styles/withStyles';

import styles from './styles';
import Chart from 'src/core/models/ui/chart';
import Charts from 'src/components/common/chart/container';
import {
    COMPLETION_GROUP,
    METRICS_COMPLETION,
    TIME_AGGREGATOR_COMPLETION
} from 'src/components/common/chart/Aggregation/aggregationTypes';
import { groupData } from 'src/utils/chart';
import { DEGREE_FILTRATOR, INTERNATIONAL_FILTRATOR } from 'src/components/common/chart/Filters/filterTypes';

const DEFAULT_OPTIONS = {
    groupField: 'report_date',
    target: 'completed',
    line: 'university_name',
    groupType: 'YEAR',
    serverOption: {
        dateTrunc: 'year',
        degreeName: ['master', 'bachelor'],
        isInternational: 'all'
    },
    chartOption: {
        vAxis: {
            viewWindow: {}
        },
    },
    yUnit: ''
};

interface CourseCompletionProps extends WithStyles<any> {
    compliance: Array<any>
    initCourseCompletion: any;
    setGroupParameters: any;
    groupType: Chart;
}


class CourseCompletion extends React.Component<CourseCompletionProps> {

    componentDidMount() {
        this.props.initCourseCompletion(
            DEFAULT_OPTIONS
        );
        this.props.setGroupParameters(
            DEFAULT_OPTIONS
        );
    }

    render() {

        const { classes } = this.props;

        return (
            <div className={classes.wrap}>
                <Charts
                    title="Completion"
                    options={this.props.groupType.chartOption}
                    yUnit={this.props.groupType.yUnit}
                    aggregation={data => {
                        this.props.initCourseCompletion({
                            ...this.props.groupType.serverOption,
                            ...data.serverOption
                        });
                        this.props.setGroupParameters({
                            ...data,
                            serverOption: {
                                ...this.props.groupType.serverOption,
                                ...data.serverOption
                            }
                        });
                    }}
                    aggregations={[COMPLETION_GROUP, TIME_AGGREGATOR_COMPLETION, METRICS_COMPLETION]}
                    filters={[DEGREE_FILTRATOR, INTERNATIONAL_FILTRATOR]}
                    filterChange={data => {
                        this.props.initCourseCompletion({
                            ...this.props.groupType.serverOption,
                            ...data
                        });
                        this.props.setGroupParameters({
                            serverOption: {
                                ...this.props.groupType.serverOption,
                                ...data
                            }
                        });
                    }}
                    serverOption={this.props.groupType.serverOption}
                    data={
                        groupData(
                            this.props.groupType.groupField,
                            this.props.groupType.target,
                            this.props.groupType.line,
                            this.props.compliance,
                            this.props.groupType.groupType
                        )
                    }
                    types={['columnChart', 'lineChart']}/>
            </div>
        );
    }
}

export default withStyles(styles)(CourseCompletion);
