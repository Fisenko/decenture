import {connect} from 'react-redux';
import {bindActionCreators, Dispatch} from 'redux';
import {withRouter} from 'react-router-dom';
import State from 'src/core/models/state';
import CourseCompletion from 'src/components/compliance/course_completion/index';
import {initCourseCompletion} from "src/core/actions/compliance";
import {setGroupParameters} from "src/core/actions/ui/chart";

const mapStateToProps = (state: State, props) => ({
    compliance: state.compliance.list,
    groupType: state.ui.chart
});

const mapDispatchToProps = (dispatch: Dispatch) =>
    bindActionCreators({
        initCourseCompletion,
        setGroupParameters,
    }, dispatch);

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps,
    )(CourseCompletion),
);
