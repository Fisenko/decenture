import * as React from 'react';
import { WithStyles } from '@material-ui/core';
import withStyles from '@material-ui/core/styles/withStyles';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import { Route, RouterProps, Switch } from 'react-router';

import styles from './styles';
import Overall from 'src/components/compliance/overall';
import Attendance from 'src/components/compliance/attendance/container';
import CourseCompletion from 'src/components/compliance/course_completion/container';
import Payment from 'src/core/models/payment';
import Page from 'src/components/common/page';

interface ComplianceProps extends RouterProps, WithStyles<any> {
    payment: Payment;
    getPaymentByUniversity: any;
    getUniversityAnalytics: any;
    getStudentsAnalytics: any;
    analytics: {
        university: any;
        students: Array<any>;
    };
}

class Compliance extends React.Component<ComplianceProps> {
    componentDidMount() {
        if (this.props.payment.isEmpty()) {
            this.props.getPaymentByUniversity();
        }

        const {
            analytics: { university, students },
        } = this.props;

        if (!students.length) {
            this.props.getStudentsAnalytics();
        }
        if (!Object.keys(university).length) {
            this.props.getUniversityAnalytics();
        }
    }

    handleCallToRouter = (event, value) => {
        this.props.history.push(value);
    };

    render() {
        const { classes } = this.props;

        return (
            <Page title="Compliance">
                <Tabs
                    value={this.props.history.location.pathname}
                    onChange={this.handleCallToRouter}
                    style={{ marginBottom: 32 }}
                >
                    <Tab label="Overall" value="/compliance" />
                    <Tab label="Attendance" value="/compliance/attendance" />
                    <Tab label="Course Completion" value="/compliance/courseCompletion" />
                </Tabs>
                <Switch>
                    <Route exact path="/compliance" component={Overall} />
                    <Route path="/compliance/attendance" component={Attendance} />
                    <Route path="/compliance/courseCompletion" component={CourseCompletion} />
                </Switch>
            </Page>
        );
    }
}

export default withStyles(styles)(Compliance);
