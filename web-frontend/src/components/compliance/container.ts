import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import { withRouter } from 'react-router-dom';

import State from 'src/core/models/state';
import { getPaymentByUniversity } from 'src/core/actions/payment';
import Compliance from './Compliance';
import { getUniversityAnalytics, getStudentsAnalytics } from 'src/core/actions/analytics';

const mapStateToProps = (state: State, props) => ({
    payment: state.payment.current.info,
    analytics: state.analytics,
});

const mapDispatchToProps = (dispatch: Dispatch) =>
    bindActionCreators({ getPaymentByUniversity, getUniversityAnalytics, getStudentsAnalytics }, dispatch);

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps,
    )(Compliance),
);
