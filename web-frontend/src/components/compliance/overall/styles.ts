import {StyleRulesCallback} from '@material-ui/core';


const styles: StyleRulesCallback = (theme): any => ({
    wrap: {
        backgroundColor: theme.palette.background.default,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around',
    },

    universities: {
        backgroundColor: '#FFFFFF',
        flex: 1,
        marginRight: 10,
        padding: 15,
    },

    table: {
        backgroundColor: '#FFFFFF',
        flex: 2,
        marginLeft: 10,
        flexDirection: 'column',
        padding: 15,
    },

    clear: {
        fontSize: 22,
    },

    clearContainer: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: '50%',
        width: 40,
        height: 40,
    },

    title: {
        padding: 10,
    },
});

export default styles;
