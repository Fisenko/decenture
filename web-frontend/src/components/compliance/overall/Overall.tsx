import * as React from 'react';
import { withStyles, WithStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Clear from '@material-ui/icons/Clear';
import Divider from '@material-ui/core/Divider';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import TableHead from '@material-ui/core/TableHead';
import Table from '@material-ui/core/Table';
import { TableBody } from '@material-ui/core';

import styles from './styles';
import Univesity from 'src/core/models/university';

interface OverallProps extends WithStyles<any> {
    university: Univesity;
    analytics: {
        university: any;
    };
}

class Overall extends React.Component<OverallProps> {
    render() {
        const { classes, university, analytics } = this.props;

        const attendanceRate = analytics.university.totalProgressWithEx
            ? parseInt(analytics.university.totalProgressWithEx, 10).toFixed()
            : 0;
        const courceCompletionRate = analytics.university.courseCompletionRate
            ? parseInt(analytics.university.courseCompletionRate, 10).toFixed()
            : 0;

        return (
            <div className={classes.wrap}>
                <div className={classes.universities}>
                    <Typography color="primary" variant="h6" className={classes.title}>
                        {university.officialName}
                    </Typography>
                    <List component="nav">
                        <Divider/>
                        <ListItem>
                            <ListItemIcon>
                                <div
                                    className={classes.clearContainer}
                                    style={{ backgroundColor: 'rgba(85, 216, 254, 0.3)' }}
                                >
                                    <Clear className={classes.clear} style={{ color: 'rgba(85, 216, 254, 1)' }}/>
                                </div>
                            </ListItemIcon>
                            <ListItemText secondary="Tier 4 Guidelines"/>
                        </ListItem>
                        <Divider/>
                        <ListItem>
                            <ListItemIcon>
                                <div
                                    className={classes.clearContainer}
                                    style={{ backgroundColor: 'rgba(95, 227, 161, 0.2)' }}
                                >
                                    <Clear className={classes.clear} style={{ color: 'rgba(95, 227, 161, 1)' }}/>
                                </div>
                            </ListItemIcon>
                            <ListItemText secondary="Standing"/>
                        </ListItem>
                        <Divider/>
                    </List>
                </div>
                <div className={classes.table}>
                    <Typography color="primary" variant="h6" style={{ padding: 10 }}>
                        Summary
                    </Typography>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell>Criteria</TableCell>
                                <TableCell>Rate</TableCell>
                                <TableCell>Required rate</TableCell>
                                <TableCell>Status</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            <TableRow>
                                <TableCell>Attendance</TableCell>
                                <TableCell>{attendanceRate}%</TableCell>
                                <TableCell>{'>90%'}</TableCell>
                                <TableCell>{'Met-Excellent Standing'}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell>Cource Completion</TableCell>
                                <TableCell>{courceCompletionRate}%</TableCell>
                                <TableCell>{'>85%'}</TableCell>
                                <TableCell>{'Met-Borderline'}</TableCell>
                            </TableRow>
                        </TableBody>
                    </Table>
                </div>
            </div>
        );
    }
}

export default withStyles(styles)(Overall);
