import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import { withRouter } from 'react-router-dom';

import Overall from './Overall';
import State from 'src/core/models/state';

const mapStateToProps = (state: State, props) => ({
    university: state.university,
    analytics: state.analytics,
});

const mapDispatchToProps = (dispatch: Dispatch) => bindActionCreators({}, dispatch);

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps,
    )(Overall),
);
