import * as React from 'react';
import { WithStyles } from '@material-ui/core';
import withStyles from '@material-ui/core/styles/withStyles';

import styles from 'src/components/compliance/attendance/styles';
import Charts from 'src/components/common/chart/container';
import { groupData } from 'src/utils/chart';
import Chart from 'src/core/models/ui/chart';
import {
    ATTENDANCE_GROUP,
    METRICS_ATTENDANCE,
    TIME_AGGREGATOR
} from 'src/components/common/chart/Aggregation/aggregationTypes';
import { DEGREE_FILTRATOR, INTERNATIONAL_FILTRATOR } from 'src/components/common/chart/Filters/filterTypes';

interface AttendanceProps extends WithStyles<any> {
    attendance: Array<any>;
    initAttendance: any;
    groupType: Chart;
    setGroupParameters: any;
}


const DEFAULT_OPTIONS = {
    groupField: 'report_date',
    target: 'visited',
    line: 'name',
    groupType: 'YEAR',
    serverOption: {
        dateTrunc: 'year',
        degreeName: ['master', 'bachelor'],
        isInternational: 'all'
    },
    chartOption: {
        vAxis: {
            viewWindow: {}
        },
    },
    yUnit: ''
};

class Attendance extends React.Component<AttendanceProps> {

    componentDidMount() {
        this.props.initAttendance(
            DEFAULT_OPTIONS
        );
        this.props.setGroupParameters(
            DEFAULT_OPTIONS
        );
    }

    render() {
        const { classes } = this.props;

        return (
            <div className={classes.wrap}>
                <Charts
                    title="Attendance"
                    options={this.props.groupType.chartOption}
                    yUnit={this.props.groupType.yUnit}
                    aggregation={data => {
                        this.props.initAttendance({
                            ...this.props.groupType.serverOption,
                            ...data.serverOption
                        });
                        this.props.setGroupParameters({
                            ...data,
                            serverOption: {
                                ...this.props.groupType.serverOption,
                                ...data.serverOption
                            }
                        });
                    }}
                    aggregations={[ATTENDANCE_GROUP, TIME_AGGREGATOR, METRICS_ATTENDANCE]}
                    filters={[DEGREE_FILTRATOR, INTERNATIONAL_FILTRATOR]}
                    filterChange={data => {
                        this.props.initAttendance({
                            ...this.props.groupType.serverOption,
                            ...data
                        });
                        this.props.setGroupParameters({
                            serverOption: {
                                ...this.props.groupType.serverOption,
                                ...data
                            }
                        });
                    }}
                    serverOption={this.props.groupType.serverOption}
                    data={
                        groupData(
                            this.props.groupType.groupField,
                            this.props.groupType.target,
                            this.props.groupType.line,
                            this.props.attendance,
                            this.props.groupType.groupType
                        )
                    }
                    types={['columnChart', 'lineChart']}/>
            </div>
        );
    }
}

export default withStyles(styles)(Attendance);
