import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import { withRouter } from 'react-router-dom';
import State from 'src/core/models/state';
import { getPaymentByUniversity } from 'src/core/actions/payment';
import Attendance from 'src/components/compliance/attendance/index';
import {initAttendance} from "src/core/actions/analytics";
import {setGroupParameters} from "src/core/actions/ui/chart";

const mapStateToProps = (state: State, props) => ({
    attendance: state.analytics.attendance,
    groupType: state.ui.chart
});

const mapDispatchToProps = (dispatch: Dispatch) =>
    bindActionCreators({
        getPaymentByUniversity,
        initAttendance,
        setGroupParameters,
    }, dispatch);

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps,
    )(Attendance),
);
