import { StyleRulesCallback } from '@material-ui/core';

const styles: StyleRulesCallback = (theme): any => ({
    title: {
        marginBottom: 20,
        paddingLeft: 10,
    },
});

export default styles;
