import * as React from 'react';
import { withStyles, WithStyles } from '@material-ui/core/styles';

import styles from './styles';
import Page from 'src/components/common/page/Page';
import { Paper, Table as MaterialTable, TableBody, TableHead } from '@material-ui/core';
import InvoiceRow from 'src/components/erp/table/InvoiceRow';
import HeaderRow from 'src/components/erp/table/HeaderRow';

interface InvoiceViewerProps extends WithStyles<any> {
    getReconciledInvoices: any;

    invoices: Array<any>;
}

class InvoiceViewer extends React.Component<InvoiceViewerProps> {

    componentDidMount() {
        this.props.getReconciledInvoices();
    }

    renderItems = () => {
        return this.props.invoices.map((invoice, index) =>
            <InvoiceRow key={index} index={index} invoice={invoice}/>);
    };

    render() {
        const { classes } = this.props;

        return (
            <main className={classes.content}>
                <Page title="ERP Invoices">
                    <Paper>
                        <div className={classes.tableWrapper}>
                            <MaterialTable className={classes.table}>
                                <TableHead>
                                    <HeaderRow/>
                                </TableHead>
                                <TableBody>
                                    {this.renderItems()}
                                </TableBody>
                            </MaterialTable>
                        </div>
                    </Paper>
                </Page>
            </main>
        );
    }
}

export default withStyles(styles)(InvoiceViewer);
