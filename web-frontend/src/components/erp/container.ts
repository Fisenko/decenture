import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import { withRouter } from 'react-router-dom';
import State from 'src/core/models/state';
import InvoiceViewer from 'src/components/erp/InvoiceViewer';
import { getReconciledInvoices } from 'src/core/actions/erp';

const mapStateToProps = (state: State, props) => ({
    invoices: state.erp.invoices,
});

const mapDispatchToProps = (dispatch: Dispatch) =>
    bindActionCreators({
        getReconciledInvoices,
    }, dispatch);

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps,
    )(InvoiceViewer),
);
