import { StyleRulesCallback } from '@material-ui/core';

const styles: StyleRulesCallback = theme => ({
    content: {
        flex: '1 0 auto',
        padding: '30px 75px',
        overflow: 'auto',
    },
    table: {
        minWidth: 700,
    },
    tableWrapper: {
        padding: 24,
        overflowX: 'auto',
    },
});

export default styles;
