import { StyleRulesCallback } from '@material-ui/core';

const styles: StyleRulesCallback = theme => ({
    appBarSpacer: theme.mixins.toolbar,
    header: {
        display: 'flex',
        justifyContent: 'center',
    },
});

export default styles;
