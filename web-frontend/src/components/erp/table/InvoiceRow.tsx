import * as React from 'react';
import { TableCell, TableRow, withStyles } from '@material-ui/core';
import commonStyles from 'src/components/common/styles';

const InvoiceRow = (props: any) => {

    const { invoice } = props;

    return (
        <TableRow>
            <TableCell>{props.index + 1}</TableCell>
            {Object.values(invoice).map(value => {
                return invoice.id
                    ? null
                    : <TableCell key={Math.random().toString(36).substring(7)}>{value}</TableCell>;
            })
            }
        </TableRow>
    );
};

export default withStyles(commonStyles)(InvoiceRow);
