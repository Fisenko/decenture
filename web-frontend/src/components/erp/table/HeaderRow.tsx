import * as React from 'react';
import { TableCell, TableRow, withStyles } from '@material-ui/core';
import styles from 'src/components/common/footer/styles';

const HeaderRow = (props: any) => {

    const {classes} = props;

    return (
        <TableRow className={classes.header}>
            <TableCell>id</TableCell>
            <TableCell>BNK_ID_NBR</TableCell>
            <TableCell>BANK_ACCOUNT_NUM</TableCell>
            <TableCell>TRAN_REF_ID</TableCell>
            <TableCell>TRAN_DT</TableCell>
            <TableCell>BUSINESS_UNIT</TableCell>
            <TableCell>TRAN_AMT</TableCell>
            <TableCell>CURRENCY_CD</TableCell>
            <TableCell>TRAN_DESCR</TableCell>
            <TableCell>RECON_TRANS_CODE</TableCell>
            <TableCell>RECON_TYPE</TableCell>
            <TableCell>RECON_STATUS</TableCell>
            <TableCell>RECONCILE_DT</TableCell>
            <TableCell>RECONCILE_OPRID</TableCell>
            <TableCell>RECON_CYCLE_NBR</TableCell>
            <TableCell>STTLMNT_DT_ACTUAL</TableCell>
            <TableCell>ACCTG_TMPL_ID</TableCell>
            <TableCell>BUILD_ACCTG_STATUS</TableCell>
            <TableCell>TRA_PROCESS_STATUS</TableCell>
            <TableCell>PROCESS_INSTANCE</TableCell>
            <TableCell>OPRID</TableCell>
            <TableCell>DTTM_ENTERED</TableCell>
            <TableCell>RECON_RUN_ID</TableCell>
            <TableCell>BANK_ACCT_RVL_AMT</TableCell>
        </TableRow>
    );
};

export default withStyles(styles)(HeaderRow);
