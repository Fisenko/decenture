import { StyleRulesCallback } from '@material-ui/core';

const styles: StyleRulesCallback = theme => ({
    alignRight: {
        textAlign: 'right',
    },
});

export default styles;
