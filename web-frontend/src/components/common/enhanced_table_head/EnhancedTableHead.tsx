import * as React from 'react';
import classNames from 'classnames';
import { TableHead, TableRow, TableCell, Tooltip, TableSortLabel, withStyles, WithStyles } from '@material-ui/core';
import { SortDirection } from '@material-ui/core/TableCell';

import { ISortDirection } from 'src/core/models';
import commonStyles from 'src/components/common/styles';

export interface TableHeadRow {
    id: string;
    numeric: boolean;
    disablePadding: boolean;
    label: string;
    direction?: SortDirection;
    right?: boolean;
}

export interface EnhancedTableHeadProps extends WithStyles {
    rows: Array<TableHeadRow>;
    orderBy: string;
    order: ISortDirection;
    onRequestSort: any;
}

class EnhancedTableHead extends React.Component<EnhancedTableHeadProps> {
    createSortHandler = property => event => {
        this.props.onRequestSort(event, property);
    };

    render() {
        const { classes, order, orderBy, rows } = this.props;

        return (
            <TableHead>
                <TableRow>
                    {rows.map(row => {
                        return (
                            <TableCell
                                className={classNames(row.right && classes.right)}
                                key={row.id}
                                numeric={row.numeric}
                                padding={row.disablePadding ? 'none' : 'default'}
                                sortDirection={orderBy === row.id ? order : false}
                            >
                                <Tooltip
                                    title="Sort"
                                    placement={row.numeric ? 'bottom-end' : 'bottom-start'}
                                    enterDelay={300}
                                >
                                    <TableSortLabel
                                        active={orderBy === row.id}
                                        direction={order}
                                        onClick={this.createSortHandler(row.id)}
                                    >
                                        {row.label}
                                    </TableSortLabel>
                                </Tooltip>
                            </TableCell>
                        );
                    })}
                </TableRow>
            </TableHead>
        );
    }
}

export default withStyles(commonStyles)(EnhancedTableHead);
