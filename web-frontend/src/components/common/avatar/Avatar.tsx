import * as React from 'react';
import { withStyles, WithStyles } from '@material-ui/core/styles';
import classNames from 'classnames';

import styles from './styles';

export interface AvatarProps extends WithStyles<any> {
    className?: string;
    name: string;
}

class Avatar extends React.Component<AvatarProps> {
    render() {
        const { classes, className, name = 'University' } = this.props;

        return (
            <div className={classNames(classes.root, className)}>
                <div>{name[0]}</div>
            </div>
        );
    }
}

export default withStyles(styles)(Avatar);
