import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { bindActionCreators, Dispatch } from 'redux';

import Avatar from './Avatar';
import State from 'src/core/models/state';

const mapStateToProps = (state: State, props) => ({
    name: state.university.name,
});

const mapDispatchToProps = (dispatch: Dispatch) => bindActionCreators({}, dispatch);

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps,
    )(Avatar),
);
