import { StyleRulesCallback } from '@material-ui/core';

const styles: StyleRulesCallback = (theme): any => ({
    root: {
        display: 'flex',
        borderRadius: '50%',
        backgroundColor: '#512DA8',
        color: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
    },
});

export default styles;
