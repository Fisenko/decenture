import { StyleRulesCallback } from '@material-ui/core';

const styles: StyleRulesCallback = theme => ({
    root: {
        flexShrink: 0,
        color: theme.palette.text.primary,
        marginLeft: theme.spacing.unit * 2.5,
    },
});

export default styles;
