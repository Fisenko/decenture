import { StyleRulesCallback } from '@material-ui/core';

const styles: StyleRulesCallback = theme => ({
    root: {
        display: 'flex',
        height: '100vh',
    },
    appBarSpacer: theme.mixins.toolbar,
    contentWrapper: {
        width: '100%',
        display: 'flex',
        flexDirection: 'column',
        overflow: 'auto',
    },
    content: {
        flex: '1 0 auto',
        padding: '30px 75px',
        overflow: 'auto',
    },
});

export default styles;
