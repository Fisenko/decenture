import * as React from 'react';
import { withStyles, WithStyles } from '@material-ui/core/styles';
import classNames from 'classnames';

import styles from './styles';
import Footer from '../footer';
import Header from '../header';
import Sidebar from '../sidebar';

interface PageProps extends WithStyles {
    children: any;
}

const Wrapper = (props: PageProps) => {
    const { children, classes } = props;

    return (
        <div className={classes.root}>
            <Header />
            <Sidebar />
            <div className={classes.contentWrapper}>
                <main className={classes.content}>
                    <div className={classes.appBarSpacer} />
                    {children}
                </main>
                <Footer />
            </div>
        </div>
    );
};

export default withStyles(styles)(Wrapper);
