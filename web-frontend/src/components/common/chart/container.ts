import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import { withRouter } from 'react-router-dom';

import Charts from './index';
import { chooseChartType, toggleAdditional } from 'src/core/actions/ui/chart';


const mapStateToProps = (state, props) => ({
    type: state.ui.chart.type,
    additionalOpen: state.ui.chart.additional,
});

const mapDispatchToProps = (dispatch: Dispatch) =>
    bindActionCreators({
        chooseChartType,
        toggleAdditional
    }, dispatch);

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps,
    )(Charts),
);
