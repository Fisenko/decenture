import { withStyles, WithStyles } from '@material-ui/core';
import * as React from 'react';
import styles from 'src/components/common/chart/Filters/styles';
import FilterCheckBox from 'src/components/common/chart/Filters/FilterCheckBox';
import FilterRadioButton from 'src/components/common/chart/Filters/FilterRadioButton';


export interface FiltersProps extends WithStyles<any> {
    additionalOpen: boolean;
    filters: Array<any>;
    serverOption: any;
    filterChange: any;
}

class Filters extends React.Component<FiltersProps> {

    render() {
        const { classes } = this.props;
        return (
            <div className={classes.filterWrap}>
                {this.props.filters.map((filter, i) => {
                        switch (filter.type) {
                            case'checkBox':
                                return <FilterCheckBox
                                    data={filter}
                                    serverOption={this.props.serverOption}
                                    key={i}
                                    filterChange={this.props.filterChange}
                                />;
                            case'radioButton':
                                return <FilterRadioButton
                                    data={filter}
                                    serverOption={this.props.serverOption}
                                    key={i}
                                    filterChange={this.props.filterChange}
                                />;
                        }
                    }
                )}
            </div>
        );
    }
}

export default withStyles(styles)(Filters);
