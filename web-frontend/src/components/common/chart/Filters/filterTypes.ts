export const DEGREE_FILTRATOR = {
    list: [{
        title: 'Master',
        value: 'master'
    }, {
        title: 'Bachelor',
        value: 'bachelor',
    },],
    key: 'degreeName',
    type: 'checkBox',
    title: 'Degree name'
};

export const INTERNATIONAL_FILTRATOR = {
    list: [
        {
            title: 'All',
            value: 'all',
        }, {
            title: 'International',
            value: 'international',
        },
        {
            title: 'Local',
            value: 'local',
        },
    ],
    key: 'isInternational',
    type: 'radioButton',
    title: 'International'
};
