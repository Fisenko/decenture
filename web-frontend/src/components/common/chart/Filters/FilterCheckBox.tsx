import { withStyles, WithStyles } from '@material-ui/core';
import * as React from 'react';
import styles from 'src/components/common/chart/Filters/styles';
import FormControlLabel from '@material-ui/core/FormControlLabel/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox/Checkbox';


export interface FilterCheckBoxProps extends WithStyles<any> {
    data: {
        list: Array<{
            title: string;
            value: string;
        }>
        key: string;
        title: string;
    }
    serverOption: any;
    filterChange: any;
}

class FilterCheckBox extends React.Component<FilterCheckBoxProps> {
    filterChange(serverOption, key, value) {
        const i = this.props.serverOption[key].indexOf(value);
        if (i === -1) {
            this.props.serverOption[key].push(value);
        } else {
            this.props.serverOption[key].splice(i, 1);
        }
        this.props.filterChange(this.props.serverOption);
    }

    render() {
        const { classes } = this.props;
        return (
            <div className={classes.filter}>
                <div className={classes.title}>{this.props.data.title}</div>
                {!!this.props.serverOption[this.props.data.key] &&
                this.props.data.list.map((item, i) => (
                    <div key={i} className={classes.checkbox}>
                        <FormControlLabel
                            classes={{ root: classes.formControl }}
                            control={
                                <Checkbox
                                    classes={{ root: classes.checkBox }}
                                    checked={
                                        this.props.serverOption[this.props.data.key].indexOf(item.value) !== -1
                                    }
                                    onChange={() => this.filterChange(
                                        this.props.serverOption,
                                        this.props.data.key, item.value
                                    )}
                                    value={item.value}
                                    color="secondary"
                                />
                            }
                            label={item.title}
                        />
                    </div>
                ))}
            </div>
        );
    }
}

export default withStyles(styles)(FilterCheckBox);
