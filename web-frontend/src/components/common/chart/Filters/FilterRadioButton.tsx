import { withStyles, WithStyles } from '@material-ui/core';
import * as React from 'react';
import styles from 'src/components/common/chart/Filters/styles';
import FormControlLabel from '@material-ui/core/FormControlLabel/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox/Checkbox';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';

export interface FilterCheckBoxProps extends WithStyles<any> {
    data: {
        list: Array<{
            title: string;
            value: any;
        }>
        key: string;
        title: string;
    }
    serverOption: any;
    filterChange: any;
}

class FilterCheckBox extends React.Component<FilterCheckBoxProps> {
    filterChange(serverOption, key, value) {
        const i = this.props.serverOption[key].indexOf(value);
        if (i === -1) {
            this.props.serverOption[key].push(value);
        } else {
            this.props.serverOption[key].splice(i, 1);
        }
        this.props.filterChange(this.props.serverOption);
    }

    render() {
        const { classes } = this.props;
        return (
            <div className={classes.filter}>
                <div className={classes.title}>{this.props.data.title}</div>
                <RadioGroup
                    className={classes.group}
                    onChange={(e) => {
                        this.props.filterChange(
                            { [this.props.data.key]: e.target['value'] }
                        );
                    }}
                    value={this.props.serverOption[this.props.data.key]}
                >
                    {this.props.data.list.map((item, i) => (
                        <FormControlLabel
                            key={i}
                            classes={{ root: classes.formControl }}
                            value={item.value}
                            control={<Radio classes={{ root: classes.checkBox }} color="secondary"/>}
                            label={item.title}
                        />
                    ))}
                </RadioGroup>
            </div>
        );
    }
}

export default withStyles(styles)(FilterCheckBox);
