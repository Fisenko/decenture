import { StyleRulesCallback } from '@material-ui/core';
import { COLORS } from 'src/theme';

const styles: StyleRulesCallback = theme => ({
    filterWrap: {
        backgroundColor: '#F4F9FE',
        display: 'flex',
        padding: '20px 40px',
        marginTop: '20px'
    },
    title: {
        color: COLORS.blueDark,
        fontSize: '17px',
        fontWeight: 500,
        marginBottom: '10px'
    },
    filter: {
        margin: '5px',
        minWidth: '190px'
    },
    formControl: {
        height: '28px'
    },
    checkBox: {
        color: '#BFD8FC',
        borderRadius: '3px',
    }
});

export default styles;
