import { withStyles, WithStyles } from '@material-ui/core';
import * as React from 'react';

import Chart from 'react-google-charts';
import styles from 'src/components/common/chart/Pie/styles';

const COLORS = [
    '#40D7FB', '#49E3A2', '#FFB65A', '#FF8276', '#A5A3F7',
    '#a52714', '#0000ff', '#ff0000', '#00ff00', '#caf700',
    '#00fb63', '#e36100', '#ff5b3f', '#ff8276', '#2d81f7',
    '#00fb09', '#0ce300', '#ff0900', '#FF8276', '#A5A3F7',
    '#40D7FB', '#49E3A2', '#FFB65A', '#FF8276', '#A5A3F7',
    '#40D7FB', '#49E3A2', '#FFB65A', '#FF8276', '#A5A3F7'
];


const options = {
    width: '100%',
    height: '600px',
    left: 20,
    chartArea: { width: '85%', left: 0 },
    colors: COLORS,
    vAxis: {
        title: 'Weight'
    },
    pieHole: 0.5,
    pieSliceText: 'none',
    titleTextStyle: {
        color: '#7992B0',
        fontSize: 20
    },
    tooltip: { trigger: 'none' },
    legend: 'none'
};

export interface PieProps extends WithStyles<any> {
    data: Array<any>;
    options: any;
    yUnit?: string;
}

class Pie extends React.Component<PieProps> {

    render() {
        const { classes } = this.props;

        return (
            <div className={classes.wrapper}>
                {!!this.props.data.length && this.props.data.map((pie, i) => (
                    <div key={i} className={classes.chart}>
                        <div className={classes.chartWrap}>
                        <Chart
                            chartType="PieChart"
                            data={pie}
                            width={this.props.options.width || options.width}
                            height={this.props.options.height || options.height}
                            options={{ ...options, title: pie[0][1], ...this.props.options }}
                        />
                            </div>
                        <div className={classes.description}>
                            {pie.slice(1).map((item, i) => (
                                <div key={i} className={classes.descriptionList}>
                                    <div className={classes.color} style={{ backgroundColor: COLORS[i] }}></div>
                                    <div>{item[0]}</div>
                                    <div>{this.props.yUnit}{item[1].toFixed(3)}</div>
                                </div>
                            ))}
                        </div>
                    </div>
                ))}
            </div>
        );
    }
}

export default withStyles(styles)(Pie);
