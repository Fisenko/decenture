import {StyleRulesCallback} from '@material-ui/core';

const styles: StyleRulesCallback = theme => ({
    wrapper: {
        width: '100%',
    },
    chart: {
        display: 'flex'
    },
    description: {
        width: '30%',
        minWidth: '240px',
        marginTop: '30px',
        flexDirection: 'column'
    },
    chartWrap: {
      width: '50%'
    },
    color: {
        width: '20px',
        height: '12px',
        borderRadius: '8px',
    },
    descriptionList: {
        display: 'flex',
        flexWrap: 'wrap',
        maxHeight: '300px',
        minWidth: '240px',
        alignItems: 'center',
        fontSize: '12px',
        '& > div': {
            margin: '5px'
        },
        '& > div:nth-child(2)': {
            color: 'rgb(0, 47, 108)'
        },
        '& > div:nth-child(3)': {
            color: '#7992B0',
            marginLeft: 'auto'
        }
    },
});

export default styles;
