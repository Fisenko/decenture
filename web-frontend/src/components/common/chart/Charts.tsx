import * as React from 'react';
import {
    WithStyles,
    withStyles,
} from '@material-ui/core';
import styles from './styles';
import classNames from 'classnames';
import Button from '@material-ui/core/Button/Button';
import Pie from 'src/components/common/chart/Pie/Pie';
import Bar from 'src/components/common/chart/Bar/Bar';
import Line from 'src/components/common/chart/Line/Line';
import { prepareData, preparePieChart } from 'src/utils/chart';
import Aggregation from './Aggregation/Aggregation';
import Filters from 'src/components/common/chart/Filters/Filters';
import Icon from '@material-ui/core/Icon';

const TYPES = {
    pieChart: 'Pie Chart',
    columnChart: 'Bar Graph',
    lineChart: 'Line Chart',
};

export interface ChartProps extends WithStyles<any> {
    match: any;
    history: any;
    location: any;
    title: string;
    chooseChartType: any;
    aggregation?: any;
    aggregations?: Array<any>
    type: string;
    types: Array<string>;
    data: { lines: any, duration: any };
    options?: any;
    yUnit?: string;
    toggleAdditional?: any;
    additionalOpen?: boolean;
    filters?: Array<any>;
    serverOption?: any;
    filterChange?: any;
}

class Charts extends React.Component<ChartProps> {

    componentDidMount() {
        this.props.chooseChartType(this.props.types[0]);
    }

    render() {
        const { classes } = this.props;
        return (
            <div className={classNames(classes.wrapper)}>
                <div className={classes.panel}>
                    <div className={classes.title}>
                        {this.props.title}
                    </div>
                    <div className={classes.filterPanel}>
                        {!!this.props.filters && <Button
                            variant="outlined"
                            onClick={() => this.props.toggleAdditional(this.props.additionalOpen)}
                            className={
                                classNames(
                                    classes.filterButton,
                                    { [classes.filterButtonActive]: this.props.additionalOpen }
                                )
                            }
                        >
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                <path d="M10 18h4v-2h-4v2zM3 6v2h18V6H3zm3 7h12v-2H6v2z"/>
                            </svg>
                            Filters
                        </Button>}
                        {this.props.aggregations && this.props.aggregations.map((aggregate, i) => (
                            <Aggregation
                                key={i}
                                onSelect={this.props.aggregation}
                                data={aggregate}
                            />
                        ))}
                    </div>
                    <div className={classes.btnPanel}>
                        {this.props.types.length > 1 && this.props.types.map((chartType, i) => (
                            <Button
                                variant="outlined"
                                key={i}
                                className={
                                    classNames(
                                        classes.button,
                                        { [classes.activeButton]: chartType === this.props.type }
                                    )
                                }
                                onClick={() => {
                                    this.props.chooseChartType(chartType);
                                }}
                            >
                                {TYPES[chartType]}
                            </Button>
                        ))}
                    </div>
                </div>
                {!!this.props.filters && this.props.additionalOpen && <Filters
                    additionalOpen={this.props.additionalOpen}
                    serverOption={this.props.serverOption}
                    filters={this.props.filters}
                    filterChange={this.props.filterChange}
                />}
                <div>
                    {this.props.type === 'pieChart' &&
                    <Pie data={preparePieChart(this.props.data)} options={this.props.options}
                         yUnit={this.props.yUnit}/>}
                    {this.props.type === 'columnChart' &&
                    <Bar data={prepareData(this.props.data)} options={this.props.options}/>}
                    {this.props.type === 'lineChart' &&
                    <Line data={prepareData(this.props.data)} options={this.props.options}/>}
                </div>
            </div>
        );
    }

}

export default withStyles(styles)(Charts);
