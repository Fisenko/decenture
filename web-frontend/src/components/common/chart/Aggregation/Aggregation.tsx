import { withStyles, WithStyles } from '@material-ui/core';
import * as React from 'react';
import Select from '@material-ui/core/Select';

import styles from 'src/components/common/chart/Aggregation/styles';


export interface AggregationProps extends WithStyles<any> {
    data: Array<{ value: any, title: string }>;
    onSelect: any;
}

class Aggregation extends React.Component<AggregationProps> {

    render() {
        const { classes } = this.props;
        return (
            <Select
                classes={{ root: classes.selectRoot, select: classes.select }}
                native
                disableUnderline
                onChange={e => {
                    this.props.onSelect(
                        this.props.data[parseInt(e.target.value, 10)]['value']
                    );
                }}
            >
                {this.props.data.map((option, i) => (
                    <option
                        value={i}
                        key={i}
                    >
                        {option.title}
                    </option>
                ))}
            </Select>
        );
    }
}

export default withStyles(styles)(Aggregation);
