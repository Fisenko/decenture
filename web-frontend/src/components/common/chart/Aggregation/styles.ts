import {StyleRulesCallback} from '@material-ui/core';

const styles: StyleRulesCallback = theme => ({
    selectRoot: {
        marginRight: '10px',
        backgroundColor: '#ffffff',
        border: '1px solid rgba(0, 0, 0, 0.23)',
        borderRadius: '5px',
        overflowX: 'hidden'
    },
    select: {
        paddingLeft: '10px',
    },
});

export default styles;
