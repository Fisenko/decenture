export const TIME_AGGREGATOR = [
    {
        title: 'Year',
        value: {
            groupType: 'YEAR',
            serverOption: {
                beginDate: 0,
                dateTrunc: 'year',
            }
        }
    },
    {
        title: 'Month',
        value: {
            groupType: 'MONTH',
            serverOption: {
                beginDate: 0,
                dateTrunc: 'month',
            }
        }
    },
    {
        title: 'Week',
        value: {
            groupType: 'DATE',
            serverOption: {
                beginDate: (new Date()).getTime() / 1000 - 1 * 6 * 30 * 24 * 60 * 60,
                dateTrunc: 'week',
            }
        }
    },
    {
        title: 'Day',
        value: {
            groupType: 'DATE',
            serverOption: {
                beginDate: (new Date()).getTime() / 1000 - 1 * 12 * 30 * 24 * 60 * 60,
                dateTrunc: 'day',
            }
        }
    }
];

export const TIME_AGGREGATOR_COMPLETION = [
    {
        title: 'Year',
        value: {
            groupType: 'YEAR',
            serverOption: {
                beginDate: 0,
                dateTrunc: 'year',
            }
        }
    },
    {
        title: 'Month',
        value: {
            groupType: 'MONTH',
            serverOption: {
                beginDate: 0,
                dateTrunc: 'month',
            }
        }
    },
];

export const METRICS_ATTENDANCE = [
    {
        title: 'Total',
        value: {
            target: 'visited',
            chartOption: {
                vAxis: {
                    viewWindow: {}
                },
            },
            yUnit: ''
        }
    },
    {
        title: 'Percent',
        value: {
            target: 'current_progress_with_ex',
            chartOption: {
                vAxis: {
                    format: '#\'%\'',
                    viewWindow: {
                        min: 30,
                        max: 100
                    }
                },
            },
            yUnit: '%'
        }
    }
];

export const ATTENDANCE_GROUP = [
    {
        title: 'All',
        value: {
            line: 'name',
            serverOption: {
                groupByIsInternational: null,
                groupByDegreeName: null
            }
        }
    },
    {
        title: 'Group international',
        value: {
            line: 'is_international',
            serverOption: {
                groupByIsInternational: true,
                groupByDegreeName: null
            }
        }
    },
    {
        title: 'Group degree name',
        value: {
            line: 'degree_name',
            serverOption: {
                groupByIsInternational: null,
                groupByDegreeName: true
            }
        }
    },
];
export const COMPLETION_GROUP = [
    {
        title: 'All',
        value: {
            line: 'university_name',
            serverOption: {
                groupByIsInternational: null,
                groupByDegreeName: null
            }
        }
    },
    {
        title: 'Group international',
        value: {
            line: 'is_international',
            serverOption: {
                groupByIsInternational: true,
                groupByDegreeName: null
            }
        }
    },
    {
        title: 'Group degree name',
        value: {
            line: 'degree_name',
            serverOption: {
                groupByIsInternational: null,
                groupByDegreeName: true
            }
        }
    },
];

export const PAYMENT_GROUP = [
    {
        title: 'All',
        value: {
            line: 'title',
        }
    },
    {
        title: 'Group international',
        value: {
            line: 'isInternational',
        }
    },
    {
        title: 'Group degree name',
        value: {
            line: 'degreeName',
        }
    },
];

export const METRICS_COMPLETION = [
    {
        title: 'Total',
        value: {
            target: 'completed',
            chartOption: {
                vAxis: {
                    viewWindow: {}
                },
            },
            yUnit: ''
        }
    },
    {
        title: 'Percent',
        value: {
            target: 'completion_rate',
            chartOption: {
                vAxis: {
                    format: '#\'%\'',
                    viewWindow: {
                        min: 30,
                        max: 100
                    }
                },
            },
            yUnit: '%'
        }
    }
];
