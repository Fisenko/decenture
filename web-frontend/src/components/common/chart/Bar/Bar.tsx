import {withStyles, WithStyles} from '@material-ui/core';
import * as React from 'react';

import Chart from 'react-google-charts';
import styles from 'src/components/common/chart/Bar/styles';


const options = {
    chartArea: {width: '85%'},
    colors: ['#40D7FB', '#49E3A2', '#FFB65A', '#FF8276', '#A5A3F7'],
    legend: {position: 'top'},
};

export interface BarProps extends WithStyles<any> {
    data: Array<any>;
    options: any;
}

class Bar extends React.Component<BarProps> {

    render() {
        return (
            !!this.props.data.length && <Chart
                chartType="ColumnChart"
                data={this.props.data}
                width="100%"
                height="600px"
                options={{...options, ...this.props.options}}
            />
        );
    }
}

export default withStyles(styles)(Bar);
