import { StyleRulesCallback } from '@material-ui/core';
import { COLORS } from 'src/theme';

const styles: StyleRulesCallback = theme => ({
    wrapper: {
        backgroundColor: '#ffffff',
        padding: '30px 45px',
        width: '100%'
    },
    flex: {
        display: 'flex'
    },
    chart: {},
    button: {
        borderRadius: 0,
        borderRightWidth: 0,
        lineHeight: 1.2,
        margin: 0,
        '&:first-child': {
            borderRadius: '5px 0 0 5px'
        },
        '&:last-child': {
            borderRightWidth: '1px',
            borderRadius: '0 5px 5px 0'
        }
    },
    filterButton: {
        lineHeight: 1.2,
        margin: '0 10px',
        padding: '5px 16px',
        fill: COLORS.blueDark,
        '&:hover': {
            color: COLORS.white,
            backgroundColor: COLORS.blue,
            fill: COLORS.white
        },
        '& svg':{
            marginRight: '5px'
        }
    },
    filterButtonActive: {
        color: COLORS.white,
        backgroundColor: COLORS.blue,
        fill: COLORS.white
    },
    panel: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end'
    },
    title: {
        marginRight: 'auto',
        color: 'rgb(0, 47, 108)',
        fontSize: '20px'
    },
    btnPanel: {
        display: 'flex',
        flexWrap: 'nowrap',
    },
    activeButton: {
        color: COLORS.blue
    }
});

export default styles;
