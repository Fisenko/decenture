import * as React from 'react';

import Search from 'src/components/common/search';
import { debounce } from 'src/utils';

export interface FilterProps {
    list: Array<any>;
    search: string;
    setUI: any;
}

class Filter extends React.Component<FilterProps> {
    debouncedFilter = () => {};

    constructor(props) {
        super(props);

        this.debouncedFilter = debounce(this.filterBySearch, 200);
    }

    componentDidUpdate(prevProps: Readonly<any>) {
        const { search } = this.props;

        if (prevProps.search !== search) {
            this.debouncedFilter();
        }
    }

    filterBySearch = () => {
        const { list, search } = this.props;

        const filtered = list.filter(obj => {
            try {
                Object.values(obj).forEach(value => {
                    if (typeof value === 'string' && value.toLowerCase().includes(search.toLowerCase())) {
                        throw true;
                    }
                    if (typeof value === 'number' && value.toString().includes(search)) {
                        throw true;
                    }
                });
            } catch (result) {
                return result;
            }

            return false;
        });

        this.props.setUI({ filtered });
    };

    handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const { name, value } = e.target;
        this.props.setUI({ [name]: value });
    };

    render() {
        const { search } = this.props;

        return <Search value={search} onChange={this.handleInputChange} />;
    }
}

export default Filter;
