import * as React from 'react';
import { withStyles, WithStyles } from '@material-ui/core/styles';

import styles from './styles';
import { Typography } from '@material-ui/core';

interface PageProps extends WithStyles<any> {
    title: string;
    children: JSX.Element[] | JSX.Element;
}

const Page = (props: PageProps) => {
    const { classes, title } = props;

    return (
        <>
            <Typography className={classes.title} variant="h4">
                {title}
            </Typography>
            {props.children}
        </>
    );
};

export default withStyles(styles)(Page);
