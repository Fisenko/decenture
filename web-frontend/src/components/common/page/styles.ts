import { StyleRulesCallback } from '@material-ui/core';

const styles: StyleRulesCallback = theme => ({
    title: {
        marginBottom: 20,
    },
});

export default styles;
