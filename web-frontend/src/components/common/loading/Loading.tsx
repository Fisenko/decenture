import * as React from 'react';
import { withStyles, WithStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import { Typography } from '@material-ui/core';

import styles from './styles';

export interface LoadingProps extends WithStyles<any> {}

const Loading = (props: LoadingProps) => {
    const { classes } = props;

    return (
        <div className={classes.container}>
            <CircularProgress className={classes.icon} />
            <Typography>Loading...</Typography>
        </div>
    );
};

export default withStyles(styles)(Loading);
