import * as React from 'react';
import { NavLink, NavLinkProps } from 'react-router-dom';
import { withStyles, ListItem } from '@material-ui/core';

import styles from './styles';

interface LinkProps extends NavLinkProps {
    classes: any;
}

const Link = ({ children, classes, ...otherProps }: LinkProps) => (
    <ListItem button className={classes.listItem}>
        <NavLink className={classes.navLink} activeClassName={classes.activeStyle} {...otherProps}>
            {children}
        </NavLink>
    </ListItem>
);

export default withStyles(styles)(Link);
