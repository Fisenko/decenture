import { StyleRulesCallback } from '@material-ui/core';

const styles: StyleRulesCallback = theme => ({
    listItem: {
        padding: 0,
    },
    navLink: {
        display: 'flex',
        height: 60,
        alignItems: 'center',
        width: '100%',
        paddingLeft: 19,
        textDecoration: 'none',
        borderLeft: '5px solid #2979FF00',
        color: '#99ACC4',
    },
    activeStyle: {
        backgroundColor: '#012961',
        borderLeft: '5px solid #2979FF',
        color: '#fff',
    },
});

export default styles;
