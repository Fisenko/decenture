import * as React from 'react';
import { withStyles, WithStyles } from '@material-ui/core/styles';
import classNames from 'classnames';
import Drawer from '@material-ui/core/Drawer';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import { List, ListItemText, ListItemIcon, ListItem } from '@material-ui/core';
import HomeOutlined from '@material-ui/icons/HomeOutlined';
import AccountBalanceWalletRounded from '@material-ui/icons/AccountBalanceWalletRounded';
import QuestionAnswer from '@material-ui/icons/QuestionAnswer';
import DonutSmall from '@material-ui/icons/DonutSmall';
import BarChart from '@material-ui/icons/BarChart';

import styles from './styles';
import { getImage } from 'src/utils/image_holder';
import Link from './link';

interface SidebarProps extends WithStyles<any> {
    isOpen: boolean;
    toggleIsOpen: any;
}

class Sidebar extends React.Component<SidebarProps> {
    render() {
        const { classes, isOpen, toggleIsOpen } = this.props;

        return (
            <Drawer
                variant="permanent"
                classes={{
                    paper: classNames(classes.drawerPaper, !isOpen && classes.drawerPaperClose),
                }}
                open={isOpen}
            >
                <div>
                    <div className={classes.toolbarIcon}>
                        <img className={classes.logo} src={getImage('decentureLogoWhite')} />
                        <IconButton className={classes.toggleButton} onClick={toggleIsOpen}>
                            <ChevronLeftIcon />
                        </IconButton>
                    </div>
                    <Divider />
                    <List>
                        <Link exact to="/">
                            <ListItemIcon>
                                <HomeOutlined />
                            </ListItemIcon>
                            <ListItemText disableTypography={true} primary="Home" />
                        </Link>
                        <Link to="/compliance">
                            <ListItemIcon>
                                <DonutSmall />
                            </ListItemIcon>
                            <ListItemText disableTypography={true} primary="Compliance" />
                        </Link>
                        <Link to="/finance">
                            <ListItemIcon>
                                <BarChart />
                            </ListItemIcon>
                            <ListItemText disableTypography={true} primary="Finance" />
                        </Link>
                        <Link to="/transactions">
                            <ListItemIcon>
                                <AccountBalanceWalletRounded />
                            </ListItemIcon>
                            <ListItemText disableTypography={true} primary="Transactions" />
                        </Link>
                    </List>
                </div>
                <List className={classes.withoutPadding}>
                    <Link to="/contact-us">
                        <ListItemIcon className="">
                            <QuestionAnswer />
                        </ListItemIcon>
                        <ListItemText disableTypography={true} primary="Contact Us" />
                    </Link>
                </List>
            </Drawer>
        );
    }
}

export default withStyles(styles)(Sidebar);
