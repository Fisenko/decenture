import { StyleRulesCallback } from '@material-ui/core';

import { COLORS } from 'src/theme';

const drawerWidth = 240;

const styles: StyleRulesCallback = theme => ({
    toolbarIcon: {
        backgroundColor: '#012961',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: '0 8px',
        ...theme.mixins.toolbar,
    },
    drawerPaper: {
        overflow: 'hidden',
        backgroundColor: COLORS.blueDark,
        justifyContent: 'space-between',
        position: 'relative',
        whiteSpace: 'nowrap',
        width: drawerWidth,
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    drawerPaperClose: {
        overflowX: 'hidden',
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        width: theme.spacing.unit * 7,
        [theme.breakpoints.up('sm')]: {
            width: theme.spacing.unit * 9,
        },
    },
    logo: {
        height: '25.6px',
        marginRight: 8,
    },
    toggleButton: {
        color: '#B1BFD2',
    },
    withoutPadding: {
        padding: 0,
    },
});

export default styles;
