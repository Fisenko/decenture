import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { bindActionCreators, Dispatch } from 'redux';

import Sidebar from './Sidebar';
import State from 'src/core/models/state';
import { toggleIsOpen } from 'src/core/actions/ui/header';

const mapStateToProps = (state: State, props) => ({
    isOpen: state.ui.header.info.isOpen,
});

const mapDispatchToProps = (dispatch: Dispatch) =>
    bindActionCreators(
        {
            toggleIsOpen,
        },
        dispatch,
    );

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps,
    )(Sidebar),
);
