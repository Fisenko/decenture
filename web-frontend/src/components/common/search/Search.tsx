import * as React from 'react';
import { withStyles, WithStyles } from '@material-ui/core/styles';
import InputBase from '@material-ui/core/InputBase';
import SearchIcon from '@material-ui/icons/Search';

import styles from './styles';

export interface SearchProps extends WithStyles {
    value: string;
    onChange: any;
}

class Search extends React.Component<SearchProps> {
    render() {
        const { classes, value, onChange } = this.props;

        return (
            <div className={classes.root}>
                <div className={classes.icon}>
                    <SearchIcon />
                </div>
                <InputBase
                    placeholder="Search…"
                    name="search"
                    value={value}
                    onChange={onChange}
                    classes={{
                        root: classes.inputRoot,
                        input: classes.input,
                    }}
                />
            </div>
        );
    }
}

export default withStyles(styles)(Search);
