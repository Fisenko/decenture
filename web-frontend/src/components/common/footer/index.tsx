import * as React from 'react';
import Typography from '@material-ui/core/Typography';
import { withStyles, WithStyles } from '@material-ui/core/styles';

import styles from './styles';

interface FooterProps {}

const Footer = (props: FooterProps & WithStyles<any>) => {
    return (
        <footer className={props.classes.layout}>
            <Typography align="center" color="textSecondary" component="p">
                2018 Decenture, Inc. All rights reserved
            </Typography>
        </footer>
    );
};

export default withStyles(styles)(Footer);
