import { StyleRulesCallback } from '@material-ui/core';

const styles: StyleRulesCallback = (theme): any => ({
    layout: {
        marginBottom: 16,
    },
});

export default styles;
