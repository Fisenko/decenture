import * as React from 'react';
import {
    Table as MaterialTable,
    WithStyles,
    TableBody,
    TableRow,
    TableFooter,
    TablePagination,
    withStyles,
} from '@material-ui/core';

import styles from './styles';
import EnhancedTableHead from '../enhanced_table_head';
import TablePaginationActions from '../table_pagination_actions';
import { TableHeadRow } from '../enhanced_table_head/EnhancedTableHead';
import { stableSort, getSorting } from 'src/utils/sort';
import { ISortDirection } from 'src/core/models';

export interface TableProps extends WithStyles<any> {
    Row: React.ReactType;

    rows: Array<any>;
    tableHead: Array<TableHeadRow>;
    page: number;
    rowsPerPage: number;
    order: ISortDirection;
    orderBy: string;

    setUI(state: object): void;
}

class Table extends React.Component<TableProps> {
    handleRequestSort = (event, property) => {
        const orderBy = property;
        let order = ISortDirection.desc;

        if (this.props.orderBy === property && this.props.order === ISortDirection.desc) {
            order = ISortDirection.asc;
        }

        this.props.setUI({ order, orderBy });
    };

    handleChangePage = (event, page) => {
        this.props.setUI({ page });
    };

    handleChangeRowsPerPage = event => {
        const rowsPerPage = event.target.value;
        this.props.setUI({ rowsPerPage });
    };

    renderRows() {
        const { rows, rowsPerPage, page, order, orderBy, Row } = this.props;

        return stableSort(rows, getSorting(order, orderBy))
            .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
            .map(row => <Row key={row.id} {...row} />);
    }

    render() {
        const { classes, rows, rowsPerPage, page, order, orderBy, tableHead } = this.props;

        return (
            <div className={classes.tableWrapper}>
                <MaterialTable className={classes.table}>
                    <EnhancedTableHead
                        rows={tableHead}
                        order={order}
                        orderBy={orderBy}
                        onRequestSort={this.handleRequestSort}
                    />
                    <TableBody>{this.renderRows()}</TableBody>
                    <TableFooter>
                        <TableRow>
                            <TablePagination
                                colSpan={tableHead.length}
                                count={rows.length}
                                rowsPerPage={rowsPerPage}
                                page={page}
                                onChangePage={this.handleChangePage}
                                onChangeRowsPerPage={this.handleChangeRowsPerPage}
                                ActionsComponent={TablePaginationActions}
                            />
                        </TableRow>
                    </TableFooter>
                </MaterialTable>
            </div>
        );
    }
}

export default withStyles(styles)(Table);
