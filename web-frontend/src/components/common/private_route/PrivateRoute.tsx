import * as React from 'react';
import { Route, Redirect } from 'react-router';

import Loading from '../loading';

export default ({ isAuth, isPending, component: Component, ...otherProps }) => {
    if (isPending) {
        return <Loading />;
    }

    if (isAuth) {
        return <Route {...otherProps} component={Component} />;
    }

    return <Redirect to="/login" />;
};
