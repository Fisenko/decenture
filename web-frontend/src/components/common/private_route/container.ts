import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import PrivateRoute from './PrivateRoute';
import State from 'src/core/models/state';

const mapStateToProps = (state: State, props) => ({
    ...state.ui.info,
});

export default withRouter(connect(mapStateToProps)(PrivateRoute));
