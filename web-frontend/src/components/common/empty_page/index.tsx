import * as React from 'react';
import { withStyles, WithStyles } from '@material-ui/core/styles';

import styles from './styles';
import Footer from '../footer';

interface EmptyPageProps extends WithStyles<any> {
    className: string;
    children: JSX.Element[] | JSX.Element;
}

const EmptyPage = (props: EmptyPageProps) => {
    return (
        <div className={props.classes.layout}>
            <main className={props.className}>{props.children}</main>
            <Footer />
        </div>
    );
};

export default withStyles(styles)(EmptyPage);
