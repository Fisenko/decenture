import { StyleRulesCallback } from '@material-ui/core';

const styles: StyleRulesCallback = (theme): any => ({
    layout: {
        display: 'flex',
        flexDirection: 'column',
        minHeight: '100vh',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
});

export default styles;
