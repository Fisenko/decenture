import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { bindActionCreators, Dispatch } from 'redux';

import Header from './Header';
import State from 'src/core/models/state';
import { toggleIsOpen } from 'src/core/actions/ui/header';

const mapStateToProps = (state: State, props) => ({
    ...state.ui.header.info,
    notifications: state.user.current.info.notifications,
});

const mapDispatchToProps = (dispatch: Dispatch) =>
    bindActionCreators(
        {
            toggleIsOpen,
        },
        dispatch,
    );

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps,
    )(Header),
);
