import { StyleRulesCallback } from '@material-ui/core';
import { COLORS } from 'src/theme';

const styles: StyleRulesCallback = theme => ({
    profile: {
        display: 'flex',
        marginLeft: 10,
        paddingLeft: 15,
        borderLeft: '1px solid',
        borderLeftColor: theme.palette.grey[400],
    },
    avatar: {
        marginLeft: 20,
        width: 40,
        height: 40,
    },
    icon: {
        fontSize: 20,
        position: 'fixed',
        color: '#B1BFD2',
        marginLeft: 5,
    },
    name: {
        fontSize: 13,
        color: '#002F6C',
        flexGrow: 1,
    },
    id: {
        fontSize: 11,
        color: '#7F96B5',
        flexGrow: 1,
    },
    center: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
    },
    menu: {
        top: 40,
    },
    menuItem: {
        width: 200,
        justifyContent: 'center',
    },
    arrow: {
        color: COLORS.blueDark,
    },
});

export default styles;
