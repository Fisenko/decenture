import * as React from 'react';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import { RouterProps } from 'react-router';
import { withStyles, WithStyles } from '@material-ui/core/styles';
import KeyboardArrowDown from '@material-ui/icons/KeyboardArrowDown';

import styles from './styles';
import { getImage } from 'src/utils/image_holder';
import Avatar from 'src/components/common/avatar';

const idLength = 12;

interface ProfileProps extends RouterProps, WithStyles<any> {
    id: string;
    name: string;
    anchorEl: any;
    logout: any;
    openMenu: any;
    closeMenu: any;
    getUniversity: any;
}

class Profile extends React.Component<ProfileProps> {
    componentDidMount() {
        this.props.getUniversity();
    }

    handleOpen = (e: React.SyntheticEvent<EventTarget>) => {
        this.props.openMenu(e.currentTarget);
    };

    handleClose = () => {
        this.props.closeMenu();
    };

    handleAboutClick = () => {
        this.props.history.push('/about');
        this.props.closeMenu();
    };

    handleSettingsClick = () => {
        this.props.history.push('/settings');
        this.props.closeMenu();
    };

    handleLogoutClick = () => {
        this.props.logout();
        this.props.closeMenu();
    };

    renderMenu = () => {
        const { classes, anchorEl } = this.props;

        return (
            <Menu
                className={classes.menu}
                id="render-props-menu"
                anchorEl={anchorEl}
                open={!!anchorEl}
                onClose={this.handleClose}
            >
                <MenuItem className={classes.menuItem} onClick={this.handleAboutClick}>
                    About
                </MenuItem>
                <MenuItem className={classes.menuItem} onClick={this.handleSettingsClick}>
                    Settings
                </MenuItem>
                <MenuItem className={classes.menuItem} onClick={this.handleLogoutClick}>
                    Logout
                </MenuItem>
            </Menu>
        );
    };

    render() {
        const { classes, id = '', name = '' } = this.props;
        const smallId = id.substring(id.length - idLength);

        return (
            <div className={classes.profile}>
                <div className={classes.center} style={{ minWidth: 95 }}>
                    <div className={classes.name}>
                        <a
                            className={classes.arrow}
                            href="#"
                            aria-owns={open ? 'render-props-menu' : null}
                            aria-haspopup="true"
                            onClick={this.handleOpen}
                        >
                            {name}
                            <KeyboardArrowDown className={classes.icon} />
                        </a>
                    </div>
                    <div className={classes.id}>ID: {smallId}</div>
                </div>
                <Avatar className={classes.avatar} />
                {this.renderMenu()}
            </div>
        );
    }
}

export default withStyles(styles)(Profile);
