import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { bindActionCreators, Dispatch } from 'redux';

import Profile from './Profile';
import State from 'src/core/models/state';
import { logout } from 'src/core/actions/user';
import { openMenu, closeMenu } from 'src/core/actions/ui/profile';
import { getUniversity } from 'src/core/actions/university';

const mapStateToProps = (state: State, props) => ({
    ...state.ui.header.profile.info,
    id: state.user.current.info.id,
    name: state.university.name,
});

const mapDispatchToProps = (dispatch: Dispatch) =>
    bindActionCreators(
        {
            logout,
            openMenu,
            closeMenu,
            getUniversity,
        },
        dispatch,
    );

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps,
    )(Profile),
);
