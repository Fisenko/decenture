import * as React from 'react';
import classNames from 'classnames';
import { withStyles, WithStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Badge from '@material-ui/core/Badge';
import MenuIcon from '@material-ui/icons/Menu';
import NotificationsIcon from '@material-ui/icons/Notifications';

import styles from './styles';
import Profile from './profile';
import HeaderClass from 'src/core/models/ui/header';

interface HeaderProps extends WithStyles<any>, HeaderClass {
    toggleIsOpen: any;
    notifications: number;
}

class Header extends React.Component<HeaderProps> {
    renderNotificationsIcon = () => {
        const { notifications } = this.props;

        if (notifications) {
            return (
                <IconButton color="inherit">
                    <Badge badgeContent={notifications}>
                        <NotificationsIcon />
                    </Badge>
                </IconButton>
            );
        }

        return (
            <IconButton color="inherit">
                <NotificationsIcon />
            </IconButton>
        );
    };

    render() {
        const { classes, isOpen } = this.props;

        return (
            <AppBar position="absolute" className={classNames(classes.appBar, isOpen && classes.appBarShift)}>
                <Toolbar disableGutters={!isOpen} className={classes.toolbar}>
                    <IconButton
                        color="inherit"
                        aria-label="Open drawer"
                        onClick={this.props.toggleIsOpen}
                        className={classNames(classes.menuButton, isOpen && classes.menuButtonHidden)}
                    >
                        <MenuIcon />
                    </IconButton>
                    <div className={classes.right}>
                        {this.renderNotificationsIcon()}
                        <Profile />
                    </div>
                </Toolbar>
            </AppBar>
        );
    }
}

export default withStyles(styles)(Header);
