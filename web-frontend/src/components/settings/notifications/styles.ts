import { StyleRulesCallback } from '@material-ui/core';


const styles: StyleRulesCallback = (theme): any => ({
    wrap: {
        display: 'flex',
        flexDirection: 'column',
        flexWrap: 'wrap',
    },

    title: {
        fontWeight: 'bold',
    },

    subtitle: {
        fontSize: 13,
    },

    table: {
        marginTop: 40,
        marginBottom: 40,
        width: 672,
        height: 164,
    },

    tableHead: {
        height: 22,
    },

    tableCell: {
        fontSize: 13,
        height: 22,
        fontWeight: 'bold',
    },

    button: {
        width: 160,
        height: 18,
        fontSize: 13,
    },
});

export default styles;
