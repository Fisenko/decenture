import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import { withRouter } from 'react-router-dom';

import Notifications from './Notifications';
import State from 'src/core/models/state';
import { emailNotificationsInputChange, notificationsInputChange } from 'src/core/actions/settings/notifications';

const mapStateToProps = (state: State, props) => ({
    emailNotifications: state.user.current.settings.notifications.email.info,
    notifications: state.user.current.settings.notifications.info,
});

const mapDispatchToProps = (dispatch: Dispatch) => bindActionCreators({
    notificationsInputChange,
    emailNotificationsInputChange,
}, dispatch);

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps,
    )(Notifications),
);
