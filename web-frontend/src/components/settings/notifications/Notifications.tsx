import * as React from 'react';
import { withStyles, WithStyles } from '@material-ui/core/styles';

import styles from './styles';
import Typography from '@material-ui/core/Typography';
import Table from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import { TableRow } from '@material-ui/core';
import TableCell from '@material-ui/core/TableCell';
import TableBody from '@material-ui/core/TableBody';
import Button from '@material-ui/core/Button';
import Switch from '@material-ui/core/Switch';
import NotificationsModel from 'src/core/models/notifications';

interface NotificationsProps extends WithStyles<any> {

    notifications: NotificationsModel;
    emailNotifications: NotificationsModel;
    notificationsInputChange: any;
    emailNotificationsInputChange: any;
}

class Notifications extends React.Component<NotificationsProps> {

    handleNotificationsChange = (checked, name) => {
        this.props.notificationsInputChange({
            name: name,
            value: checked,
        });
    };

    handleEmailNotificationsChange = (checked, name) => {
        this.props.emailNotificationsInputChange({
            name: name,
            value: checked,
        });
    };

    render() {
        console.log('notifications: ', this.props.notifications);
        console.log('emailNotifications: ', this.props.emailNotifications);
        const { classes } = this.props;

        return (
            <div className={classes.wrap}>
                <div>
                    <Typography variant="subtitle1" color="textPrimary" className={classes.title}>
                        Notification Settings
                    </Typography>
                    <Typography variant="body1" className={classes.subtitle}>
                        Select witch notifications you want to receive
                    </Typography>
                </div>
                <Table className={classes.table}>
                    <TableHead className={classes.tableHead}>
                        <TableRow>
                            <TableCell className={classes.tableCell}>Notification Topic</TableCell>
                            <TableCell className={classes.tableCell}>Notification Component</TableCell>
                            <TableCell className={classes.tableCell}>Email</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        <TableRow>
                            <TableCell className={classes.tableCell} component="th" scope="row">
                                Messages
                            </TableCell>
                            <TableCell>
                                <Switch
                                    onChange={(e, checked) => this.handleNotificationsChange(checked, 'message')}
                                    checked={this.props.notifications.message}
                                    color="secondary"
                                />
                            </TableCell>
                            <TableCell>
                                <Switch
                                    onChange={(e, checked) => this.handleEmailNotificationsChange(checked, 'message')}
                                    checked={this.props.emailNotifications.message}
                                    color="secondary"
                                />
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell className={classes.tableCell} component="th" scope="row">
                                Analytics
                            </TableCell>
                            <TableCell>
                                <Switch
                                    onChange={(e, checked) => this.handleNotificationsChange(checked, 'analytics')}
                                    checked={this.props.notifications.analytics}
                                    color="secondary"
                                />
                            </TableCell>
                            <TableCell>
                                <Switch
                                    onChange={(e, checked) => this.handleEmailNotificationsChange(checked, 'analytics')}
                                    checked={this.props.emailNotifications.analytics}
                                    color="secondary"
                                />
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell className={classes.tableCell} component="th" scope="row">
                                Invoices
                            </TableCell>
                            <TableCell>
                                <Switch
                                    onChange={(e, checked) => this.handleNotificationsChange(checked, 'invoices')}
                                    checked={this.props.notifications.invoices}
                                    color="secondary"
                                />
                            </TableCell>
                            <TableCell>
                                <Switch
                                    onChange={(e, checked) => this.handleEmailNotificationsChange(checked, 'invoices')}
                                    checked={this.props.emailNotifications.invoices}
                                    color="secondary"/>
                            </TableCell>
                        </TableRow>
                    </TableBody>
                </Table>
                <Button variant="contained" color="secondary" className={classes.button}>
                    Update Notifications
                </Button>
            </div>
        );
    }
}

export default withStyles(styles)(Notifications);
