import * as React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import { withStyles, WithStyles } from '@material-ui/core/styles';

import styles from '../styles';
import { getImage } from 'src/utils/image_holder';
import User from 'src/core/models/user';
import Univesity from 'src/core/models/university';
import Avatar from 'src/components/common/avatar';

export interface SettingsProps extends WithStyles<any> {
    user: User;
    university: Univesity;
}

class General extends React.Component<SettingsProps> {
    handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
    };

    render() {
        const { classes, user, university } = this.props;

        return (
            <form className={classes.form} onSubmit={this.handleSubmit}>
                <div className={classes.row}>
                    <Avatar className={classes.avatar} src={getImage('avatar')} alt="avatar" />
                    <div className={classes.col}>
                        <Button style={{ maxWidth: 128 }} variant="outlined" color="secondary">
                            Change Photo
                        </Button>
                        <Typography style={{ marginTop: 8, fontSize: 11 }} variant="subtitle1">
                            JPG, GIF, PNG, Max size of 800K
                        </Typography>
                    </div>
                </div>
                <TextField label="Full Name" value={user.name} className={classes.textField} margin="normal" />
                <TextField label="University" value={university.name} className={classes.textField} margin="normal" />
                <TextField disabled label="Account ID" value={user.id} className={classes.textField} margin="normal" />
                <TextField disabled label="Email" value={user.email} className={classes.textField} margin="normal" />
                <TextField label="Phone" value="+44 (646) 243 23 48" className={classes.textField} margin="normal" />
                <div style={{ marginTop: 20 }}>
                    <Button type="submit" variant="contained" color="secondary" className={classes.button}>
                        Update Settings
                    </Button>
                </div>
            </form>
        );
    }
}

export default withStyles(styles)(General);
