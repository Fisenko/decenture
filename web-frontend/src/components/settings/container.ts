import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import { withRouter } from 'react-router-dom';

import Settings from './Settings';
import State from 'src/core/models/state';

const mapStateToProps = (state: State, props) => ({
    user: state.user.current.info,
});

const mapDispatchToProps = (dispatch: Dispatch) => bindActionCreators({}, dispatch);

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps,
    )(Settings),
);
