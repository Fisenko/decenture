import { StyleRulesCallback } from '@material-ui/core';

import { COLORS } from 'src/theme';

const styles: StyleRulesCallback = theme => ({
    tabsRoot: {
        paddingLeft: 24,
    },
    content: {
        padding: '24px 32px',
        minHeight: 500,
    },
    form: {
        display: 'flex',
        flexDirection: 'column',
        flexWrap: 'wrap',
    },
    avatar: {
        width: 96,
        height: 96,
        fontSize: 40,
    },
    row: {
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'nowrap',
        justifyContent: 'flex-start',
    },
    col: {
        marginLeft: 32,
        display: 'flex',
        flexDirection: 'column',
        flexWrap: 'nowrap',
        justifyContent: 'center',
    },
    textField: {
        width: 330,
    },
    center: {
        display: 'flex',
        justifyContent: 'center',
    },
    title: {
        fontWeight: 'bold',
    },
    subtitle: {
        fontSize: 13,
    },
});

export default styles;
