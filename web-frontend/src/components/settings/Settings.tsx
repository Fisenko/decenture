import * as React from 'react';
import { Route, Switch, RouterProps } from 'react-router';
import { AppBar, Paper, Tabs, Tab } from '@material-ui/core';
import { withStyles, WithStyles } from '@material-ui/core/styles';

import Page from 'src/components/common/page';
import styles from './styles';
import General from './general';
import Password from './password';
import Notifications from 'src/components/settings/notifications';

export interface SettingsProps extends WithStyles<any>, RouterProps {
    user: any;
}

class Settings extends React.Component<SettingsProps> {
    handleChange = (event, value) => {
        this.props.history.push(value);
    };

    render() {
        const { classes } = this.props;

        return (
            <Page title="Settings">
                <Paper>
                    <Tabs
                        className={classes.tabsRoot}
                        value={this.props.history.location.pathname}
                        onChange={this.handleChange}
                    >
                        <Tab label="General" value="/settings" />
                        <Tab label="Password" value="/settings/password" />
                        <Tab label="Notifications" value="/settings/notifications" />
                    </Tabs>
                    <div className={classes.content}>
                        <Switch>
                            <Route exact path="/settings" component={General} />
                            <Route path="/settings/password" component={Password} />
                            <Route path="/settings/notifications" component={Notifications} />
                        </Switch>
                    </div>
                </Paper>
            </Page>
        );
    }
}

export default withStyles(styles)(Settings);
