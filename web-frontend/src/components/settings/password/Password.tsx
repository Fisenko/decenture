import * as React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import { withStyles, WithStyles } from '@material-ui/core/styles';

import styles from '../styles';

export interface PasswordProps extends WithStyles<any> {
    user: any;
}

class Password extends React.Component<PasswordProps> {
    handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const { name, value } = e.target;
    };

    handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
    };

    render() {
        const { classes, user } = this.props;

        return (
            <div>
                <Typography variant="subtitle1" color="textPrimary" className={classes.title}>
                    Change Password
                </Typography>
                <Typography variant="body1" className={classes.subtitle}>
                    Change your password or recover your current one. Please use 8+ characters with at least 1 number.
                </Typography>
                <form className={classes.form} onSubmit={this.handleSubmit}>
                    <TextField type="password" label="Current Password" className={classes.textField} margin="normal" />
                    <TextField type="password" label="New Password" className={classes.textField} margin="normal" />
                    <TextField type="password" label="Confirm Password" className={classes.textField} margin="normal" />
                    <div style={{ marginTop: 20 }}>
                        <Button type="submit" variant="contained" color="secondary" className={classes.button}>
                            Change Password
                        </Button>
                    </div>
                </form>
            </div>
        );
    }
}

export default withStyles(styles)(Password);
