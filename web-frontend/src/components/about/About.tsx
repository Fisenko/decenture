import * as React from 'react';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { withStyles, WithStyles } from '@material-ui/core/styles';

import styles from './styles';
import Page from 'src/components/common/page';

export interface AboutProps extends WithStyles<any> {}

class About extends React.Component<AboutProps> {
    render() {
        const { classes } = this.props;

        return (
            <Page title="About">
                <Card className={classes.card}>
                    <CardHeader title="We Are Decenture" />
                    <CardContent>
                        <Typography component="p" className={classes.paragraph}>
                            Our mission is to build a fairer & more transparent ecosystem for students.
                        </Typography>
                        <Typography component="p" className={classes.paragraph}>
                            To do that, we are using blockchain technology to bring all transactions onto one platform
                            and revolutionising the way the higher education sector will interact in the future.
                        </Typography>
                        <Typography component="p" className={classes.paragraph}>
                            Let’s address the elephant in the room. Universities struggle for funding and student debt
                            has ballooned to stratospheric levels. Governments need better systems to manage taxpayers’
                            funds distribution. This problem is a big deal.
                        </Typography>
                        <Typography component="p" className={classes.paragraph}>
                            Decenture uses blockchain technology to provide an efficient economic framework for the
                            higher education ecosystem where everyone will contribute to curbing student debt through
                            collaboration & social consciousness.
                        </Typography>
                    </CardContent>
                </Card>
            </Page>
        );
    }
}

export default withStyles(styles)(About);
