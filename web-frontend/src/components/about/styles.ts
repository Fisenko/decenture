import { StyleRulesCallback } from '@material-ui/core';

const styles: StyleRulesCallback = theme => ({
    card: {
        minWidth: 500,
        maxWidth: 900,
    },
    paragraph: {
        marginBottom: 20,
    }
});

export default styles;
