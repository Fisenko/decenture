import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import { withRouter } from 'react-router-dom';

import State from 'src/core/models/state';
import { getPaymentByUniversity } from 'src/core/actions/payment';
import Analytics from 'src/components/analytics/Analytics';

const mapStateToProps = (state: State, props) => ({
    payment: state.payment.current.info,
});

const mapDispatchToProps = (dispatch: Dispatch) =>
    bindActionCreators({ getPaymentByUniversity }, dispatch);

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps,
    )(Analytics),
);
