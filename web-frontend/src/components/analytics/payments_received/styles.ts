import {StyleRulesCallback} from '@material-ui/core';


const styles: StyleRulesCallback = (theme): any => ({

    wrap: {
        backgroundColor: '#FFF',
        display: 'flex',
        flex: 1,
        minHeight: 500,
        flexDirection: 'column',
        alignItems: 'center',
    },

    chart: {
        flex: 1,
    },

});

export default styles;
