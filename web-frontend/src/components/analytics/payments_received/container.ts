import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import { withRouter } from 'react-router-dom';

import State from 'src/core/models/state';
import { getPaymentByUniversity } from 'src/core/actions/payment';
import PaymentsReceived from 'src/components/analytics/payments_received/index';
import {setGroupParameters} from "src/core/actions/ui/chart";

const mapStateToProps = (state: State, props) => ({
    payment: state.payment.paid.list,
    groupType: state.ui.chart
});

const mapDispatchToProps = (dispatch: Dispatch) =>
    bindActionCreators({
        getPaymentByUniversity,
        setGroupParameters,
    }, dispatch);

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps,
    )(PaymentsReceived),
);
