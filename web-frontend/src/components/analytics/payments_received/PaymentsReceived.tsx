import * as React from 'react';
import { WithStyles } from '@material-ui/core';
import withStyles from '@material-ui/core/styles/withStyles';

import styles from './styles';
import Charts from 'src/components/common/chart/container';
import { groupData, aggregate, filter } from 'src/utils/chart';
import Chart from 'src/core/models/ui/chart';
import { PAYMENT_GROUP, TIME_AGGREGATOR } from 'src/components/common/chart/Aggregation/aggregationTypes';

interface PaymentsReceivedProps extends WithStyles<any> {
    payment: Array<any>;
    groupType: Chart;
    setGroupParameters: any;
}

const DEFAULT_OPTIONS = {
    groupField: 'paidDate',
    target: 'amount',
    line: 'title',
    groupType: 'YEAR',
    serverOption: {},
    chartOption: {
        vAxis: {
            format: '#\'$\'',
        },
    },
    yUnit: '$',
};

class PaymentsReceived extends React.Component<PaymentsReceivedProps> {

    componentDidMount() {
        this.props.setGroupParameters(
            DEFAULT_OPTIONS,
        );
    }

    render() {
        const { classes } = this.props;
        return (
            <div className={classes.wrap}>
                <Charts
                    title="Payment"
                    options={this.props.groupType.chartOption}
                    yUnit={this.props.groupType.yUnit}
                    aggregation={data => {
                        this.props.setGroupParameters({
                            ...data,
                            serverOption: {
                                ...this.props.groupType.serverOption,
                                ...data.serverOption,
                            },
                        });
                    }}
                    aggregations={[PAYMENT_GROUP, TIME_AGGREGATOR]}
                    data={
                        groupData(
                            this.props.groupType.groupField,
                            this.props.groupType.target,
                            this.props.groupType.line,
                            aggregate(
                                this.props.payment,
                                this.props.groupType.groupField,
                                this.props.groupType.target,
                                this.props.groupType.line,
                                this.props.groupType.groupType),
                            this.props.groupType.groupType,
                        )
                    }
                    types={['columnChart', 'lineChart']}/>
            </div>
        );
    }
}

export default withStyles(styles)(PaymentsReceived);
