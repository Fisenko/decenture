import * as React from 'react';
import { Typography, WithStyles } from '@material-ui/core';
import withStyles from '@material-ui/core/styles/withStyles';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import { Route, RouterProps, Switch } from 'react-router';

import styles from './styles';
import Overall from 'src/components/analytics/overall/container';
import PaymentsReceived from 'src/components/analytics/payments_received/container';
import Payment from 'src/core/models/payment';

interface AnalyticsProps extends RouterProps, WithStyles<any> {
    payment: Payment;
    getPaymentByUniversity: any;
}

class Analytics extends React.Component<AnalyticsProps> {
    componentDidMount() {
        if (this.props.payment.isEmpty()) {
            this.props.getPaymentByUniversity();
        }
    }

    handleCallToRouter = (event, value) => {
        this.props.history.push(value);
    };

    render() {
        const { classes } = this.props;
        return (
            <div className={classes.wrap}>
                <Typography color="primary" variant="h4" className={classes.title}>
                    Finance
                </Typography>
                <Tabs
                    value={this.props.history.location.pathname}
                    onChange={this.handleCallToRouter}
                    style={{ marginBottom: 32 }}
                >
                    <Tab label="Overall" value="/finance" />
                    <Tab label="Payments Received" value="/finance/payments-received" />
                </Tabs>
                <Switch>
                    <Route exact path="/finance" component={Overall} />
                    <Route path="/finance/payments-received" component={PaymentsReceived} />
                </Switch>
            </div>
        );
    }
}

export default withStyles(styles)(Analytics);
