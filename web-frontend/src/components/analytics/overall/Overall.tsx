import * as React from 'react';
import { withStyles, WithStyles } from '@material-ui/core/styles';
import { Cached, Error, Timer } from '@material-ui/icons';
import classNames from 'classnames';
import { Divider, Typography, Button } from '@material-ui/core';

import styles from './styles';
import Payment from 'src/core/models/payment';
import { moneyFormatter } from 'src/utils';
import FullReport from 'src/components/analytics/overall/FullReport';
import { PAYMENT_GROUP, TIME_AGGREGATOR } from 'src/components/common/chart/Aggregation/aggregationTypes';
import { addColumn, aggregate, groupData } from 'src/utils/chart';
import Charts from 'src/components/common/chart/container';

interface OverallProps extends WithStyles<any> {
    payment: Payment;
    toggleFullReportVisible: any;
    isVisibleFullReport: boolean;
    invoices: {
        paid: Array<any>;
        unpaid: Array<any>;
    }
}

class Overall extends React.Component<OverallProps> {
    render() {
        const { classes } = this.props;
        return (
            <div className={classes.wrap}>
                <div className={classes.flexContainer}>
                    <div className={classes.smallElement}>
                        <div className={classes.iconContainer}>
                            <Cached className={classes.cached}/>
                        </div>
                        <div>
                            <Typography variant="h6" style={{ color: 'rgba(95, 227, 161, 1)' }}>
                                {moneyFormatter.format(this.props.payment.paidInvoicesSumAmount)}
                            </Typography>
                            <Typography>{`${this.props.payment.paidInvoicesCount} payments were processed`}</Typography>
                        </div>
                    </div>
                    <div className={classes.smallElement}>
                        <div className={classes.iconContainer} style={{ backgroundColor: 'rgba(255, 244, 229, 1)' }}>
                            <Error className={classes.error}/>
                        </div>
                        <div>
                            <Typography variant="h6" style={{ color: 'rgba(255, 183, 77, 1)' }}>
                                {moneyFormatter.format(this.props.payment.pendingInvoicesSumAmount)}
                            </Typography>
                            <Typography>
                                {`${this.props.payment.pendingInvoicesCount} payments need to approve`}
                            </Typography>
                        </div>
                    </div>
                    <div className={classes.smallElement}>
                        <div className={classes.iconContainer} style={{ backgroundColor: 'rgba(228, 233, 239, 1)' }}>
                            <Timer className={classes.timer}/>
                        </div>
                        <div>
                            <Typography variant="h6" style={{ color: 'rgba(127, 150, 181, 1)' }}>
                                {moneyFormatter.format(this.props.payment.unpaidInvoicesSumAmount)}
                            </Typography>
                            <Typography>
                                {`${this.props.payment.unpaidInvoicesCount} payments are yet to be processed`}
                            </Typography>
                        </div>
                    </div>
                </div>
                <div className={classes.flexContainer}>
                    <div className={classNames(classes.smallElement, classes.balanceElement)}>
                        <div className={classes.flexElementTop}>
                            <Typography variant="h6">Balance</Typography>
                            <div className={classes.total}>
                                <Typography className={classes.flexElement}>Total</Typography>
                                <Typography className={classNames(classes.flexElement, classes.totalSum)}>
                                    {moneyFormatter.format(this.props.payment.totalBalance)}
                                </Typography>
                            </div>
                        </div>
                        <Divider/>
                        <div className={classes.flexElementMiddle}>
                            <div className={classNames(classes.total, classes.totalBeginTitle)}>
                                <Typography>Today's Beginning Balance</Typography>
                                <Typography className={classes.totalSumBegin}>
                                    {moneyFormatter.format(this.props.payment.todaysBeginningBalance)}
                                </Typography>
                            </div>
                            <div className={classNames(classes.total, classes.totalBeginTitle)}>
                                <Typography>Month's Beginning Balance</Typography>
                                <Typography className={classes.totalSumBegin}>
                                    {moneyFormatter.format(this.props.payment.monthsBeginningBalance)}
                                </Typography>
                            </div>
                        </div>
                        <Divider/>
                        <div className={classes.flexElementBottom}>
                            <Button className={classes.fullReportButton}
                                    onClick={this.props.toggleFullReportVisible}>
                                View Full Report
                            </Button>
                        </div>
                    </div>
                    <div className={classes.bigElement}>
                        <Charts
                            title='Invoices'
                            options={{
                                vAxis: {
                                    format: '#\'$\'',
                                },
                                chartArea: { width: '90%', left: 0 },
                                height: '360px',
                            }}
                            yUnit={'$'}

                            aggregations={[]}
                            data={
                                groupData(
                                    'status',
                                    'amount',
                                    'type',
                                    aggregate(
                                        addColumn([
                                            ...this.props.invoices.paid,
                                            ...this.props.invoices.unpaid
                                        ], { type: '' }),
                                        'status',
                                        'amount',
                                        'type',
                                        null),
                                    null,
                                )
                            }
                            types={['pieChart']}/>
                    </div>
                </div>
                <div>
                    {this.props.isVisibleFullReport && <FullReport classes={classes} invoices={this.props.invoices} />}
                </div>
            </div>
        );
    }
}

export default withStyles(styles)(Overall);
