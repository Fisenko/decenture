import * as React from 'react';
import { WithStyles } from '@material-ui/core';
import withStyles from '@material-ui/core/styles/withStyles';
import { RouterProps } from 'react-router';

import styles from './styles';
import { addColumn, aggregate, groupData } from 'src/utils/chart';
import Charts from 'src/components/common/chart/container';

interface FullReportProps {
    invoices: {
        paid: Array<any>;
        unpaid: Array<any>;
    };
    classes: any;
}

class FullReport extends React.Component<FullReportProps> {

    render() {
        const { classes } = this.props;
        return (
            <div className={classes.fullReport}>
                <div>
                    <Charts
                        title='Invoice Type'
                        options={{
                            vAxis: {
                                format: '#\'$\'',
                            },
                            chartArea: { width: '100%', left: 0 },
                            height: '360px',
                        }}
                        yUnit={'$'}

                        aggregations={[]}
                        data={
                            groupData(
                                'title',
                                'amount',
                                'type',
                                aggregate(
                                    addColumn([
                                        ...this.props.invoices.paid,
                                        ...this.props.invoices.unpaid
                                    ], { type: '' }),
                                    'title',
                                    'amount',
                                    'type',
                                    null),
                                null,
                            )
                        }
                        types={['pieChart']}/>
                </div>
                <div className={classes.flexRow}>
                    <div>
                        <Charts
                            title='Degree Name'
                            options={{
                                vAxis: {
                                    format: '#\'$\'',
                                },
                                chartArea: { width: '100%', left: 0 },
                                height: '260px',
                            }}
                            yUnit={'$'}

                            aggregations={[]}
                            data={
                                groupData(
                                    'degreeName',
                                    'amount',
                                    'type',
                                    aggregate(
                                        addColumn([
                                            ...this.props.invoices.paid,
                                            ...this.props.invoices.unpaid
                                        ], { type: '' }),
                                        'degreeName',
                                        'amount',
                                        'type',
                                        null),
                                    null,
                                )
                            }
                            types={['pieChart']}/>
                    </div>
                    <div>
                        <Charts
                            title='International/Not International'
                            options={{
                                vAxis: {
                                    format: '#\'$\'',
                                },
                                chartArea: { width: '100%', left: 0 },
                                height: '260px',
                            }}
                            yUnit={'$'}

                            aggregations={[]}
                            data={
                                groupData(
                                    'isInternational',
                                    'amount',
                                    'type',
                                    aggregate(
                                        addColumn([
                                            ...this.props.invoices.paid,
                                            ...this.props.invoices.unpaid
                                        ], { type: '' }),
                                        'isInternational',
                                        'amount',
                                        'type',
                                        null),
                                    null,
                                )
                            }
                            types={['pieChart']}/>
                    </div>
                </div>
            </div>
        );
    }
}

export default withStyles(styles)(FullReport);
