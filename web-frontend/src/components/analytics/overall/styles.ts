import { StyleRulesCallback } from '@material-ui/core';


const styles: StyleRulesCallback = (theme): any => ({
    wrap: {
        backgroundColor: theme.palette.background.default,
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
        flexWrap: 'wrap',
    },

    flexContainer: {
        backgroundColor: theme.palette.background.default,
        display: 'flex',
        justifyContent: 'space-between',
        marginBottom: 32,
    },

    secondContainer: {
        backgroundColor: theme.palette.background.default,
        display: 'flex',
        flexWrap: 'wrap',
        minHeight: '500px',
    },

    smallElement: {
        display: 'flex',
        width: 'calc(33% - 16px)',
        padding: '24px',
        backgroundColor: '#fff',
        alignItems: 'center',
    },

    balanceElement: {
        flexDirection: 'column',
        alignItems: 'stretch',
        justifyContent: 'center',
    },

    total: {
        display: 'flex',
        justifyContent: 'space-between',
        width: '100%',
        flexDirection: 'row',
        fontSize: 15,
        marginBottom: 10,
        color: 'primary',
        marginTop: 10,
    },

    flexElementTop: {
        flex: 1,
    },

    totalSum: {
        color: 'rgba(74, 217, 145, 1)',
        fontSize: 18,
    },

    flexElementMiddle: {
        flex: 2,
    },

    totalSumBegin: {
        color: 'rgba(127, 150, 181, 1)',
        fontSize: 13,
    },

    flexElementBottom: {
        display: 'flex',
        flex: 0.5,
        justifyContent: 'center',
    },

    fullReportButton: {
        flex: 1,
    },

    fullReport: {
        flex: 1,
        display: 'flex',
        flexWrap: 'wrap',
        backgroundColor: '#FFF',
        minHeight: 500,
        '& > div': {
            width: '100%',
            marginBottom: '50px'
        }
    },
    flexRow: {
      display: 'flex',
      '& > div':{
          width: '50%'
      }
    },
    totalBeginTitle: {
        marginTop: 28,
    },

    bigElement: {
        flexGrow: 1,
        marginLeft: '32px',
        padding: '24px',
        minHeight: '300px',
        backgroundColor: '#fff',
    },

    cached: {
        fontSize: 20,
        color: 'rgba(74, 217, 145, 1)',
    },

    error: {
        fontSize: 20,
        color: 'rgba(255, 202, 131, 1)',
    },

    timer: {
        fontSize: 20,
        color: 'rgba(177, 191, 210, 1)',
    },

    iconContainer: {
        backgroundColor: 'rgba(218, 247, 232, 1)',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: '50%',
        width: 40,
        height: 40,
        marginRight: 10,
    },

});

export default styles;
