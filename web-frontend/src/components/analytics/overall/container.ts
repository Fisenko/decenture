import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import { withRouter } from 'react-router-dom';

import State from 'src/core/models/state';
import { getPaymentByUniversity } from 'src/core/actions/payment';
import Overall from 'src/components/analytics/overall/Overall';
import { toggleFullReportVisible } from 'src/core/actions/ui/analitics';

const mapStateToProps = (state: State, props) => ({
    payment: state.payment.current.info,
    isVisibleFullReport: state.ui.analytics.info.isVisibleFullReport,
    invoices: {
        paid: state.payment.paid.list,
        unpaid: state.payment.unpaid.list
    }
});

const mapDispatchToProps = (dispatch: Dispatch) =>
    bindActionCreators({ getPaymentByUniversity, toggleFullReportVisible }, dispatch);

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps,
    )(Overall),
);
