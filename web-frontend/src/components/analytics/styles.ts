import { StyleRulesCallback } from '@material-ui/core';


const styles: StyleRulesCallback = (theme): any => ({
    wrap: {
        backgroundColor: theme.palette.background.default,
        display: 'flex',
        flexDirection: 'column',
    },

    title: {
        marginBottom: 20,
        paddingLeft: 10,
    },
});

export default styles;
