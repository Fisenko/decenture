import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import { withRouter } from 'react-router-dom';

import App from 'src/App';
import { initMain } from 'src/core/actions/main';

const mapStateToProps = (state, props) => ({
    user: state.user,
});

const mapDispatchToProps = (dispatch: Dispatch) =>
    bindActionCreators(
        {
            initMain: initMain,
        },
        dispatch,
    );

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps,
    )(App),
);
