const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const path = require('path');

const DIR = path.resolve(__dirname);
const config = {
    mode: 'development',
    devtool: 'inline-source-map',
    entry: path.join(DIR, 'src/index.tsx'),
    output: {
        filename: 'bundle.js',
        path: DIR + '/dist',
        publicPath: '/',
        pathinfo: false,
    },

    resolve: {
        extensions: [' ', '.ts', '.tsx', '.js', '.svg', '.json'],
        alias: {
            src: path.resolve(DIR, 'src'),
        },
    },
    devServer: {
        host: process.env.HOST || '0.0.0.0',
        hot: false,
        progress: true,
        port: process.env.WEB_FRONTEND_PORT || 4600,
        historyApiFallback: true,
        contentBase: path.join(DIR, 'dist'),
        disableHostCheck: true,
    },

    module: {
        rules: [
            {
                enforce: 'pre',
                test: /\.tsx?$/,
                use: 'source-map-loader',
                exclude: /node_modules/,
            },
            {
                test: /\.tsx?$/,
                loader: 'ts-loader',
                exclude: /node_modules/,
                options: {
                    transpileOnly: true,
                    experimentalWatchApi: true,
                },
            },
            {
                test: /\.css$/,
                include: path.join(DIR, 'src'),
                loaders: [
                    'style-loader',
                    {
                        loader: 'typings-for-css-modules-loader',
                        options: {
                            modules: true,
                            namedExport: true,
                        },
                    },
                ],
            },
            {
                test: /\.(svg|jpg)$/,
                exclude: /node_modules/,
                use: {
                    loader: 'url-loader',
                },
            },
        ],
    },

    plugins: [
        new webpack.NamedModulesPlugin(),
        new webpack.HotModuleReplacementPlugin(),
        new HtmlWebpackPlugin({
            template: path.resolve(DIR, 'index.html'),
        }),
        new CopyWebpackPlugin([
            {
                from: path.resolve(DIR, 'public'),
                to: path.resolve(DIR, 'dist/public'),
            },
        ]),
        // TODO: replace with some values. DO NOT PASS ALL ENVIRONMENTS
        new webpack.DefinePlugin({
            'process.env':  JSON.stringify(process.env)
        })
    ],
    node: {
        process: false,
        fs: 'empty',
    },
};

module.exports = config;
