Decenture 
### How to install
- npm install
- react-native link react-native-nfc-manager
- react-native link react-native-vector-icons
- react-native link react-native-svg
- react-native link react-native-fingerprint-scanner

### How to run

- cd mobile-frontend
- First window shell: REACT_NATIVE_BACKEND_HOST=<IP_ADDRESS> npm start
- Second window shell: npm run android
- if watchman stuck use: watchman watch-del-all && watchman shutdown-server
                        

### How to debug

- same as run
- Press Ctrl+M (or press Menu button on phone/shake your phone)
- Press Debug JS Remotely
- Go to http://localhost:8081/debugger-ui/
- Optional*: Ctrl+M -> Dev settings -> Debug server host & port for device -> localhost:8081
- Use it :)

##Overview
### how to generate keys
`keytool -genkey -v -keystore my-release-key.jks -keyalg RSA -keysize 2048 -validity 10000 -alias my-alias`

Note: keytool is located in the bin/ directory in your JDK. To locate your JDK from Android Studio, select File > Project Structure, and then click SDK Location and you will see the JDK location

### How to build with expo
In first shell window execute `vagga mobile-frontend`
In second shell window `vagga mobile-frontend-build`
Note: use key server `./my-release-key.jks` with pass `QWEcde123$`

### How to build with native code

1. Run metro bundler (in first window shell):

`
REACT_NATIVE_BACKEND_HOST=<YOUR_IP_ADDRESS> npm start --reset-cache
` 

2. Download index.android.bundle (in second window shell):  

`
curl "http://localhost:8081/index.android.bundle?platform=android" -o "android/app/src/main/assets/index.android.bundle"
`

5. Build android release:

`
cd android/ && ./gradlew assembleRelease
`

######Note: Option #4 not necessary. You can use your keys"
4. How to generate keys:

`
cd .. && keytool -genkey -v -keystore my-release-key-2.jks -keyalg RSA -keysize 2048 -validity 10000 -alias my-alias
` 

5. Sign application with keys: 

`
jarsigner -verbose -keystore my-release-key-2.jks android/app/build/outputs/apk/release/app-release-unsigned.apk my-alias
`

6. Archive to .apk: 

`
zipalign -f -v 4 android/app/build/outputs/apk/release/app-release-unsigned.apk decenture.apk
`

7. Install apk on device (Linux only):


`
adb install decenture.apk
`




