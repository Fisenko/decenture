import React from 'react';
import Expo from 'expo';
import AppWrapper from './AppWrapper';

if (process.env.NODE_ENV === 'development') {
    Expo.KeepAwake.activate();
}

Expo.registerRootComponent(AppWrapper);