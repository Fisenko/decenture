import 'intl';
import 'intl/locale-data/jsonp/en';

import React from 'react';
import { Provider } from 'react-redux';
import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';
import { IntlProvider } from 'react-intl';
import { StatusBar } from 'react-native';
import { createReactNavigationReduxMiddleware } from 'react-navigation-redux-helpers';

const navigationMiddleware = createReactNavigationReduxMiddleware('root', (state: any) => state.nav);

import rootReducer from './core/reducers';
import AppContainer from './container';
import { STATUS_BAR_COLOR } from './common/styles';

const store = createStore(rootReducer, applyMiddleware(thunk, navigationMiddleware));

class AppWrapper extends React.Component {
    render() {
        return (
            <>
                <StatusBar backgroundColor={STATUS_BAR_COLOR} />
                <Provider store={store}>
                    <IntlProvider locale="en">
                        <AppContainer />
                    </IntlProvider>
                </Provider>
            </>
        );
    }
}

export default AppWrapper;
