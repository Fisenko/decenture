import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    background: {
        backgroundColor: '#fff',
    },
    container: {
        flex: 1,
        alignItems: 'stretch',
        justifyContent: 'flex-start',
        paddingVertical: 12,
        paddingHorizontal: 12,
    },
    date: {
        marginVertical: 12,
    },
});
