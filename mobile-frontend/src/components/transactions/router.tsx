import { createStackNavigator } from 'react-navigation';
import Transactions from './container';
import InvoiceDetails from './details/invoice/container';
import TransactionDetails from './details/transaction/container';
import { headerNavigationOptions } from 'DecentureMobile/src/utils/router_utils';

const routerConfig = {
    Transactions: {
        screen: Transactions,
        navigationOptions: {
            title: 'Transactions',
        },
    },
    TransactionDetails: {
        screen: TransactionDetails,
        navigationOptions: {
            title: 'Transaction Details',
        },
    },
    InvoiceDetails: {
        screen: InvoiceDetails,
        navigationOptions: {
            title: 'Invoice Details',
        },
    },
};

export default createStackNavigator(routerConfig, {
    headerMode: 'screen',
    initialRouteName: 'Transactions',
    navigationOptions: headerNavigationOptions,
});
