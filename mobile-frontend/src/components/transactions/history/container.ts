import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';

import History from './index';
import { getTransactions, selectTransaction } from 'DecentureMobile/src/core/actions/transaction';
import { setDetailsVisible, setTxSelectedType } from 'DecentureMobile/src/core/actions/ui/transaction_details';

const mapStateToProps = (state: any) => ({
    transactions: state.user.transaction.list,
    selected: state.user.transaction.current.info,
    accounts: state.user.current.account.list,
});

const mapDispatchToProps = (dispatch: Dispatch) =>
    bindActionCreators(
        {
            getTransactions,
            selectTransaction,
            setDetailsVisible,
            setTxSelectedType,
        },
        dispatch,
    );

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(History);
