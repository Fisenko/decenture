import * as React from 'react';
import { NavigationScreenProp, NavigationState } from 'react-navigation';
import { SectionList, SectionListRenderItemInfo, Text, SectionListData, StyleProp, TextStyle } from 'react-native';
import moment from 'moment';

import TransactionModel, { TransactionType } from 'DecentureMobile/src/core/models/transaction';
import Account from 'DecentureMobile/src/core/models/Account';
import Transaction from './transaction';
import { DATE_FORMAT } from 'DecentureMobile/src/utils/format_utils';
import styles from '../styles';
import DummyView from 'DecentureMobile/src/components/common/dummy';

export interface HistoryProps {
    navigation: NavigationScreenProp<NavigationState>;
    transactions: Array<TransactionModel>;
    accounts: Array<Account>;
    selected: TransactionModel;

    style?: StyleProp<TextStyle>;

    getTransactions(): any;
    selectTransaction(transaction: TransactionModel): any;
    setDetailsVisible(): any;
    setTxSelectedType(txType: TransactionType): any;
}

// @ts-ignore
const mock: Array<TransactionModel> = [
    {
        amount: 4543,
        datetime: new Date('2018-11-11T10:32:10.174Z'),
        description: 'Room and Board',
        id: '60e164b086e4c24415038ada2a950cec713c24ffb1a3f3eb10b942fe88cff0zx',
        name: 'Room and Board',
        receiver: 'Hogwarts account',
        sender: 'Harry Potter personal account',
        type: 'Invoice',
        isReceiver: false,
    },
    {
        amount: 4821,
        datetime: new Date('2018-11-12T10:32:10.174Z'),
        description: 'Room and Board',
        id: '60e164b086e4c24415038ada2a950cec713c24ffb1a3f3eb10b942fe88cff0zz',
        name: 'Room and Board',
        receiver: 'Hogwarts account',
        sender: 'Harry Potter personal account',
        type: 'Invoice',
        isReceiver: false,
    },
    {
        amount: 4569,
        datetime: new Date('2018-11-11T10:32:10.174Z'),
        description: 'Room and Board',
        id: '60e164b086e4c24415038ada2a950cec713c24ffb1a3f3eb10b942fe88cff0zc',
        name: 'Room and Board',
        receiver: 'Hogwarts account',
        sender: 'Harry Potter personal account',
        type: 'Invoice',
        isReceiver: true,
    },
    {
        amount: 4325,
        datetime: new Date('2018-11-12T10:32:10.174Z'),
        description: 'Room and Board',
        id: '60e164b086e4c24415038ada2a950cec713c24ffb1a3f3eb10b942fe88cff0zv',
        name: 'Room and Board',
        receiver: 'Hogwarts account',
        sender: 'Harry Potter personal account',
        type: 'Invoice',
        isReceiver: true,
    },
    {
        amount: 6588,
        datetime: new Date('2018-11-12T10:32:10.174Z'),
        description: 'Room and Board',
        id: '60e164b086e4c24415038ada2a950cec713c24ffb1a3f3eb10b942fe88cff0zg',
        name: 'Room and Board',
        receiver: 'Hogwarts account',
        sender: 'Harry Potter personal account',
        type: 'Invoice',
        isReceiver: true,
    },
];

class History extends React.Component<HistoryProps> {
    handleTransactionPress = (transaction: TransactionModel) => {
        if (this.props.selected.id === transaction.id) {
            this.props.selectTransaction(new TransactionModel());
        } else {
            this.props.selectTransaction(transaction);
        }
    };

    isSender = (transaction: TransactionModel): boolean => {
        const { accounts } = this.props;

        return !!accounts.find(account => account.name === transaction.sender);
    };

    renderItem = ({ item }: SectionListRenderItemInfo<TransactionModel>) => {
        const isSender = this.isSender(item);

        return (
            <Transaction
                key={item.id}
                transaction={item}
                onPress={this.handleTransactionPress}
                isSender={isSender}
                isSelected={item.id === this.props.selected.id}
            />
        );
    };

    renderSectionHeader = (info: { section: SectionListData<TransactionModel> }) => (
        <Text style={styles.date}>{info.section.title}</Text>
    );

    render() {
        const transactions = this.props.transactions; // .concat(...mock);

        const transactionsByDate: {
            [key: string]: Array<TransactionModel>;
        } = transactions.reduce((accumulator: any, transaction: TransactionModel) => {
            const date = moment(transaction.datetime).format(DATE_FORMAT);

            accumulator[date] ? accumulator[date].push(transaction) : (accumulator[date] = [transaction]);

            return accumulator;
        }, {});

        // console.log('transactionsByDate', transactionsByDate);

        const sections = Object.entries(transactionsByDate).map((entry: [string, Array<TransactionModel>]) => ({
            title: entry[0],
            data: entry[1],
        }));

        // console.log('sections', sections);

        return (
            <SectionList
                style={this.props.style}
                renderItem={this.renderItem}
                renderSectionHeader={this.renderSectionHeader}
                sections={sections}
                keyExtractor={(item: TransactionModel) => item.id}
                ListEmptyComponent={<DummyView />}
            />
        );
    }
}

export default History;
