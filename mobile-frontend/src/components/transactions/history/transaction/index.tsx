import * as React from 'react';

import styles from '../../unpaid/invoice/styles';
import TransactionModel from 'DecentureMobile/src/core/models/transaction';
import Card from 'DecentureMobile/src/components/transactions/card';
import TransactionDetails from 'DecentureMobile/src/components/transactions/details/transaction/container';

interface TransactionProps {
    transaction: TransactionModel;
    isSender: boolean;
    isSelected: boolean;

    onPress(item: TransactionModel): void;
}

const codeLength = 6;

const Transaction = (props: TransactionProps) => {
    const { transaction, isSender } = props;

    const handlePress = () => {
        props.onPress(transaction);
    };

    const priceStyle = isSender ? styles.gray : styles.green;
    const code = transaction.id.substring(transaction.id.length - codeLength);

    return (
        <Card
            description={transaction.receiver}
            price={transaction.amount}
            title={transaction.description}
            code={code}
            onPress={handlePress}
            priceStyle={priceStyle}
            isOpen={props.isSelected}
            isSender={props.isSender}
        >
            <TransactionDetails />
        </Card>
    );
};

export default Transaction;
