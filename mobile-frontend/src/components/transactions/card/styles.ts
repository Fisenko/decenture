import { StyleSheet } from 'react-native';
import { DECENTURE_COLOR } from 'DecentureMobile/src/common/styles';

export default StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    content: {
        flex: 1,
        marginLeft: 10,
    },
    row: {
        flexDirection: 'row',
    },
    title: {
        flexGrow: 1,
        fontSize: 16,
        color: '#000',
    },
    price: {
        fontSize: 16,
        color: '#d17369',
    },
    description: {
        flexGrow: 1,
        fontSize: 12,
        color: '#818181',
    },
    code: {
        fontSize: 12,
        color: '#818181',
    },
    smallText: {
        fontSize: 12,
    },
});
