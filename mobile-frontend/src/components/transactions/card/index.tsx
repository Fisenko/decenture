import * as React from 'react';
import { View, Text, TouchableOpacity, StyleProp, TextStyle } from 'react-native';
import { FormattedNumber } from 'react-intl';
import { Icon } from 'react-native-material-ui';

import styles from './styles';
import NameAvatar from 'DecentureMobile/src/components/common/name_avatar';

interface CardProps {
    title: string;
    price: number;
    description: string;
    code: string;
    isOpen?: boolean | undefined;
    isSender?: boolean | undefined;

    priceStyle?: StyleProp<TextStyle>;

    onPress: any;
}

const Card: React.SFC<CardProps> = props => {
    const renderPrice = (formattedNumber: string) => {
        const [dollars, cents] = formattedNumber.split('.');

        return (
            <Text style={[styles.price, props.priceStyle]}>
                {props.isSender && '-'}${dollars}.<Text style={styles.smallText}>{cents}</Text>
            </Text>
        );
    };

    const renderArrow = () => {
        if (props.isOpen === undefined) {
            return null;
        }

        let iconName = 'keyboard-arrow-down';
        if (props.isOpen) {
            iconName = 'keyboard-arrow-up';
        }

        return <Icon color="#B2B2B2" name={iconName} size={25} />;
    };

    return (
        <View style={{ paddingVertical: 5 }}>
            <TouchableOpacity style={styles.container} onPress={props.onPress}>
                <NameAvatar name={props.title} />
                <View style={styles.content}>
                    <View style={styles.row}>
                        <Text style={styles.title}>{props.title}</Text>
                        <FormattedNumber value={props.price} minimumFractionDigits={2} maximumFractionDigits={2}>
                            {renderPrice}
                        </FormattedNumber>
                    </View>
                    <View style={styles.row}>
                        <Text style={styles.description}>{props.description}</Text>
                        <Text style={styles.code}>#{props.code}</Text>
                    </View>
                </View>
                {renderArrow()}
            </TouchableOpacity>
            {props.isOpen && props.children}
        </View>
    );
};

export default Card;
