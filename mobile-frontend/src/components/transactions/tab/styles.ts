import { StyleSheet } from 'react-native';
import { DECENTURE_COLOR } from 'DecentureMobile/src/common/styles';

export default StyleSheet.create({
    container: {
        marginBottom: 12,
    },
    header: {
        flex: 0,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: DECENTURE_COLOR,
        borderTopLeftRadius: 6,
        borderTopRightRadius: 6,
        padding: 8,
    },
    text: {
        fontSize: 16,
    },
    white: {
        color: '#fff',
    },
    transactionContainer: {
        borderBottomWidth: 1,
        borderBottomColor: '#d7d7d7',
    },
});
