import * as React from 'react';
import { Text, View } from 'react-native';
import { Icon } from 'react-native-material-ui';

import styles from './styles';

interface TabProps {
    name: string;
    isOpen: boolean;
    children: React.ReactNode;

    onOpenBarPress(isOpen: boolean): void;
}

export default class Tab extends React.Component<TabProps> {
    toggleOpen = () => {
        this.props.onOpenBarPress(!this.props.isOpen);
    };

    renderArrow = () => {
        const name = this.props.isOpen ? 'arrow-drop-down' : 'arrow-drop-up';

        return <Icon color="white" name={name} size={25} />;
    };

    render() {
        const { children } = this.props;

        return (
            <View style={styles.container}>
                <View style={styles.header} onTouchEnd={this.toggleOpen}>
                    <Text style={[styles.text, styles.white]}>{this.props.name}</Text>
                    {this.renderArrow()}
                </View>
                {this.props.isOpen && children}
            </View>
        );
    }
}
