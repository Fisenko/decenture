import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';

import Transaction from './index';

const mapStateToProps = (state: any) => ({
    transaction: state.user.transaction.current.info,
});

const mapDispatchToProps = (dispatch: Dispatch) => bindActionCreators({}, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Transaction);
