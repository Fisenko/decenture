import * as React from 'react';
import { View, Text } from 'react-native';
import moment from 'moment';

import styles from './styles';
import { TIME_FORMAT, DATE_FORMAT } from 'DecentureMobile/src/utils/format_utils';
import Transaction from 'DecentureMobile/src/core/models/transaction';

interface DetailsProps {
    transaction: Transaction;
}

const Details = (props: DetailsProps) => {
    const { transaction } = props;
    const datetime = moment(transaction.datetime);

    return (
        <View style={styles.container}>
            <View style={styles.detailsRow}>
                <Text style={styles.headerText}>Time</Text>
                <Text style={styles.subheaderText}>{datetime.format(TIME_FORMAT)}</Text>
            </View>
            <View style={styles.detailsRow}>
                <Text style={styles.headerText}>Date</Text>
                <Text style={styles.subheaderText}>{datetime.format(DATE_FORMAT)}</Text>
            </View>
        </View>
    );
};

export default Details;
