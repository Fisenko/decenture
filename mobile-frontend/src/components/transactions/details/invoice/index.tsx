import * as React from 'react';
import { View, Text } from 'react-native';
import { Button } from 'react-native-material-ui';
import { NavigationScreenProp, NavigationState } from 'react-navigation';

import styles from './styles';
import Invoice from 'DecentureMobile/src/core/models/invoice';

interface DetailsProps {
    navigation: NavigationScreenProp<NavigationState>;
    invoice: Invoice;

    payInvoice(invoice: Invoice): any;
}

const InvoiceDetails = (props: DetailsProps) => {
    const { invoice } = props;

    const handlePayPress = () => {
        props.payInvoice(invoice);
        props.navigation.navigate('StatusPage');
    };

    return (
        <View style={styles.container}>
            <View style={styles.alignLeft}>
                <Text style={styles.headerText}>From:</Text>
                <Text style={styles.subheaderText}>{invoice.from}</Text>
                <Text style={styles.headerText}>Billed to:</Text>
                <Text style={styles.subheaderText}>{invoice.to}</Text>
            </View>
            <Button
                style={{ text: styles.payText, container: styles.payContainer }}
                text="Pay Now"
                onPress={handlePayPress}
            />
        </View>
    );
};

export default InvoiceDetails;
