import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import { withNavigation } from 'react-navigation';

import Invoice from './index';
import { payInvoice } from 'DecentureMobile/src/core/actions/invoice';

const mapStateToProps = (state: any) => ({
    invoice: state.user.invoice.current.info,
});

const mapDispatchToProps = (dispatch: Dispatch) => bindActionCreators({ payInvoice }, dispatch);

export default withNavigation(connect(
    mapStateToProps,
    mapDispatchToProps,
)(Invoice));
