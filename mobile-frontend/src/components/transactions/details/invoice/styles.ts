import { Dimensions, StyleSheet } from 'react-native';

import { DECENTURE_COLOR } from 'DecentureMobile/src/common/styles';

const window = Dimensions.get('window');

export default StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        marginTop: 5,
        marginLeft: 50,
        backgroundColor: '#fff',
    },
    detailsRow: {
        flex: 0,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingBottom: 20,
    },
    alignRight: {
        flex: 0,
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    alignLeft: {
        flex: 0,
        flexDirection: 'column',
    },
    alignCenter: {
        flex: 0,
        flexDirection: 'row',
        justifyContent: 'center',
    },
    image: {
        height: 100,
        resizeMode: 'contain',
    },
    caption: {
        color: '#1e3250',
        fontSize: 24,
        textAlign: 'center',
        marginTop: 20,
        marginBottom: 10,
    },
    details: {
        flex: 0,
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: 50,
        marginBottom: 10,
    },
    title: {
        fontWeight: 'bold',
        marginBottom: 20,
        fontSize: 25,
    },
    subtitle: {
        fontSize: 20,
    },
    text: {
        padding: 15,
    },
    headerText: {
        fontWeight: 'bold',
        fontSize: 14,
        color: 'black',
    },
    subheaderText: {
        maxWidth: window.width * 0.5,
        fontSize: 16,
    },
    itemText: {
        paddingBottom: 15,
        maxWidth: window.width / 3,
    },
    amount: {
        padding: 15,
        fontWeight: 'bold',
        textAlign: 'right',
        paddingRight: 30,
        fontSize: 25,
        color: '#538ED4',
    },
    invoiceContainer: {
        flexDirection: 'column',
        alignItems: 'center',
    },
    payText: {
        color: DECENTURE_COLOR,
    },
    payContainer: {
        width: 100,
        marginLeft: 'auto',
    },
});
