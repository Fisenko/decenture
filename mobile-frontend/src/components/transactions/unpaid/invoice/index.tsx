import * as React from 'react';

import InvoiceModel from 'DecentureMobile/src/core/models/invoice';
import Card from 'DecentureMobile/src/components/transactions/card';
import InvoiceDetails from 'DecentureMobile/src/components/transactions/details/invoice/container';

interface InvoiceProps {
    invoice: InvoiceModel;
    isSelected: boolean;

    onPress?(item: InvoiceModel): void;
}

const codeLength = 6;

const Invoice = (props: InvoiceProps) => {
    const handlePress = () => {
        if (props.onPress) {
            props.onPress(props.invoice);
        }
    };

    const prepareItem = (item: InvoiceModel) => {
        return {
            title: item.description,
            description: item.email || '',
            code: item.id.substring(item.id.length - codeLength),
            price: item.amount,
        };
    };

    const row = prepareItem(props.invoice);

    return (
        <Card {...row} onPress={handlePress} isOpen={props.isSelected}>
            <InvoiceDetails />
        </Card>
    );
};

export default Invoice;
