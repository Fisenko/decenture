import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    icon: {
        width: 32,
        height: 28,
        resizeMode: 'contain',
        marginLeft: 10,
    },
    content: {
        flexGrow: 1,
        marginLeft: 5,
        marginRight: 5,
    },
    flexRow: {
        flexDirection: 'row',
    },
    name: {
        flexGrow: 1,
        color: '#000',
        padding: 3,
    },
    description: {
        flexGrow: 1,
        fontSize: 11,
        color: '#818181',
        padding: 3,
    },
    currency: {
        textAlign: 'right',
        padding: 3,
    },
    code: {
        textAlign: 'right',
        fontSize: 11,
        color: '#727A84',
        padding: 3,
    },
    red: { color: '#d17369' },
    green: { color: '#8c9e37' },
    gray: { color: '#00000066' },
});
