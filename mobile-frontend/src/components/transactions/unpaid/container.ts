import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';

import Unpaid from './index';
import { getInvoices, selectInvoice } from 'DecentureMobile/src/core/actions/invoice';

const mapStateToProps = (state: any) => ({
    invoices: state.user.invoice.list,
    selected: state.user.invoice.current.info,
});

const mapDispatchToProps = (dispatch: Dispatch) =>
    bindActionCreators(
        {
            getInvoices,
            selectInvoice,
        },
        dispatch,
    );

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Unpaid);
