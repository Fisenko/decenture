import * as React from 'react';
import { NavigationScreenProp, NavigationState } from 'react-navigation';

import InvoiceModel from 'DecentureMobile/src/core/models/invoice';
import Invoice from './invoice';
import DummyView from 'DecentureMobile/src/components/common/dummy';

export interface UnpaidProps {
    navigation: NavigationScreenProp<NavigationState>;
    invoices: Array<InvoiceModel>;
    selected: InvoiceModel;

    getInvoices(): any;
    selectInvoice(invoice: InvoiceModel): any;
}

class Unpaid extends React.Component<UnpaidProps> {
    handleInvoicePress = (invoice: InvoiceModel) => {
        if (invoice.id === this.props.selected.id) {
            this.props.selectInvoice(new InvoiceModel());
        } else {
            this.props.selectInvoice(invoice);
        }
    };

    render() {
        if (!this.props.invoices.length) {
            return <DummyView />;
        }

        return this.props.invoices.map((item: InvoiceModel) => (
            <Invoice
                key={item.id}
                invoice={item}
                onPress={this.handleInvoicePress}
                isSelected={item.id === this.props.selected.id}
            />
        ));
    }
}

export default Unpaid;
