import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';

import Transactions from './index';
import { getTransactions } from 'DecentureMobile/src/core/actions/transaction';
import { setInvoiceTabOpen, setTxTabOpen } from 'DecentureMobile/src/core/actions/ui/transaction_details';
import { getInvoices } from 'DecentureMobile/src/core/actions/invoice';

const mapStateToProps = (state: any) => ({
    invoices: state.user.invoice.list,
    transactions: state.user.transaction.list,
    ui: state.ui.configs.transactions,
    invoicesUI: state.ui.configs.invoices,
});

const mapDispatchToProps = (dispatch: Dispatch) =>
    bindActionCreators(
        {
            getTransactions,
            getInvoices,
            setTxTabOpen,
            setInvoiceTabOpen,
        },
        dispatch,
    );

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Transactions);
