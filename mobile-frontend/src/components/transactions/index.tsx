import * as React from 'react';
import { ScrollView, View } from 'react-native';
import { NavigationScreenProp, NavigationState } from 'react-navigation';

import Tab from './tab';
import styles from './styles';
import Invoice from 'DecentureMobile/src/core/models/invoice';
import History from 'DecentureMobile/src/components/transactions/history/container';
import Unpaid from 'DecentureMobile/src/components/transactions/unpaid/container';
import RefreshControl from 'DecentureMobile/src/components/common/refresh_control';
import Transaction from 'DecentureMobile/src/core/models/transaction';

interface TransactionsProps {
    navigation: NavigationScreenProp<NavigationState>;
    invoices: Array<Invoice>;
    transactions: Array<Transaction>;
    ui: {
        isDetailsVisible: boolean;
        isTxTabOpen: boolean;
        isInvoiceTabOpen: boolean;
        isRefreshing: boolean;
    };
    invoicesUI: {
        isRefreshing: boolean;
    };

    getInvoices(): any;
    getTransactions(): any;
    setTxTabOpen(isOpen: boolean): any;
    setInvoiceTabOpen(isOpen: boolean): any;
}

export default class Transactions extends React.Component<TransactionsProps> {
    componentDidMount() {
        if (!this.props.invoices.length) {
            this.props.getInvoices();
        }
        if (!this.props.transactions.length) {
            this.props.getTransactions();
        }
    }

    componentDidUpdate(prevProps: Readonly<TransactionsProps>) {
        // update transactions after paying invoice
        if (prevProps.invoices.length !== this.props.invoices.length) {
            this.props.getTransactions();
        }
    }

    handleRefresh = () => {
        this.props.getTransactions();
        this.props.getInvoices();
    };

    render() {
        const { isInvoiceTabOpen, isTxTabOpen } = this.props.ui;
        const isRefreshing = this.props.ui.isRefreshing || this.props.invoicesUI.isRefreshing;

        return (
            <View style={{ flex: 1 }}>
                <ScrollView
                    style={styles.background}
                    refreshControl={<RefreshControl refreshing={isRefreshing} onRefresh={this.handleRefresh} />}
                >
                    <View style={styles.container}>
                        <Tab name="Unpaid" onOpenBarPress={this.props.setInvoiceTabOpen} isOpen={isInvoiceTabOpen}>
                            <Unpaid navigation={this.props.navigation} />
                        </Tab>
                        <Tab name="Transaction History" onOpenBarPress={this.props.setTxTabOpen} isOpen={isTxTabOpen}>
                            <History navigation={this.props.navigation} />
                        </Tab>
                    </View>
                </ScrollView>
            </View>
        );
    }
}
