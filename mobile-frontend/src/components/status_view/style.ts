import { Dimensions, StyleSheet } from 'react-native';

export const window = Dimensions.get('window');

export const IMAGE_HEIGHT = window.width / 3.5;

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF',
        justifyContent: 'space-around',
    },
    innerContainer: {
        justifyContent: 'space-between',
    },
    logoSmall: {
        height: IMAGE_HEIGHT,
        width: window.width,
        resizeMode: 'contain',
        marginBottom: 40,
    },
    text: {
        fontSize: 17,
        textAlign: 'center',
        marginBottom: 20,
    },
    buttonContainer: {
        marginVertical: 10,
    },
    buttonText: {
        color: '#72afdd',
    },
});
