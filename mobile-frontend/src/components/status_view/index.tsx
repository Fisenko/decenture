import * as React from 'react';
import { ActivityIndicator, Image, Text, TouchableOpacity, View } from 'react-native';
import { NavigationScreenProp, NavigationState, NavigationActions } from 'react-navigation';
import { Button } from 'react-native-material-ui';

import styles from './style';
import { commonStyles } from '../common/styles';
import { StatusViewPage } from '../../core/models/AppStatusView';

const TIMEOUT = 2000;

interface StatusViewProps {
    navigation: any;
    statusView: {
        currInfo: {
            label: string;
            logo: number;
            navigateToScreen: string;
            status: StatusViewPage;
            withNavigation: boolean;
            navigateTimeoutId: number;
        };
    };
    setUIConfig(systemKey: string, field: string | number, value: any): any;
}

export default class AppStatusView extends React.Component<StatusViewProps> {
    componentDidUpdate(prevProps: Readonly<StatusViewProps>) {
        const {
            navigation,
            statusView: { currInfo },
        } = this.props;

        if (prevProps.statusView.currInfo.navigateTimeoutId === currInfo.navigateTimeoutId) {
            if (currInfo.status === StatusViewPage.SUCCESS) {
                const navigateTimeoutId = setTimeout(() => navigation.navigate(currInfo.navigateToScreen), TIMEOUT);
                this.props.setUIConfig('AppStatusView', 'currInfo', { ...currInfo, navigateTimeoutId });
            } else {
                clearTimeout(this.props.statusView.currInfo.navigateTimeoutId);
            }
        }
    }

    tryAgain = () => {
        clearTimeout(this.props.statusView.currInfo.navigateTimeoutId);
        this.props.navigation.navigate(this.props.statusView.currInfo.navigateToScreen);
    };

    navigateToSettings = () => {
        // clearTimeout(this.props.statusView.currInfo.navigateTimeoutId);
    };

    navigateToHome = () => {
        clearTimeout(this.props.statusView.currInfo.navigateTimeoutId);
        this.props.navigation.reset(
            [NavigationActions.navigate({ routeName: 'Root' })], 0);
        // this.props.navigation.navigate('Home');
    };

    handleClose = () => {
        const { currInfo } = this.props.statusView;

        clearTimeout(this.props.statusView.currInfo.navigateTimeoutId);
        this.props.navigation.navigate(currInfo.navigateToScreen);
    };

    resolveView(statusView: any) {
        const { currInfo } = statusView;

        if (!currInfo) {
            return <View />;
        }

        return (
            <TouchableOpacity onPress={this.handleClose}>
                <View style={styles.innerContainer}>
                    {currInfo.status === StatusViewPage.LOADING ? (
                        <ActivityIndicator size="large" color="#0000ff" />
                    ) : (
                        <Image source={currInfo.logo} style={commonStyles.logoSmall} />
                    )}
                    <Text style={commonStyles.text}>{currInfo.label}</Text>
                </View>
            </TouchableOpacity>
        );
    }

    renderNavigation = () => {
        return (
            <View>
                <Button
                    style={{
                        text: styles.buttonText,
                        container: styles.buttonContainer,
                    }}
                    text="Try Again"
                    onPress={this.tryAgain}
                />
                <Button
                    style={{
                        text: styles.buttonText,
                        container: styles.buttonContainer,
                    }}
                    text="Settings"
                    onPress={this.navigateToSettings}
                />
                <Button
                    style={{
                        text: styles.buttonText,
                        container: styles.buttonContainer,
                    }}
                    text="Home"
                    onPress={this.navigateToHome}
                />
            </View>
        );
    };

    render() {
        const { statusView } = this.props;
        const { withNavigation } = statusView.currInfo;

        return (
            <View style={styles.container}>
                {this.resolveView(statusView)}
                {withNavigation && this.renderNavigation()}
            </View>
        );
    }
}
