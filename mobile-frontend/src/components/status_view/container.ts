import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import AppStatusView from './index';
import { setUIConfig } from '../../core/actions/ui';

const mapStateToProps = (state: any, props: any) => ({
    statusView: state.ui.configs.AppStatusView,
});

const mapDispatchToProps = (dispatch: Dispatch) => bindActionCreators({
    setUIConfig,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(AppStatusView);
