import {connect} from 'react-redux';
import {bindActionCreators, Dispatch} from 'redux';
import EmailVerification from './index';
import { onUserVerification } from 'DecentureMobile/src/core/actions/user';

const mapStateToProps = (state: any, props: any) => ({
});

const mapDispatchToProps = (dispatch: Dispatch) => bindActionCreators({
    onUserVerification,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(EmailVerification)
