import * as React from 'react';
import { Image, Text, View } from 'react-native';
import { Button } from 'react-native-material-ui';
import { NavigationScreenProp, NavigationState } from 'react-navigation';

import { commonStyles } from 'DecentureMobile/src/components/common/styles';
import { getImage } from 'DecentureMobile/src/utils/image_holder';

interface Props {
    navigation: NavigationScreenProp<NavigationState>;
    onUserVerification: Function;
}

export default class EmailVerification extends React.Component<Props> {
    constructor(props: any) {
        super(props);
    }

    componentDidMount() {
        this.props.onUserVerification();
    }

    onVerifyButtonClick = () => {
        this.props.navigation.navigate('StatusPage');
    };

    render() {
        return (
            <View style={commonStyles.container}>
                <Image source={getImage('logo')} style={[commonStyles.logoSmall, { top: '10%' }]} />
                <Text style={[commonStyles.text, { marginBottom: 100 }]}>
                    {`We have sent a verification\nlink to your email.\nPlease confirm.`}
                </Text>
                <Button
                    primary
                    style={{ container: commonStyles.button }}
                    onPress={this.onVerifyButtonClick}
                    text="Verify"
                />
            </View>
        );
    }
}
