import * as React from 'react';
import { Image, KeyboardAvoidingView, Text, TextInput, View } from 'react-native';
import { Button } from 'react-native-material-ui';
import { NavigationScreenProp, NavigationState } from 'react-navigation';
import Moment from 'moment';
import DateTimePicker from 'react-native-modal-datetime-picker';

import { commonStyles } from '../../common/styles';
import { getImage } from 'DecentureMobile/src/utils/image_holder';
import { AppUser } from 'DecentureMobile/src/core/models/AppUser';
import { DATE_FORMAT } from 'DecentureMobile/src/utils/format_utils';

interface Props {
    user: AppUser;
    errors: {
        errorMsg: string;
    };
    navigation: NavigationScreenProp<NavigationState>;

    isDatePickerVisible: boolean;
    isKeyboardActive: boolean;

    onRegistrationInputChange(data: { [key: string]: any }): any;

    onRegistrationNextButtonClicked(data: AppUser, navigation?: NavigationScreenProp<NavigationState>): any;

    setRegistrationError(errMsg: string): any;

    setDatePickerVisible: Function;
}

const initDate = new Date(2000, 0, 1);

export default class Register extends React.Component<Props> {
    constructor(props: any) {
        super(props);
    }

    validate = (): boolean => {
        const { user, setRegistrationError } = this.props;

        if (!user.firstName) {
            setRegistrationError('First name cannot be empty!');
            return false;
        }
        if (!user.lastName) {
            setRegistrationError('Last name cannot be empty!');
            return false;
        }
        if (!user.dateOfBirth) {
            setRegistrationError('Date of birth cannot be empty!');
            return false;
        }
        if (!user.email) {
            setRegistrationError('Email cannot be empty!');
            return false;
        }
        if (!user.studentNumber) {
            setRegistrationError('Student number cannot be empty!');
            return false;
        }

        return true;
    };

    onNextButtonClick = () => {
        if (this.validate()) {
            this.props.onRegistrationNextButtonClicked(this.props.user, this.props.navigation);
        }
    };

    _handleDatePicked = (date: Date) => {
        this.props.onRegistrationInputChange({
            name: 'dateOfBirth',
            value: date,
        });
        this.props.setDatePickerVisible(false);
    };

    _onInputChange = (name: string, value: any) => {
        this.props.onRegistrationInputChange({ name, value });
    };

    render() {
        const { dateOfBirth } = this.props.user;

        return (
            <View style={commonStyles.container}>
                <KeyboardAvoidingView
                    style={commonStyles.keyboardAvoid}
                    behavior="padding"
                    keyboardVerticalOffset={1124}
                    enabled
                >
                    <Image source={getImage('logo')} style={commonStyles.logoSmall} />
                    <Text style={commonStyles.text}>Tell us about yourself!</Text>
                    <TextInput
                        placeholder="First Name"
                        style={commonStyles.input}
                        underlineColorAndroid="#538ED4"
                        selectionColor="#538ED4"
                        onChangeText={text => this._onInputChange('firstName', text)}
                    />
                    <TextInput
                        placeholder="Last Name"
                        style={commonStyles.input}
                        underlineColorAndroid="#538ED4"
                        selectionColor="#538ED4"
                        onChangeText={text => this._onInputChange('lastName', text)}
                    />
                    <TextInput
                        placeholder="Date of Birth"
                        style={commonStyles.input}
                        underlineColorAndroid="#538ED4"
                        selectionColor="#538ED4"
                        pointerEvents="none"
                        onTouchStart={() => this.props.setDatePickerVisible(true)}
                        value={dateOfBirth && Moment(dateOfBirth).format(DATE_FORMAT)}
                    />
                    <TextInput
                        placeholder="University Email"
                        style={commonStyles.input}
                        keyboardType="email-address"
                        underlineColorAndroid="#538ED4"
                        selectionColor="#538ED4"
                        autoCapitalize="none"
                        onChangeText={text => this._onInputChange('email', text)}
                    />
                    <TextInput
                        placeholder="Student Number"
                        style={[commonStyles.input]}
                        underlineColorAndroid="#538ED4"
                        selectionColor="#538ED4"
                        onChangeText={text => this._onInputChange('studentNumber', text)}
                    />
                </KeyboardAvoidingView>
                <Text style={commonStyles.errorText}>{this.props.errors.errorMsg}</Text>
                <Button
                    primary
                    style={{ container: commonStyles.button }}
                    onPress={this.onNextButtonClick}
                    text="Next"
                />
                <DateTimePicker
                    isVisible={this.props.isDatePickerVisible}
                    datePickerModeAndroid="spinner"
                    onConfirm={this._handleDatePicked}
                    onCancel={() => this.props.setDatePickerVisible(false)}
                    date={initDate}
                />
            </View>
        );
    }
}
