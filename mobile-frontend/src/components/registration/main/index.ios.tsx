import * as React from 'react';
import { Image, Keyboard, KeyboardAvoidingView, Text, TextInput, View } from 'react-native';
import { NavigationScreenProp, NavigationState } from 'react-navigation';
import Moment from 'moment';
import DateTimePicker from 'react-native-modal-datetime-picker';

import { commonStyles } from '../../common/styles';
import { getImage } from 'DecentureMobile/src/utils/image_holder';
import { AppUser } from 'DecentureMobile/src/core/models/AppUser';
import Button from 'DecentureMobile/src/components/common/button';
import { DATE_FORMAT } from 'DecentureMobile/src/utils/format_utils';

interface Props {
    user: AppUser;
    errors: {
        errorMsg: string;
    };
    navigation: NavigationScreenProp<NavigationState>;

    isDatePickerVisible: boolean;

    onRegistrationInputChange(data: { [key: string]: any }): any;

    onRegistrationNextButtonClicked(data: AppUser, navigation?: NavigationScreenProp<NavigationState>): any;

    setRegistrationError(errMsg: string): any;

    setDatePickerVisible: Function;
}

const initDate = new Date(2000, 0, 1);

export default class Register extends React.Component<Props> {
    constructor(props: any) {
        super(props);
    }

    validate = (): boolean => {
        const { user, setRegistrationError } = this.props;

        if (!user.firstName) {
            setRegistrationError('First name cannot be empty!');
            return false;
        }
        if (!user.lastName) {
            setRegistrationError('Last name cannot be empty!');
            return false;
        }
        if (!user.dateOfBirth) {
            setRegistrationError('Date of birth cannot be empty!');
            return false;
        }
        if (!user.email) {
            setRegistrationError('Email cannot be empty!');
            return false;
        }
        if (!user.studentNumber) {
            setRegistrationError('Student number cannot be empty!');
            return false;
        }

        return true;
    };

    onNextButtonClick = () => {
        if (this.validate()) {
            this.props.onRegistrationNextButtonClicked(this.props.user, this.props.navigation);
        }
    };

    _handleDatePicked = (date: Date) => {
        this.props.onRegistrationInputChange({
            name: 'dateOfBirth',
            value: date,
        });
        this.props.setDatePickerVisible(false);
    };

    _onInputChange = (name: string, value: any) => {
        this.props.onRegistrationInputChange({ name, value });
    };

    _handleDateTextInputClick = () => {
        Keyboard.dismiss();
        this.props.setDatePickerVisible(true);
    };

    render() {
        const { dateOfBirth } = this.props.user;

        return (
            <View style={{ flex: 1 }}>
                <KeyboardAvoidingView style={commonStyles.containerIOS} behavior="position" keyboardVerticalOffset={40}>
                    <Image source={getImage('logo')} style={commonStyles.logoSmall} />
                    <Text style={commonStyles.text}>Tell us about yourself!</Text>
                    <View style={commonStyles.textInputView}>
                        <TextInput
                            style={commonStyles.inputIOS}
                            placeholder="First Name"
                            onChangeText={text => this._onInputChange('firstName', text)}
                        />
                        <TextInput
                            placeholder="Last Name"
                            style={commonStyles.inputIOS}
                            onChangeText={text => this._onInputChange('lastName', text)}
                        />
                        <TextInput
                            placeholder="Date of Birth"
                            style={commonStyles.inputIOS}
                            onTouchStart={this._handleDateTextInputClick}
                            editable={false}
                            value={dateOfBirth && Moment(dateOfBirth).format(DATE_FORMAT)}
                        />
                        <TextInput
                            placeholder="University Email"
                            keyboardType="email-address"
                            autoCapitalize="none"
                            style={commonStyles.inputIOS}
                            onChangeText={text => this._onInputChange('email', text)}
                        />
                        <TextInput
                            placeholder="Student Number"
                            style={commonStyles.inputIOS}
                            onChangeText={text => this._onInputChange('studentNumber', text)}
                        />
                        <Text style={commonStyles.errorText}>{this.props.errors.errorMsg}</Text>
                    </View>
                    <Button
                        primary
                        raised
                        style={{ container: commonStyles.buttonIOS }}
                        text="Next"
                        onPress={this.onNextButtonClick}
                    />
                    <DateTimePicker
                        isVisible={this.props.isDatePickerVisible}
                        onConfirm={this._handleDatePicked}
                        onCancel={() => this.props.setDatePickerVisible(false)}
                        date={initDate}
                    />
                </KeyboardAvoidingView>
            </View>
        );
    }
}
