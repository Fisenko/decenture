import {connect} from 'react-redux';
import {bindActionCreators, Dispatch} from 'redux';
import Register from './index';
import {
    onRegistrationInputChange,
    onRegistrationNextButtonClicked,
    setRegistrationError,
} from '../../../core/actions/user';
import {setDatePickerVisible} from 'DecentureMobile/src/core/actions/ui/registration';

const mapStateToProps = (state: any, props: any) => ({
    user: state.user.current.info,
    isDatePickerVisible: state.ui.configs.registration.isDatePickerVisible,
    errors: state.errors.configs.registration,
});

const mapDispatchToProps = (dispatch: Dispatch) => bindActionCreators({
    onRegistrationNextButtonClicked,
    onRegistrationInputChange,
    setDatePickerVisible,
    setRegistrationError,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Register);
