import {connect} from 'react-redux';
import {bindActionCreators, Dispatch} from 'redux';
import PasswordConfirmation from './index';
import { onRegistrationInputChange, registration } from '../../../core/actions/user';
import { setErrorConfig } from 'DecentureMobile/src/core/actions/error';

const mapStateToProps = (state: any, props: any) => ({
    user: state.user.current.info,
    errors: state.errors.configs.registration,
});

const mapDispatchToProps = (dispatch: Dispatch) => bindActionCreators({
    registration,
    onRegistrationInputChange,
    setErrorConfig,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(PasswordConfirmation);
