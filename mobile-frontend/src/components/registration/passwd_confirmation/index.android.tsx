import * as React from 'react';
import { Image, KeyboardAvoidingView, Text, TextInput, View } from 'react-native';
import { NavigationScreenProp, NavigationState } from 'react-navigation';

import { Button } from 'react-native-material-ui';
import { commonStyles } from '../../common/styles';
import { getImage } from 'DecentureMobile/src/utils/image_holder';
import { AppUser } from 'DecentureMobile/src/core/models/AppUser';

interface Props {
    user: AppUser;
    registration: Function;
    onRegistrationInputChange: Function;
    setErrorConfig: Function;

    navigation: NavigationScreenProp<NavigationState>;

    errors: {
        confirmPassword: string;
    };
}

export default class PasswordConfirmation extends React.Component<Props> {
    constructor(props: any) {
        super(props);
    }

    onCreateAccountButtonClick = () => {
        if (this._validateInput()) {
            this.props.registration(this.props.user);
            this.props.navigation.navigate('EmailVerification');
        }
    };

    _validateInput = () => {
        const { password, confirmPassword } = this.props.user;

        if (password === '') {
            this.props.setErrorConfig('registration', 'confirmPassword', 'Password cannot be empty!');
            return false;
        }
        if (password !== confirmPassword) {
            this.props.setErrorConfig('registration', 'confirmPassword', 'Password Does Not Match!');
            return false;
        }

        return true;
    };

    _onInputChange = (name: string, value: any) => {
        this.props.onRegistrationInputChange({ name, value });
    };

    render() {
        return (
            <View style={commonStyles.container}>
                <KeyboardAvoidingView
                    style={commonStyles.keyboardAvoid}
                    behavior="padding"
                    keyboardVerticalOffset={500}
                >
                    <Image source={getImage('logo')} style={commonStyles.logoSmall} />
                    <Text style={[commonStyles.text, { marginBottom: 40 }]}>
                        Nearly there!
                        {'\n'}
                        Just think of a password.
                    </Text>
                    <TextInput
                        placeholder="Enter Password"
                        style={commonStyles.input}
                        underlineColorAndroid="#538ED4"
                        selectionColor="#538ED4"
                        secureTextEntry
                        onChangeText={text => this._onInputChange('password', text)}
                    />
                    <TextInput
                        placeholder="Confirm Password"
                        style={commonStyles.input}
                        underlineColorAndroid="#538ED4"
                        selectionColor="#538ED4"
                        secureTextEntry
                        onChangeText={text => this._onInputChange('confirmPassword', text)}
                    />
                </KeyboardAvoidingView>
                <Text style={commonStyles.errorText}>{this.props.errors.confirmPassword}</Text>
                <Button
                    primary
                    style={{ container: commonStyles.button }}
                    onPress={this.onCreateAccountButtonClick}
                    text="Create Account"
                />
            </View>
        );
    }
}
