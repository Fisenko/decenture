import * as React from 'react';
import { KeyboardAvoidingView, TextInput, View, Text } from 'react-native';
import { Button } from 'react-native-material-ui';
import { commonStyles } from '../../common/styles';
import { Picker } from 'react-native';
import { styles } from './styles';

interface Props {
    bankAccount: {
        bankName: string;
        nameOnAccount: string;
        accountNumber: number;
        sortCode: number;
    };
    navigation?: any;
    bankInputChange: any;
}

enum bankName {
    HSBC = 'HSBC',
    TEST = 'TEST',
}

export default class BankAccount extends React.Component<Props, any> {

    render() {
        return (
            <View style={commonStyles.container}>
                <Text style={{ top: '10%' }}>Lets just add your bank details!</Text>
                <KeyboardAvoidingView style={commonStyles.keyboardAvoid}
                                      behavior='padding'>

                    <View>
                        <Text style={styles.label}>Bank Name</Text>
                        <Picker
                            selectedValue={this.props.bankAccount.bankName}
                            style={[{paddingTop: 0}, styles.picker]}
                            onValueChange={(itemValue, itemIndex) => this.props.bankInputChange({
                                name: 'bankName',
                                value: itemValue
                            })}>
                            <Picker.Item label={bankName.HSBC} value={bankName.HSBC}/>
                            <Picker.Item label={bankName.TEST} value={bankName.TEST}/>
                        </Picker>
                    </View>
                    <View>
                        <Text style={styles.label}>Name on Account</Text>
                        <TextInput
                            style={[commonStyles.input,{paddingTop: 0}]}
                            underlineColorAndroid='#538ED4'
                            selectionColor='#538ED4'
                            onChangeText={(text) => {
                                this.props.bankInputChange({
                                    name: 'nameOnAccount',
                                    value: text
                                });
                            }}
                        />
                    </View>
                    <View>
                        <Text style={styles.label}>Account Number</Text>
                        <TextInput
                            style={[{paddingTop: 0}, commonStyles.input]}
                            underlineColorAndroid='#538ED4'
                            selectionColor='#538ED4'
                            keyboardType='numeric'
                            maxLength={16}
                            onChangeText={(text) => {
                                this.props.bankInputChange({
                                    name: 'accountNumber',
                                    value: text
                                });
                            }}

                        />
                    </View>
                    <View>
                        <Text style={styles.label}>Sort Code</Text>
                        <TextInput
                            style={[commonStyles.input, { width: 70, paddingTop: 0 }]}
                            underlineColorAndroid='#538ED4'
                            selectionColor='#538ED4'
                            keyboardType='numeric'
                            maxLength={2}
                            onChangeText={(text) => {
                                this.props.bankInputChange({
                                    name: 'sortCode',
                                    value: text
                                });
                            }}

                        />
                    </View>
                    <View style={{ height: 60 }}/>
                </KeyboardAvoidingView>
                <Button primary raised
                        text={'Add'}
                        style={{ container: commonStyles.button }}
                        onPress={() => {
                            this.props.navigation.navigate('FundsHome');
                        }}
                />
            </View>
        );
    }
}
