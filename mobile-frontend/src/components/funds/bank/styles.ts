import {Dimensions, StyleSheet} from 'react-native';

const window = Dimensions.get('window');

export const styles = StyleSheet.create({
    label: {
        backgroundColor: 'transparent',
        marginHorizontal: 10,
        paddingLeft: 5,
        paddingBottom: 0,
        fontSize: 10,
    },

    picker: {
        width: window.width - 100,
        borderWidth: 1,
    },

    inputFieldView:{
        backgroundColor: 'transparent',
    }
});