import {bindActionCreators, Dispatch} from 'redux';
import {connect} from 'react-redux';
import BankAccount from './bankAccount';
import { bankInputChange } from '../../../core/actions/payment';

const mapStateToProps = (state: any, props: any) => ({
    bankAccount: state.user.current.payment.current.bank.current.info
});

const mapDispatchToProps = (dispatch: Dispatch) => bindActionCreators({
    bankInputChange
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(BankAccount);
