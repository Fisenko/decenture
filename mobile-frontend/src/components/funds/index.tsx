import { createStackNavigator } from 'react-navigation';

import AddCard from './card/container';
import FundsHome from './home/container';
import BankAccount from './bank/container';
import Deposit from './deposit/container';
import { headerNavigationOptions } from 'DecentureMobile/src/utils/router_utils';

export const fundsRouteConfigMap = {
    Funds: {
        screen: FundsHome,
        navigationOptions: {
            title: 'Top Up',
        },
    },
    AddCard: {
        screen: AddCard,
        navigationOptions: {
            title: 'Add Card',
        },
    },
    BankAccount: {
        screen: BankAccount,
        navigationOptions: {
            title: 'Bank Account',
        },
    },
    Deposit: {
        screen: Deposit,
        navigationOptions: {
            title: 'Deposit',
        },
    },
};

export default createStackNavigator(fundsRouteConfigMap, {
    headerMode: 'screen',
    initialRouteName: 'Funds',
    navigationOptions: headerNavigationOptions,
});
