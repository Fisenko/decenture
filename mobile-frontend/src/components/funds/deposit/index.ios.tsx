import * as React from 'react';
import { KeyboardAvoidingView, Picker, Text, TextInput, View, Keyboard } from 'react-native';
import { Button } from 'react-native-material-ui';
import { styles } from 'DecentureMobile/src/components/funds/deposit/styles';
import { commonStyles } from 'DecentureMobile/src/components/common/styles';
import { NavigationActions } from 'react-navigation';

interface Props {
    deposit: {
        amount: any;
        recurring: any;
        cardId: any;
    };
    navigation: any;
    depositInputChange: any;
    initDepositTransaction: any;
}

export default class Deposit extends React.Component<Props, any> {
    render() {
        return (
            <View style={styles.container}>
                <KeyboardAvoidingView style={commonStyles.keyboardAvoid} behavior="padding">
                    <View>
                        <Text style={styles.label}>How much would you like to add?</Text>
                        <View style={styles.textInputView}>
                            <TextInput
                                style={styles.input}
                                keyboardType="number-pad"
                                returnKeyType="done"
                                placeholder="$"
                                onChangeText={text => {
                                    this.props.depositInputChange({
                                        name: 'amount',
                                        value: text,
                                    });
                                }}
                            />
                        </View>
                    </View>
                </KeyboardAvoidingView>
                <View style={styles.buttonContainer}>
                    <Button
                        primary
                        raised
                        text="Add"
                        style={{ container: styles.button }}
                        onPress={() => {
                            Keyboard.dismiss();
                            this.props.initDepositTransaction(this.props.deposit);
                            this.props.navigation.reset(
                                [NavigationActions.navigate({ routeName: 'Funds' })], 0);
                            this.props.navigation.navigate('StatusPage');
                        }}
                    />
                </View>
            </View>
        );
    }
}
