import { bindActionCreators, Dispatch } from 'redux';
import { connect } from 'react-redux';

import Deposit from 'DecentureMobile/src/components/funds/deposit';
import { depositInputChange, initDepositTransaction } from 'DecentureMobile/src/core/actions/payment';

const mapStateToProps = (state: any) => ({
    deposit: state.user.current.payment.current.deposit.current.info,
});

const mapDispatchToProps = (dispatch: Dispatch) => bindActionCreators({
    depositInputChange,
    initDepositTransaction,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Deposit);
