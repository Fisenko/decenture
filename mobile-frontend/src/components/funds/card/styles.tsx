import {StyleSheet, Dimensions} from 'react-native';

const window = Dimensions.get('window');

export const IMAGE_HEIGHT = window.width / 3;
export const IMAGE_HEIGHT_SMALL = window.width / 7;

export default StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        flex: 1,
        flexDirection: 'column',
        alignContent: 'stretch',
        paddingLeft: 30,
        paddingRight: 30,
    },
    addCardContainer: {
        backgroundColor: '#fff',
        flex: 1,
    },
    button: {
        backgroundColor: '#538ED4',
        borderRadius: 40,
        width: window.width - 100,
        padding: 15,
        marginTop: 30,
        height: 50
    },
    subtext: {
        color: '#538ED4',
        fontSize: 17,
        fontWeight: '100',
        flexDirection: 'row',
        alignItems: 'center',
        padding: 9,
        paddingLeft: 35
    },
    homeView: {
        flexDirection: 'row',
        alignItems: 'center',
        padding: 9,
        paddingLeft: 35
    },
    bankCardsContainer: {
        flex: 0,
        flexDirection: 'column',
        justifyContent: 'flex-start',
    },
    title: {
        padding: 15
    },
    icon: {
        padding: 12,
        fontSize: 24,
        textAlign: 'center'
    },
    card: {
        flexDirection: 'row',
        flexWrap: 'nowrap',
    },
    imgContainer: {
        padding: 5
    },
    img: {
        width: 130,
        height: 70,
        borderRadius: 5
    },
    cardDescription: {
        padding: 15,
        flex: 1,
        alignItems: 'flex-end',
    },
    //CardView
    cardContainer: {},
    cardFace: {},
    cardViewIcon: {
        top: 15,
        left: 15,
        width: 60,
        height: 40,
        resizeMode: 'contain',
    },
    baseText: {
        color: 'rgba(255, 255, 255, 0.8)',
        backgroundColor: 'transparent',
    },
    placeholder: {
        color: 'rgba(255, 255, 255, 0.5)',
    },
    focused: {
        fontWeight: 'bold',
        color: 'rgba(255, 255, 255, 1)',
    },
    number: {
        fontSize: 21,
        position: 'absolute',
        top: 95,
        left: 28,
    },
    name: {
        fontSize: 16,
        position: 'absolute',
        bottom: 20,
        left: 25,
        right: 100,
    },
    expiryLabel: {
        fontSize: 9,
        position: 'absolute',
        bottom: 40,
        left: 218,
    },
    expiry: {
        fontSize: 16,
        position: 'absolute',
        bottom: 20,
        left: 220,
    },
    amexCVC: {
        fontSize: 16,
        position: 'absolute',
        top: 73,
        right: 30,
    },
    cvc: {
        fontSize: 16,
        position: 'absolute',
        top: 80,
        right: 30,
    },
    //CreditCardInput
    creditCardInputContainer: {
        marginTop: 10,
        alignItems: 'center',
        paddingLeft: 10,
        paddingRight: 10,
        width: window.width,
    },
    form: {
        marginTop: 20,
        width: window.width,
    },
    inputContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderBottomWidth: 1,
        borderColor: '#e3e3e3',
    },
    inputLabel: {
        padding: 10,
        fontWeight: 'bold',
        position: 'absolute',
        color: 'black',
        fontSize: 12,
    },
    input: {
        paddingLeft: 10,
        paddingRight: 10,
        height: 60,
        position: 'relative',
        top: 15,
        fontSize: 16,
        color: 'black',
    },

    //CCInput
    baseInputStyle: {
        color: 'black',
        flex: 1
    },
    ccInput: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
});
