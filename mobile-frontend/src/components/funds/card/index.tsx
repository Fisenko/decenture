import CreditCardInput from './CreditCardInput';
import * as React from 'react';
import { Platform, View, Keyboard } from 'react-native';
import { Button } from 'react-native-material-ui';
import { NavigationScreenProp, NavigationState } from 'react-navigation';
import styles from './styles';

interface AddCardProps {
    card: any;
    cardInputChange: any;
    saveCard: any;
    navigation: NavigationScreenProp<NavigationState>;
    cardInfo: any;
    setUIConfig: any;
}

export default class AddCard extends React.Component<AddCardProps, any> {
    componentWillUnmount() {
        this.props.setUIConfig('creditCard', 'currInfo', {
            status: {},
            values: {},
        });
    }

    _onFocus = (field: any) => console.log('focusing', field);

    onAddButtonClick = () => {
        if (this.props.card.valid) {
            this.props.saveCard(this.props.card);
            this.props.navigation.navigate('StatusPage');
            Keyboard.dismiss();
        }
    };

    render() {
        const cardFontFamily = Platform.select({ ios: 'Courier', android: 'monospace' });

        return (
            <View style={styles.addCardContainer}>
                <View>
                    <CreditCardInput
                        card={this.props.card}
                        cardInfo={this.props.cardInfo}
                        setCardUI={this.props.setUIConfig}
                        autoFocus
                        cardFontFamily={cardFontFamily}
                        requiresName
                        cardScale={1}
                        requiresPostalCode={false}
                        requiresCVC
                        onFocus={this._onFocus}
                        onChange={this.props.cardInputChange}
                    />
                </View>
                <View style={{ alignItems: 'center' }}>
                    <Button
                        primary
                        raised
                        text={'Add'}
                        style={{ container: styles.button }}
                        onPress={this.onAddButtonClick}
                    />
                </View>
            </View>
        );
    }
}
