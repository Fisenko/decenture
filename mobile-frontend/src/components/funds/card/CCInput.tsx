import * as React from 'react';
import { Text, TextInput, TouchableOpacity, View } from 'react-native';
import { CardValidationStatus } from 'DecentureMobile/src/core/models/payment/CardValidationStatus';
import styles from './styles';

interface Props {
    field: string;
    label: string;
    value: string;
    placeholder: string;
    keyboardType: any;
    inputStyle: any;
    labelStyle: any;
    status: CardValidationStatus;

    containerStyle: any;

    onFocus: Function;
    onChange: Function;
    onBecomeEmpty: Function;
    onBecomeValid: Function;
}

export default class CCInput extends React.Component<Props, any> {


    constructor(props: Readonly<Props>) {
        super(props);
    }

    //TODO remove it
    static defaultProps = {
        label: '',
        value: '',
        status: CardValidationStatus.incomplete,
        keyboardType: 'numeric',
        containerStyle: {},

        onFocus: () => {
        },
        onChange: () => {
        },
        onBecomeEmpty: () => {
        },
        onBecomeValid: () => {
        },
    };

    componentDidUpdate(prevProps: any) {
        const { status, value, onBecomeEmpty, onBecomeValid, field } = prevProps;
        const { status: newStatus, value: newValue } = this.props;

        if (value !== '' && newValue === '') {
            onBecomeEmpty(field);
        }
        if (status !== CardValidationStatus.valid && newStatus === CardValidationStatus.valid) {
            onBecomeValid(field);
        }
    }

    //FIXME remove refs(deprecated)
    // @ts-ignore
    focus = () => this.refs.input.focus();

    _onFocus = () => this.props.onFocus(this.props.field);
    _onChange = (value: any) => this.props.onChange(this.props.field, value);


    render() {
        const {
            label, value, placeholder, status, keyboardType,
            containerStyle, inputStyle, labelStyle,
        } = this.props;

        return (
            <TouchableOpacity onPress={this.focus}
                              activeOpacity={0.99}>
                <View style={[containerStyle]}>
                    {!!label && <Text style={[labelStyle]}>{label}</Text>}
                    <TextInput ref="input"
                               keyboardType={keyboardType}
                               autoCapitalize={'words'}
                               autoCorrect={false}
                               style={[
                                   styles.baseInputStyle,
                                   inputStyle,
                                   ((status === CardValidationStatus.valid) ? { color: 'black' } :
                                       (status === CardValidationStatus.invalid) ? { color: 'red' } :
                                           {}),
                               ]}
                               underlineColorAndroid="transparent"
                               returnKeyType="done"
                               placeholderTextColor="gray"
                               placeholder={placeholder}
                               value={value}
                               onFocus={this._onFocus}
                               onChangeText={this._onChange}/>
                </View>
            </TouchableOpacity>
        );
    }
}
