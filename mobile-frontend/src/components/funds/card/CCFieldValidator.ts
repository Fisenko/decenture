import * as valid from 'card-validator';
import { every, pick, values } from 'lodash';
import { CardValidationStatus } from 'DecentureMobile/src/core/models/payment/CardValidationStatus';

const toStatus = (validation: any) => {
    if (validation.isValid) {
        return CardValidationStatus.valid;
    }

    if (validation.isPotentiallyValid) {
        return CardValidationStatus.incomplete;
    } else {
        return CardValidationStatus.invalid;
    }
};

const FALLBACK_CARD = { gaps: [4, 8, 12], lengths: [16], code: { size: 3 } };
export default class CCFieldValidator {

    _displayedFields: any;

    constructor(displayedFields: any, validatePostalCode?: any) {
        this._displayedFields = displayedFields;
    }

    validateValues = (formValues: any) => {
        const numberValidation = valid.number(formValues.number);
        const expiryValidation = valid.expirationDate(formValues.expiry);
        const maxCVCLength = (numberValidation.card || FALLBACK_CARD).code.size;
        const cvcValidation = valid.cvv(formValues.cvc, maxCVCLength);

        const validationStatuses = pick({
            number: toStatus(numberValidation),
            expiry: toStatus(expiryValidation),
            cvc: toStatus(cvcValidation),
            name: !!formValues.name ? CardValidationStatus.valid : CardValidationStatus.incomplete,
        }, this._displayedFields);

        return {
            valid: every(values(validationStatuses), (status: any) => status === CardValidationStatus.valid),
            status: validationStatuses,
        };
    };
}
