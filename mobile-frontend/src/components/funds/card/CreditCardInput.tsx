import * as React from 'react';
import { Dimensions, findNodeHandle, NativeModules, ScrollView, View } from 'react-native';

import CreditCard from './CardView';
import CCInput from './CCInput';
import style from './styles';
// @ts-ignore
import compact from 'lodash.compact';
import CCFieldFormatter from 'DecentureMobile/src/components/funds/card/CCFieldFormatter';
import CCFieldValidator from 'DecentureMobile/src/components/funds/card/CCFieldValidator';
import { isPlatformAndroid } from 'DecentureMobile/src/utils/app_utils';


const CARD_NUMBER_INPUT_WIDTH = Dimensions.get('window').width;
const CVC_INPUT_WIDTH = CARD_NUMBER_INPUT_WIDTH / 2 * 0.9;
const EXPIRY_INPUT_WIDTH = CVC_INPUT_WIDTH;
const CARD_NUMBER_INPUT_WIDTH_OFFSET = 40;
const NAME_INPUT_WIDTH = CARD_NUMBER_INPUT_WIDTH;
const PREVIOUS_FIELD_OFFSET = 40;
const POSTAL_CODE_INPUT_WIDTH = 120;

interface Props {
    cardScale: number;
    cardFontFamily: string;
    card: any;
    focused?: string;
    autoFocus: boolean;
    onFocus: Function;
    onChange: Function;
    requiresName: boolean;
    requiresCVC: boolean;
    requiresPostalCode: boolean;
    cardInfo: any;
    setCardUI: any;

}

/* eslint react/prop-types: 0 */ // https://github.com/yannickcr/eslint-plugin-react/issues/106
export default class CreditCardInput extends React.Component<Props, any> {

    //TODO remove it
    static defaultProps = {
        autoFocus: false,
        onChange: () => {
        },
        onFocus: () => {
        },
        requiresCVC: true,
    };

    constructor(props: any) {
        super(props);
    }

    componentDidMount() {
        this._focus(this.props.focused);
        setTimeout(() => { // Hacks because componentDidMount happens before component is rendered
            this.props.autoFocus && this.focus('number');
        });
    }

    componentWillReceiveProps(newProps: any) {
        if (this.props.cardInfo.focused !== newProps.cardInfo.focused) {
            this._focus(newProps.cardInfo.focused);
        }
    };

    _focus = (field: any) => {
        if (!field) {
            return;
        }

        //FIXME remove refs(deprecated)
        // @ts-ignore
        const nodeHandle = findNodeHandle(this.refs[field]);

        NativeModules.UIManager.measureLayoutRelativeToParent(nodeHandle,
            (e: any) => {
                throw e;
            },
            (x: any) => {
                // @ts-ignore
                if (isPlatformAndroid()) {
                    const scrollResponder = this.refs.Form.getScrollResponder();
                    scrollResponder.scrollTo({ x: Math.max(x - PREVIOUS_FIELD_OFFSET, 0), animated: true });
                }
                this.refs[field].focus();
            });
    };

    focus = (field = 'number') => {
        this.props.setCardUI('creditCard', 'focused', field);
    };

    _onFocus = (field: any) => {
        this.focus(field);
        this.props.onFocus(field);
    };

    _change = (field: any, value: any) => {
        this.setValues({ [field]: value });
    };

    _inputProps = (field: any) => {
        const {
            cardInfo,
        } = this.props;

        return {
            inputStyle: style.input,
            labelStyle: style.inputLabel,
            ref: field, field,

            value: cardInfo.currInfo.values[field],
            status: cardInfo.currInfo.status[field],

            onFocus: this._onFocus,
            onChange: this._change,
            onBecomeEmpty: this._focusPreviousField,
            onBecomeValid: this._focusNextField,
        };
    };

    setValues = (values: any) => {
        const { cardInfo } = this.props;
        const newValues = { ...cardInfo.currInfo.values, ...values };
        const displayedFields = this._displayedFields();
        const formattedValues = (new CCFieldFormatter(displayedFields)).formatValues(newValues);
        const validation = (new CCFieldValidator(displayedFields)).validateValues(formattedValues);
        const newState = { values: formattedValues, ...validation };

        this.props.setCardUI('creditCard', 'currInfo', newState);
        this.props.onChange(newState);
    };

    _displayedFields = () => {
        const { requiresName, requiresCVC, requiresPostalCode } = this.props;
        return compact([
            'number',
            requiresName ? 'name' : null,
            'expiry',
            requiresCVC ? 'cvc' : null,
            requiresPostalCode ? 'postalCode' : null,
        ]);
    };

    _focusPreviousField = (field: any) => {
        const displayedFields = this._displayedFields();
        const fieldIndex = displayedFields.indexOf(field);
        const previousField = displayedFields[fieldIndex - 1];
        if (previousField) {
            this.focus(previousField);
        }
    };

    _focusNextField = (field: any) => {
        if (field === 'name') {
            return;
        }
        // Should not focus to the next field after name (e.g. when requiresName & requiresPostalCode are true
        // because we can't determine if the user has completed their name or not)

        const displayedFields = this._displayedFields();
        const fieldIndex = displayedFields.indexOf(field);
        const nextField = displayedFields[fieldIndex + 1];
        if (nextField) {
            this.focus(nextField);
        }
    };

    render() {
        const {
            cardInfo: { focused },
            requiresName, requiresCVC,
            cardScale, cardFontFamily, card
        } = this.props;

        const { number, expiry, cvc, name, type } = card;

        return (
            <View style={style.creditCardInputContainer}>
                <CreditCard focused={focused}
                            brand={type}
                            scale={cardScale}
                            fontFamily={cardFontFamily}
                            number={number}
                            name={requiresName ? name : ' '}
                            expiry={expiry}
                            cvc={cvc}
                />
                <ScrollView ref="Form"
                            horizontal={false}
                            keyboardShouldPersistTaps="always"
                            scrollEnabled={false}
                            showsHorizontalScrollIndicator={false}
                            style={style.form}>
                    <CCInput {...this._inputProps('number')}
                             label="CARD NUMBER"
                             keyboardType="numeric"
                             placeholder="1234 5678 1234 5678"
                             containerStyle={[style.inputContainer, {
                                 width: CARD_NUMBER_INPUT_WIDTH,
                             }]}/>
                    <View>
                        {requiresName &&
                        <CCInput {...this._inputProps('name')}
                                 keyboardType="default"
                                 label="CARDHOLDER'S NAME"
                                 placeholder="FULL NAME"
                                 containerStyle={[style.inputContainer, {
                                     width: CARD_NUMBER_INPUT_WIDTH,
                                 }]}/>}
                    </View>
                    <View
                        style={{
                            flex: 1,
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                        }}>
                        <CCInput {...this._inputProps('expiry')}
                                 keyboardType="numeric"
                                 label="EXPIRY"
                                 placeholder="MM/YY"
                                 containerStyle={[style.inputContainer, {
                                     width: EXPIRY_INPUT_WIDTH,
                                 }]}/>
                        {requiresCVC &&
                        <CCInput {...this._inputProps('cvc')}
                                 keyboardType="numeric"
                                 label="CVC/CVV"
                                 placeholder="CVC"
                                 containerStyle={[style.inputContainer, {
                                     width: CVC_INPUT_WIDTH
                                 }]}/>}
                    </View>
                </ScrollView>
            </View>
        );
    }
}
