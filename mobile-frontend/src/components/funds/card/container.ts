import { bindActionCreators, Dispatch } from 'redux';
import { connect } from 'react-redux';
import AddCard from './index';
import { setUIConfig } from 'DecentureMobile/src/core/actions/ui';
import { cardInputChange, saveCard } from 'DecentureMobile/src/core/actions/payment';

const mapStateToProps = (state: any) => ({
    card: state.user.current.payment.current.card.current.info,
    cardInfo: state.ui.configs.creditCard,
});

const mapDispatchToProps = (dispatch: Dispatch) => bindActionCreators({
    cardInputChange,
    saveCard,
    setUIConfig,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(AddCard);
