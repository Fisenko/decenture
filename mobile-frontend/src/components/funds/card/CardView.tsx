import * as React from 'react';
import { Image, ImageBackground, Text, View } from 'react-native';
// @ts-ignore
import FlipCard from 'react-native-flip-card';
import style from './styles';
import { getImage } from 'DecentureMobile/src/utils/image_holder';

const BASE_SIZE = { width: 300, height: 190 };
const DEFAULT_CARD_NUMBER = '•••• •••• •••• ••••';
const DEFAULT_CARD_EXPIRY = '••/••';
const DEFAULT_CARD_NAME = 'FULL NAME';

interface Props {
    focused: string;
    brand: string;
    name: string;
    number: string;
    expiry: string;
    cvc: string;
    scale: number;
    fontFamily: string;
}

enum ImageSide {
    front = 'imageFront',
    back = 'imageBack',
}

const blurOut = (e: any) => {
    let value = '';
    let length = e.toString().length;
    while (length--) {
        value += '•';
    }
    return value;
};

/* eslint react/prop-types: 0 */ // https://github.com/yannickcr/eslint-plugin-react/issues/106
export default class CardView extends React.Component <Props, any> {

    render() {

        const {
            focused, brand, name = '', number, expiry, cvc, scale, fontFamily,
        } = this.props;
        const isAmex = brand === 'american-express';

        const shouldFlip = !isAmex && focused === 'cvc';
        const containerSize = { ...BASE_SIZE, height: BASE_SIZE.height * scale };

        const transform = {
            transform: [
                { scale },
                { translateY: ((BASE_SIZE.height * (scale - 1) / 2)) },
            ],
        };

        return (
            <View style={[style.cardContainer, containerSize]}>
                <FlipCard style={{ borderWidth: 0 }}
                          flipHorizontal
                          flipVertical={false}
                          friction={10}
                          perspective={2000}
                          clickable={false}
                          flip={shouldFlip}>
                    <ImageBackground style={[BASE_SIZE, style.cardFace, transform]}
                                     source={getImage(ImageSide.front)}>
                        <Image style={[style.cardViewIcon]}
                               source={getImage(brand)}/>
                        <Text style={[style.baseText, { fontFamily }, style.number, !number && style.placeholder,
                            focused === 'number' && style.focused]}>
                            {!number ? DEFAULT_CARD_NUMBER : number}
                        </Text>
                        <Text style={[style.baseText, { fontFamily }, style.name, !name && style.placeholder,
                            focused === 'name' && style.focused]}
                              numberOfLines={1}>
                            {!name ? DEFAULT_CARD_NAME : name}
                        </Text>
                        <Text style={[style.baseText, { fontFamily }, style.expiryLabel, style.placeholder,
                            focused === 'expiry' && style.focused]}>
                            MONTH/YEAR
                        </Text>
                        <Text style={[style.baseText, { fontFamily }, style.expiry, !expiry && style.placeholder,
                            focused === 'expiry' && style.focused]}>
                            {!expiry ? DEFAULT_CARD_EXPIRY : expiry}
                        </Text>
                        {isAmex &&
                        <Text style={[style.baseText, { fontFamily }, style.amexCVC, !cvc && style.placeholder,
                            focused === 'cvc' && style.focused]}>
                            {blurOut(cvc)}
                        </Text>}
                    </ImageBackground>
                    <ImageBackground style={[BASE_SIZE, style.cardFace, transform]}
                                     source={getImage(ImageSide.back)}>
                        <Text style={[style.baseText, style.cvc, !cvc && style.placeholder,
                            focused === 'cvc' && style.focused]}>
                            {blurOut(cvc)}
                        </Text>
                    </ImageBackground>
                </FlipCard>
            </View>
        );
    }
}
