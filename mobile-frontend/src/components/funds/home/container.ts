import { bindActionCreators, Dispatch } from 'redux';
import { connect } from 'react-redux';

import FundsHome from 'DecentureMobile/src/components/funds/home';
import { depositInputChange, getAllCardsNumberByToken } from 'DecentureMobile/src/core/actions/payment';

const mapStateToProps = (state: any) => ({
    cards: state.user.current.payment.current.card.list,
});

const mapDispatchToProps = (dispatch: Dispatch) =>
    bindActionCreators(
        {
            getAllCardsNumberByToken,
            depositInputChange,
        },
        dispatch,
    );

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(FundsHome);
