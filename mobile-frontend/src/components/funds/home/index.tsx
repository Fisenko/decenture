import * as React from 'react';
import { Image, Text, TouchableOpacity, View, ScrollView, ImageBackground } from 'react-native';
import { NavigationScreenProp, NavigationState } from 'react-navigation';

import styles from 'DecentureMobile/src/components/funds/home/styles';
import { getImage } from 'DecentureMobile/src/utils/image_holder';

const POINTS: string = '•••• •••• ••••';

interface CardProps {
    card: {
        id: string;
        number: any;
        name: string;
    };
    deposit?: {
        cardId: string;
    };
    navigation: NavigationScreenProp<NavigationState>;
    depositInputChange: any;
}

class Card extends React.Component<CardProps, any> {
    render() {
        return (
            <TouchableOpacity
                style={styles.card}
                onPress={() => {
                    this.props.depositInputChange({
                        name: 'cardId',
                        value: this.props.card.id,
                    });
                    this.props.navigation.navigate('Deposit');
                }}
            >
                <View style={styles.imgContainer}>
                    <ImageBackground source={getImage('imageFront')} style={styles.img}/>
                </View>
                <View style={styles.cardDescription}>
                    <Text>{`${this.props.card.name}`}</Text>
                    <Text>{`${POINTS} ${this.props.card.number}`}</Text>
                </View>
            </TouchableOpacity>
        );
    }
}

interface FoundsHomeProps {
    cards: Array<CardProps>;
    getAllCardsNumberByToken: any;
    navigation: NavigationScreenProp<NavigationState>;
    depositInputChange: any;
}

export default class FundsHome extends React.Component<FoundsHomeProps, any> {
    componentDidMount() {
        this.props.getAllCardsNumberByToken();
    }

    render() {
        return (
            <ScrollView ref="Form"
                        horizontal={false}
                        showsVerticalScrollIndicator={false}
                        style={styles.container}
            >
                <View style={styles.bankCardsContainer}>
                    <Text style={styles.title}>Bank cards</Text>
                    {this.props.cards.map((card: any, index: any) => (
                        <Card
                            card={card}
                            key={index}
                            navigation={this.props.navigation}
                            depositInputChange={this.props.depositInputChange}
                        />
                    ))}
                    <TouchableOpacity style={styles.homeView}
                                      onPress={() => this.props.navigation.navigate('AddCard')}>
                            <Image source={getImage('add')} style={styles.iconAdd}/>
                            <Text style={styles.textButton}>Add New Card</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.border}/>
                <View style={styles.bankCardsContainer}>
                    <Text style={styles.title}>Other</Text>
                    <TouchableOpacity style={styles.homeView} onPress={() => console.log('pressed')}>
                            <Image source={getImage('paypal')} style={styles.iconPayPay} />
                            <Text style={styles.textButton}>Pay Pal</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.homeView} onPress={() => console.log('pressed')}>
                            <Image source={getImage('bank')} style={styles.iconBank} />
                            <Text style={styles.textButton}>Bank Account</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.homeView} onPress={() => console.log('pressed')}>
                            <Image source={getImage('apple_pay')} style={styles.iconApplePay} />
                            <Text style={styles.textButton}>Apple Pay</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        );
    }
}
