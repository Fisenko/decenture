import { StyleSheet, Dimensions } from 'react-native';

const window = Dimensions.get('window');


export const WIDTH = window.width;
export const IMAGE_HEIGHT = window.width / 3;
export const IMAGE_HEIGHT_SMALL = window.width / 7;

export default StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        flex: 1,
        flexDirection: 'column',
        alignContent: 'stretch',
        paddingLeft: 30,
        paddingRight: 30,
    },
    homeView: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        paddingTop: 25,
        paddingBottom: 25,
    },
    bankCardsContainer: {
        flex: 1,
        alignContent: 'stretch',
        justifyContent: 'space-around',
    },
    title: {
        fontSize: 18,
        paddingTop: 30,
        paddingBottom: 10,
    },
    textButton: {
        fontSize: 18,
    },
    iconAdd: {
        marginLeft: 16,
        resizeMode: 'contain',
        width: 19,
        height: 19,
        marginRight: 34,
    },
    iconApplePay: {
        resizeMode: 'contain',
        width: 48,
        height: 20,
        marginRight: 22,
    },
    iconPayPay: {
        resizeMode: 'contain',
        width: 50,
        height: 15,
        marginRight: 18,
    },
    iconBank: {
        resizeMode: 'contain',
        width: 22,
        height: 25,
        marginRight: 32,
        marginLeft: 15,
    },
    card: {
        flexDirection: 'row',
        flexWrap: 'nowrap',
        paddingLeft: 9,
    },
    imgContainer: {
        padding: 5,
    },
    img: {
        width: 130,
        height: 70,
        borderRadius: 5,
    },
    cardDescription: {
        padding: 15,
        flex: 1,
        alignItems: 'flex-end',
    },
    cardHolder: {
        color: '#fff',
        textAlign: 'center',
    },
    border: {
        padding: 5,
        borderColor: '#00000014',
        borderBottomWidth: 1,
    },
});
