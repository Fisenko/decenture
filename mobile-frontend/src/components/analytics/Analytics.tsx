import * as React from 'react';
import { Image, Text, TouchableOpacity, View, ScrollView } from 'react-native';

import styles from './styles';
import { getImage } from 'DecentureMobile/src/utils/image_holder';

export default class AnalyticsHome extends React.Component<any, any> {

    render() {
        return (
            <ScrollView style={styles.main}>
                <View style={styles.container}>
                    <View style={styles.container}>
                        <View style={styles.topRow}>
                            <View style={styles.topLeft}>
                                <TouchableOpacity
                                    onPress={() => this.props.navigation.navigate('Attendance')}>
                                    <View style={styles.button}>
                                        <Image
                                            source={getImage('attendance')}
                                            style={styles.logo}
                                        />
                                        <Text style={styles.text}>
                                            Attendance
                                        </Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                            <View style={styles.topRight}>
                                <TouchableOpacity
                                    onPress={() => this.props.navigation.navigate('Spending')}>
                                    <View style={styles.button}>
                                        <Image
                                            source={getImage('payment')}
                                            style={styles.logo}
                                        />
                                        <Text style={styles.text}>
                                            Spending
                                        </Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={styles.topRow}>
                            <View style={styles.topLeft}>
                                <TouchableOpacity disabled>
                                    <View style={styles.button}>
                                        <Image
                                            source={getImage('certificateInactive')}
                                            style={styles.logo}
                                        />
                                        <Text style={styles.textInactive}>
                                            Certification
                                        </Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                            <View style={styles.topRight}>
                                <TouchableOpacity disabled>
                                    <View style={styles.button}>
                                        <Image
                                            source={getImage('scheduleInactive')}
                                            style={styles.logo}
                                        />
                                        <Text style={styles.textInactive}>
                                            My Schedule
                                        </Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={styles.bottomRow}>
                            <View style={styles.topLeft}>
                                <TouchableOpacity disabled>
                                    <View style={styles.button}>
                                        <Image
                                            source={getImage('transcriptsInactive')}
                                            style={styles.logo}
                                        />
                                        <Text style={styles.textInactive}>
                                            Transcripts
                                        </Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                            <View style={styles.topRight}>
                                <TouchableOpacity disabled>
                                    <View style={styles.button}>
                                        <Image
                                            source={getImage('messageInactive')}
                                            style={styles.logo}
                                        />
                                        <Text style={styles.textInactive}>
                                            Messages
                                        </Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={styles.bottomRow}>
                            <View style={styles.bottomLeft}>
                                <TouchableOpacity disabled>
                                    <View style={styles.button}>
                                        <Image
                                            source={getImage('insuranceInactive')}
                                            style={styles.logo}
                                        />
                                        <Text style={styles.textInactive}>
                                            Insurance
                                        </Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                            <View style={styles.bottomRight}>
                                <TouchableOpacity disabled>
                                    <View style={styles.button}>
                                        <Image
                                            source={getImage('offersInactive')}
                                            style={styles.logo}
                                        />
                                        <Text style={styles.textInactive}>
                                            Offers
                                        </Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </View>
            </ScrollView>
        );
    }
}
