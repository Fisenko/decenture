import { createStackNavigator } from 'react-navigation';
import AnalyticsHome from './index';
import Attendance from '../attendance';
import { headerNavigationOptions } from 'DecentureMobile/src/utils/router_utils';
import Spending from 'DecentureMobile/src/components/spending';

export const analyticsRouterConfig = {
    Analytics: {
        screen: AnalyticsHome,
        navigationOptions: {
            title: 'More',
        },
    },
    Attendance: {
        screen: Attendance,
        navigationOptions: {
            title: 'Attendance',
        },
    },
    Spending: {
        screen: Spending,
        navigationOptions: {
            title: 'Spending',
        },
    },
};

export default createStackNavigator(
    analyticsRouterConfig,
    {
        headerMode: 'screen',
        initialRouteName: 'Analytics',
        navigationOptions: headerNavigationOptions,
    },
);
