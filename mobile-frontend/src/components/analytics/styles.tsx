import { StyleSheet } from 'react-native';
import { BOTTOM_BAR_ICON_HEIGHT, BOTTOM_BAR_ICON_WIDTH } from '../../common/styles';
import { window } from 'DecentureMobile/src/components/common/styles';

export default StyleSheet.create({
    main: {
        backgroundColor: '#FFF'
    },
    container: {
        backgroundColor: '#FFF',
        flex: 1,
        alignItems: 'center',
        padding: 10,
    },
    button: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    logo: {
        resizeMode: 'contain',
        width: 40,
        height: 40,
        margin: 7,
    },
    title: {
        textAlign: 'center',
        padding: 5,
        marginTop: 20,
        paddingBottom: 30,
        fontSize: 20,
        color: 'black',
        maxWidth: window.height / 3,
    },
    text: {
        fontSize: 15,
        color: '#000000',
    },
    textInactive: {
        fontSize: 15,
        color: '#B1B1B1',
    },
    topRow: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'flex-end',
        justifyContent: 'center',
    },
    topLeft: {
        flex: 1,
        borderColor: '#00000014',
        borderRightWidth: 1,
        borderBottomWidth: 1,
        paddingBottom: 20,
        paddingTop: 20,
    },
    topRight: {
        flex: 1,
        borderColor: '#00000014',
        borderBottomWidth: 1,
        paddingBottom: 20,
        paddingTop: 20,
    },
    bottomRow: {
        flexDirection: 'row',
        flex: 1,
        alignItems: 'flex-end',
        justifyContent: 'center',
    },
    bottomLeft: {
        flex: 1,
        borderColor: '#00000014',
        borderRightWidth: 1,
        paddingBottom: 20,
        paddingTop: 20,
    },
    bottomRight: {
        flex: 1,
        borderColor: '#00000014',
        paddingBottom: 20,
        paddingTop: 20,
    },
});
