import React from 'react';

import NfcManager, { TagEvent } from 'react-native-nfc-manager';
import { TabProps } from 'DecentureMobile/src/components/common/tab_bar/bar_component/BottomTabComponent';
import BottomTabComponent from 'DecentureMobile/src/components/common/tab_bar/bar_component';

import { extractTextFromTag, startDetection, startNfc, stopDetection } from 'DecentureMobile/src/utils/nfc_utils';
import { isPlatformAndroid } from 'DecentureMobile/src/utils/app_utils';

interface RecordTabProps extends TabProps {

    setAttendanceNfcScanStatus: any;
    attendanceRecordInputChange: any;
    setFingerprintModalVisible: any;
    setNfcSupport: any;
    setErrorConfig: any;

    isNfcScanEnabled: string;
}

const NFC_SCANNER_TTL = 10000;

class RecordTab extends React.Component<RecordTabProps> {

    componentDidMount() {
        NfcManager.isSupported()
        .then(supported => {
            if (supported) {
                startNfc();
            } else {
                this.props.setErrorConfig('record', 'nfc', 'NFC not supported');
                this.props.setNfcSupport(false);
            }
        })
        .catch((error: any) => this.props.setErrorConfig('record', 'nfc', error.name));
    }

    onTabClick = () => {
        this.startNFCListening();
        this.props.setActiveSceneName(this.props.navigateTo);
        this.props.navigation.navigate(this.props.navigateTo);
    };

    startNFCListening = () => {
        isPlatformAndroid()
            ? this.startAndroidNfcRecording()
            : startDetection(this.onTagDiscovered);
    };

    startAndroidNfcRecording = () => {
        if (!this.props.isNfcScanEnabled) {
            this.props.setAttendanceNfcScanStatus(true);
            startDetection(this.onTagDiscovered);
            setTimeout(() => {
                stopDetection();
                this.props.setAttendanceNfcScanStatus(false);
            }, NFC_SCANNER_TTL);
        }
    };

    onTagDiscovered = (tag: TagEvent) => {
        const text = extractTextFromTag(tag);
        this.props.attendanceRecordInputChange({ name: 'nfcId', value: text });
        this.props.setFingerprintModalVisible(true);
        stopDetection();
    };

    render() {
        return (
            <BottomTabComponent
                imageName={this.props.imageName}
                title={this.props.title}
                navigateTo={this.props.navigateTo}
                navigation={this.props.navigation}
                onPress={this.onTabClick}
            />
        );
    }
}

export default RecordTab;
