import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import RecordTab from './RecordTab';
import { setActiveSceneName } from 'DecentureMobile/src/core/actions/ui/tab_bar';
import { setErrorConfig } from 'DecentureMobile/src/core/actions/error';
import { setFingerprintModalVisible, setNfcSupport } from 'DecentureMobile/src/core/actions/ui/nfc';
import { attendanceRecordInputChange, setAttendanceNfcScanStatus } from 'DecentureMobile/src/core/actions/attendance';


const mapStateToProps = (state: any) => ({
    activeSceneName: state.ui.configs.tabBar.activeSceneName,
    isNfcScanEnabled: state.user.current.student.attendance.current.info.isNfcScanEnabled,
});

const mapDispatchToProps = (dispatch: Dispatch) =>
    bindActionCreators(
        {
            attendanceRecordInputChange,
            setFingerprintModalVisible,
            setAttendanceNfcScanStatus,
            setActiveSceneName,
            setErrorConfig,
            setNfcSupport,
        },
        dispatch,
    );

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(RecordTab);
