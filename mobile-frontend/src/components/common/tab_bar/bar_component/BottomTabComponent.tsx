import React from 'react';
import { Text, TouchableWithoutFeedback, View } from 'react-native';
// @ts-ignore
import { BottomTabBar } from 'react-navigation-tabs';
import Icon from 'DecentureMobile/src/utils/icon_utils';
import { NavigationScreenProp, NavigationState } from 'react-navigation';

import styles from './styles';
import { TAB_BAR_PRESS_IN, TAB_BAR_PRESS_OUT } from 'DecentureMobile/src/common/styles';

export interface TabProps {
    title: string;
    imageName: string;

    navigateTo: string;
    navigation: NavigationScreenProp<NavigationState>;

    onPress?: any;
    activeSceneName: string
    setActiveSceneName: any;
}

class BottomTabComponent extends React.Component<TabProps> {

    onTabClick = () => {
        this.props.setActiveSceneName(this.props.navigateTo);
        this.props.navigation.navigate(this.props.navigateTo);
    };

    render() {

        const color = this.props.activeSceneName === this.props.navigateTo
            ? TAB_BAR_PRESS_IN
            : TAB_BAR_PRESS_OUT;

        return (
            <View style={styles.container}>
                <TouchableWithoutFeedback onPress={this.props.onPress ? this.props.onPress : this.onTabClick}>
                    <View style={styles.iconContainer}>
                        <Icon size={24} color={color} name={this.props.imageName}/>
                        <Text style={[styles.text, { color: color }]}>
                            {this.props.title}
                        </Text>
                    </View>
                </TouchableWithoutFeedback>
            </View>
        );
    }
}

export default BottomTabComponent;
