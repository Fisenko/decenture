import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    iconContainer: {
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        minWidth: 65,
        marginTop: 5,
    },
    text: {
        textAlign: 'center',
        backgroundColor: 'transparent',
        fontSize: 11,
        marginBottom: 1.5,
        color: 'red',
    },
});
