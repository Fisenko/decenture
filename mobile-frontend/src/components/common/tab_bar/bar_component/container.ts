import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import BottomTabComponent from './BottomTabComponent';
import { setActiveSceneName } from 'DecentureMobile/src/core/actions/ui/tab_bar';


const mapStateToProps = (state: any) => ({
    activeSceneName: state.ui.configs.tabBar.activeSceneName,
});

const mapDispatchToProps = (dispatch: Dispatch) =>
    bindActionCreators(
        {
            setActiveSceneName,
        },
        dispatch,
    );

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(BottomTabComponent);
