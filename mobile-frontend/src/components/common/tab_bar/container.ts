import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';

import TabBar from './index';
import {
    setVisible,
    setInvisible,
} from 'DecentureMobile/src/core/actions/ui/tab_bar';

const mapStateToProps = (state: any) => ({
    isVisible: state.ui.configs.tabBar.isVisible,
});

const mapDispatchToProps = (dispatch: Dispatch) =>
    bindActionCreators(
        {
            setVisible,
            setInvisible,
        },
        dispatch,
    );

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(TabBar);
