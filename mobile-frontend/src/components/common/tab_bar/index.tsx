import React from 'react';
import { Platform, Keyboard } from 'react-native';
// @ts-ignore
import { BottomTabBar } from 'react-navigation-tabs';

interface TabBarProps {
    isVisible: boolean;

    setVisible: any;
    setInvisible: any;
}

export default class TabBar extends React.Component<TabBarProps> {
    keyboardEventListeners: Array<any> = [];

    componentDidMount() {
        if (Platform.OS === 'android') {
            this.keyboardEventListeners = [
                Keyboard.addListener('keyboardDidShow', () =>
                    this.props.setInvisible(),
                ),
                Keyboard.addListener('keyboardDidHide', () =>
                    this.props.setVisible(),
                ),
            ];
        }
    }

    componentWillUnmount() {
        this.keyboardEventListeners &&
            this.keyboardEventListeners.forEach(eventListener =>
                eventListener.remove(),
            );
    }

    render() {
        if (!this.props.isVisible) {
            return null;
        }
        return <BottomTabBar {...this.props} />;
    }
}
