import * as React from 'react';
import { View, ViewStyle } from 'react-native';

import styles from './styles';

interface ContainerProps {
    children: React.ReactNode;
    style? : ViewStyle;
}

const ViewContainer: React.SFC<ContainerProps> = ({children, style = {}} : ContainerProps) => {
    return <View style={[styles.container, style]}>{children}</View>;
};

export default ViewContainer;
