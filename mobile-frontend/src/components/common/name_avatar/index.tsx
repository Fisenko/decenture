import * as React from 'react';
import { View, Text } from 'react-native';

import styles from './styles';
import { stringToHEXColor } from 'DecentureMobile/src/utils/color_utils';

interface AvatarProps {
    name: string;
}

const NameAvatar: React.SFC<AvatarProps> = ({ name = 'TX' }) => {
    const names = name.replace(' and ', ' ').split(' ', 2);
    const label = names.length === 2 ? `${names[0][0]}${names[1][0]}` : name.substring(0, 2);
    const backgroundColor = `${stringToHEXColor(name)}80`;

    return (
        <View style={[styles.container, { backgroundColor }]}>
            <Text style={styles.text}>{label.toUpperCase()}</Text>
        </View>
    );
};

export default NameAvatar;
