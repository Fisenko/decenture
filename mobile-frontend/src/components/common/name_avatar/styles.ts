import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        backgroundColor: '#4E8BD0',
        justifyContent: 'center',
        alignItems: 'center',
        width: 40,
        height: 40,
        borderRadius: 20,
    },
    text: {
        textTransform: 'uppercase',
        fontSize: 16,
        color: '#fff',
    },
});
