import * as React from 'react';
import { View, ViewStyle } from 'react-native';
import styles from './styles';

interface FrameProps {
    children: React.ReactNode;
    style?: ViewStyle;
}

const ComponentFrame = ({ children, style = {} }: FrameProps) => {
    return (
        <View style={styles.container}>
            <View style={styles.border}/>
            <View style={styles.horizontal}/>
            <View style={styles.vertical}/>
            <View style={styles.childContainer}>
                {children}
            </View>
        </View>
    );
};

export default ComponentFrame;
