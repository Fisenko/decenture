import { StyleSheet } from 'react-native';
import { DECENTURE_COLOR } from 'DecentureMobile/src/common/styles';

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center'
    },
    border: {
        height: 300,
        width: 300,
        borderColor: DECENTURE_COLOR,
        borderWidth: 10,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
    },
    horizontal: {
        backgroundColor: 'white',
        position: 'absolute',
        justifyContent: 'center',
        alignItems: 'center',
        height: 150,
        width: 310,
    },
    vertical: {
        backgroundColor: 'white',
        position: 'absolute',
        justifyContent: 'center',
        alignItems: 'center',
        height: 310,
        width: 150,
    },
    childContainer: {
        backgroundColor: 'white',
        position: 'absolute',
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
    },
});
