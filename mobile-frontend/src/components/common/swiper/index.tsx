import { STATUS_BAR_COLOR } from 'DecentureMobile/src/common/styles';
import * as React from 'react';
import RNSwiper from 'react-native-swiper';

import styles from './styles';

interface SwiperProps {
    children: React.ReactNode;
    loop?: boolean;
    dotColor?: string;
    activeDotColor?: string;
}

const Swiper: React.SFC<SwiperProps> = props => {
    const { children, ...otherProps } = props;

    return (
        <RNSwiper style={styles.container} paginationStyle={styles.pagination} {...otherProps}>
            {children}
        </RNSwiper>
    );
};

Swiper.defaultProps = {
    loop: false,
    dotColor: '#B1B1B1',
    activeDotColor: STATUS_BAR_COLOR,
};

export default Swiper;
