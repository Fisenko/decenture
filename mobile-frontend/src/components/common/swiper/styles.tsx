import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        backgroundColor: '#fff',
    },
    pagination: {
        top: 20,
        bottom: 'auto',
    },
});
