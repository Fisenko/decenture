import { Dimensions, StyleSheet } from 'react-native';

export const window = Dimensions.get('window');

export const IMAGE_HEIGHT = window.width / 3.5;

export const commonStyles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: '#FFF',
    },
    input: {
        height: 40,
        backgroundColor: 'transparent',
        marginHorizontal: 10,
        marginVertical: 5,
        width: window.width - 100,
        padding: 5,
        fontSize: 17,
        paddingBottom: 12,
    },
    logoSmall: {
        height: IMAGE_HEIGHT,
        width: window.width,
        resizeMode: 'contain',
        marginBottom: 40,
    },
    text: {
        fontSize: 17,
        textAlign: 'center',
        marginBottom: 20,
    },
    button: {
        backgroundColor: '#538ED4',
        borderRadius: 40,
        width: window.width - 120,
        height: 50,
        bottom: '9%',
    },
    keyboardAvoid: {
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 20,
    },
    errorText: {
        marginVertical: 20,
        color: 'red',
    },
    containerIOS: {
        flex: 1,
        backgroundColor: '#FFF',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: 50,
    },
    inputIOS: {
        height: 40,
        backgroundColor: 'transparent',
        marginHorizontal: 10,
        marginVertical: 5,
        width: window.width - 100,
        fontSize: 17,
        borderBottomWidth: 1,
        borderBottomColor: '#538ED4',
    },
    stretchWidth: {
        width: '100%',
    },
    textInputView: {
        alignItems: 'center',
        justifyContent: 'space-around',
    },
    buttonIOS: {
        alignSelf: 'center',
        width: window.width - 100,
    },
});
