import * as React from 'react';
import { RefreshControl as RC, RefreshControlProps } from 'react-native';

import { DECENTURE_COLOR } from 'DecentureMobile/src/common/styles';

const RefreshControl: React.SFC<RefreshControlProps> = props => {
    return <RC progressBackgroundColor="white" colors={[DECENTURE_COLOR]} {...props} />;
};

export default RefreshControl;
