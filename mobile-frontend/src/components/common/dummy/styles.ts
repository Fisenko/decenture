import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        marginTop: 5,
        flex: 1,
        justifyContent: 'flex-start',
    },
    text: {
        textAlign: 'center',
        fontSize: 16,
        color: 'rgb(0, 0, 0)',
    },
});
