import * as React from 'react';
import { Text, View } from 'react-native';
import styles from './styles';

const DummyView: React.SFC = props => {
    return (
        <View style={styles.container}>
            <Text style={styles.text}>No data.</Text>
        </View>
    );
};

export default DummyView;
