import { StyleSheet } from 'react-native';
import {isPlatformAndroid} from "DecentureMobile/src/utils/app_utils";
import { DECENTURE_COLOR } from 'DecentureMobile/src/common/styles';

export default StyleSheet.create({
    container: {
        flexDirection: 'column',
        backgroundColor: DECENTURE_COLOR,
    },
    icon: {
        width: 30,
        height: 40,
        resizeMode: 'contain',
        marginLeft: 15,
        marginRight: 15,
        textAlign: 'center',
        textAlignVertical: 'center',
    },
    navContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginLeft: 15,
    },
    nav: {
        borderBottomWidth: 0.5,
        borderBottomColor: 'gray'
    },
    arrowText: {
        padding: 10,
        fontSize: 18,
        textAlign: 'center',
        color: '#538ED4',
    },
    header: {
        flexDirection: 'row',
        alignItems: 'center',
    }
});
