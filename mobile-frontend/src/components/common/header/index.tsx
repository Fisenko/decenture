import * as React from 'react';
import { TouchableOpacity, View } from 'react-native';

import styles from './styles';

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

const Header = (props: any) => {

    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <TouchableOpacity>
                    <MaterialIcons style={styles.icon} color="white" name="notifications" size={25}/>
                </TouchableOpacity>
                <TouchableOpacity>
                    <MaterialIcons style={styles.icon} color="white" name="settings" size={25}/>
                </TouchableOpacity>
            </View>
        </View>
    );
};

export default Header;
