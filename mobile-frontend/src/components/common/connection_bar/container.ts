import { connect } from 'react-redux';

import ConnectionBar from './index';

const mapStateToProps = (state: any) => ({
    status: state.ui.configs.socket.status,
});

export default connect(mapStateToProps)(ConnectionBar);
