import * as React from 'react';
import { Text, View, StyleSheet } from 'react-native';

import { SocketState } from 'DecentureMobile/src/core/models/ui';
import { isPlatformAndroid } from 'DecentureMobile/src/utils/app_utils';

interface ConnectionBarProps {
    status: {
        state: SocketState;
        attemptNumber: number;
    };
}

const ConnectionBar: React.SFC<ConnectionBarProps> = props => {
    // TODO: Add other statuses
    // if (props.status.state === SocketState.Connected) {
    //     return (
    //         <View style={[styles.container, styles.successContainer]}>
    //             <Text style={styles.text}>Connected</Text>
    //         </View>
    //     );
    // } else if (props.status.state === SocketState.Disconnected) {
    //     return (
    //         <View style={[styles.container, styles.errorContainer]}>
    //             <Text style={styles.text}>Disconnected</Text>
    //         </View>
    //     );
    // } else if (props.status.state === SocketState.Reconnecting) {
    //     return (
    //         <View style={[styles.container, styles.errorContainer]}>
    //             <Text style={styles.text}>Reconnecting... ({props.status.attemptNumber})</Text>
    //         </View>
    //     );
    // } else if (props.status.state === SocketState.Reconnected) {
    //     return (
    //         <View style={[styles.container, styles.errorContainer]}>
    //             <Text style={styles.text}>Reconnected</Text>
    //         </View>
    //     );
    // }

    if (props.status.state !== SocketState.Reconnecting) {
        return null;
    }

    return (
        <View style={[styles.container, styles.errorContainer]}>
            <Text style={styles.text}>Reconnecting... ({props.status.attemptNumber})</Text>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        marginTop: isPlatformAndroid() ? 0 : 20,
        padding: 2,
    },
    errorContainer: {
        backgroundColor: '#D03801',
    },
    text: {
        color: '#fff',
    },
    successContainer: {
        backgroundColor: '#3a0',
    },
});

export default ConnectionBar;
