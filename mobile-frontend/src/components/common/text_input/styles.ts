import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    input: {
        height: 40,
        backgroundColor: 'transparent',
        marginHorizontal: 10,
        marginVertical: 5,
        padding: 5,
        fontSize: 17,
        paddingBottom: 12,
    },
});
