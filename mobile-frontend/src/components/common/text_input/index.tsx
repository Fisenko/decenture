import * as React from 'react';
import { TextInput, TextInputProps } from 'react-native';

import styles from './styles';

export default (props: TextInputProps) => {
    const { style = {}, ...other } = props;

    return <TextInput
        style={[styles.input, style]}
        underlineColorAndroid="#538ED4"
        selectionColor="#538ED4"
        {...other}
    />;
};
