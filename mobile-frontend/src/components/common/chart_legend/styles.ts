import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        flexWrap: 'wrap',
        alignItems: 'flex-start',
        justifyContent: 'center',
    },
    view: {
        marginHorizontal: 5,
        marginVertical: 10,
        height: 20,
    },
    text: {
        textAlign: 'left',
        flexWrap: 'wrap',
        paddingRight: 20,
        fontSize: 12,
        maxWidth: 120,
    },
    colorBar: {
        borderRadius: 2,
        width: 20,
        height: 10,
        marginRight: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    legendItem: {
        marginVertical: 5,
        flexDirection: 'row',
        alignItems: 'center',
        minHeight: 25
    },
});
