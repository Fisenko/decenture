import React from 'react';
import { Text, View, ViewProps } from 'react-native';
import styles from './styles';
import { FormattedNumber } from 'react-intl';

export type ChartLegendItem = {
    color: string;
    name: string;
    value?: number;
}

interface ChartLegendProps extends ViewProps {
    data: Array<ChartLegendItem>;
}

const renderLegendItem = (item: ChartLegendItem, key: any) => {
    const { value = 0 } = item;

    const renderAmount = (formattedNumber: string) => {
        return <Text style={styles.text}>{item.name} ${formattedNumber}</Text>;
    };

    return (
        <View key={key} style={styles.legendItem}>
            <View style={[styles.colorBar, { backgroundColor: item.color }]}/>
            <FormattedNumber value={value} minimumFractionDigits={2} maximumFractionDigits={2}>
                {renderAmount}
            </FormattedNumber>
        </View>
    );
};


const ChartLegend: React.SFC<ChartLegendProps> = props => {

    const { style = {} } = props;

    return (
        <View style={[style, { paddingLeft: 10 }]}>
            <View style={styles.container}>
                {props.data.map((value, index) => renderLegendItem(value, index))}
            </View>
        </View>
    );

};

export default ChartLegend;
