import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        backgroundColor: '#538ED4',
        borderRadius: 40,
        paddingVertical: 15,
        paddingHorizontal: 30,
        height: 50,
        paddingBottom: 20,
    },
    text: {},
});
