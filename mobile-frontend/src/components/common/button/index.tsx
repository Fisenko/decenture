import * as React from 'react';
import * as Material from 'react-native-material-ui';
import { ButtonProps } from 'react-native-material-ui';

import styles from './styles';

const Button = (props: ButtonProps) => {
    const { style = { container: {}, text: {} }, ...other } = props;
    const mergedStyle = {
        container: [styles.container, style.container],
        text: [styles.text, style.text],
    };

    // @ts-ignore
    return <Material.Button style={mergedStyle} {...other} />;
};

export default Button;
