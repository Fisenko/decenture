import React from 'react';
import TransactionModel from 'DecentureMobile/src/core/models/transaction';
import { ScrollView, Text, View } from 'react-native';
import ViewContainer from 'DecentureMobile/src/components/common/view_container';
import { FormattedNumber } from 'react-intl';
import styles from './styles';
import { convertSpendingData } from 'DecentureMobile/src/utils/data_utils';
import PieChart from 'DecentureMobile/src/components/spending/charts/pie/PieChart';
import LineChart from 'DecentureMobile/src/components/spending/charts/line/LineChart';
import DummyView from 'DecentureMobile/src/components/common/dummy';

interface SpendingProps {
    transactions: Array<TransactionModel>;
}

class Spending extends React.Component<SpendingProps> {

    renderData = (data: any) => {

        return (
            <View>
                <Text style={styles.text}>Monthly Spending</Text>
                <PieChart style={{ flex: 1, minHeight: 250 }} data={data.pieData}/>
                <View style={styles.border}/>
                <Text style={styles.text}>Daily Spending</Text>
                <LineChart style={styles.lineChart} data={data.lineData}/>
            </View>
        );
    };

    render() {

        const data = convertSpendingData(this.props.transactions);

        return (
            <ViewContainer style={{ padding: 10 }}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    {
                        data.total === 0
                            ? <DummyView/>
                            : this.renderData(data)
                    }
                </ScrollView>
            </ViewContainer>
        );
    }

}

export default Spending;
