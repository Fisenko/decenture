import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center'
    },
    pieContainer: {
        flexDirection: 'row',
        justifyContent: 'center'
    }
});
