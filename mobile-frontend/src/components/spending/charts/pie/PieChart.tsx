import * as React from 'react';
import * as Charts from 'react-native-svg-charts';
import { View, ViewStyle } from 'react-native';
import styles from './styles';
import ChartLegend from 'DecentureMobile/src/components/common/chart_legend/ChartLegend';

export type PieChartData = {
    color: string;
    key: string;
    name: string;
    svg: any;
    value: number;
};

interface PieChartProps {
    data: Array<PieChartData>;
    style?: ViewStyle;
}

const PieChart: React.SFC<PieChartProps> = ({ data, style = {} }: PieChartProps) => {

    return (
        <View style={[styles.pieContainer, style]}>
            <View style={[styles.container, { flex: 1.5 }]}>
                <Charts.PieChart
                    style={{ flex: 1 }}
                    outerRadius="90%"
                    innerRadius={3}
                    data={data}
                >
                </Charts.PieChart>
            </View>
            <View style={[styles.container, { alignItems: 'flex-start' }]}>
                <ChartLegend data={data}/>
            </View>
        </View>
    );
};

export default PieChart;
