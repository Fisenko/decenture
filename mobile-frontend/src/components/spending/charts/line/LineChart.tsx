import * as React from 'react';
import * as Charts from 'react-native-svg-charts';
import { View, ViewStyle } from 'react-native';
import { DECENTURE_COLOR } from 'DecentureMobile/src/common/styles';
import styles from './styles';
import { AXES_SVG, VERTICAL_CONTENT_INSET } from 'DecentureMobile/src/components/spending/charts/line/config';
import * as shape from 'd3-shape';

export type LineChartData = {
    value: number;
    date: number;
}

interface LineChartProps {
    data: Array<LineChartData>;
    style?: ViewStyle;
}

const LineChart: React.SFC<LineChartProps> = ({ data, style = {} }: LineChartProps) => {

    return (
        <View style={[styles.container, style]}>
            <View style={{ flex: 1, flexDirection: 'row' }}>
                <Charts.YAxis
                    yAccessor={({ item }) => item.value}
                    data={data}
                    style={styles.yAxis}
                    contentInset={VERTICAL_CONTENT_INSET}
                    svg={AXES_SVG}
                    numberOfTicks={8}
                    formatLabel={(value, index) => `$${value}`}
                />
                <View style={{ flex: 1 }}>
                    <Charts.LineChart
                        xAccessor={({ item }) => item.date}
                        yAccessor={({ item }) => item.value}
                        style={{ flex: 1 }}
                        data={data}
                        gridMin={0}
                        contentInset={VERTICAL_CONTENT_INSET}
                        svg={{
                            strokeWidth: 1,
                            stroke: DECENTURE_COLOR,
                        }}
                    >
                        <Charts.Grid/>
                    </Charts.LineChart>
                    <Charts.XAxis
                        xAccessor={({ item }) => item.date}
                        style={styles.xAxis}
                        data={data}
                        formatLabel={value => value}
                        contentInset={{ left: 10, right: 10 }}
                        svg={AXES_SVG}
                        numberOfTicks={5}
                    />
                </View>
            </View>
        </View>
    );
};

export default LineChart;
