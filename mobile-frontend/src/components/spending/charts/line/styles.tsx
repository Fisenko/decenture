import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center'
    },
    pieContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center'
    },
    yAxis: {
        marginBottom: 30,
        paddingRight: 5,
    },
    xAxis: {
        marginHorizontal: -10,
        height: 30,
        paddingTop: 5,
    },
});
