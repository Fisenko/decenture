import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';

import Spending from './Spending';

const mapStateToProps = (state: any) => ({
    transactions: state.user.transaction.list,
});

const mapDispatchToProps = (dispatch: Dispatch) => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Spending);
