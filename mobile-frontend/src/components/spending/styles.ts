import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    text: {
        fontSize: 16,
        textAlign: 'center',
        color: 'black',
        padding: 10,
        paddingTop: 20,
        fontWeight: 'bold',
    },
    border: {
        padding: 10,
        borderColor: '#00000014',
        borderBottomWidth: 1,
        marginHorizontal: 30,
    },
    lineChart: {
        height: 200,
        padding: 10,
        paddingTop: 20,
    },
});
