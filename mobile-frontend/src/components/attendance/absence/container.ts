import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';

import Absence from './index';
import { getUserAttendance } from '../../../core/actions/attendance';

const mapStateToProps = (state: any) => ({
    absence: state.user.current.student.attendance.list.absence,
    isRefreshing: state.ui.configs.attendance.isRefreshing,
});

const mapDispatchToProps = (dispatch: Dispatch) => bindActionCreators({
    getUserAttendance,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Absence);
