import {StyleSheet, Dimensions} from 'react-native';

const window = Dimensions.get('window');

export default StyleSheet.create({
    container: {
        backgroundColor: 'transparent',
        flex: 1,
        alignItems: 'stretch',
        padding: 20
    },
    title: {
        fontSize: 17,
        padding: 30,
        color: '#1E88E5',
        textAlign: 'center',
    },
    content: {
        flexDirection: 'column',
        padding: 5
    },
    balls: {
        height: 100,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        borderBottomColor: '#EDEDED',
        borderBottomWidth: 1
    },
    ball: {
        borderRadius: 30,
        borderWidth: 2,
        borderColor: '#10DB8C',
        width: 30,
        height: 30,
        justifyContent: 'center',
        alignItems: 'center'
    },
    ballText: {
        fontWeight: 'bold',
        fontSize: 19,
        color: '#10DB8C'
    },
    red: {
        color: '#EA373F'
    },
    borderRed: {
        borderColor: '#EA373F'
    },
    almostRed: {
        backgroundColor: '#EA373F',
        borderColor: '#EA373F'
    },
    white: {
        color: '#ffffff'
    },
    lesson: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: 5,
    },
    left: {
        flex: 2,
        textAlign: 'left',
        paddingRight: 10,
        color: '#2A5386',
        marginRight: 15,
    },
    right: {
        flex: 3,
        textAlign: 'left',
        paddingLeft: 30,
        color: '#2A5386',
    }
});
