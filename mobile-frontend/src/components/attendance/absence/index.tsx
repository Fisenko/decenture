import * as React from 'react';
import { ScrollView, Text, View } from 'react-native';
import moment from 'moment';

import styles from './styles';
import { DATETIME_FORMAT } from 'DecentureMobile/src/utils/format_utils';
import RefreshControl from 'DecentureMobile/src/components/common/refresh_control';

// Обоснованный костыль
const ABSENTEEISM = [0, 1, 2, 3, 4];

interface AbsenceProps {
    isRefreshing: boolean;
    absence: Array<any>;
    getUserAttendance: () => any;
}

export default class Absence extends React.Component<AbsenceProps> {
    componentDidMount() {
        if (!this.props.absence.length) {
            this.props.getUserAttendance();
        }
    }

    handleRefresh = () => {
        this.props.getUserAttendance();
    };

    render() {
        const { absence } = this.props;
        return (
            <View style={styles.container}>
                <Text style={styles.title}>Permitted Absences</Text>
                <View style={styles.content}>
                    <View style={styles.balls}>
                        {ABSENTEEISM.map((i, key) => (
                            <View
                                style={[
                                    styles.ball,
                                    i + 1 < absence.length ? styles.borderRed : null,
                                    i + 1 === absence.length ? styles.almostRed : null,
                                ]}
                                key={key}
                            >
                                <Text
                                    style={[
                                        styles.ballText,
                                        i + 1 < absence.length ? styles.red : null,
                                        i + 1 === absence.length ? styles.white : null,
                                    ]}
                                >
                                    {i + 1}
                                </Text>
                            </View>
                        ))}
                    </View>
                </View>
                <Text style={styles.title}>Unexcused absences</Text>
                <ScrollView
                    refreshControl={
                        <RefreshControl refreshing={this.props.isRefreshing} onRefresh={this.handleRefresh} />
                    }
                    showsVerticalScrollIndicator={false}
                >
                    <View style={styles.content}>
                        {absence.map((lecture, i) => {
                            // TODO: will be removed after processing data on backend
                            const lectureInfo = moment(lecture.dateTime).format(DATETIME_FORMAT) + ' ' + lecture.place;
                            return (
                                <View style={styles.lesson} key={i}>
                                    <Text style={styles.left}>{lecture.title}</Text>
                                    <Text style={styles.right}>{lectureInfo}</Text>
                                </View>
                            );
                        })}
                    </View>
                </ScrollView>
            </View>
        );
    }
}
