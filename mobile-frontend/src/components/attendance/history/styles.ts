import { StyleSheet } from 'react-native';
import { window } from '../../common/styles';

export default StyleSheet.create({
    rowContainer: {
        flex: 1,
        marginTop: 20,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    columnContainer: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    barChart: {
        flex: 1,
        height: window.height / 2,
    },
    barChartContent: {
        top: 30,
    },
    yAxis: {
        flex: 1,
        height: window.height / 2,
    },
    yAxisContainer: {
        width: 50,
        flexDirection: 'column'
    },
    yAxisContent: {
        top: 30,
        bottom: 10,
    },
    xAxis: {
        flex: 0.2,
        width: window.width / 1.5,
    },
    title: {
        fontSize: 17,
        padding: 30,
        color: '#1E88E5',
        textAlign: 'center',
    },
    titleView: {
        flex: 0.2,
        paddingTop: 20,
    },
    content: {
        flex: 1,
        justifyContent: 'center'
    },
});

export const svgStyles = {
    rotation: 60,
    y: 20,
    originY: 30,
};
