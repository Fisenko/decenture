export const barChartYAxisLabels = [50, 62.5, 75, 87.5, 100];
export const barChartSpaceInner = 0.3;
export const barChartSpaceOuter = 0.5;
export const barChartBarLabelSpaceOff = 20;

export const barChartGridMin = 50;
export const barChartBarGridMax = 100;

export const numberAxisTicks = 4;
