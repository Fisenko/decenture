import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';

import History from './index';
import { getUserAttendance } from '../../../core/actions/attendance';

const mapStateToProps = (state: any) => ({
    percentage: state.user.current.student.attendance.list.percentage,
    isRefreshing: state.ui.configs.attendance.isRefreshing,
});

const mapDispatchToProps = (dispatch: Dispatch) => bindActionCreators({
    getUserAttendance,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(History);
