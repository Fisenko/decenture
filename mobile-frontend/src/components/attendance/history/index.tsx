import * as React from 'react';
import { Text, View } from 'react-native';
import { BarChart, XAxis, YAxis } from 'react-native-svg-charts';
import * as ReactNativeSvg from 'react-native-svg';
import * as scale from 'd3-scale';

import styles, { svgStyles } from './styles';
import * as chartConfig from 'DecentureMobile/src/components/attendance/history/chartConfig';
import { commonStyles } from '../../common/styles';
import { convertToBarChartData } from '../../../utils/chart_utils';
import DummyView from 'DecentureMobile/src/components/common/dummy';
import ViewContainer from 'DecentureMobile/src/components/common/view_container';
import {
    barChartBarGridMax,
    barChartGridMin,
    numberAxisTicks
} from 'DecentureMobile/src/components/attendance/history/chartConfig';

interface HistoryProps {
    isRefreshing: boolean;

    percentage: Array<any>;
    getUserAttendance: () => any;
}

class History extends React.Component<HistoryProps> {

    componentDidMount() {
        if (!this.props.percentage.length) {
            this.props.getUserAttendance();
        }
    }

    handleRefresh = () => {
        this.props.getUserAttendance();
    };

    createLabels() {
        return ({ x, y, bandwidth, data }: { [key: string]: any }) => (
            data.map((value: any, index: any) => {
                let innerValue = parseInt(value.currentProgressWithEx);
                return (
                    <ReactNativeSvg.Text
                        key={index}
                        x={x(index) + (bandwidth / 2)}
                        y={innerValue < chartConfig.barChartBarLabelSpaceOff ? y(innerValue) - 10 : y(innerValue) + 15}
                        fontSize={14}
                        fill={innerValue >= chartConfig.barChartBarLabelSpaceOff ? 'white' : 'black'}
                        alignmentBaseline="middle"
                        textAnchor="middle"
                    >
                        {innerValue}
                    </ReactNativeSvg.Text>
                );
            })
        );
    }

    defineXAxisFontSize = () => {
        return this.props.percentage.length > 4 ?
            {
                bottom: 0,
                fontSize: 12
            } :
            {
                bottom: 10,
                fontSize: 20
            };
    };

    renderView = (data: Array<any>) => {

        const XAxisProps = this.defineXAxisFontSize();
        // const Labels = this.createLabels();

        return (
            <View style={styles.rowContainer}>
                <View style={styles.yAxisContainer}>
                    <YAxis style={styles.yAxis}
                           svg={{}}
                           data={chartConfig.barChartYAxisLabels}
                           //@ts-ignore
                           contentInset={styles.yAxisContent}
                           numberOfTicks={numberAxisTicks}
                           formatLabel={(value) => value + '%'}
                    />
                    <View style={{ flex: 0.2 }}/>
                </View>
                <View>
                    <BarChart
                        style={styles.barChart}
                        data={data}
                        yAccessor={({ item }) => item.currentProgressWithEx}
                        //@ts-ignore
                        contentInset={{...styles.barChartContent, bottom: XAxisProps.bottom}}
                        spacingInner={chartConfig.barChartSpaceInner}
                        spacingOuter={chartConfig.barChartSpaceOuter}
                        gridMin={barChartGridMin}
                        gridMax={barChartBarGridMax}
                    >
                    </BarChart>
                    <XAxis style={styles.xAxis}
                           svg={{...svgStyles, fontSize: XAxisProps.fontSize}}
                           data={data}
                           scale={scale.scaleBand}
                           spacingInner={chartConfig.barChartSpaceInner}
                           spacingOuter={chartConfig.barChartSpaceOuter}
                           formatLabel={(_, index) => data[index].lectureCourseName}
                    />
                </View>
            </View>
        );
    };

    render() {

        const { percentage } = this.props;
        const data = convertToBarChartData(percentage);

        // TODO: Fix labels
        // const Labels = this.createLabels();
        return (
            <ViewContainer>
                <View style={styles.titleView}>
                    <Text style={styles.title}>
                        Attendance By Module
                    </Text>
                </View>
                <View style={styles.content}>
                    {
                        data.length === 0 ?
                            <DummyView/> :
                            this.renderView(data)
                    }
                </View>
            </ViewContainer>
        );
    }
}

export default History;
