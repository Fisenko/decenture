import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';

import Lecture from './index';
import { getUserAttendance } from 'DecentureMobile/src/core/actions/attendance';

const mapStateToProps = (state: any) => ({
    history: state.user.current.student.attendance.list.history,
    isRefreshing: state.ui.configs.attendance.isRefreshing,
});

const mapDispatchToProps = (dispatch: Dispatch) => bindActionCreators({
    getUserAttendance,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Lecture);
