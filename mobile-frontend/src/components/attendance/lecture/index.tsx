import * as React from 'react';
import { SectionList, Text, View } from 'react-native';
import moment from 'moment';

import styles from './styles';
import ViewContainer from 'DecentureMobile/src/components/common/view_container';
import { TIME_FORMAT } from 'DecentureMobile/src/utils/format_utils';
import DummyView from 'DecentureMobile/src/components/common/dummy';
import LectureAttendance from 'DecentureMobile/src/core/models/attendance/lectureAttendance';
import RefreshControl from 'DecentureMobile/src/components/common/refresh_control';

interface Props {
    history: Array<any>;
    isRefreshing: boolean;
    getUserAttendance: () => any;
}

export default class AttendanceLecture extends React.Component<Props> {
    componentDidMount() {
        if (!this.props.history.length) {
            this.props.getUserAttendance();
        }
    }

    handleRefresh = () => {
        this.props.getUserAttendance();
    };

    renderHistorySectionList = (item: LectureAttendance) => {
        return (
            <View key={item.id} style={styles.lesson}>
                <View style={styles.left}>
                    <Text style={styles.topLeftText}>
                        {`${item.lecture.name}`}
                    </Text>
                    <Text style={styles.bottomLeftText}>
                        {`${item.lecture.code}`}
                    </Text>
                </View>
                <View style={styles.right}>
                    <Text style={styles.topRightText}>
                        {`${moment(item.datetime).format(TIME_FORMAT)}`}
                    </Text>
                    <Text style={styles.bottomRightText}>
                        {`${item.theatre.name}`}
                    </Text>
                </View>
            </View>
        );
    };

    render() {
        return (
            <ViewContainer>
                <View style={styles.titleView}>
                    <Text style={styles.title}>Attendance</Text>
                </View>
                <View style={styles.content}>
                    <SectionList
                        refreshControl={
                            <RefreshControl refreshing={this.props.isRefreshing} onRefresh={this.handleRefresh} />
                        }
                        showsVerticalScrollIndicator={false}
                        renderSectionHeader={({ section: { date } }) => <Text style={styles.date}>{date}</Text>}
                        renderItem={({ item }) => this.renderHistorySectionList(item)}
                        sections={this.props.history}
                        keyExtractor={(item, index) => item + index}
                        ListEmptyComponent={<DummyView />}
                    />
                </View>
            </ViewContainer>
        );
    }

}
