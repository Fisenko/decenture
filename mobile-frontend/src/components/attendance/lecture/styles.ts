import { Dimensions, StyleSheet } from 'react-native';

const window = Dimensions.get('window');

export default StyleSheet.create({
    container: {
        backgroundColor: 'transparent',
        flex: 1,
        alignItems: 'stretch',
    },
    title: {
        textAlign: 'center',
        fontSize: 18,
        padding: 30,
        color: '#1E88E5',
    },
    date: {
        paddingBottom: 9,
        paddingLeft: 15,
    },
    lesson: {
        display: 'flex',
        flexDirection: 'row',
        alignContent: 'flex-start',
        padding: 5,
        paddingRight: 15,
        paddingLeft: 15,
        justifyContent: 'space-between',
    },
    left: {
        textAlign: 'left',
        color: '#2A5386',
        maxWidth: window.width / 3,
        fontSize: 18,
    },
    right: {
        marginLeft: 15,
        color: '#2A5386',
        maxWidth: window.width / 3,
        fontSize: 12,
    },
    topLeftText: {
        fontSize: 18,
        color: '#000000',
        paddingBottom: 6,
    },
    topRightText: {
        fontSize: 20,
        textAlign: 'right',
        color: '#1E88E5',
        paddingBottom: 4,
    },
    bottomLeftText: {
        fontSize: 12,
        paddingLeft: 2,
    },
    bottomRightText: {
        fontSize: 12,
        textAlign: 'right',
    },
    titleView: {
        flex: 0.2,
        paddingTop: 20,
    },
    content: {
        flex: 1,
        justifyContent: 'center',
    },
});
