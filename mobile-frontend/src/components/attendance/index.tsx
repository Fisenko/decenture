import * as React from 'react';

import History from './history/container';
import Lecture from './lecture/container';
import Absence from './absence/container';
import Swiper from '../common/swiper';

const Attendance: React.SFC = () => (
    <Swiper>
        <Absence />
        <Lecture />
        <History />
    </Swiper>
);

export default Attendance;
