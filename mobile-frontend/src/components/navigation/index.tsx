import React from 'react';
import { BackHandler } from 'react-native';
import { NavigationActions } from 'react-navigation';

import Navigator from './navigator/container';

interface NavigationProps {
    nav: any;

    dispatch: any;
}

class Navigation extends React.PureComponent<NavigationProps> {
    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
    }

    onBackPress = () => {
        const { dispatch, nav } = this.props;

        // TODO: Fix application collapse
        if (nav.index === 0) {
            return false;
        }

        dispatch(NavigationActions.back());
        return true;
    };

    render() {
        return <Navigator />;
    }
}

export default Navigation;
