import { connect } from 'react-redux';

import Navigation from './index';

const mapStateToProps = (state: any) => ({
    nav: state.nav,
});

export default connect(mapStateToProps)(Navigation);
