import { connect } from 'react-redux';

import Navigator from './index';

const mapStateToProps = (state: any) => ({
    state: state.nav,
});

// @ts-ignore
export default connect(mapStateToProps)(Navigator);
