import { reduxifyNavigator } from 'react-navigation-redux-helpers';

import routes from 'DecentureMobile/src/routes';

export default reduxifyNavigator(routes, 'root');
