import * as React from 'react';
import TouchID from 'react-native-touch-id';

interface Props {
    setModalVisible: Function;
    attendanceRecordInputChange: Function;
    recordAttendance: Function;
    navigation?: any;
    errorMsg: string;
}

class AuthBiometric extends React.Component<Props> {

    componentDidMount() {
        TouchID.authenticate('Scan your fingerprint on the device scanner to continue',
            { fallbackLabel: '' } as any)
        .then((success: any) => {
            this.props.attendanceRecordInputChange({
                name: 'isAuth',
                value: true,
            });
            this.props.recordAttendance();
            this.props.setModalVisible(false);
        })
        .catch((error: any) => {
            this.props.setModalVisible(false);
        });

    }

    render() {
        return false;
    }
}

export default AuthBiometric;

