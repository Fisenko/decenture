import * as React from 'react';
// @ts-ignore
import FingerprintScanner from 'react-native-fingerprint-scanner';
import { Image, Text, TouchableOpacity, View } from 'react-native';
import Modal from 'react-native-modal';

import Button from 'DecentureMobile/src/components/common/button';
import { styles } from 'DecentureMobile/src/components/record/styles';
import { getImage } from 'DecentureMobile/src/utils/image_holder';
import { commonStyles } from 'DecentureMobile/src/components/common/styles';

interface Props {
    setModalVisible: Function;
    attendanceRecordInputChange: Function;
    recordAttendance: Function;
    setError: Function;
    navigation?: any;
    errorMsg: string;
}

class AuthBiometric extends React.Component<Props, any> {
    componentDidMount() {
        FingerprintScanner.isSensorAvailable().catch((error: any) =>
            this.props.setError('record', 'fingerprint', error.name),
        );
        FingerprintScanner
            .authenticate({ onAttempt: this.handleAuthenticationAttempted })
                .then(() => {
                    this.props.attendanceRecordInputChange({
                        name: 'isAuth',
                        value: true
                    });
                    this.props.recordAttendance();
                    this.props.setModalVisible(false);
                })
                .catch((error: Error) => {
                    console.log(error);
                });
    }

    componentWillUnmount() {
        FingerprintScanner.release();
    }

    handleAuthenticationAttempted = (error: any) => {
        this.props.setError('record', 'fingerprint', error.name);
    };

    onCloseButtonClick = () => {
        this.props.setError('record', 'fingerprint', '');
        this.props.setModalVisible(false);
    };

    handleModalClose = () => {
        this.props.setModalVisible(false);
    };

    render() {
        return (
            <Modal style={styles.modal} isVisible={true}>
                <View style={styles.modalContent}>
                    <TouchableOpacity onPress={this.handleModalClose}>
                        <Image
                            source={getImage('fingerprint')}
                            style={[styles.fingerPrintImage, styles.flexElement]}
                        />
                    </TouchableOpacity>
                    <View style={styles.flexElement}>
                        <Text style={commonStyles.errorText}>
                            {this.props.errorMsg}
                        </Text>
                    </View>
                    <View style={styles.flexElement}>
                        <Text style={styles.fontBold}>
                            Touch ID To Verify
                        </Text>
                    </View>
                    <View style={styles.flexElement}>
                        <Text>Please verify your identity</Text>
                    </View>
                    <Button
                        primary
                        raised
                        text="Cancel"
                        style={{ container: styles.modalButtonContainer }}
                        onPress={this.onCloseButtonClick}
                    />
                </View>
            </Modal>
        );
    }
}

export default AuthBiometric;