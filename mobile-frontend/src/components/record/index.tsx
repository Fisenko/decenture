import * as React from 'react';
import { Text, View } from 'react-native';
import { NavigationScreenProp, NavigationState } from 'react-navigation';
import { TagEvent } from 'react-native-nfc-manager';

import ModalFingerprint from 'DecentureMobile/src/components/record/auth_biometric';

import { styles } from './styles';
import { commonStyles } from 'DecentureMobile/src/components/common/styles';

import Icon from 'DecentureMobile/src/utils/icon_utils';

interface Props {
    navigation: NavigationScreenProp<NavigationState>;

    setFingerprintModalVisible: any;
    setErrorConfig: any;

    attendanceRecordInputChange: any;
    recordAttendance: any;

    ui: {
        isFingerModalVisible: boolean;
        isNfcSupported: boolean;
    };
    errors: {
        nfc: string;
        fingerprint: string;
    };
}

const TEXT = 'Hold your phone near the\nlecture recording device';

export default class Record extends React.Component<Props, any> {

    recordAttendance = () => {
        this.props.recordAttendance();
        this.props.navigation.navigate('StatusPage');
    };

    render() {
        const {
            ui: { isFingerModalVisible },
            setFingerprintModalVisible,
            attendanceRecordInputChange,
            setErrorConfig,
            errors,
        } = this.props;

        return (
            <View style={styles.container}>
                {isFingerModalVisible && (
                    <ModalFingerprint
                        setModalVisible={setFingerprintModalVisible}
                        attendanceRecordInputChange={attendanceRecordInputChange}
                        recordAttendance={this.recordAttendance}
                        errorMsg={errors.fingerprint}
                        setError={setErrorConfig}
                    />
                )}
                <Icon name="record_icon" color="#B1B1B1" size={250}/>
                <Text style={commonStyles.errorText}>{this.props.errors.nfc || ''}</Text>
                <Text style={[commonStyles.text, { color: '#1E88E5' }]}>{TEXT}</Text>
            </View>
        );
    }
}
