import { bindActionCreators, Dispatch } from 'redux';
import { connect } from 'react-redux';
import Record from './index';
import { setFingerprintModalVisible } from 'DecentureMobile/src/core/actions/ui/nfc';
import { attendanceRecordInputChange, recordAttendance, } from 'DecentureMobile/src/core/actions/attendance';
import { setErrorConfig } from 'DecentureMobile/src/core/actions/error';

const mapStateToProps = (state: any) => ({
    ui: state.ui.configs.record,
    errors: state.errors.configs.record,
});

const mapDispatchToProps = (dispatch: Dispatch) => bindActionCreators({
    setFingerprintModalVisible,
    attendanceRecordInputChange,
    recordAttendance,
    setErrorConfig,
}, dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(Record);
