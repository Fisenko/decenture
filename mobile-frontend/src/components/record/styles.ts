import { Dimensions, StyleSheet } from 'react-native';

const window = Dimensions.get('window');

import { BOTTOM_BAR_ICON_HEIGHT } from './../../common/styles';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    backgroundImage: {
        marginTop: 50,
        resizeMode: 'contain',
        alignSelf: 'center',
        height: 200,
        width: 200,
    },
    fingerPrintImage: {
        height: 50,
        width: 50,
    },
    modal: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    modalContent: {
        backgroundColor: '#fff',
        justifyContent: 'space-around',
        alignItems: 'center',
        padding: 30,
        borderRadius: 6,
    },
    modalButtonContainer: {
        borderRadius: 40,
    },
    fontBold: {
        fontWeight: 'bold',
    },
    flexElement: {
        marginBottom: 20,
    },
});
