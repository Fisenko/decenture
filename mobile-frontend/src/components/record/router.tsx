import { createStackNavigator } from 'react-navigation';
import Record from 'DecentureMobile/src/components/record/container';
import Header from 'DecentureMobile/src/components/common/header';
import React from 'react';
import { headerNavigationOptions } from 'DecentureMobile/src/utils/router_utils';

export const homeRouterConfig = {
    Record: {
        screen: Record,
        navigationOptions: {
            title: 'Scan Attendance',
        },
    },
};

export default createStackNavigator(
    homeRouterConfig,
    {
        headerMode: 'screen',
        initialRouteName: 'Record',
        navigationOptions: headerNavigationOptions,
    },
);
