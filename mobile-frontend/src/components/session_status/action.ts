import { NavigationActions } from 'react-navigation';
import { setUIConfig } from 'DecentureMobile/src/core/actions/ui';
import * as UserActionTypes from 'DecentureMobile/src/core/actions/user/actionTypes';
import { Dispatch } from 'redux';
import RNExitApp from 'react-native-exit-app';

export const exitApp = () => {
    RNExitApp.exitApp();
};

export const navigateLogin = () => (dispatch: Dispatch) => {
    // TODO  subscribe in init on SESSION_EXPIRED and set status true
    dispatch(setUIConfig('sessionExpired', 'status', true));
    dispatch(NavigationActions.navigate({ routeName: 'Auth' }));
    dispatch({ type: UserActionTypes.LOGOUT_WITHOUT_CLEAN });
};
