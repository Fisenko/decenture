import React from 'react';
import { View } from 'react-native';
import styles from './styles';
import Dialog from 'react-native-dialog';

interface Props {
    isAuth: boolean;
    sessionExpired: boolean;
    exitApp: any;
    navigateLogin: any;
}

export default class SessionStatus extends React.Component<Props, any> {

    render() {
        const extraProps = {
            onBackdropPress: this.props.navigateLogin,
        };

        return (
            <View>
                <Dialog.Container
                    visible={this.props.isAuth && !this.props.sessionExpired}
                    {...extraProps}
                >
                    <Dialog.Title style={styles.title}>Your session has expired</Dialog.Title>
                    <Dialog.Button style={styles.button} label="EXIT" onPress={() => this.props.exitApp()} />
                    <Dialog.Button style={styles.button} label="LOGIN" onPress={() => this.props.navigateLogin()}/>
                </Dialog.Container>
            </View>
        );
    }
}
