import { StyleSheet} from 'react-native';

export default StyleSheet.create({
    title: {
        fontWeight: 'normal',
    },
    button: {
        color: '#538ED4',
    },
});
