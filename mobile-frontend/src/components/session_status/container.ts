import { connect } from 'react-redux';

import SessionStatus from './SessionStatus';
import { exitApp, navigateLogin } from 'DecentureMobile/src/components/session_status/action';
import { bindActionCreators, Dispatch } from 'redux';

const mapStateToProps = (state: any) => ({
    isAuth: state.user.current.info.isAuth || false,
    sessionExpired: state.ui.configs.sessionExpired.status || false,
});

const mapDispatchToProps = (dispatch: Dispatch) => bindActionCreators({
    exitApp,
    navigateLogin,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(SessionStatus);
