import { bindActionCreators, Dispatch } from 'redux';
import { connect } from 'react-redux';
import Picker from 'DecentureMobile/src/components/home/picker/Picker';
import { oneToOnePaymentInputChange, sendFunds } from 'DecentureMobile/src/core/actions/payment';

const mapStateToProps = (state: any) => ({
    payment: state.user.current.payment.current.one_to_one.info
});

const mapDispatchToProps = (dispatch: Dispatch) => bindActionCreators({
    oneToOnePaymentInputChange,
    sendFunds,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Picker);
