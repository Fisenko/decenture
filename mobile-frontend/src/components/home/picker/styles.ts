import {Dimensions, StyleSheet} from 'react-native';

const window = Dimensions.get('window');

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'stretch',
        justifyContent: 'space-around',
        backgroundColor: '#fff',
    },

    label: {
        backgroundColor: 'transparent',
        marginHorizontal: 10,
        padding: 10,
        fontSize: 15,
        textAlign: 'center',
    },

    input: {
        height: 40,
        backgroundColor: '#fff',
        width: window.width - 100,
        color: 'black',
        padding: 5,
        fontSize: 17,
        paddingBottom: 12,
    },

    picker: {
        width: window.width - 100,
        height: 44,
    },
    itemStyle: {
        height: 44,
    },
    inputFieldView: {
        backgroundColor: 'transparent',
    },

    buttonContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        margin: 10,
        height: window.height / 4,
    },

    button: {
        backgroundColor: '#538ED4',
        borderRadius: 40,
        width: window.width / 3,
        height: 50,
    },

    keyboardAvoid: {
        flex: 1,
        marginTop: '15%',
        marginBottom: '15%',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'space-between',
        top: '15%',
    },
    textInputView: {
        borderWidth: 2,
        borderColor: '#c2c2c1',
        borderRadius: 5,
    },
});
