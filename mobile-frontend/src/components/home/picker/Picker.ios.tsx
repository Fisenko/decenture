import * as React from 'react';
import { KeyboardAvoidingView, Text, TextInput, View, Keyboard } from 'react-native';
import { Button } from 'react-native-material-ui';
import { styles } from 'DecentureMobile/src/components/funds/deposit/styles';
import { commonStyles } from 'DecentureMobile/src/components/common/styles';
import { NavigationActions } from 'react-navigation';
import { OneToOnePayment } from 'DecentureMobile/src/core/models/payment/OneToOnePayment';

interface PickerProps {
    navigation: any;
    oneToOnePaymentInputChange: any;
    payment: OneToOnePayment;

    sendFunds: any;
}

export default class Picker extends React.Component<PickerProps, any> {
    render() {
        return (
            <View style={styles.container}>
                <KeyboardAvoidingView style={commonStyles.keyboardAvoid} behavior="padding">
                    <View>
                        <Text style={styles.label}>How much would you like to send?</Text>
                        <View style={styles.textInputView}>
                            <TextInput
                                style={styles.input}
                                keyboardType="number-pad"
                                returnKeyType="done"
                                placeholder="$"
                                onChangeText={text => {
                                    this.props.oneToOnePaymentInputChange({
                                        name: 'amount',
                                        value: parseInt(text,10),
                                    });
                                }}
                            />
                        </View>
                    </View>
                </KeyboardAvoidingView>
                <View style={styles.buttonContainer}>
                    <Button
                        primary
                        raised
                        text="Send"
                        style={{ container: styles.button }}
                        onPress={() => {
                            Keyboard.dismiss();
                            this.props.sendFunds(this.props.payment);
                            this.props.navigation.reset(
                                [NavigationActions.navigate({ routeName: 'Home' })], 0);
                            this.props.navigation.navigate('StatusPage');
                        }}
                    />
                </View>
            </View>
        );
    }
}
