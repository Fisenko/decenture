import * as React from 'react';

import { Text, View, } from 'react-native';
import { NavigationScreenProp, NavigationState } from 'react-navigation';

import styles from './styles';

import QRCodeScanner from 'react-native-qrcode-scanner';
import ComponentFrame from 'DecentureMobile/src/components/common/frame';

interface SendProps {
    navigation: NavigationScreenProp<NavigationState>;
    oneToOnePaymentInputChange: any;
}

export default class Send extends React.Component<SendProps, any> {

    onSuccess = (encoded: any) => {
        this.props.oneToOnePaymentInputChange({
            name: 'receiver',
            value: encoded.data,
        });
        this.props.navigation.navigate('Picker');
    };

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.text}>
                    Scan QR Code to Send Money
                </Text>
                <ComponentFrame>
                    <View style={styles.cameraContainer}>
                        <QRCodeScanner
                            onRead={this.onSuccess}
                            cameraStyle={styles.camera}
                            cameraProps={{captureAudio: false}}
                        />
                    </View>
                </ComponentFrame>
            </View>
        );
    }
}
