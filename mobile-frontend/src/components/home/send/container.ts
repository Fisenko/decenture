import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';

import Send from './Send';
import { oneToOnePaymentInputChange } from 'DecentureMobile/src/core/actions/payment';

const mapStateToProps = (state: any) => ({
});

const mapDispatchToProps = (dispatch: Dispatch) =>
    bindActionCreators(
        {
            oneToOnePaymentInputChange,
        },
        dispatch,
    );

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Send);
