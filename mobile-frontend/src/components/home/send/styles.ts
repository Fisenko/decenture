import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
    },
    text: {
        fontSize: 16,
        textAlign: 'center',
        color: 'black',
        padding: 30,
    },
    cameraContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        width: 250,
        height: 250,
    },
    camera: {
        width: 250,
        height: 250,
        alignSelf: 'center',
        justifyContent: 'center',
    },
});
