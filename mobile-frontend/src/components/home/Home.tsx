import * as React from 'react';
import { FormattedNumber } from 'react-intl';
// @ts-ignore
import ParallaxScrollView from 'react-native-parallax-scroll-view';

import RefreshControl from 'DecentureMobile/src/components/common/refresh_control';
import Swiper from 'DecentureMobile/src/components/common/swiper';
import HomeHeader from 'DecentureMobile/src/components/home/home_header';
import StickyHeader from 'DecentureMobile/src/components/home/sticky_header';
import { NavigationScreenProp, NavigationState } from 'react-navigation';
import Attendance from '../attendance/lecture/container';
import TransactionsHistory from '../transactions/history/container';

import { DECENTURE_COLOR } from 'DecentureMobile/src/common/styles';

interface HomeProps {
    navigation: NavigationScreenProp<NavigationState>;

    logout: any;
    amount: number;
    getAmount: any;
    getAccounts: any;
    getTransactions: any;

    ui: {
        isRefreshing: boolean;
    };
}

export default class Home extends React.Component<HomeProps, any> {
    componentDidMount() {
        this.props.getAmount();
        this.props.getAccounts();
        this.props.getTransactions();
    }

    handleRefresh = () => {
        this.props.getTransactions();
    };

    render() {
        return (
            <Swiper>
                <ParallaxScrollView
                    refreshControl={
                        <RefreshControl
                            refreshing={this.props.ui.isRefreshing}
                            onRefresh={this.handleRefresh}
                        />}
                    backgroundColor={DECENTURE_COLOR}
                    parallaxHeaderHeight={300}
                    stickyHeaderHeight={75}
                    renderStickyHeader={() => <StickyHeader amount={this.props.amount}/>}
                    renderForeground={() => <HomeHeader amount={this.props.amount}
                                                        navigation={this.props.navigation}/>}>
                    <TransactionsHistory style={{ padding: 10 }} navigation={this.props.navigation}/>
                </ParallaxScrollView>
                <Attendance/>
            </Swiper>
        );
    }
}
