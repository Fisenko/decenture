import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';

import Home from './Home';
import { logout } from 'DecentureMobile/src/core/actions/user';
import { getAmount } from 'DecentureMobile/src/core/actions/payment';
import { getAccounts } from 'DecentureMobile/src/core/actions/user/account';
import { getTransactions } from 'DecentureMobile/src/core/actions/transaction';

const mapStateToProps = (state: any) => ({
    amount: state.user.current.payment.current.info,
    ui: state.ui.configs.transactions,
});

const mapDispatchToProps = (dispatch: Dispatch) =>
    bindActionCreators(
        {
            logout,
            getAmount,
            getAccounts,
            getTransactions,
        },
        dispatch,
    );

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Home);
