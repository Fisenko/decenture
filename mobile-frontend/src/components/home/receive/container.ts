import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';

import Receive from './Receive';

const mapStateToProps = (state: any) => ({
    email: state.user.current.info.email,
});

const mapDispatchToProps = (dispatch: Dispatch) =>
    bindActionCreators(
        {},
        dispatch,
    );

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Receive);
