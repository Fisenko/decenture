import { Text, View } from 'react-native';
import * as React from 'react';
import styles from './styles';
//@ts-ignore
import QRCode from 'react-native-qrcode';

interface ReceiveProps {
    email: string;
    amount?: number;
}

export default class Receive extends React.Component<ReceiveProps, any> {

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.text}>Scan QR Code to Receive Money</Text>
                <QRCode
                    value={this.props.email}
                    size={200}
                    bgColoxr='black'
                    fgColor='white'/>
            </View>
        );
    };
}
