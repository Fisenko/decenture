import { createStackNavigator } from 'react-navigation';
import Home from 'DecentureMobile/src/components/home';
import React from 'react';
import { headerNavigationOptions } from 'DecentureMobile/src/utils/router_utils';
import Receive from 'DecentureMobile/src/components/home/receive';
import Send from 'DecentureMobile/src/components/home/send';
import Picker from 'DecentureMobile/src/components/home/picker';

export const homeRouterConfig = {
    Home: {
        screen: Home,
        navigationOptions: {
            title: 'Home',
        },
    },
    Receive: {
        screen: Receive,
        navigationOptions: {
            title: 'Receive Funds',
        },
    },
    Send: {
        screen: Send,
        navigationOptions: {
            title: 'Send Funds',
        },
    },
    Picker: {
        screen: Picker,
        navigationOptions: {
            title: 'Send Funds',
        },
    }
};

export default createStackNavigator(
    homeRouterConfig,
    {
        headerMode: 'screen',
        initialRouteName: 'Home',
        navigationOptions: headerNavigationOptions,
    },
);
