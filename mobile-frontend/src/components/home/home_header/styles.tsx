import { IMAGE_HEIGHT_SMALL } from 'DecentureMobile/src/common/styles';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        height: 300,
    },
    text: {
        fontSize: 16,
        textAlign: 'center',
        color: 'white',
    },
    amount: {
        fontSize: 30,
        textAlign: 'center',
        color: 'white',
    },
    logoSmall: {
        height: IMAGE_HEIGHT_SMALL,
        width: IMAGE_HEIGHT_SMALL,
        resizeMode: 'contain',
        marginBottom: 10,
    },
    subtext: {
        fontSize: 15,
        fontWeight: '100',
        textAlign: 'center',
        color: 'white',
    },
    buttonsContainer: {
        flexDirection: 'row' ,
        width: 200,
        justifyContent: 'space-around',
        marginTop: 40
    },
});
