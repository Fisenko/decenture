import * as React from 'react';
import { FormattedNumber } from 'react-intl';
import { Image, Text, TouchableOpacity, View } from 'react-native';
import { NavigationScreenProp, NavigationState } from 'react-navigation';

import styles from './styles';
import { getImage } from 'DecentureMobile/src/utils/image_holder';

interface Props {
    amount: number;
    navigation: NavigationScreenProp<NavigationState>;
}

const HomeHeader = (props: Props) => {

    const sendIcon = getImage('send');
    const receiveIcon = getImage('receive');

    const renderPrice = (formattedNumber: string) => {
        return <Text style={styles.amount}>${formattedNumber}</Text>;
    };

    return (
        <View style={styles.container}>
            <FormattedNumber value={props.amount} minimumFractionDigits={2} maximumFractionDigits={2}>
                {renderPrice}
            </FormattedNumber>
            <Text style={styles.text}>Your current balance</Text>
            <View style={styles.buttonsContainer}>
                <TouchableOpacity onPress={() => props.navigation.navigate('Send')}>
                    <View style={{ alignItems: 'center' }}>
                        <Image source={sendIcon} style={styles.logoSmall}/>
                        <Text style={styles.subtext}>Send</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => props.navigation.navigate('Receive')}>
                    <View style={{ alignItems: 'center' }}>
                        <Image source={receiveIcon} style={styles.logoSmall}/>
                        <Text style={styles.subtext}>Receive</Text>
                    </View>
                </TouchableOpacity>
            </View>
        </View>);
};

export default HomeHeader;
