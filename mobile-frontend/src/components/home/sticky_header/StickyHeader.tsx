import * as React from 'react';
import { FormattedNumber } from 'react-intl';
import { Text, View } from 'react-native';

import styles from './styles';

interface Props {
    amount: number;
}

const StickyHeader = (props: Props) => {

    const renderPrice = (formattedNumber: string) => {
        return <Text style={styles.amount}>${formattedNumber}</Text>;
    };

    return (
        <View style={styles.container}>
            <FormattedNumber value={props.amount} minimumFractionDigits={2} maximumFractionDigits={2}>
                {renderPrice}
            </FormattedNumber>
        </View>
    );
};

export default StickyHeader;
