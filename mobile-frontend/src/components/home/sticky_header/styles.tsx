import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        marginTop: 30,
        alignItems: 'center',
    },
    amount: {
        fontSize: 30,
        textAlign: 'center',
        color: 'white',
    },
});
