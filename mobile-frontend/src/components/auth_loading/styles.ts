import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-around',
    },
    logo: {
        height: 150,
        width: 200,
        resizeMode: 'contain',
    },
});
