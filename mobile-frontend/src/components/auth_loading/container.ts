import { connect } from 'react-redux';

import AuthLoading from './index';
import { checkLogin } from 'DecentureMobile/src/core/actions/app';

const mapStateToProps = (state: any) => ({
    isAuth: state.user.current.info.isAuth,
});

const mapDispatchToProps = {
    checkLogin,
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(AuthLoading);
