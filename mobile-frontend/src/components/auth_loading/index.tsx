import * as React from 'react';
import { NavigationScreenProp, NavigationState } from 'react-navigation';
import { View, ActivityIndicator, StyleSheet, Image } from 'react-native';

import styles from './styles';
import { DECENTURE_COLOR } from 'DecentureMobile/src/common/styles';
import { getImage } from 'DecentureMobile/src/utils/image_holder';

interface AuthLoadingProps {
    isAuth: boolean;

    checkLogin(): any;
    navigation: NavigationScreenProp<NavigationState>;
}

class AuthLoading extends React.Component<AuthLoadingProps> {
    componentDidMount() {
        this.props.checkLogin();
    }

    render() {
        return (
            <View style={styles.container}>
                <Image source={getImage('decentureLogo')} style={styles.logo} />
                <ActivityIndicator size="large" color={DECENTURE_COLOR} />
            </View>
        );
    }
}

export default AuthLoading;
