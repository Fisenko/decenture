import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        backgroundColor: '#FFF',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-around',
        padding: 50,
    },
    logo: {
        height: '30%',
        resizeMode: 'contain',
        marginBottom: 30,
    },
    subtext: {
        alignSelf: 'center',
        color: '#538ED4',
        fontSize: 17,
        fontWeight: '100',
        marginBottom: 30,
    },
    stretchWidth: {
        width: '100%',
    },
    errorContainer: { marginBottom: 40 },
});
