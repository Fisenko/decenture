import * as React from 'react';
import { Image, Keyboard, KeyboardAvoidingView, Text, View } from 'react-native';
import { NavigationScreenProp, NavigationState } from 'react-navigation';

import styles from './styles';
import Button from '../common/button';
import TextInput from '../common/text_input';
import { getImage } from 'DecentureMobile/src/utils/image_holder';
import { commonStyles } from 'DecentureMobile/src/components/common/styles';

interface Props {
    user: {
        email: string;
        password: string;
    };

    errors: {
        errorMsg: string;
    };

    loginStudent: any;
    loginInputChange: any;
    navigation: NavigationScreenProp<NavigationState>;
}

export default class Login extends React.Component<Props, any> {
    render() {
        return (
            <KeyboardAvoidingView style={styles.container} behavior="padding" keyboardVerticalOffset={20}>
                <Image source={getImage('decentureLogo')} style={styles.logo} />
                <View style={styles.stretchWidth}>
                    <TextInput
                        style={commonStyles.inputIOS}
                        placeholder="University Email"
                        autoCapitalize="none"
                        keyboardType="email-address"
                        value={this.props.user.email}
                        onChangeText={text => {
                            this.props.loginInputChange({
                                name: 'email',
                                value: text,
                            });
                        }}
                    />
                    <TextInput
                        style={commonStyles.inputIOS}
                        placeholder="Password"
                        textContentType="password"
                        secureTextEntry
                        value={this.props.user.password}
                        onChangeText={text => {
                            this.props.loginInputChange({
                                name: 'password',
                                value: text,
                            });
                        }}
                    />
                    <Text style={styles.subtext} onPress={() => this.props.navigation.navigate('Registration')}>
                        New User
                    </Text>
                </View>
                <Text style={commonStyles.errorText}>{this.props.errors.errorMsg}</Text>
                <Button
                    primary
                    raised
                    style={{ container: styles.stretchWidth }}
                    text="Login"
                    onPress={() => {
                        Keyboard.dismiss();
                        this.props.loginStudent(this.props.user);
                    }}
                />
            </KeyboardAvoidingView>
        );
    }
}
