import * as React from 'react';
import { Image, Keyboard, KeyboardAvoidingView, Text, View } from 'react-native';
import { NavigationScreenProp, NavigationState } from 'react-navigation';

import styles from './styles';
import Button from '../common/button';
import TextInput from '../common/text_input';
import { getImage } from 'DecentureMobile/src/utils/image_holder';
import { commonStyles } from 'DecentureMobile/src/components/common/styles';

interface Props {
    user: {
        email: string;
        password: string;
    };

    errors: {
        errorMsg: string;
    };

    loginStudent: any;
    loginInputChange: any;
    navigation: NavigationScreenProp<NavigationState>;
}

export default class Login extends React.Component<Props, any> {
    render() {
        return (
            <KeyboardAvoidingView style={styles.container}>
                <Image source={getImage('decentureLogo')} style={styles.logo} />
                <View style={styles.stretchWidth}>
                    <TextInput
                        placeholder="University Email"
                        textContentType="emailAddress"
                        keyboardType="email-address"
                        autoCapitalize="none"
                        value={this.props.user.email}
                        onChangeText={text => {
                            this.props.loginInputChange({
                                name: 'email',
                                value: text,
                            });
                        }}
                    />
                    <TextInput
                        placeholder="Password"
                        textContentType="password"
                        secureTextEntry
                        value={this.props.user.password}
                        onChangeText={text => {
                            this.props.loginInputChange({
                                name: 'password',
                                value: text,
                            });
                        }}
                    />
                    <Text style={styles.subtext} onPress={() => this.props.navigation.navigate('Registration')}>
                        New User
                    </Text>
                </View>
                <View style={styles.errorContainer}>
                    <Text style={commonStyles.errorText}>{this.props.errors.errorMsg || ' '}</Text>
                </View>
                <Button
                    primary
                    raised
                    style={{ container: styles.stretchWidth }}
                    text="Login"
                    onPress={() => {
                        Keyboard.dismiss();
                        this.props.loginStudent(this.props.user);
                    }}
                />
            </KeyboardAvoidingView>
        );
    }
}
