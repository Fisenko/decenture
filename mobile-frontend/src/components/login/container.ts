import { bindActionCreators, Dispatch } from 'redux';
import { connect } from 'react-redux';
// @ts-ignore
import Login from './index';
import { loginInputChange, loginStudent } from 'DecentureMobile/src/core/actions/user';

const mapStateToProps = (state: any) => ({
    user: state.user.current.info,
    errors: state.errors.configs.login,
});

const mapDispatchToProps = (dispatch: Dispatch) => bindActionCreators({
    loginStudent,
    loginInputChange,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Login);
