import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';

import Security from './index';
import { setSuccessAppStatusView } from 'DecentureMobile/src/core/actions/ui/app_status_view';

const mapDispatchToProps = (dispatch: Dispatch) =>
    bindActionCreators(
        {
            setSuccessAppStatusView,
        },
        dispatch,
    );

export default connect(
    null,
    mapDispatchToProps,
)(Security);
