import * as React from 'react';
import { View, Image, Text, TouchableOpacity } from 'react-native';
import { NavigationScreenProp, NavigationState } from 'react-navigation';

import styles from './styles';
import Button from '../common/button';
import { getImage } from '../../utils/image_holder';
import { StatusViewParams } from '../../core/actions/ui/app_status_view';

export interface SecurityProps {
    navigation: NavigationScreenProp<NavigationState>;
    setSuccessAppStatusView: StatusViewParams;
}

export default class SecurityComponent extends React.Component<SecurityProps, any> {
    onSkipClick = () => {
        this.props.navigation.navigate('Login');
    }

    onFingerprintClick = () => {
        this.props.setSuccessAppStatusView('Login', 'Fingerprint\nVerification Seccessful');
        this.props.navigation.navigate('StatusPage');
    }

    onFaceIDClick = () => {
        this.props.setSuccessAppStatusView('Login', 'Face ID\nVerification Seccessful');
        this.props.navigation.navigate('StatusPage');
    }

    render() {
        const logo = getImage('logo');
        const fingerprint = getImage('fingerprintButton');
        const faceID = getImage('faceID');

        return (
            <View style={styles.container}>
                <Image source={logo} style={styles.logo} />
                <Text style={styles.textCenter}>Would you like to secure your account with your biometrics?</Text>
                <View style={styles.iconContainer}>
                    <TouchableOpacity onPress={this.onFingerprintClick}>
                        <Image source={fingerprint} style={styles.icon} />
                        <Text style={styles.iconText}>Fingerprint</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={this.onFaceIDClick}>
                        <Image source={faceID} style={styles.icon} />
                        <Text style={styles.iconText}>Face ID</Text>
                    </TouchableOpacity>
                </View>
                <Button primary onPress={this.onSkipClick} text="Skip" />
            </View>
        );
    }
}
