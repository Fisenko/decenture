import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        padding: 20,
        justifyContent: 'space-around',
    },
    logo: {
        height: 100,
        width: 100,
        resizeMode: 'contain',
    },
    iconContainer: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-around',
    },
    icon: {
        height: 100,
        width: 100,
        resizeMode: 'contain',
    },
    iconText: {
        textAlign: 'center',
    },
    textCenter: {
        textAlign: 'center',
    },
});
