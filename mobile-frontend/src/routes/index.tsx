import { createBottomTabNavigator, createStackNavigator, createSwitchNavigator } from 'react-navigation';

import styles from './styles';
import TabBar from '../components/common/tab_bar/container';
import Funds from '../components/funds/index';
import HomeNavigator from '../components/home/router';
import TransactionsNavigator from '../components/transactions/router';
import AnalyticsNavigator from '../components/analytics/router';
import Login from '../components/login/container';
import Register from '../components/registration/main/container';
import PasswordConfirmation from '../components/registration/passwd_confirmation/container';
import EmailVerification from '../components/registration/email_confirmation/container';
import Security from '../components/security/container';
import AppStatusView from '../components/status_view/container';
import { buildNavigationOptions, buildRecordNavigationOptions } from '../utils/router_utils';
import { isPlatformAndroid } from '../utils/app_utils';
import RecordNavigator from '../components/record/router';
import AuthLoading from '../components/auth_loading/container';

const appRouteConfig = {
    Record: {
        screen: RecordNavigator,
        navigationOptions: ({ navigation }: { [key: string]: any }) =>
            buildRecordNavigationOptions('Record', 'icon-record', 'Record', navigation),
    },
    Funds: {
        screen: Funds,
        navigationOptions: ({ navigation }: { [key: string]: any }) =>
            buildNavigationOptions('Add Funds', 'icon-add_funds', 'Funds', navigation),
    },
    Home: {
        screen: HomeNavigator,
        navigationOptions: ({ navigation }: { [key: string]: any }) =>
            buildNavigationOptions('Home', 'icon-home', 'Home', navigation),
    },
    Transactions: {
        screen: TransactionsNavigator,
        navigationOptions: ({ navigation }: { [key: string]: any }) =>
            buildNavigationOptions('Transactions', 'icon-transaction', 'Transactions', navigation),
    },
    Analytics: {
        screen: AnalyticsNavigator,
        navigationOptions: ({ navigation }: { [key: string]: any }) =>
            buildNavigationOptions('More', 'icon-analytic', 'Analytics', navigation),
    },
};

const MainTabNavigator = createBottomTabNavigator(appRouteConfig, {
    initialRouteName: 'Home',
    tabBarComponent: isPlatformAndroid() ? TabBar : undefined,
    tabBarOptions: {
        style: styles.style,
        tabStyle: styles.tabStyle,
    },
});

const AppStack = createStackNavigator(
    {
        Root: MainTabNavigator,
        StatusPage: AppStatusView,
    },
    {
        initialRouteKey: 'Root',
        headerMode: 'none',
    },
);

const authRouteConfig = {
    Login: {
        screen: Login,
        navigationOptions: {
            header: null,
        },
    },
    Registration: {
        screen: Register,
        navigationOptions: {
            title: 'Registration',
        },
    },
    RegistrConfirmPass: {
        screen: PasswordConfirmation,
        navigationOptions: {
            title: 'PasswordConfirmation',
        },
    },
    EmailVerification: {
        screen: EmailVerification,
        navigationOptions: {
            title: 'EmailVerification',
        },
    },
    Security: {
        screen: Security,
    },
    StatusPage: {
        screen: AppStatusView,
        navigationOptions: {
            header: null,
        },
    },
};

const AuthStack = createStackNavigator(authRouteConfig, {
    headerMode: isPlatformAndroid() ? 'none' : 'screen',
    cardStyle: {
        backgroundColor: '#FFF',
        opacity: 1,
    },
});

export default createSwitchNavigator(
    {
        AuthLoading,
        Auth: AuthStack,
        App: AppStack,
    },
    {
        initialRouteName: 'AuthLoading',
    },
);
