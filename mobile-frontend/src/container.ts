import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';

import App from './App';
import { setUIConfig } from 'DecentureMobile/src/core/actions/ui';
import { checkLogin } from 'DecentureMobile/src/core/actions/app';
import { changeSocketStatus } from 'DecentureMobile/src/core/actions/ui/socket';

const mapStateToProps = (state: any) => ({
    appStateStatus: state.ui.configs.appState.status,
});

const mapDispatchToProps = (dispatch: Dispatch) =>
    bindActionCreators(
        {
            setUIConfig,
            checkLogin,
            changeSocketStatus,
        },
        dispatch,
    );

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(App);
