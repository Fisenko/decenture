import { Dimensions } from 'react-native';

const window = Dimensions.get('window');

export const IMAGE_HEIGHT = window.width / 3;
export const IMAGE_HEIGHT_SMALL = window.width / 7;
export const BOTTOM_BAR_ICON_HEIGHT = window.width / 15;
export const BOTTOM_BAR_ICON_WIDTH = BOTTOM_BAR_ICON_HEIGHT;

export const DECENTURE_COLOR = '#1E88E5';
export const STATUS_BAR_COLOR = '#1178D2';

export const TAB_BAR_PRESS_IN = DECENTURE_COLOR;
export const TAB_BAR_PRESS_OUT = '#B1B1B1';


export default {
    container: {
        backgroundColor: '#FFF',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        top: 15,
    },
    input: {
        height: 40,
        backgroundColor: 'transparent',
        marginHorizontal: 10,
        marginVertical: 5,
        width: window.width - 100,
        color: 'black',
        padding: 5,
        fontSize: 17,
        paddingBottom: 12,
    },
    logo: {
        height: IMAGE_HEIGHT,
        resizeMode: 'contain',
        marginBottom: 120,
        padding: 10,
        marginTop: 20,
    },
    bottom_bar_image: {
        height: BOTTOM_BAR_ICON_HEIGHT,
        resizeMode: 'contain',
        margin: 10,
    },
    button: {
        backgroundColor: '#538ED4',
        borderRadius: 40,
        width: window.width - 100,
        padding: 15,
        marginTop: 30,
        height: 50,
    },
    subtext: {
        color: '#538ED4',
        fontSize: 17,
        fontWeight: '100',
    },
};
