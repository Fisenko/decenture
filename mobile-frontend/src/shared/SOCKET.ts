import io from 'socket.io-client';

import { SocketState } from 'DecentureMobile/src/core/models/ui';
import { SocketStatus } from 'DecentureMobile/src/core/actions/ui/socket';

const debug: debug.IDebugger = require('debug')('decenture:socket');

// TODO: Fix bug when hot reload does not disconnect old instance

export class SOCKET {
    // TODO: extract to config
    private static port = process.env.REACT_NATIVE_BACKEND_PORT || 4601;
    private static host = process.env.REACT_NATIVE_BACKEND_HOST || 'localhost';
    private static _instance: SOCKET;
    private static _isOpen = false;
    private static _QUEUE: Array<any> = [];
    private static token: string = '';

    private static LISTENER_MAP: { [listener: string]: (data: any) => void } = {};
    public static ws: SocketIOClient.Socket;

    constructor() {
        debug('============ BACKEND URL: %s:%d ============', SOCKET.host, SOCKET.port);
        debug('============ ENVIRONMENTS: %s ============', JSON.stringify(process.env));

        if (SOCKET._instance) {
            return SOCKET._instance;
        }
        this.init();
        SOCKET._instance = this;
    }

    clearConnection = () => {
        if (SOCKET.ws) {
            SOCKET.ws.close();
            SOCKET._isOpen = false;
        }
    };

    emitQueue = () => {
        debug('EMIT QUEUE: %O', SOCKET._QUEUE);
        while (SOCKET._QUEUE.length > 0) {
            const query = SOCKET._QUEUE.shift();
            this.emit(query.code, query.data);
        }
    };

    init = () => {
        debug('INIT');
        this.clearConnection();

        SOCKET.ws = io(`ws://${SOCKET.host}:${SOCKET.port}`);

        SOCKET.ws.on('disconnect', () => {
            SOCKET._isOpen = false;
        });

        SOCKET.ws.on('OPEN', () => {
            debug('OPEN');
            SOCKET.ws.emit('CHECK_TOKEN', JSON.stringify({ token: SOCKET.token }));
        });

        SOCKET.ws.on('SET_TOKEN', (data: string) => {
            debug('SET_TOKEN: %s', data);
            const res = JSON.parse(data);
            SOCKET._isOpen = true;
            SOCKET.token = res.token;
            this.emitQueue();
        });

        SOCKET.ws.on('message', this.onMessage);
    };

    onMessage = (response: any) => {
        const { code, data, status, errMsg } = JSON.parse(response);
        debug('RESPONSE: %s %O [status: %d] %s', code, data, status, errMsg);
        if (status === 0 || status === 1) {
            try {
                if (code && data) {
                    if (typeof SOCKET.LISTENER_MAP[code] === 'function') {
                        SOCKET.LISTENER_MAP[code]({ status, data, errMsg });
                    } else {
                        debug('WS:MESSAGE_WITHOUT_HANDLER %s', code);
                    }
                }
            } catch (e) {
                // TODO: think about console.log() on production
                debug('WS:MESSAGE_PARSE_ERROR %O, error: %O', data, e);
            }
        } else {
            debug('WS:ERROR %s', errMsg);
        }
    };

    subscribe = (messageCode: any, func: any) => {
        debug('SUBSCRIBE: %s %O', messageCode, func);
        SOCKET.LISTENER_MAP[messageCode] = func;
    };

    emit = (code: string, data: any = '') => {
        if (SOCKET._isOpen) {
            debug('EMIT: %s %O', code, data);
            SOCKET.ws.emit('message', JSON.stringify({ code, token: SOCKET.token, data }));
        } else {
            debug('PUSH TO QUEUE: %s %O', code, data);
            SOCKET._QUEUE.push({ code, data });
        }
    };

    onStateChange = (callback: SocketStatus) => {
        SOCKET.ws.on('connect', () => callback(SocketState.Connected));
        SOCKET.ws.on('disconnect', () => callback(SocketState.Disconnected));
        SOCKET.ws.on('reconnecting', (attemptNumber: number) => callback(SocketState.Reconnecting, attemptNumber));
        SOCKET.ws.on('reconnect', () => callback(SocketState.Reconnected));
    };
}

const ws = new SOCKET();
ws.subscribe('ANSWER', (data: any) => debug('WS:ANSWER %O', data));
ws.subscribe('ERROR', (data: any) => debug('WS_BACKEND:ERROR %O', data.error));
ws.subscribe('OPEN_SUCCESS', (data: any) => {
});

export default ws;
