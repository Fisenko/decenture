const IMAGES: { [key: string]: any } = {
    logo: require('../../public/images/logo_no_name.png'),
    decentureLogo: require('../../public/images/logo_with_name.png'),
    analytics: require('../../public/images/analytics.png'),
    successfulLogo: require('../../public/images/successful.png'),
    notSuccessfulLogo: require('../../public/images/not_successful.png'),
    bottomBarRecord: require('../../public/images/record/record.png'),
    bottomBarFunds: require('../../public/images/add_funds.png'),
    bottomBarHome: require('../../public/images/logo_main.png'),
    bottomBarTransactions: require('../../public/images/transactions.png'),
    Invoice: require('../../public/images/money.png'),
    money: require('../../public/images/money.png'),
    coffee: require('../../public/images/coffee.png'),
    room: require('../../public/images/room.png'),
    settings: require('../../public/images/header/settings.png'),
    notification: require('../../public/images/header/notification.png'),
    fingerprint: require('../../public/images/record/finger.png'),
    recordNFC: require('../../public/images/record/record_lecture.png'),
    faceID: require('../../public/images/security/faceID.png'),
    fingerprintButton: require('../../public/images/security/fingerprint.png'),
    send: require('../../public/images/home/send.png'),
    receive: require('../../public/images/home/receive.png'),

    // Card
    cvc: require('DecentureMobile/public/images/funds/stp_card_cvc.png'),
    cvc_amex: require('DecentureMobile/public/images/funds/stp_card_cvc_amex.png'),
    'american-express': require('DecentureMobile/public/images/funds/stp_card_amex.png'),
    'diners-club': require('DecentureMobile/public/images/funds/stp_card_diners.png'),
    mastercard: require('DecentureMobile/public/images/funds/stp_card_mastercard.png'),
    discover: require('DecentureMobile/public/images/funds/stp_card_discover.png'),
    jcb: require('DecentureMobile/public/images/funds/stp_card_jcb.png'),
    placeholder: require('DecentureMobile/public/images/funds/stp_card_unknown.png'),
    visa: require('DecentureMobile/public/images/funds/stp_card_visa.png'),

    add: require('DecentureMobile/public/images/funds/add.png'),
    bank: require('DecentureMobile/public/images/funds/bank.png'),
    paypal: require('DecentureMobile/public/images/funds/paypal.png'),
    apple_pay: require('DecentureMobile/public/images/funds/apple_pay.png'),

    imageFront: require('DecentureMobile/public/images/funds/card-front.png'),
    imageBack: require('DecentureMobile/public/images/funds/card-back.png'),

    // Analytics active
    attendance: require('../../public/images/analytics/attandance.png'),
    certificate: require('../../public/images/analytics/certificates.png'),
    payment: require('../../public/images/analytics/payments.png'),
    schedule: require('../../public/images/analytics/schedule.png'),
    transcripts: require('../../public/images/analytics/transcripts.png'),
    message: require('../../public/images/analytics/message.png'),
    insurance: require('../../public/images/analytics/insurance.png'),
    offers: require('../../public/images/analytics/offers.png'),

    // Analytics inactive
    attendanceInactive: require('../../public/images/analytics/attandance_inactive.png'),
    certificateInactive: require('../../public/images/analytics/certificates_inactive.png'),
    paymentInactive: require('../../public/images/analytics/payments_inactive.png'),
    scheduleInactive: require('../../public/images/analytics/schedule_inactive.png'),
    transcriptsInactive: require('../../public/images/analytics/transcripts_inactive.png'),
    messageInactive: require('../../public/images/analytics/message_inactive.png'),
    insuranceInactive: require('../../public/images/analytics/insurance_inactive.png'),
    offersInactive: require('../../public/images/analytics/offers_inactive.png'),
};

export const getImage = (name: string) => {
    return IMAGES[name];
};
