import { createIconSetFromFontello } from 'react-native-vector-icons';

import fontelloConfig from './font_config.json';

export default createIconSetFromFontello(fontelloConfig);
