import LectureAttendance from 'DecentureMobile/src/core/models/attendance/lectureAttendance';
import moment from 'moment';
import TransactionModel from 'DecentureMobile/src/core/models/transaction';
import Spending from 'DecentureMobile/src/core/models/spending/spending';
import {
    CHART_COLORS,
    compareDateTime,
    getRandomColor,
    groupAndAggregateBy,
} from 'DecentureMobile/src/utils/chart_utils';
import { extractDayNumber, isDateInCurrentMonth } from 'DecentureMobile/src/utils/datetime_utils';

export const convertAttendanceHistory = (history: LectureAttendance[]) => {

    const historyByDate: Map<string, Array<LectureAttendance>> = history.reduce(
        (accumulator: any, lectureAttendance: LectureAttendance) => {
            const date = moment(lectureAttendance.datetime).format('DD/MM/YYYY');

            let dateMap = accumulator.get(date);
            if (dateMap) {
                dateMap.push(lectureAttendance);
            } else {
                dateMap = new Array<LectureAttendance>();
                dateMap.push(lectureAttendance);
                accumulator.set(date, dateMap);
            }
            return accumulator;
        },
        new Map<string, Array<LectureAttendance>>(),
    );

    return Array
        .from(historyByDate.keys())
        .map((key: string) => ({ date: key, data: historyByDate.get(key) }));
};

export const convertSpendingData = (transactions: TransactionModel[]) => {

    if (transactions.length === 0) {
        return new Spending();
    }

    const currentMonthTransactions = transactions
        .filter((tx: TransactionModel) => isDateInCurrentMonth(tx.datetime))
        .sort((a, b) => compareDateTime(a.datetime, b.datetime));

    const spending = new Spending();

    const pieData = preparePieData(currentMonthTransactions);
    const lineData = prepareLineData(currentMonthTransactions);
    const total = lineData.reduce((accumulator: any, data: any) => {
        return accumulator += data.value;
    }, 0);

    spending.pieData = pieData;
    spending.lineData = lineData;
    spending.total = total;

    return spending;
};


const preparePieData = (transactions: TransactionModel[]) => {

    const aggregatedPieData = groupAndAggregateBy(transactions, 'description', 'amount');

    return Array
        .from(aggregatedPieData.keys())
        .map((key, index) => {
                const value = aggregatedPieData.get(key) || 0;
                const color = CHART_COLORS[index]
                    ? CHART_COLORS[index]
                    : getRandomColor();

                return {
                    value,
                    color,
                    name: key,
                    svg: {
                        fill: color,
                    },
                    key: `pie-${index}`,
                };
            }
        );
};

const prepareLineData = (transactions: TransactionModel[]) => {

    const aggregatedLineData = groupAndAggregateBy(transactions, 'datetime', 'amount',
        extractDayNumber);

    const lineData = [];

    for (let i = 1; i < moment().daysInMonth(); i++) {
        const key = String(i);
        let item = { value: 0, date: i };

        if (aggregatedLineData.get(key)) {
            item = {
                value: aggregatedLineData.get(key) || 0,
                date: i,
            };
        }
        lineData.push(item);
    }

    return lineData;
};


