import moment from 'moment';

export const isDateInCurrentMonth = (date: Date) => {
    const momentTime = moment(date);
    const startMonth = moment().startOf('month');
    const endMonth = moment().endOf('month');

    return (momentTime.isSame(startMonth) || momentTime.isSame(endMonth) ||
        (momentTime.isBefore(endMonth) && (momentTime.isAfter(startMonth))));
};

export const extractDayNumber = (value: string) => {
    return moment(value).format('D');
};
