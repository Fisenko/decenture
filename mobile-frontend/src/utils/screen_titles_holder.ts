const SCREEN_TITLES: { [key: string]: string } = {
    Home: 'Home',
    Transactions: 'Transaction History',
    Record: 'Ready to Scan!',
    Analytics: 'Analytics',
    Funds: 'Top Up',
    AttendanceHistory: 'Attendance History'
};

export const getTitle = (routeName: string) =>
    SCREEN_TITLES[routeName];
