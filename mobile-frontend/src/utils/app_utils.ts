import { Platform } from 'react-native';

export const isPlatformAndroid = () => {
    return Platform.OS === 'android';
};
