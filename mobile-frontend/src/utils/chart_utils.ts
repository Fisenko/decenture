import TransactionModel from 'DecentureMobile/src/core/models/transaction';

const baseColor = '#98989C';

export const enum ORDER {
    ASC = 1,
    DESC = -1,
}

export const CHART_COLORS = [
    '#F46A63', '#ED4A82', '#AF52BF', '#8561C5', '#6676C1',
    '#56ACF2', '#32C9DC', '#32AA9F', '#6FBE72', '#8BC34A',
    '#CDDC39', '#FFEB3B', '#FFCD38', '#FFBC5A', '#FF784E'
];

export const convertToBarChartData = (attendanceHistory: Array<any>) => {
    return attendanceHistory.map((value, index) => {
        const color = CHART_COLORS[index];
        return {
            ...value,
            svg: {
                fill: color ? color : getRandomColor(),
            },
        };
    });
};

const getMathRandomColor = () => {
    return Math.floor(Math.random() * 256);
};

export const getRandomColor = () => {
    return `rgb(${getMathRandomColor()},${getMathRandomColor()},${getMathRandomColor()})`;
};

export const groupAndAggregateBy = (data: Array<any>,
                                    groupBy: string,
                                    aggregateBy: string,
                                    convertGroupField?: Function): Map<string, number> => {

    return data.reduce((accumulator: any, item: any) => {

            const type = convertGroupField
                ? convertGroupField(item[groupBy])
                : item[groupBy];

            let aggregatedValue = accumulator.get(type);

            aggregatedValue
                ? accumulator.set(type, aggregatedValue += item[aggregateBy])
                : accumulator.set(type, item[aggregateBy]);

            return accumulator;
        },
        new Map<string, number>(),
    );

};

export const compareDateTime = (a: Date, b: Date, order: ORDER = ORDER.ASC) => {
    const aTime = new Date(a).getTime();
    const bTime = new Date(b).getTime();

    if (aTime > bTime) {
        return 1 * order;
    }
    if (aTime < bTime) {
        return -1 * order;
    }
    return 0;
};
