import { Platform } from 'react-native';

import NfcManager, { NdefParser, TagEvent } from 'react-native-nfc-manager';

export const startNfc = () => {
    NfcManager.start({
        onSessionClosedIOS: () => {
            console.log('ios session closed');
        }
    })
    .then(result => {
        console.log('start OK', result);
    })
    .catch(error => {
        console.warn('start fail', error);
    });

    if (Platform.OS === 'android') {
        NfcManager.getLaunchTagEvent()
        .then(tag => {
            console.log('launch tag', tag);
        })
        .catch(err => {
            console.log(err);
        });
        NfcManager.isEnabled()
        .then(enabled => {
            console.log('enabled');
        })
        .catch(err => {
            console.log(err);
        });
        NfcManager.onStateChanged(
            event => {
                if (event.state === 'on') {
                    console.log('enabled');
                } else if (event.state === 'off') {
                    console.log('not enabled');
                } else if (event.state === 'turning_on') {
                    console.log('not enabled');
                } else if (event.state === 'turning_off') {
                    console.log('not enabled');
                }
            }
        )
        .then(sub => {
            console.log(sub);
        })
        .catch((err: any) => {
            console.warn(err);
        });
    }
};

export const startDetection = (listener: (tag: TagEvent) => void) => {
    NfcManager.registerTagEvent(listener)
    .then(result => {
        console.log('registerTagEvent OK', result);
    })
    .catch(error => {
        console.warn('registerTagEvent fail', error);
    });
};

export const stopDetection = () => {
    NfcManager.unregisterTagEvent()
    .then(result => {
        console.log('unregisterTagEvent OK', result);
    })
    .catch(error => {
        console.warn('unregisterTagEvent fail', error);
    });
};


export const extractTextFromTag = (tag: TagEvent) => {
    return NdefParser.parseText(tag.ndefMessage[0]);
};

