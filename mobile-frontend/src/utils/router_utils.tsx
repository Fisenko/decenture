import * as React from 'react';

import { getTitle } from 'DecentureMobile/src/utils/screen_titles_holder';
import { DECENTURE_COLOR } from 'DecentureMobile/src/common/styles';
import Header from 'DecentureMobile/src/components/common/header';
import { isPlatformAndroid } from 'DecentureMobile/src/utils/app_utils';
import { NavigationScreenProp, NavigationState } from 'react-navigation';
import BottomTabComponent from 'DecentureMobile/src/components/common/tab_bar/bar_component';
import RecordTab from 'DecentureMobile/src/components/common/tab_bar/record';

export const buildNavigationOptions = (title: string, imageName: string, navigateTo: string,
                                       navigation: NavigationScreenProp<NavigationState>) => ({
    title,
    tabBarButtonComponent: () =>
        (<BottomTabComponent title={title} imageName={imageName} navigateTo={navigateTo} navigation={navigation}/>)
});

export const buildRecordNavigationOptions = (title: string, imageName: string, navigateTo: string,
                                             navigation: NavigationScreenProp<NavigationState>) => ({
    title,
    tabBarButtonComponent: () =>
        (<RecordTab title={title} imageName={imageName} navigateTo={navigateTo} navigation={navigation}/>)
});

export const headerNavigationOptions = {
    headerTintColor: 'white',
    headerTitleStyle: {
        color: 'white',
        fontSize: 17,
    },
    headerStyle: {
        marginTop: isPlatformAndroid() ? 0 : 10,
        backgroundColor: DECENTURE_COLOR,
    },
    headerRight: <Header />,
};

const extractRouteName = (state: any): string => {
    if (state.index !== undefined) {
        return extractRouteName(state.routes[state.index]);
    }

    return state.routeName;
};

export const withTitle = (navigator: any) => {
    navigator.navigationOptions = (props: any) => {
        const routeName = extractRouteName(props.navigation.state);
        return {
            title: getTitle(routeName),
            index: props.navigation.state.index,
        };
    };

    return navigator;
};
