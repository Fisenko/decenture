export const DATE_FORMAT = 'DD/MM/YYYY';
export const TIME_FORMAT = 'hh:mm A';
export const DATETIME_FORMAT = `${DATE_FORMAT} ${TIME_FORMAT}`;
