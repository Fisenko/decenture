import { hot, setConfig } from 'react-hot-loader';

import * as React from 'react';
import { getTheme, ThemeContext } from 'react-native-material-ui';
import { AppState, AppStateStatus, View } from 'react-native';

import { MobileAppState } from 'DecentureMobile/src/core/models/ui';
import ws from 'DecentureMobile/src/shared/SOCKET';
import Navigation from './components/navigation/container';
import ConnectionBar from 'DecentureMobile/src/components/common/connection_bar/container';
import { SocketStatus } from 'DecentureMobile/src/core/actions/ui/socket';
import { SocketState } from 'DecentureMobile/src/core/models/ui';
import SessionStatus from 'DecentureMobile/src/components/session_status';

interface Props {
    checkLogin(): any;
    changeSocketStatus: SocketStatus;
    setUIConfig: any;

    appStateStatus: AppStateStatus;
}

class App extends React.Component<Props, any> {
    constructor(props: any) {
        super(props);
        console.disableYellowBox = true;
    }

    componentDidMount() {
        AppState.addEventListener('change', this.handleAppStateChange);
        ws.onStateChange(this.handleSocketStatusChange);
    }

    componentWillUnmount() {
        AppState.removeEventListener('change', this.handleAppStateChange);
    }

    handleAppStateChange = (nextAppState: AppStateStatus) => {
        const pattern = new RegExp(`${MobileAppState.BACKGROUND}`);
        if (this.props.appStateStatus.match(pattern) && nextAppState === 'active') {
            ws.init();
            ws.onStateChange(this.handleSocketStatusChange);
            this.props.checkLogin();
        }
        this.props.setUIConfig('appState', 'status', nextAppState);
    };

    handleSocketStatusChange: SocketStatus = (state, attemptNumber) => {
        this.props.changeSocketStatus(state, attemptNumber);
        if (state === SocketState.Reconnected) {
            this.props.checkLogin();
        }
    };

    render() {
        return (
            <ThemeContext.Provider value={getTheme(uiTheme)}>
                <View style={{ flex: 1 }}>
                    <ConnectionBar />
                    <SessionStatus />
                    <Navigation />
                </View>
            </ThemeContext.Provider>
        );
    }
}

const uiTheme = {
    palette: {
        primaryColor: '#FFF',
    },
    toolbar: {
        container: {
            height: 50,
        },
    },
};

// @ts-ignore
if (module.hot) {
    setConfig({ logLevel: 'no-errors-please' });
    module.id = './src/AppWrapper.tsx';
}

export default hot(module)(App);
