import { NavigationActions } from 'react-navigation';

import { CHECK_LOGIN, CHECK_LOGIN_RESPONSE } from 'DecentureMobile/src/common/socket_channels';
import { LOGIN_STUDENT_SUCCESS, LOGOUT_STUDENT_SUCCESS } from 'DecentureMobile/src/core/actions/user/actionTypes';
import ws from 'DecentureMobile/src/shared/SOCKET';

export const checkLogin = (user: any) => (dispatch: any) => {
    ws.emit(CHECK_LOGIN, user);
    ws.subscribe(CHECK_LOGIN_RESPONSE, (res: any) => {
        if (res.status === 0) {
            dispatch({
                type: LOGIN_STUDENT_SUCCESS,
                payload: res.data,
            });
            dispatch(NavigationActions.navigate({ routeName: 'App' }));
        } else {
            dispatch(NavigationActions.navigate({ routeName: 'Auth' }));
        }
    });
};
