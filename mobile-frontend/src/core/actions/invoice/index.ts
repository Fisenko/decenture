import * as SocketChannel from 'DecentureMobile/src/common/socket_channels';
import * as InvoiceActionTypes from 'DecentureMobile/src/core/actions/invoice/actionTypes';
import ws from 'DecentureMobile/src/shared/SOCKET';
import {
    setFailedAppStatusView,
    setLoadingAppStatusView,
    setSuccessAppStatusView,
} from 'DecentureMobile/src/core/actions/ui/app_status_view';
import { setUIConfig } from 'DecentureMobile/src/core/actions/ui';
import Invoice from 'DecentureMobile/src/core/models/invoice';

export const getInvoices = () => (dispatch: any, getState: any) => {
    dispatch(setUIConfig('invoices', 'isRefreshing', true));
    const user = getState().user.current.info;

    ws.emit(SocketChannel.GET_USER_INVOICES, user);

    ws.subscribe(SocketChannel.GET_USER_INVOICES_RESPONSE, (res: any) => {
        if (res.status === 0) {
            dispatch({
                type: InvoiceActionTypes.GET_INVOICES_SUCCESS,
                payload: res.data,
            });
        } else {
            // TODO: Add error message
        }
        dispatch(setUIConfig('invoices', 'isRefreshing', false));
    });
};

export const selectInvoice = (invoice: Invoice) => ({
    type: InvoiceActionTypes.SELECT_INVOICE,
    payload: invoice,
});

export const payInvoice = (invoice: Invoice) => (dispatch: any) => {
    dispatch(setLoadingAppStatusView('Transactions'));

    ws.emit(SocketChannel.PAY_INVOICE, invoice);

    ws.subscribe(SocketChannel.PAY_INVOICE_RESPONSE, (res: any) => {
        if (res.status === 0) {
            dispatch(setSuccessAppStatusView('Transactions', 'Transaction Successful'));
            dispatch({
                type: InvoiceActionTypes.PAY_INVOICE_SUCCESS,
                payload: invoice,
            });
        } else {
            dispatch(setFailedAppStatusView('Transactions', res.errMsg, true));
            dispatch({
                type: InvoiceActionTypes.PAY_INVOICE_FAILED,
                payload: res.data,
            });
        }
    });
};
