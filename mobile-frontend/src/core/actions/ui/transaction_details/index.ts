import { setUIConfig } from '../index';
import { TransactionType } from 'DecentureMobile/src/core/models/transaction';

export const setDetailsVisible = () =>
    setUIConfig('transactions', 'isDetailsVisible', true);

export const setDetailsInvisible = () =>
    setUIConfig('transactions', 'isDetailsVisible', false);

export const setRecentDetailsVisible = () =>
    setUIConfig('transactions', 'isRecentDetailsVisible', true);

export const setRecentDetailsInvisible = () =>
    setUIConfig('transactions', 'isRecentDetailsVisible', false);

export const setTxSelectedType = (txType: TransactionType) =>
    setUIConfig('transactions', 'txType', txType);

export const setTxTabOpen = (isOpen: boolean) =>
    setUIConfig('transactions', 'isTxTabOpen', isOpen);

export const setInvoiceTabOpen = (isOpen: boolean) =>
    setUIConfig('transactions', 'isInvoiceTabOpen', isOpen);
