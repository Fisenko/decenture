import { setUIConfig } from '../index';

export const setDatePickerVisible = (visible: boolean) =>
    setUIConfig('registration', 'isDatePickerVisible', visible);
