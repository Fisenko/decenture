import { setUIConfig } from '../index';

export const setVisible = () =>
    setUIConfig('tabBar', 'isVisible', true);

export const setInvisible = () =>
    setUIConfig('tabBar', 'isVisible', false);

export const setActiveSceneName = (sceneName: string) =>
    setUIConfig('tabBar', 'activeSceneName', sceneName);
