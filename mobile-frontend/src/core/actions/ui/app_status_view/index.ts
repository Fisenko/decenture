import { StatusViewPage } from 'DecentureMobile/src/core/models/AppStatusView';
import { getImage } from 'DecentureMobile/src/utils/image_holder';
import { setUIConfig } from '../index';

export type StatusViewParams = (
    navigateToScreen: string,
    text?: string,
    withNavigation?: boolean,
) => any;

export const setSuccessAppStatusView: StatusViewParams = (
    navigateToScreen,
    text,
) =>
    setUIConfig('AppStatusView', 'currInfo', {
        status: StatusViewPage.SUCCESS,
        logo: getImage('successfulLogo'),
        navigateToScreen: navigateToScreen,
        label: text,
    });

export const setFailedAppStatusView: StatusViewParams = (
    navigateToScreen,
    text,
    withNavigation,
) =>
    setUIConfig('AppStatusView', 'currInfo', {
        status: StatusViewPage.FAILED,
        logo: getImage('notSuccessfulLogo'),
        navigateToScreen: navigateToScreen,
        label: text,
        withNavigation,
    });

export const setLoadingAppStatusView: StatusViewParams = navigateToScreen =>
    setUIConfig('AppStatusView', 'currInfo', {
        status: StatusViewPage.LOADING,
        navigateToScreen: navigateToScreen,
        label: 'Loading...',
    });
