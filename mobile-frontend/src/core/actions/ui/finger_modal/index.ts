import { setUIConfig } from '../index';

export const setFingerprintModalVisible = (visible: boolean) =>
    setUIConfig('record', 'isFingerModalVisible', visible);

export const setNfcSupport = (support: boolean) =>
    setUIConfig('record', 'isNfcSupported', support);




