import { SET_UI_CONFIG } from './actionTypes';

export const setUIConfig = (systemKey: string, field: string | number, value: any) => ({
    type: SET_UI_CONFIG,
    systemKey,
    field,
    value,
});
