import { SocketState } from 'DecentureMobile/src/core/models/ui';
import { setUIConfig } from '../index';

export type SocketStatus = (state: SocketState, attemptNumber?: number) => any;

export const changeSocketStatus: SocketStatus = (state, attemptNumber) =>
    setUIConfig('socket', 'status', { state, attemptNumber });
