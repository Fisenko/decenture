import * as AttendanceActionTypes from './actionTypes';
import * as SocketChannels from 'DecentureMobile/src/common/socket_channels';
import ws from 'DecentureMobile/src/shared/SOCKET';
import {
    setFailedAppStatusView,
    setLoadingAppStatusView,
    setSuccessAppStatusView,
} from 'DecentureMobile/src/core/actions/ui/app_status_view';
import { setErrorConfig } from 'DecentureMobile/src/core/actions/error';
import { setUIConfig } from 'DecentureMobile/src/core/actions/ui';


export const getUserAttendance = (data?: any) => (dispatch: any, getState: any) => {
    dispatch(setUIConfig('attendance', 'isRefreshing', true));
    ws.emit(SocketChannels.GET_USER_ATTENDANCE);

    ws.subscribe(SocketChannels.GET_USER_ATTENDANCE_RESPONSE, (res: any) => {
        dispatch(setUIConfig('attendance', 'isRefreshing', false));
        res.status === 0 ?
            dispatch({
                type: AttendanceActionTypes.GET_ATTENDANCE_HISTORY_COMPLETE,
                payload: res.data,
            }) :
            dispatch({
                type: AttendanceActionTypes.GET_ATTENDANCE_HISTORY_REJECT,
                payload: res.errMsg,
            });
    });
};

export const attendanceRecordInputChange = (data: { [key: string]: any }) => ({
    type: AttendanceActionTypes.ATTENDANCE_RECORD_INPUT_CHANGE,
    payload: data,
});

export const recordAttendance = () => (dispatch: any, getState: any) => {

    const attendanceRecord = getState().user.current.student.attendance.current.info;

    dispatch(setLoadingAppStatusView('Record'));

    if (attendanceRecord.isAuth) {
        ws.emit(SocketChannels.RECORD_ATTENDANCE, { nfcId: attendanceRecord.nfcId });
    }

    dispatch(setDefaultAttendanceRecord());

    ws.subscribe(SocketChannels.RECORD_ATTENDANCE_RESPONSE, (res: any) => {
        if (res.status === 0) {
            dispatch(setSuccessAppStatusView('Record', 'Attendance Recorded'));
            dispatch({
                type: AttendanceActionTypes.RECORD_ATTENDANCE_SUCCESS,
                payload: res.data,
            });
        } else {
            dispatch(setFailedAppStatusView('Record', 'Attendance Failed'));
            dispatch({
                type: AttendanceActionTypes.RECORD_ATTENDANCE_FAILED,
                payload: res.errMsg,
            });
        }
    });
};

export const setDefaultAttendanceRecord = (data: { [key: string]: any } = {}) => (dispatch: any) => {
    dispatch({
        type: AttendanceActionTypes.SET_DEFAULT_ATTENDANCE_RECORD,
        payload: data,
    });

    dispatch(setErrorConfig('record', 'nfc', ''));
    dispatch(setErrorConfig('record', 'fingerprint', ''));
};

export const setAttendanceNfcScanStatus = (status: boolean) => ({
    type: AttendanceActionTypes.RECORD_ATTENDANCE_NFC_STATUS_CHANGE,
    payload: status,
});
