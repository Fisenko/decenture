import { SET_ERROR_CONFIG } from './actionTypes';

export const setErrorConfig = (systemKey: string, field: string | number, value: any) => ({
    type: SET_ERROR_CONFIG,
    systemKey,
    field,
    value
});

export const setComponentDefaultErrors = (systemKey: string, errors: { [key: string]: any }) => (dispatch: any) => {

    Object.keys(errors).forEach((errorField: string) => {
            if (errors[errorField]) {
                dispatch(setErrorConfig(systemKey, errorField, ''));
            }
        }
    );
};

