import { NavigationScreenProp, NavigationState, NavigationActions } from 'react-navigation';

import * as UserActionTypes from './actionTypes';
import * as SocketChannels from 'DecentureMobile/src/common/socket_channels';
import ws from 'DecentureMobile/src/shared/SOCKET';
import {
    setFailedAppStatusView,
    setLoadingAppStatusView,
    setSuccessAppStatusView,
} from 'DecentureMobile/src/core/actions/ui/app_status_view';
import { setComponentDefaultErrors, setErrorConfig } from 'DecentureMobile/src/core/actions/error';
import { AppUser } from 'DecentureMobile/src/core/models/AppUser';

export const setRegistrationError = (errMsg: string) => setErrorConfig('registration', 'errorMsg', errMsg);

export const registration = (user: AppUser) => (dispatch: any) => {
    dispatch(setErrorConfig('registration', 'confirmPassword', ''));

    ws.emit(SocketChannels.REGISTRATION, user);

    ws.subscribe(SocketChannels.REGISTRATION_RESPONSE, (res: any) => {
        if (res.status === 0) {
            dispatch({
                type: UserActionTypes.REGISTRATION_COMPLETED,
                payload: res.data,
            });
        } else {
            dispatch({
                type: UserActionTypes.REGISTRATION_REJECTED,
                payload: res.data,
            });
        }
    });
};

export const onRegistrationNextButtonClicked = (data: AppUser, navigation: NavigationScreenProp<NavigationState>) => (
    dispatch: any,
) => {
    dispatch(setErrorConfig('registration', 'confirmPassword', ''));

    ws.emit(SocketChannels.CHECK_USER_EXISTS, data);

    ws.subscribe(SocketChannels.CHECK_USER_EXISTS_RESPONSE, (res: any) => {
        if (res.status === 0) {
            dispatch({
                type: UserActionTypes.REGISTRATION_NEXT_BUTTON_CLICK,
                payload: res,
            });
            navigation.navigate('RegistrConfirmPass');
        } else {
            dispatch(setErrorConfig('registration', 'errorMsg', res.errMsg));
        }
    });
};

export const onRegistrationInputChange = (data: { [key: string]: any }) => (dispatch: any, getState: any) => {
    const errors = getState().errors.configs.registration;

    setComponentDefaultErrors('registration', errors)(dispatch);

    dispatch({
        type: UserActionTypes.REGISTRATION_INPUT_CHANGE,
        payload: data,
    });
};

export const loginInputChange = (data: any) => (dispatch: any, getState: any) => {
    const errors = getState().errors.configs.login;

    if (errors.errorMsg) {
        dispatch(setErrorConfig('login', 'errorMsg', ''));
    }

    dispatch({
        type: UserActionTypes.LOGIN_INPUT_CHANGE,
        payload: data,
    });
};

export const loginStudent = (user: any) => (dispatch: any) => {
    dispatch(setErrorConfig('login', 'errorMsg', ''));

    ws.emit(SocketChannels.LOGIN, user);

    ws.subscribe(SocketChannels.LOGIN_RESPONSE, (res: any) => {
        if (res.status === 0) {
            dispatch({
                type: UserActionTypes.LOGIN_STUDENT_SUCCESS,
                payload: res.data,
            });
            dispatch(NavigationActions.navigate({ routeName: 'App' }));
        } else {
            dispatch(setErrorConfig('login', 'errorMsg', res.errMsg));
        }
    });
};

export const logout = (user: any) => (dispatch: any) => {
    ws.emit(SocketChannels.LOGOUT, user);

    dispatch({ type: UserActionTypes.LOGOUT_STUDENT_SUCCESS });
    dispatch(NavigationActions.navigate({ routeName: 'Auth' }));
};

export const onUserVerification = () => (dispatch: any) => {
    dispatch(setLoadingAppStatusView('Login'));

    ws.subscribe(SocketChannels.VERIFIED_USER_RESPONSE, (res: any) => {
        if (res.status === 0) {
            dispatch(setSuccessAppStatusView('App', 'Verification Successful!'));
            dispatch({
                type: UserActionTypes.VERIFIED_USER_SUCCESS,
                payload: res,
            });
            dispatch(NavigationActions.navigate({ routeName: 'App' }));
        } else {
            dispatch(setFailedAppStatusView('Login', 'Cannot verify user!'));
        }
    });
};
