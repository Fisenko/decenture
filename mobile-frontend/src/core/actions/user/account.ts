import * as SocketChannels from 'DecentureMobile/src/common/socket_channels';
import * as actionTypes from './actionTypes';
import ws from 'DecentureMobile/src/shared/SOCKET';

export const getAccounts = () => (dispatch: any) => {
    ws.emit(SocketChannels.GET_USER_ACCOUNTS);

    ws.subscribe(SocketChannels.GET_USER_ACCOUNTS_RESPONSE, (res: any) => {
        if (res.status === 0) {
            dispatch({
                type: actionTypes.GET_ACCOUNTS_SUCCESS,
                payload: res.data,
            });
        } else {
        }
    });
};
