import ws from 'DecentureMobile/src/shared/SOCKET';

import * as ActionTypes from './actionTypes';
import * as SocketChannels from 'DecentureMobile/src/common/socket_channels';

import {
    setFailedAppStatusView,
    setLoadingAppStatusView,
    setSuccessAppStatusView,
} from 'DecentureMobile/src/core/actions/ui/app_status_view';

import { Card } from 'DecentureMobile/src/core/models/payment/Card';
import { OneToOnePayment } from 'DecentureMobile/src/core/models/payment/OneToOnePayment';
import { Dispatch } from 'redux';


export const cardInputChange = (data: any) => {
    const card = new Card(data.values);
    card.valid = data.valid;

    return {
        type: ActionTypes.CARD_INPUT_CHANGE,
        payload: card,
    };
};

export const bankInputChange = (data: any) => {
    return {
        type: ActionTypes.BANK_INPUT_CHANGE,
        payload: data,
    };
};

export const depositInputChange = (data: any) => ({
    type: ActionTypes.DEPOSIT_INPUT_CHANGE,
    payload: data,
});

export const getAmount = (data: any) => (dispatch: Dispatch) => {
    ws.emit(SocketChannels.GET_AMOUNT_BY_TOKEN, data);

    ws.subscribe(SocketChannels.GET_AMOUNT_BY_TOKEN_RESPONSE, (res: any) => {
        dispatch({
            type: ActionTypes.UPDATE_AMOUNT_INFO,
            payload: res.data,
        });
    });
};

export const initDepositTransaction = (data: any) => (dispatch: Dispatch) => {

    dispatch(setLoadingAppStatusView('Funds'));

    ws.emit(SocketChannels.ADD_DEPOSIT_AMOUNT_TO_PERSONAL_ACCOUNT_BY_APP_USER_ID, data);

    ws.subscribe(SocketChannels.ADD_DEPOSIT_AMOUNT_TO_PERSONAL_ACCOUNT_BY_APP_USER_ID_RESPONSE, (res: any) => {
        if (res.status === 0) {
            dispatch(setSuccessAppStatusView('Home', 'Deposit added successful'));
            dispatch({
                type: ActionTypes.UPDATE_AMOUNT_INFO,
                payload: res.data,
            });
        } else {
            dispatch(setFailedAppStatusView('Funds', res.errMsg, true));
        }
    });
};

export const saveCard = (card: Card) => (dispatch: Dispatch) => {
    dispatch(setLoadingAppStatusView('Funds'));

    ws.emit(SocketChannels.SAVE_CARD, card);
    ws.subscribe(SocketChannels.SAVE_CARD_RESPONSE, (res: any) => {
        if (res.status === 0) {
            dispatch(setSuccessAppStatusView('Funds', 'Card added'));
            card.id = res.data.id;
            dispatch({
                type: ActionTypes.UPDATE_CARD_LIST,
                payload: card,
            });
        } else {
            dispatch(setFailedAppStatusView('AddCard', res.errMsg, true));
            dispatch({
                type: ActionTypes.CARD_INPUT_CHANGE,
                payload: new Card(),
            });
        }
    });
};

export const getAllCardsNumberByToken = () => (dispatch: Dispatch) => {
    ws.emit(SocketChannels.GET_CARDS_NUMBER_BY_APP_USER_TOKEN, {});

    ws.subscribe(SocketChannels.GET_CARDS_NUMBER_BY_APP_USER_TOKEN_RESPONSE, (res: any) => {
        dispatch({
            type: ActionTypes.GET_CARDS_NUMBER_BY_APP_USER_TOKEN_SUCCESS,
            payload: res.data,
        });
    });
};

export const oneToOnePaymentInputChange = (data: any) => ({
    type: ActionTypes.ONE_TO_ONE_PAYMENT_INPUT_CHANGE,
    payload: data,
});

export const sendFunds = (payment: OneToOnePayment) => (dispatch: Dispatch) => {
    dispatch(setLoadingAppStatusView('Home'));

    ws.emit(SocketChannels.SEND_FUNDS, payment);

    ws.subscribe(SocketChannels.SEND_FUNDS_RESPONSE, (res: any) => {
        if (res.status === 0) {
            dispatch(setSuccessAppStatusView('Home', 'Transaction Successful'));
            dispatch({
                type: ActionTypes.ONE_TO_ONE_PAYMENT_SUCCESS,
                payload: payment,
            });
        } else {
            dispatch(setFailedAppStatusView('Home', res.errMsg, true));
            dispatch({
                type: ActionTypes.ONE_TO_ONE_PAYMENT_FAILED,
                payload: res.data,
            });
        }
    });
};
