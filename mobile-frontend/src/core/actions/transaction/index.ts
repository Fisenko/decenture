import ws from 'DecentureMobile/src/shared/SOCKET';
import { GET_TRANSACTION_SUCCESS, SELECT_TRANSACTION } from 'DecentureMobile/src/core/actions/transaction/actionTypes';
import { GET_USER_TRANSACTIONS, GET_USER_TRANSACTIONS_RESPONSE } from 'DecentureMobile/src/common/socket_channels';
import { setUIConfig } from 'DecentureMobile/src/core/actions/ui';

export const getTransactions = () => (dispatch: any, getState: any) => {
    // TODO: Change UI logic
    dispatch(setUIConfig('transactions', 'isRefreshing', true));
    const user = getState().user.current.info;
    ws.emit(GET_USER_TRANSACTIONS, user);

    ws.subscribe(GET_USER_TRANSACTIONS_RESPONSE, (res: any) => {
        if (res.status === 0) {
            dispatch({
                type: GET_TRANSACTION_SUCCESS,
                payload: res.data,
            });
        } else {
        }
        dispatch(setUIConfig('transactions', 'isRefreshing', false));
    });
};

export const selectTransaction = (transaction: any) => ({
    type: SELECT_TRANSACTION,
    payload: transaction,
});
