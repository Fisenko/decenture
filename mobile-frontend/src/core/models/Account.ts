export default class Account {
    public id: string;
    public name: string;
    public amount: number;
    public currency: string;
    public typeId: number;
    public statusId: number;
    public userId: string;
    public ownerId?: string;
    public entryDate?: string;
    public closeDate?: string;

    constructor(data: any = {}) {
        this.id = data.id;
        this.name = data.name;
        this.amount = data.amount;
        this.currency = data.currency;
        this.typeId = data.typeId;
        this.statusId = data.statusId;
        this.userId = data.userId;
        this.ownerId = data.ownerId;
        this.entryDate = data.entryDate;
        this.closeDate = data.closeDate;
    }
}
