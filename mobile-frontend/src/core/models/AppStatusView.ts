export enum StatusViewPage {
    LOADING,
    SUCCESS,
    FAILED,
}

export default class AppStatusView {
    status: StatusViewPage;
    label: string;
    logo: number;
    navigateToScreen: string;
    withNavigation: boolean;
    navigateTimeoutId: number;

    constructor(data: any) {
        this.status = data.status || StatusViewPage.LOADING;
        this.label = data.label || '';
        this.withNavigation = data.withNavigation || false;
        this.navigateToScreen = data.navigateToScreen || '';
        this.logo = data.logo || 0;
        this.navigateTimeoutId = data.navigateTimeoutId || 0;
    }
}
