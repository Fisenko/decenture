export class BankAccount {
    public bankName: string;
    public bankAccountName: string;
    public accountNumber: number;
    public sortCode: number;

    constructor(data: any = {}) {
        this.bankName = data.bankName || '';
        this.bankAccountName = data.bankAccountName || '';
        this.accountNumber = data.accountNumber || 0;
        this.sortCode = data.sortCode || 0;
    }
}
