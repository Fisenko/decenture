export enum CardValidationStatus {
    valid,
    invalid,
    incomplete,
}