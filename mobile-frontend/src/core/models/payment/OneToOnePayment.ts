export class OneToOnePayment {
    amount: number;
    receiver: string;
    description?: string;

    constructor(data: any = {}) {
        this.amount = data.amount || 0;
        this.receiver = data.receiver || '';
        this.description = data.description || 'One-To-One Payment';
    }
}
