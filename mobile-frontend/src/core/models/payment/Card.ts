export class Card {
    id?: string;
    number: string;
    expiry: string;
    cvc: string;
    valid: boolean;
    type?: string;
    name?: string;

    constructor(data: any = {}) {
        this.id = data.id || '';
        this.number = data.number || '';
        this.expiry = data.expiry || '';
        this.cvc = data.cvc || '';
        this.valid = data.valid || false;
        this.type = data.type || '';
        this.name = data.name || '';
    }
}
