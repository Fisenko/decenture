export default class LectureAttendance {
    id: string;
    datetime: Date;
    lecture: {
        name: string;
        code: string;
    };
    theatre: {
        name: string;
    };

    constructor(data: any = {}) {
        this.id = data.transaction_id;
        this.datetime = new Date(data.timestamp);
        this.lecture = {
            name: data.name,
            code: data.code,
        };
        this.theatre = {
            name: data.lecture_theatre_name,
        };
    }
}
