import Lecture from './lecture';

export interface IAttendance {
    history: Array<any>;
    absence: Array<Lecture>;
    percentage: Array<any>;
}

export default class Attendance {
    history: Array<any>;
    absence: Array<Lecture>;
    percentage: Array<any>;

    constructor(attendance: any = {}) {
        this.history = attendance.history || [];
        this.absence = attendance.absence || [];
        this.percentage = attendance.percentage || [];
    }
}
