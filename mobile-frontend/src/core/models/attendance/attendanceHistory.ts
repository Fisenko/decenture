import LectureAttendance from 'DecentureMobile/src/core/models/attendance/lectureAttendance';

export class AttendanceHistory {
    date: string;
    data: Array<LectureAttendance>;

    constructor(data: any = {}) {
        this.date = data.date || '';
        this.data = data.data || [];
    }
}
