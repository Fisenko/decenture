export class AttendanceRecord {

    nfcId: string;
    isAuth: boolean;
    isNfcScanEnabled: boolean;

    constructor(data: any = {}) {
        this.nfcId = data.nfcId || '';
        this.isAuth = data.isAuth || false;
        this.isNfcScanEnabled = data.isNfcScanEnabled || false;
    }
}
