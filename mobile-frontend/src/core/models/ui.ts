import { StatusViewPage } from './AppStatusView';

export enum MobileAppState {
    ACTIVE = 'active',
    INACTIVE = 'inactive',
    BACKGROUND = 'background',
}

export enum SocketState {
    Connected,
    Disconnected,
    Reconnected,
    Reconnecting,
}

export type UIAction = {
    type: string;
    systemKey: string;
    field?: any;
    value?: any;
    [anyProps: string]: any;
};

export class UIConfig {
    name: string;

    [configName: string]: any;

    constructor(name: string) {
        this.name = name;
    }
}

export class UI {
    configs: { [key: string]: any };

    constructor() {
        this.configs = {
            AppStatusView: {
                currInfo: {
                    status: StatusViewPage.LOADING,
                    label: 'Loading...',
                    navigateToScreen: 'Login',
                    withNavigation: false,
                },
            },
            transactions: {
                isDetailsVisible: false,
                isRecentDetailsVisible: false,
                isInvoiceTabOpen: true,
                isTxTabOpen: true,
                isRefreshing: false,
            },
            invoices: {
                isRefreshing: false,
            },
            record: {
                isNfcManagerStarted: false,
                isFingerModalVisible: false,
                isNfcSupported: true,
            },
            attendance: {
                isRefreshing: false,
            },
            creditCard: {
                currInfo: {
                    status: {},
                    values: {},
                },
                focused: '',
            },
            tabBar: {
                isVisible: true,
                activeSceneName: 'Home',
            },
            registration: {
                isDatePickerVisible: false,
            },
            appState: {
                status: MobileAppState.ACTIVE,
            },
            socket: {
                status: {
                    state: SocketState.Connected,
                    attemptNumber: 0,
                },
            },
            sessionExpired: {
                status: true,
            },
        };
    }
}
