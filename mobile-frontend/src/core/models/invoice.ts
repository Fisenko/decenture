export default class Invoice {
    id: string;
    code: string;
    title: string;
    from: string;
    to: string;
    ref: string;
    amount: number;
    type: string;
    description: string;
    invoiceNumber: string;

    university?: string;
    invoiceId?: string;
    invoiceDate?: string;
    dueDate?: string;
    billedFrom?: string;
    billedTo?: string;
    status?: string;
    firstName?: string;
    lastName?: string;
    email?: string;
    businessUnit?: string;
    curCd?: string;
    message?: string;
    accountTypeSf?: string;
    itemTypeCd?: string;
    entryDate? : string;

    constructor(data: any = {}) {
        this.id = data.id || '';
        this.code = data.code || '';
        this.title = data.title || '';
        this.from = data.from || '';
        this.to = data.to || '';
        this.ref = data.ref || '';
        this.amount = data.amount || 0;
        this.type = data.type || '';
        this.description = data.description || '';
        this.invoiceNumber = data.invoice_number || '';
    }
}
