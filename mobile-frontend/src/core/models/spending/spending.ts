import { LineChartData } from 'DecentureMobile/src/components/spending/charts/line/LineChart';
import { PieChartData } from 'DecentureMobile/src/components/spending/charts/pie/PieChart';

export default class Spending {

    total: number;
    pieData: Array<PieChartData>;
    lineData: Array<LineChartData>;

    constructor(
        total: number = 0,
        pieData: Array<any> = [],
        lineData: Array<any> = [],
    ) {

        this.total = total;
        this.pieData = pieData;
        this.lineData = lineData;
    }
}
