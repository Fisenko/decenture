export class AppUser {

    public firstName: string;
    public lastName: string;
    public dateOfBirth: Date;
    public email: string;
    public studentNumber: string;
    public password?: string;
    public confirmPassword?: string;
    public isAuth?: boolean;

    constructor(data: any = {}) {
        this.firstName = data.firstName || '';
        this.lastName = data.lastName || '';
        this.dateOfBirth = data.dateOfBirth || null;
        this.email = data.email || '';
        this.studentNumber = data.studentNumber || '';
        this.password = data.password || '';
        this.confirmPassword = data.confirmPassword || '';

    }
}
