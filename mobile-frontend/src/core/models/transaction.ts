export enum TransactionType {
    INVOICE,
    TRANSACTION,
}

export default class Transaction {
    id: string;
    name: string;
    amount: number;
    description: string;
    type: string;
    datetime: Date;
    receiver: string;
    sender: string;
    isReceiver: boolean;

    constructor(data: any = {}) {
        this.id = data.id || '';
        this.name = data.name || '';
        this.amount = data.amount || 0;
        this.description = data.description || '';
        this.type = data.type || '';
        this.datetime = data.datetime || new Date();
        this.receiver = data.receiver || '';
        this.sender = data.sender || '';
        this.isReceiver = data.isReceiver || true;
    }
}
