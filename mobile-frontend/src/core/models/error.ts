export type ErrorAction = {
    type: string;
    systemKey: string;
    field?: any;
    value?: any;
    [anyProps: string]: any;
};

export class ErrorConfig {
    name: string;

    [configName: string]: any;

    constructor(name: string) {
        this.name = name;
    }
}

export class Error {

    configs: { [key: string]: any };

    constructor() {
        this.configs = {
            record: {
                nfc: '',
                fingerprint: '',
            },
            registration: {
                confirmPassword: '',
                errorMsg: ''
            },
            login: {
                errorMsg: ''
            }
        };
    }
}
