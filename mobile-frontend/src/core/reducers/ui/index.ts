import { UI, UIAction, UIConfig } from 'DecentureMobile/src/core/models/ui';
import { SET_UI_CONFIG } from 'DecentureMobile/src/core/actions/ui/actionTypes';

const ui = (state = new UI(), action: UIAction) => {

    switch (action.type) {

        case SET_UI_CONFIG:
            let config = state.configs[action.systemKey];
            if (!config) {
                config = new UIConfig(action.systemKey);
                config[action.field] = action.value;
            } else {
                config = { ...config, [action.field]: action.value };
            }
            return { ...state, configs: { ...state.configs, [action.systemKey]: config } };
        default:
            return state;
    }
};

export default ui;
