import { Error, ErrorAction, ErrorConfig } from 'DecentureMobile/src/core/models/error';
import { SET_ERROR_CONFIG } from 'DecentureMobile/src/core/actions/error/actionTypes';

const errors = (state = new Error(), action: ErrorAction) => {

    switch (action.type) {

        case SET_ERROR_CONFIG:
            let config = state.configs[action.systemKey];
            if (!config) {
                config = new ErrorConfig(action.systemKey);
                config[action.field] = action.value;
            } else {
                config = { ...config, [action.field]: action.value };
            }
            return { ...state, configs: { ...state.configs, [action.systemKey]: config } };
        default:
            return state;
    }
};

export default errors;
