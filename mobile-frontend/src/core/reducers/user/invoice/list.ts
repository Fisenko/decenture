import { GET_INVOICES_SUCCESS, PAY_INVOICE_SUCCESS } from 'DecentureMobile/src/core/actions/invoice/actionTypes';
import Invoice from 'DecentureMobile/src/core/models/invoice';

const list = (state: Invoice[] = [], action: any) => {
    switch (action.type) {
        case GET_INVOICES_SUCCESS:
            return action.payload;
        case PAY_INVOICE_SUCCESS:
            return state.filter((invoice: Invoice) => invoice !== action.payload);
        default:
            return state;
    }
};

export default list;
