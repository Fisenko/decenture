import { SELECT_INVOICE } from 'DecentureMobile/src/core/actions/invoice/actionTypes';

const info = (state = {}, action: any) => {
    switch (action.type) {
        case SELECT_INVOICE:
            return action.payload;
        default:
            return state;
    }
};

export default info;
