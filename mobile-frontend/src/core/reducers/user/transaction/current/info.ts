import { SELECT_TRANSACTION } from 'DecentureMobile/src/core/actions/transaction/actionTypes';
import Transaction from 'DecentureMobile/src/core/models/transaction';

const info = (state = new Transaction(), action: any) => {
    switch (action.type) {
        case SELECT_TRANSACTION:
            return action.payload;
        default:
            return state;
    }
};

export default info;
