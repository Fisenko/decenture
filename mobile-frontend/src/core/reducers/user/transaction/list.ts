import * as types from 'DecentureMobile/src/core/actions/transaction/actionTypes';

const list = (state = [], action: any) => {
    switch (action.type) {
        case types.GET_TRANSACTION_SUCCESS:
            return action.payload;
        default:
            return state;
    }
};

export default list;
