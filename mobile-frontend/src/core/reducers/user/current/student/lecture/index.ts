import { combineReducers } from 'redux';
import current from './current';
import list from './list';

const lecture = combineReducers({
    current,
    list
});

export default lecture;
