import { combineReducers } from 'redux';
import info from './info'

const current = combineReducers({
    info
});

export default current;
