import { combineReducers } from 'redux';
import current from './current';
import list from './list';

const attendance = combineReducers({
    current,
    list,
});

export default attendance;
