import { GET_ATTENDANCE_HISTORY_COMPLETE } from 'DecentureMobile/src/core/actions/attendance/actionTypes';
import Attendance from 'DecentureMobile/src/core/models/attendance/attendance';
import { convertAttendanceHistory } from 'DecentureMobile/src/utils/data_utils';

const list = (state = new Attendance(), action: any) => {
    switch (action.type) {
        case GET_ATTENDANCE_HISTORY_COMPLETE:
            return { ...state, ...action.payload, history: convertAttendanceHistory(action.payload.history) };
        default:
            return state;
    }
};

export default list;
