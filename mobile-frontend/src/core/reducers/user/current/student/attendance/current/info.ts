import * as AttendanceActionTypes from 'DecentureMobile/src/core/actions/attendance/actionTypes';
import { AttendanceRecord } from 'DecentureMobile/src/core/models/attendance/attendanceRecord';

const info = (state = new AttendanceRecord(), action: any) => {
    switch (action.type) {
        case AttendanceActionTypes.ATTENDANCE_RECORD_INPUT_CHANGE:
            return { ...state, [action.payload.name]: action.payload.value };
        case AttendanceActionTypes.SET_DEFAULT_ATTENDANCE_RECORD:
            return new AttendanceRecord();
        case AttendanceActionTypes.RECORD_ATTENDANCE_SUCCESS:
            return state;
        case AttendanceActionTypes.RECORD_ATTENDANCE_FAILED:
            return state;
        case AttendanceActionTypes.RECORD_ATTENDANCE_NFC_STATUS_CHANGE:
            return { ...state, isNfcScanEnabled: action.payload };

        default:
            return state;
    }
};

export default info;
