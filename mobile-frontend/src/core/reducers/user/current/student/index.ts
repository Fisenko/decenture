import { combineReducers } from 'redux';
import attendance from './attendance';
import lecture from './lecture';

const student = combineReducers({
    attendance,
    lecture,
});

export default student;
