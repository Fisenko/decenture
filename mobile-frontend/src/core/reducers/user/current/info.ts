import { AppUser } from 'DecentureMobile/src/core/models/AppUser';
import * as ActionTypes from 'DecentureMobile/src/core/actions/user/actionTypes';

const info = (state = new AppUser(), action: any) => {
    switch (action.type) {
        case ActionTypes.REGISTRATION_INPUT_CHANGE:
            return { ...state, [action.payload.name]: action.payload.value };
        case ActionTypes.REGISTRATION_NEXT_BUTTON_CLICK:
            return { ...state, userInfo: action.payload };
        case ActionTypes.REGISTRATION_COMPLETED:
            return { ...state, password: '', confirmPassword: '' };
        case ActionTypes.REGISTRATION_REJECTED:
            return { ...state };
        case ActionTypes.LOGIN_INPUT_CHANGE:
            return { ...state, [action.payload.name]: action.payload.value };
        case ActionTypes.LOGIN_STUDENT_SUCCESS:
            return { ...state, ...action.payload, password: '' };
        case ActionTypes.LOGOUT_STUDENT_SUCCESS:
        case ActionTypes.LOGOUT_WITHOUT_CLEAN:
            return { ...state, isAuth: false };
        case ActionTypes.VERIFIED_USER_SUCCESS:
            return { ...state, isAuth: true };
        default:
            return state;
    }
};

export default info;
