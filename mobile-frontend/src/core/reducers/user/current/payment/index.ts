import { combineReducers } from 'redux';
import current from './current';
import list from './list';

const payment = combineReducers({
    current,
    list,
});

export default payment;
