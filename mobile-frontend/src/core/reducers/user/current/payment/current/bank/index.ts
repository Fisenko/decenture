import { combineReducers } from 'redux';
import current from './current';
import list from './list';

const bank = combineReducers({
    current,
    list
});

export default bank;
