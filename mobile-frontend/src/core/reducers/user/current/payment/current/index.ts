import { combineReducers } from 'redux';
import info from './info';
import card from './card';
import bank from './bank';
import deposit from './deposit';
import one_to_one from './one_to_one';

const current = combineReducers({
    info,
    card,
    bank,
    deposit,
    one_to_one,
});

export default current;
