import { Card } from 'DecentureMobile/src/core/models/payment/Card';
import { CARD_INPUT_CHANGE, UPDATE_CARD_LIST } from 'DecentureMobile/src/core/actions/payment/actionTypes';

const info = (state = new Card(), action: any) => {
    switch (action.type) {
        case CARD_INPUT_CHANGE:
            return action.payload;
        case UPDATE_CARD_LIST:
            return new Card();
        default:
            return state;
    }
};

export default info;


