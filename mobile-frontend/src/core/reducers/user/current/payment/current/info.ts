import { ONE_TO_ONE_PAYMENT_SUCCESS, UPDATE_AMOUNT_INFO } from 'DecentureMobile/src/core/actions/payment/actionTypes';
import { PAY_INVOICE_SUCCESS } from 'DecentureMobile/src/core/actions/invoice/actionTypes';

const initialState = 0;

const info = (state = initialState, action: any) => {
    switch (action.type) {
        case UPDATE_AMOUNT_INFO:
            return action.payload.amount || initialState;
        case PAY_INVOICE_SUCCESS:
            return state - action.payload.amount;
        case ONE_TO_ONE_PAYMENT_SUCCESS:
            return state - action.payload.amount;
        default:
            return state;
    }
};

export default info;
