import { BankAccount } from 'DecentureMobile/src/core/models/payment/BankAccount';
import { BANK_INPUT_CHANGE } from 'DecentureMobile/src/core/actions/payment/actionTypes';

const info = (state = new BankAccount(), action: any) => {

    switch (action.type) {
        case BANK_INPUT_CHANGE:
            console.log(BANK_INPUT_CHANGE, action);
            return { ...state, [action.payload.name]: action.payload.value };
        default:
            return state;
    }
};

export default info;
