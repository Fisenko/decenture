import { combineReducers } from 'redux';
import current from './current';
import list from './list';

const card = combineReducers({
    current,
    list,
});

export default card;
