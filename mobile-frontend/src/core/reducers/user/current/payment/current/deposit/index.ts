import { combineReducers } from 'redux';
import current from './current';
import list from './list';

const deposit = combineReducers({
    current,
    list
});

export default deposit;