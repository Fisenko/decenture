import { Deposit } from 'DecentureMobile/src/core/models/payment/Deposit';
import { DEPOSIT_INPUT_CHANGE } from 'DecentureMobile/src/core/actions/payment/actionTypes';

const info = (state = new Deposit(), action: any) => {

    switch (action.type) {
        case DEPOSIT_INPUT_CHANGE:
            return { ...state, [action.payload.name]: action.payload.value };
        default:
            return state;
    }
};

export default info;