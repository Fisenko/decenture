import { Card } from 'DecentureMobile/src/core/models/payment/Card';
import {
    GET_CARDS_NUMBER_BY_APP_USER_TOKEN_SUCCESS,
    UPDATE_CARD_LIST,
} from 'DecentureMobile/src/core/actions/payment/actionTypes';


const list = (state: Array<Card> = [], action: any) => {
    switch (action.type) {
        case GET_CARDS_NUMBER_BY_APP_USER_TOKEN_SUCCESS:
            return action.payload;
        case UPDATE_CARD_LIST:
            const number = action.payload.number;
            action.payload.number =  + number.slice(number.length - 4, number.length);
            return state.concat([action.payload]);
        default:
            return state;
    }
};

export default list;
