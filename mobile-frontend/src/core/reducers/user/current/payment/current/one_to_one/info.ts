import { OneToOnePayment } from 'DecentureMobile/src/core/models/payment/OneToOnePayment';
import { ONE_TO_ONE_PAYMENT_INPUT_CHANGE } from 'DecentureMobile/src/core/actions/payment/actionTypes';

const info = (state = new OneToOnePayment(), action: any) => {

    switch (action.type) {
        case ONE_TO_ONE_PAYMENT_INPUT_CHANGE:
            return { ...state, [action.payload.name]: action.payload.value };
        default:
            return state;
    }
};

export default info;
