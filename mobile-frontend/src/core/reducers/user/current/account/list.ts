import { GET_ACCOUNTS_SUCCESS } from 'DecentureMobile/src/core/actions/user/actionTypes';

const list = (state = [], action: any) => {
    switch (action.type) {
        case GET_ACCOUNTS_SUCCESS:
            return action.payload;
        default:
            return state;
    }
};

export default list;
