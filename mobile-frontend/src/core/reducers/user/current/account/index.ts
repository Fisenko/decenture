import { combineReducers } from 'redux';

import list from './list';

const current = combineReducers({
    list,
});

export default current;
