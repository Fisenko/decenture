import { combineReducers } from 'redux';
import info from './info';
import payment from './payment';
import student from './student';
import account from './account';

const current = combineReducers({
    info,
    payment,
    student,
    account,
});

export default current;
