import { combineReducers } from 'redux';
import transaction from './transaction';
import invoice from './invoice';
import current from './current';
import list from './list';

const user = combineReducers({
    current,
    list,
    transaction,
    invoice,
});

export default user;
