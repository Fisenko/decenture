import { combineReducers } from 'redux';

import { LOGOUT_STUDENT_SUCCESS } from 'DecentureMobile/src/core/actions/user/actionTypes';
import user from './user';
import ui from './ui';
import errors from './errors';
import navReducer from './navigation';

const appReducer = combineReducers({
    user,
    ui,
    errors,
    nav: navReducer,
});

const rootReducer = (state: any, action: any) => {
    switch (action.type) {
        case LOGOUT_STUDENT_SUCCESS:
            state = undefined;
            break;
        default:
            break;
    }

    return appReducer(state, action);
};

export default rootReducer;
