import { createNavigationReducer } from 'react-navigation-redux-helpers';

import AppNavigator from 'DecentureMobile/src/routes';

export default createNavigationReducer(AppNavigator);
